<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Options
    |--------------------------------------------------------------------------
    |
    | Here you can define the options that are passed to all NovaTinyMCE
    | fields by default.
    |
    */

    'default_options' => [
        'content_css'   => '/vendor/tinymce/skins/ui/oxide/content.min.css',
        'skin_url'      => '/vendor/tinymce/skins/ui/oxide',
        'path_absolute' => '/',
        'plugins'       => [
            'lists preview hr anchor pagebreak image wordcount fullscreen directionality paste textpattern'
        ],
        'toolbar'       => 'undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | image | bullist numlist outdent indent | link',
        'relative_urls' => false,
        'use_lfm'       => true,
        'lfm_url'       => 'filemanager',
        'height'        => '500',
        'image_caption' => true,
        'style_formats' => [
            [
                'title' => 'Headings',
                'items' => [
                    [ 'title' => 'Heading 1', 'format' => 'h1' ],
                    [ 'title' => 'Heading 2', 'format' => 'h2' ],
                    [ 'title' => 'Heading 3', 'format' => 'h3' ],
                    [ 'title' => 'Heading 4', 'format' => 'h4' ],
                    [ 'title' => 'Heading 5', 'format' => 'h5' ],
                    [ 'title' => 'Heading 6', 'format' => 'h6' ]
                ]
            ],
            [
                'title' => 'Inline',
                'items' => [
                    [ 'title' => 'Bold', 'format' => 'bold' ],
                    [ 'title' => 'Italic', 'format' => 'italic' ],
                    [ 'title' => 'Underline', 'format' => 'underline' ],
                    [ 'title' => 'Strikethrough', 'format' => 'strikethrough' ],
                    [ 'title' => 'Superscript', 'format' => 'superscript' ],
                    [ 'title' => 'Subscript', 'format' => 'subscript' ],
                    [ 'title' => 'Code', 'format' => 'code' ]
                ]
            ],
            [
                'title' => 'Blocks',
                'items' => [
                    [ 'title' => 'Paragraph', 'format' => 'p' ],
                    [ 'title' => 'Blockquote', 'format' => 'blockquote' ],
                    [ 'title' => 'Div', 'format' => 'div' ],
                    [ 'title' => 'Pre', 'format' => 'pre' ]
                ]
            ],
            [
                'title' => 'Align',
                'items' => [
                    [ 'title' => 'Left', 'format' => 'alignleft' ],
                    [ 'title' => 'Center', 'format' => 'aligncenter' ],
                    [ 'title' => 'Right', 'format' => 'alignright' ],
                    [ 'title' => 'Justify', 'format' => 'alignjustify' ]
                ]
            ],
            [
                'title' => 'Images',
                'items' => [
                    [
                        'title'    => 'Image Left',
                        'selector' => 'img,figure',
                        'styles'   => [
                            'float'  => 'left',
                            'margin' => '0 10px 0 10px'
                        ]
                    ],
                    [
                        'title'    => 'Image Right',
                        'selector' => 'img,figure',
                        'styles'   => [
                            'float'  => 'right',
                            'margin' => '0 10px 0 10px'
                        ]
                    ],
                    [
                        'title'    => 'Image Inline',
                        'selector' => 'img,figure',
                        'styles'   => [
                            'display'     => 'inline-flex',
                            'allign-self' => 'baseline',
                            'margin'      => '0 2px 0 2px'
                        ]
                    ]
                ],
            ],

            [
                'title' => 'Tables',
                'items' => [
                    [
                        'title'    => 'Simple Table',
                        'selector' => 'table',
                        'classes'  => [ 'table' ],
                        'styles'   => []
                    ],
                    [
                        'title'    => 'Striped Table',
                        'selector' => 'table',
                        'classes'  => [ 'table', 'table-stripped' ],
                        'styles'   => []
                    ],
                    [
                        'title'    => 'Bordered Table',
                        'selector' => 'table',
                        'classes'  => [ 'table', 'table-bordered' ],
                        'styles'   => []
                    ],
                    [
                        'title'    => 'Borderless Table',
                        'selector' => 'table',
                        'classes'  => [ 'table', 'table-borderless' ],
                        'styles'   => []
                    ],
                    [
                        'title'    => 'Hoverable Rows',
                        'selector' => 'table',
                        'classes'  => [ 'table', 'table-hover' ],
                        'styles'   => []
                    ],
                    [
                        'title'    => 'Small Rows',
                        'selector' => 'table',
                        'classes'  => [ 'table', 'table-sm' ],
                        'styles'   => []
                    ],
                ],
            ],


        ]
    ],
];
