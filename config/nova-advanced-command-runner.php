<?php

use Database\Seeders\Connectors_OLD\CARIScalesSeeder;
use Database\Seeders\Connectors_OLD\ColossBESeeder;
use Database\Seeders\Connectors_OLD\ColossSeeder2009;
use Database\Seeders\Connectors_OLD\DEPABSeeder;

return [
    'commands'         => [
        // Bash command
        'Disk Usage'            => [ 'run' => 'df -h', 'type' => 'danger', 'group' => 'Statistics', 'command_type' => 'bash' ],

        // Basic command
        'Clear Cache'           => [ 'run' => 'cache:clear', 'type' => 'danger', 'group' => 'Cache', ],

        // Command with variable
        'Forget Cache'          => [ 'run' => 'cache:forget {cache key}', 'type' => 'warning', 'group' => 'Cache' ],

        // Command with advanced variable customization
        'Forget Cache variable' => [
            'run'       => 'cache:forget {cache key}',
            'type'      => 'warning',
            'group'     => 'Cache',
            'variables' => [
                [
                    'label'       => 'cache key', // This needs to match with variable defined in the command
                    'field'       => 'select', // Allowed values (text,number,tel,select,date,email,password)
                    'options'     => [
                        'uid_descriptors' => 'Clear Descriptor UID Cache',
                    ],
                    'placeholder' => 'Select An Option'
                ]
            ]
        ],
        // Command with flags
        'Run Migrations'        => [ 'run' => 'migrate --force', 'type' => 'danger', 'group' => 'Database' ],
        // Command with optional flags
        /*'Run Migrations(Optional)' => [
            'run' => 'migrate',
            'type' => 'danger',
            'group' => 'Migration',
            'flags' => [
                // These optional flags will be prompted as a checkbox for the user
                // And will be appended to the command if the user checks the checkbox
                '--force' => 'Force running in production'
            ]
        ],   */
        // Command with help text
        /* 'Run Migrations(Help)' => [
             'run' => 'migrate --force',
             'type' => 'danger',
             'group' => 'Migration',
             // You can also add html for help text.
             'help' => 'This is a destructive operation. Proceed only if you really know what you are doing.'
         ],*/

        'Run Seed' => [
            'run'       => 'db:seed --class="{class name}"',
            'group'     => 'Database',
            'type'      => 'danger',
            'variables' => [
                [
                    'label'       => 'class name', // This needs to match with variable defined in the command
                    'field'       => 'select', // Allowed values (text,number,tel,select,date,email,password)
                    'options'     => [
                        '\\\Database\\\Seeders\\\Connectors\\\EFPSAAlgorithmsForExistingDataSeeder' => 'Base - Prototype Descriptors',
                        '\\\Database\\\Seeders\\\Connectors\\\ColossBESeeder'                       => 'Winter Mortality - CARI',
                        '\\\Database\\\Seeders\\\Connectors\\\ColossSeeder2009'                     => 'Winter Mortality - COLOSS 2009',
                        '\\\Database\\\Seeders\\\Connectors\\\ColossSeeder2012'                     => 'Winter Mortality - COLOSS 2012',
                        '\\\Database\\\Seeders\\\Connectors\\\ColossSeeder2015'                     => 'Winter Mortality - COLOSS 2015',
                        '\\\Database\\\Seeders\\\Connectors\\\ColossSeeder2016'                     => 'Winter Mortality - COLOSS 2016',
                        '\\\Database\\\Seeders\\\Connectors\\\ColossSeeder2017'                     => 'Winter Mortality - COLOSS 2017',
                        '\\\Database\\\Seeders\\\Connectors\\\ColossSeeder2018'                     => 'Winter Mortality - COLOSS 2018',
                        '\\\Database\\\Seeders\\\Connectors\\\EpilobeeSeeder'                       => 'Winter Mortality - Epilobee',
                        '\\\Database\\\Seeders\\\Connectors\\\EFSABGoodSeeder'                      => 'Scales - EFSA B-Good PT',
                        '\\\Database\\\Seeders\\\Connectors\\\CARIScalesSeeder'                     => 'Scales - CARI',
                        '\\\Database\\\Seeders\\\Connectors\\\DEPABSeeder'                          => 'Analyses - DEPAB',
                        '\\\Database\\\Seeders\\\Connectors\\\SocioEconomics2020Seeder'             => 'Statistcs - FAOSTAT Socio-Economic up to 2020',
                        '\\\Database\\\Seeders\\\Connectors\\\VarroaAlertSeeder'                    => 'Varroa - Bienengesnudheit.at',
                        '\\\Database\\\Seeders\\\Connectors\\\CortevaSeeder'                        => 'Colony Development - Corteva',
                    ],
                    'placeholder' => 'Select Seeder to Run'
                ],
            ],

            'help' => 'Only run this command if the data has not been seeded yet!'
        ],

        'Data Aggregation' => [ 'run' => 'data:aggregate --queue', 'type' => 'primary', 'group' => 'Data' ],

        'Data Cache' => [ 'run' => 'data:cache --queue', 'type' => 'primary', 'group' => 'Data' ],

        'Query Live Data' => [ 'run' => 'data:live --force', 'type' => 'primary', 'group' => 'Data' ],
        // Queueing commands
        //'Clear Cache' => [ 'run' => 'cache:clear --should-queue', 'type' => 'danger', 'group' => 'Cache' ],
    ],
    // Limit the command run history to latest 10 runs
    'history'          => 10,
    // Tool name displayed in the navigation menu
    'navigation_label' => 'Command Runner',
    // Any additional info to display on the tool page. Can contain string and html.
    'help'             => '',
    // Allow running of custom artisan and bash(shell) commands
    'custom_commands'  => [ 'artisan', 'bash' ],

    // Allow running of custom artisan commands only(disable custom bash(shell) commands)
    //    'custom_commands' => ['artisan'],
    // Allow running of custom bash(shell) commands only(disable custom artisan commands)
    //    'custom_commands' => ['bash'],
    // Disable running of custom commands.
    //    'custom_commands' => [],
];
