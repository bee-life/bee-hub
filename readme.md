# The Bee Hub
The BeeHub is an integrative tool to centralise, analyse and visualise bee and pollinator-related data based on principles of collaboration and conservation.

Bees and other insect pollinators are becoming increasingly relevant in the public debate. European authorities now recognise the environmental risks pollinators face and the need for institutional action. Given their importance for ecosystems and their role in our food security, the commitment to protect pollinators has been growing, and data is essential to fulfilling this commitment.

Different agents and stakeholders are continually collecting data related to the status of pollinators, such as researchers, environmental, health or agricultural authorities, national beekeeping or farming associations. The Bee Hub has been conceived to valorise their efforts and improve collaborations based on data-sharing. At the same time, the Bee Hub is constantly developing to provide access to valuable data from different consenting sources. In a collaborative spirit, the Bee Hub centralises and presents this data, also working as a communicative tool for the benefit of bees and pollinators in general.

The Bee Hub is coordinated by BeeLife European Beekeeping Coordination, an NGO focused on the protection of pollinators and biodiversity in Europe. BeeLife has initialised the first stages of this integrative platform within the Internet of Bees (IoBee) project.

The Bee Hub is also an attempt to materialise the conclusions of the EU Bee Partnership regarding the need for further bee-data integration. The partnership is a stakeholder platform dynamised by the European Food Safety Authority that includes representatives from the beekeeping and farming sectors, NGOs, veterinarians, academia, industry, producers, and scientists.

This new tool also includes developments from the Apimondia working group on the standardisation of data on bees - Bee XML. Bee XML is the ongoing measure to reach a new model for sharing bee data, and the Bee Hub aims at implementing these standards.

### Supporting The Bee Hub

Are you interested in supporting The Bee Hub? A main contribution we are seeking is through data sharing. We are looking to integrate any pollinator and environmental-relevant data to continue developing this platform. If you have any datasets available that you are willing to share with us, you could largely contribute to advancing The Bee Hub (thes might include bee mortality rates, varroa count, scales and other digital monitoring systems, intoxication, pesticide use, phenology...).  
 
We propose potential data providers with a comprehensive data-sharing agreement on which your ownership of the data is protected. BeeLife will only introduce the data you share into the Bee Hub for centralization and communication. Your organization and any relevant partners in the recollection of the data will be displayed on the platform, so that your ownership is clearly stated. Any agreement would be revocable at any time. Data would only be displayed in the Bee Hub for as long as you and any partners relevant to the dataset agree. 
 
Do you envisage other forms of collaboration? We are open to discussions with new potential forms of contribution to the Bee Hub. 
 
You would be taking an essential part in creating a user-friendly window on the status of pollinators, identifying problematic areas and harmless areas, for the benefit of pollinators. We are counting on this tool to help improve efforts to protect bees on the field, but also to help at the institutional level. 
  
For more information regarding the Bee Hub, possibilities of participation to the initiative and data collaboration, please contact [info@bee-life.eu](mailto:info@bee-life.eu)

### Application history

- Version 0.1 - 30. 4. 2020 - Proof of concept (POI) completion
  - Addition of first 4 datasets - Epilobee Analysis, Belgium COLOSS data, Varroawarndienst Austrian Varroa infestation data and Cari scales data; see Providers.md for more information.
- Alpha 1.0 - 1. 4. 2020 - First production version
- Development start - 15. 8. 2019
  - The POI development was funded by the IoBee project, an EU-funded Fast-Track to Innovation project for technological development of bee and pollinator digital monitoring.
  - BeeLife European Beekeeping Coordination advocates and initiates The Bee Hub as an additional deliverable in IoBee.


## Installation

### 0. Server requirements

- PHP 7.2.5+ with extensions: BCMath,Ctype, Fileinfo, JSON, Mbstring, OpenSSL, PDO, Tokenizer, XML, Curl, SimpleXML
- MySQL 5.6+ or MariaDB equivalent  
- Apache 2 or Nginx
- Composer - Installation tool for PHP/Laravel dependencies for API
- npm - Installation tool for Javascript/Vue dependencies for App

### 1. Clone repository
Clone the repository with either HTTPS or SSH:

```bash
git clone https://gitlab.com/bee-life/bee-hub.git
```  

### 2. Database
Create a MySQL database with Inno DB engine and utf8mb4_unicode_ci collation.
Create a user with all permisions with the newly create database and pass all credential to .env file in following section.

### 3. Configuration file
We are using .env file for a local configuration. If it does not exist yet, create a copy of .env.example:

```bash
cp .env.example .env
```                 

Fill out the database login information and any other configuration based on your requirements. 

### 3. Install required vendor libraries
All external requirements are installed using following commands.

```bash
composer install
npm install
```

After installation finished you should run these commands to setup the framework and compile scripts:

```bash
php artisan key:generate
php artisan storage:link
php artisan translations
npm run dev
```

### 4. Data seeding
If you require a fresh database instance run the following commands:

```bash
php artisan migrate --seed
php artisan data:seed 
php artisan data:cache 
```

Note: seeding the data can take a LONG time!

Optionally, you can seed historic data as well, but this might take several hours!

```bash
php artisan data:historic 
```     

## Usage
Once environment is set up, you can run the application by opening the set url. To access administration, navigate to /administration url.
Administration offers content management and limited data management. It also offers reports and statistics.

## Contributing
For feature requests and bugs open an issue with coresponding description. Try to be as detailed as possible.
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
