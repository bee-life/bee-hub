<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Email Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the texts related to general email template and various emails send by the app.
    |
    */

    'footer' => [
        '' => '',
    ],
];
