<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Connector Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the texts related to Connectors.
    |
    */

    'meta' => [
        'index' => [
            'title'       => 'Data providers',
            'description' => 'All data providers, that shared data with Bee Hub are presented here.',
            'promotion'   => 'Become a data provider now and join the hub!',
            'join-now'    => 'Click here and fill out the form to join as a data provider',
            'read-more'   => 'Details',
        ],
    ],

    'attributes' => [
        'about-projects'  => 'Participating projects',
        'about-provider'  => 'About the provider',
        'contact-details' => 'Contact details',
    ],

    'types' => [
        'static' => 'Static',
        'remote' => 'Remote',
        'live'   => 'Live',
    ],

    'card' => [
        'read-more' => 'About the provider'
    ],
];
