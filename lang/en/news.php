<?php

return [
    'title' => 'News',
    'description' => 'Castus, primus demolitiones patienter consumere de peritus, bassus terror.',

    'published' => 'Published on :date',

    'label' => 'News',
    'singularLabel' => 'News Item',

    'read-more' => 'Read more',
];
