<?php

return [
    'meta-description' => 'The Bee Hub is a non-profit initiative of the EU Bee Partnership, which is a group of stakeholders including field practitioners such as beekeepers, veterinarians, and farmers, academia, environmental NGOs and industry, moderated by the European Food Safety Authority (EFSA). It is a Big Data approach to understand Pollinator diversity and threats in Europe. Its objective is to become a useful and effective tool in data gathering, sharing and communication. Secondly, it aims to be a collaborative tool in which beekeepers, monitoring device producers and/or research centres find a new level of cooperation.',

    'homepage' => [
         'title' => 'EU Bee Partnership Prototype Platform on Bee Health',
         'title-short' => 'Home',
    ],

    '404' => [
        'title' => 'Error 404 - Page is not found',
        'description' => 'Sorry, the page you were looking for could not be found.',
        'recommendations' => 'Maybe the following links can help you find the way:',
        'links' => [
            'homepage' => 'Homepage',
            'about' => 'About the project'
            //TODO: Add more
        ],
    ],
];
