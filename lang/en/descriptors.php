<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Descriptors Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the texts related to Descriptors.
    |
    */

    'meta' => [
        'index' => [
            'title'       => 'Descriptors',
            'description' => 'This list represent most of the data that lives within the Beehub.',
        ],
    ],

    'attributes' => [
        'category'   => 'Category',
        'about'      => 'About',
        'statistics' => 'Statistics',
        'projects'   => 'Related Projects',
    ],

    'statistics' => [
        'projects-count'    => 'Number of related projects:',
        'data-count'        => 'Total data points:',
        'data-count-unique' => 'Number of unique data points:',
    ],

    'card' => [
        'read-more' => 'More information',
    ],

    'groups' => [
        'monitoring'         => 'Automatic Monitoring',
        'demography-biology' => 'Demography and Biology',
        'health'             => 'Pollinator Health',
        'socio-economic'     => 'Socio-Economic Data',
    ],
];
