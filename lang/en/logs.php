<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Log Datatypes Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the texts related to Log Datatypes.
    |
    */

    'formatting' => [
        'thousand_separator' => ',',
        'decimal_separator'  => '.',
    ],

    'units' => [
        'percentage' => ':value%',

        'weight' => [
            'kg' => ':value kg',
            'mg' => ':value mg',
            'g'  => ':value gram|:value grams',
            't'  => ':value tone|:value tones',
        ],
        'volume' => [
            'hl' => ':value hl',
            'l'  => ':value l',
            'dl' => ':value dl',
            'ml' => ':value ml',
            'm'  => ':value m³',
            'cm' => ':value cm³',
            'mm' => ':value mm³',
            'ft' => ':value ft³',
        ],

        'area' => [
            'mm'   => ':value mm²',
            'cm'   => ':value cm²',
            'm'    => ':value m²',
            'a'    => ':value a',
            'ha'   => ':value ha',
            'km'   => ':value km²',
            'in'   => ':value inch²',
            'ft'   => ':value ft²',
            'y'    => ':value yd²',
            'mile' => ':value mi³',
        ],

        'length' => [
            'mm'   => ':value mm²',
            'cm'   => ':value cm²',
            'm'    => ':value m²',
            'a'    => ':value a',
            'ha'   => ':value ha',
            'km'   => ':value km²',
            'in'   => ':value inch²',
            'ft'   => ':value ft²',
            'y'    => ':value yd²',
            'mile' => ':value mi³',
        ],
        'speed'  => [
            'kph'   => ':value km/h',
            'm/s'   => ':value m/s',
            'mph'   => ':value mph',
            'knots' => ':value knot|:value knots',
            'fps'   => ':value fps',
            'mach'  => 'mach :value',
        ],

        'temperature'      => [
            'celsius'    => ':value℃',
            'kelvin'     => ':valueK',
            'fahrenheit' => ':value℉',
        ],
        'solar-irradiance' => [
            'wm' => ':value W/m²'
        ],
        'orientation'      => [
            'relative-north'      => 'North',
            'relative-south'      => 'South',
            'relative-east'       => 'East',
            'relative-west'       => 'West',
            'relative-north-east' => 'North-East',
            'relative-north-west' => 'North-West',
            'relative-south-east' => 'South-East',
            'relative-south-west' => 'South-West',

            'degrees' => ':value°'
        ],

        'special' => [
            'kg/km2' => ':value kg/km²',
        ],
    ],

    'double' => [
        'true'  => 'True',
        'false' => 'False',
    ],

    'resolution' => [
        '0' => 'Country',
        //   '1' => 'Major Region NUTS 1',
        //   '2' => 'Basic Region NUTS 2',
        //   '3' => 'Small Region NUTS 3',
        //   '4' => 'Local administrative unit',
        '1' => 'NUTS1',
        '2' => 'NUTS2',
        '3' => 'NUTS3',
        '4' => 'LAU',
        '5' => 'Post',
        '6' => 'Exact',
    ],

    'hive-id'          => 'Hive ID',
    'apiary-id'        => 'Apiary ID',
    'region'           => 'Location',
    'location-unknown' => 'Unknown',
    'latest-data'      => 'Displaying last known Data',
    'historic-data'    => 'Historic Data',
    'processed-data'   => 'Processed Data',

    'live-note'      => 'The displayed data was captured within last seven days and is considered Live data.',
    'historic-note'  => 'The displayed data is considered historic data.',
    'processed-note' => 'Displayed data has been processed from other sources of data.',


    'weather-sensors' => [
        'heading'           => 'Weather Station',
        'temperature'       => 'Temperature',
        'relative-humidity' => 'Relative humidity',
        'rain'              => 'Rain per square meter',
        'solar-radiation'   => 'Solar Radiation strength',
        'wind-direction'    => 'Wind Direction',
        'wind-speed'        => 'Wind Speed',
        'wind-gust'         => 'Wind Gust',
        'dew-point'         => 'Dew Point',
        'sunrise'           => 'Time of sunrise',
        'sunset'            => 'Time of sunset',
    ],

    'hive-sensors' => [
        'title' => 'Scale and sensor data',

        'hive-weight'        => 'Hive weight',
        'inside-temperature' => 'Inside temperature',
        'inside-humidity'    => 'Inside Humidity',
        'updated-at'         => 'Last Update',
        'weight-change-1'    => 'Weight change in last 1 day',
        'weight-change-3'    => 'Weight change in last 3 days',
        'weight-change-7'    => 'Weight change in last 7 days',
        'change'             => 'Change of :change',
        'weight'             => 'Total weight :weight',

        'hive-production'               => 'Hive production',
        'hive-metabolic-resting-state'  => 'Metabolic resting state consumption',
        'maximum-available-flight-time' => 'Maximum available flight time',
        'hive-production-unknown'       => ':value',
    ],

    'hive-events' => [
        'management-events' => 'Hive Management Events',
    ],


    'varroa-infestation' => [
        'title'      => 'Varroa Infestation',
        'full-title' => 'Varroa Infestation Levels',
        'tooltip'    => 'Current Value for :country: :value',

        'current-value' => 'Current Value',
        'threshold'     => 'Current Threshold',
        'data-counts'   => 'Total data points',
        'colony-counts' => 'Number of Hives',
        'apiary-counts' => 'Number of Apiaries',
        'updated-at'    => 'Last Update',

        'infestation-levels' => 'Infestation levels for :location',
        'severity'           => 'Infestation levels',
        'severity-level-0'   => 'Not enough data available',
        'severity-level-1'   => 'Infestation levels bellow threshold',
        'severity-level-2'   => 'Infestation levels within threshold',
        'severity-level-3'   => 'Infestation levels above threshold',

        'severity-level-0-short' => 'No data',
        'severity-level-1-short' => 'Bellow threshold',
        'severity-level-2-short' => 'Within threshold',
        'severity-level-3-short' => 'Above threshold',

        'validity' => 'The data is valid between :date_from and :date_to.',
    ],

    'winter-mortality' => [
        'title'      => 'Winter Mortality',
        'full-title' => 'Winter Mortality Percentage',

        'mortality'  => 'Winter Mortality',
        'data-count' => 'Included datasets',
        'measured'   => 'Measured in',
    ],


    'socio-economics' => [
        'prices-title' => 'Socio-economic Product Price',

        'year'         => 'Year',
        'prices'       => 'Value in USD',
        'prices-local' => 'Value in local currency',
        'livestock'    => 'Total livestock',
        'production'   => 'Production',
        'amount'       => 'Amount',
    ],

    'colony-development' => [
        'title'                      => 'Colony Development Studies',
        'colony-development-data'    => 'Colony Development Data',
        'hive-population'            => 'Population Counts',
        'brood-cells'                => 'Number of brood cells',
        'brood-cells-description'    => 'Number of brood cells in the whole colony. It includes capped, larva and egg cells.',
        'beebread-cells'             => 'Number of beebread cells',
        'beebread-cells-description' => 'Number of beebread cells in the whole colony.',
    ],

    'origin-generic' => [
        'link' => 'Source: :link'
    ],
];
