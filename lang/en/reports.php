<?php

return [
    'title' => 'Reports',
    'description' => 'Various papers published within the Bee Hub Platform.',

    'published_at' => 'Published on :date',
    'published_by' => 'Published by :author',
    'by' => ', by :author',

    'read-more' => 'Read more',
    'discover' => 'Discover',
    'abstract' => 'Abstract',
];
