<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Descriptor Categories Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the texts related to Descriptor Categories.
    |
    */

    'meta' => [
        'index' => [
            'title'       => 'Descriptor Categories',
            'description' => 'All currently available Descriptor Categories of the BeeHub database.',
        ],
    ],

    'attributes' => [
        'children'    => 'Subcategories',
        'descriptors' => 'Descriptors',
    ],

    'card' => [
        'read-more' => 'More information',
    ],
];
