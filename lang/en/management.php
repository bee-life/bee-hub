<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Management Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the texts related to front-end User pages and metadata forms.
    |
    */

    'nav' => [
        'account'   => 'Account',
        'projects'  => 'Projects',
        'providers' => 'Providers',
        'register' => 'Add Data',
        'logout'    => 'Logout',
    ],

    'edit' => 'Edit',
    'update' => 'Update',
    'back' => 'Back',

    'common' => [
        'contact-attributes' => 'Contact Information',
        'website' => 'Website',
        'email' => 'Email',
        'phone' => 'Phone',

        'select-date' => 'Select date',
    ],

    'profile' => [
        'title' => 'Account settings',

        'member-since' => 'You are a member of this community since: :date',

        'name'           => 'Full Name',
        'email'          => 'Email address',
        'email-note'     => 'Note: Email address must be confirmed via email',
        'account-button' => 'Update Data',
        'account-updated' => 'Your account information was updated.',

        'password-title'  => 'Password settings',
        'password'        => 'New password',
        'password-repeat' => 'Repeat password',
        'password-note'   => 'Note: For security reasons the minimum password length is 8 characters.',
        'password-button' => 'Change Password',
        'password-updated' => 'Your password was updated.',
    ],

    'projects' => [
        'title' => 'Manage Your Projects',

        'general-attributes' => 'General Project Information',

        'name' => 'Name',
        'acronym' => 'Acronym or short name',
        'slug' => 'Slug',
        'slug-note' => 'Note: Slug is based of title at the time of the project creation and can not be changed.',
        'description' => 'Description',
        'featured-image' => 'Logo',
        'replace-image' => 'Replace',
        'public' => 'The project and all its related data can be Publicly displayed',
        'public-note' => 'Note: this only concerns processed data and visualizations, while raw data is shared based on sharing agreement. This change can take up to 24 hours to be updated.',


        'updated' => 'All changes to the project have been saved.'
    ],

    'providers' => [
        'title' => 'Manage Data Providers',

        'general-attributes' => 'General Provider Information',

        'long-name' => 'Full Name',
        'short-name' => 'Acronym or short name',
        'slug' => 'Slug',
        'slug-note' => 'Note: Slug is based of title at the time of the provider creation and can not be changed.',
        'description' => 'Description',
        'address' => 'Address',
        'registry-number' => 'Legal registry number',
        'registry-number-details' => 'Required for non-persons only.',
        'featured-image' => 'Logo',
        'replace-image' => 'Replace',
        'public' => 'The provider and all its related data can be Publicly displayed',
        'public-note' => 'Note: this only concerns display of the provider itself. If you would like to limit the usage of Your data, refer to the Project..',

        'related-projects' => 'Related Projects',
        'change-relationships' => 'Change relationship',

        'updated' => 'All changes to the provider have been saved.'
    ],

    'settings' => [
        'general' => [
            'title' => 'General settings',
        ],
    ],

];
