<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'login' => [
        'nav'  => 'Login',
        'title'  => 'Login',
        'action'  => 'Login',
        'email'  => 'Email address',
        'remember'  => 'Remember Me',
        'password'  => 'Password',
        'forgot-password' => 'Lost password?'

    ],

    'registration' => [
        'intro' => [
            'title' => 'Become a Data Provider for the Bee Hub',

            'text' => 'The Bee Hub is a non-profit initiative of the EU Bee Partnership, which is a group of stakeholders including field practitioners such as beekeepers, veterinarians, and farmers, academia, environmental NGOs and industry, moderated by the European Food Safety Authority (EFSA). It is a Big Data approach to understand Pollinator diversity and threats in Europe. Its objective is to become a useful and effective tool in data gathering, sharing and communication. Secondly, it aims to be a collaborative tool in which beekeepers, monitoring device producers and/or research centres find a new level of cooperation.

In this questionnaire, we collect information about you or your institution as a data owner(s). It is in the interest of the Bee Hub to provide visibility to the individual efforts done to create data related to the health of bees and pollinators in general. One of our key parameters is for you to continue being the owner of the data, so you decide if you want to give access to your data to anyone else or not.

Thank you very much for your generosity and openness in sharing your data!!!',
            'cta'  => 'Start the process',
        ],


        'project' => [
            'title' => 'About the Project',

            'i-want-to'           => 'I would like to:',
            'share-personal-data' => 'Share my own data, of which I have full ownership rights',
            'share-project-data'  => 'Share data created as part of a project or group effort, where I have full or partial ownership rights',

            'project-name'             => 'Project Name',
            'project-name-placeholder' => 'Type in the name of the project or working group you participate in',

            'project-acronym'             => 'Project Acronym',
            'project-acronym-placeholder' => 'If the project has a short acronym, type it here',

            'project-description'             => 'Project Description',
            'project-description-placeholder' => 'Write a description about the project here. You can use simple formatting to improve the appearance of the description.',

            'project-start-date' => 'Start of the project',
            'project-end-date'   => 'End of the project',
            'project-date-note' => 'Note: fill out if the dates are relevant to your data. Please fill out the date in the form year-month-day with leading zeroes, for example 2020-04-20.',

            'public' => [
                'title'       => 'Publicly displayed information',
                'description' => 'These fields will be publicly displayed as part of the meta information that describes your data. All of these fields are optional but recommended.',

                'email'   => 'Contact e-mail address',
                'phone'   => 'Contact phone number',
                'website' => 'Website address',
            ],
        ],

        'data' => [
            'title' => 'About the Data',

            'text' => 'What kind of data are you willing to share',

            'smart-hives'     => [
                'title'                  => 'Smart Hives related data',
                'hive-temperature'       => 'In-hive temperature',
                'hive-relative-humidity' => 'In-hive relative humidity',
                'weight'                 => 'Weight',
                'counts'                 => 'Bee counts at the entrance of the hive',
                'temperature'            => 'External temperature',
                'relative-humidity'      => 'External relative humidity',
                'co2'                    => 'CO2',
                'sounds'                 => 'Sounds',
                'vibrations'             => 'Vibrations',
                'odors'                  => 'Odors',
                'other'                  => 'Other',
                'specify-other'          => 'Please specify other',
            ],
            'stress-factors'  => [
                'title'               => 'Stress factors related data',
                'pesticides-residues' => 'Pesticide residues',
                'pathogen-loads'      => 'Pathogen loads',
                'varroa'              => 'Varroa infestation level',
                'nutritional'         => 'Nutritional information',
                'weather'             => 'Weather information',
                'landscape'           => 'Landscape information (CORINE Land cover, satellite images, crops, etc)',
                'genetics'            => 'Genetics',
                'invasive'            => 'Invasive species',
                'other'               => 'Other',
                'specify-other'       => 'Please specify other',
            ],
            'beekeeping-data' => [
                'title'         => 'Beekeeping management data',
                'beekeeper'     => 'Description of the beekeeper (age, experience, professional/hobby, etc.)',
                'management'    => 'Colony management',
                'other'         => 'Other',
                'specify-other' => 'Please specify other',
            ],
            'mortality-data'  => [
                'title'    => 'Colony mortality data',
                'winter'   => 'Winter',
                'seasonal' => 'Seasonal',
            ],

            'data-provided' => [
                'title'          => 'How will the data be provided',
                'upload'         => 'As a single file or archive, up to 10MB',
                'upload-details' => 'Allowed file formats are csv, xls, xlsx, sql or zip files. There is no limit to file format within the zip file. The zip file should not be password protected. Maximum upload size is 10MB',
                'remote'         => 'As a link to a remote repository or file',
                'remote-details' => 'Please provide the link and details how to access the data.',
                'api'            => 'Via a remote API connection',
                'api-details'    => 'Please provide instructions either as a file upload or link, how to consume the API.',
                'other'          => 'Other formats',
                'other-details'  => 'Please type the details about the format of your data and how to retrieve it.',

                'upload-file'             => 'Choose file',
                'upload-file-placeholder' => 'Upload a file',
                'remote-url'              => 'Remote location URL',
                'notes'                   => 'Additional notes',
            ],

            'data-uses-title' => 'Would you like to share your raw data with other users?',

            'data-uses-open'       => 'Open: My raw data can be openly shared and downloaded by other users without restrictions.',
            'data-uses-restricted' => 'Restricted: My raw data can only be be shared by direct contact with me and following my agreement.',
            'data-uses-closed'     => 'Closed: My raw data can not be shared.',

            'data-integration-title' => 'Data integration contact',
            'contact-person'         => 'Contact person concerning data integration',
            'contact-email'          => 'Email address of the data integration contact person',
            'contact-email-details'  => 'This email address will not be published publicly but will be passed to a developer.'

        ],

        'ownership' => [
            'title' => 'About the ownership',

            'text'            => 'Who is/are the owner-s of the data?',
            'owner'           => 'Data Owner',
            'add-owner'       => 'Add another owner',
            'full-name'       => 'Full name',
            'short-name'      => 'Short name or acronym',
            'address'         => 'Address, post number and country',
            'registry-number' => 'Legal registry number',
            'registry-number-details' => 'Required for non-persons only.',
            'description'     => 'Description',
            'logo'            => 'Upload Logo',

            'public'   => [
                'title'       => 'Publicly displayed information',
                'description' => 'These fields will be publicly displayed as part of the meta information that describes your data. All of these fields are optional but recommended.',

                'email'   => 'Contact e-mail address',
                'phone'   => 'Contact phone number',
                'website' => 'Website address',
            ],
            'supplier' => 'Data Suppliers (e.g. electronic device supplier, beekeeping management app, etc.)',
        ],

        'review' => [
            'title' => 'Review before submitting',
            'text'  => 'Please review all the inputted information before submitting the form.',

            'about-project' => 'Project details',
            'about-data'    => 'Data details',
            'edit'          => 'Edit',
            'no-date'       => 'No date set',
            'no-input'       => 'No information provided',

            'data-types' => 'Types of provided data',
            'provided-data' => 'Data is provided as',
            'provided-data-description' => 'Provided notes',
            'provided-file-upload' => 'Uploaded file',
            'provided-file-upload-download' => 'Download',
            'provided-remote-link' => 'Remote link',
            'provided-remote-link-open' => 'Open',

            'provided-api-upload' => 'API upload',
            'provided-api-notes' => 'API details',
            'provided-other-notes' => 'Other type details',

            'data-other' => 'Other information about data',

            'consent-title'      => 'Data ownership consent and user generation',

            'user-auth' => 'This project will be related to the account you are currently logged in:',

            'user-name'         => 'Your Full Name',
            'user-email'         => 'User email',
            'user-email-details' => 'Write email address, with which you will be able to access and manage the data within The Bee Hub. A randomly generated password with login instructions will be send to this email after your request is validated.',

            'consent-rule' => 'In order to finish the registration process you must read and consent to the following terms and conditions.',

            'ownership-consent'   => 'I consent that the inputted and supplied data is either in my full or partial ownership and I have full authorization to share the data.',
            'public-consent'      => 'I understand that the inputted meta data (information provided within the Project and Data owner sections) might be publicly displayed and contains no private information shared without consent of its owner.',
            'terms-consent'       => 'I agree to the terms and condition under which I share my data and how The BeeHub will manage and process the uploaded data.',

            'submit' => 'Submit',
        ],

        'select-date'   => 'Select date',
        'next-step'     => 'Next step',
        'previous-step' => 'Back',

        'outro' => [
            'title' => 'Registration is almost completed',
            'text' => 'Thank you for filling out the form. Your last step towards a successful registration is setting Your password by visiting the link within the email, that was sent to Your account.',
            'cta' => 'Visit homepage',
        ],
    ],

    'emails' => [
        'welcome' => [
            'subject' => 'Complete registration',

            'title' => 'Thank you for Your interest in sharing Your data',
            'text' => 'In order to complete Your registration and get access to data management console, You need to complete the registration process by setting your password.',

            'cta' => 'Set Your password and manage Your data'
        ],
    ],
];
