<?php return [
    'legend' => 'Legend',

    'low'    => 'Low',
    'medium' => 'Medium',
    'high'   => 'High',
    'na'     => 'N/A',
];
