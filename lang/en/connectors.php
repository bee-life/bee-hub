<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Connector Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the texts related to Connectors.
    |
    */

    'meta' => [
        'index' => [
            'title'       => 'Participating projects',
            'description' => 'All participating projects are listed here. For a list of data providers, go to :link.',
            'promotion'   => 'Join as one of many data providers to support the project and the future of bees.',
            'join-now'    => 'Click here and fill out the form to join as a data provider.',
        ],
    ],

    'attributes' => [
        'about-project'   => 'About the project',
        'about-providers' => 'Data providers, that participated in the project.',
        'about-data'      => 'About shared data',
        'contact-details' => 'Contact details',
        'first-update'    => '',
        'last-update'     => '',
        'data-types'      => 'Types of data being shared',
        'data-points'     => 'Total number of data points shared',
    ],

    'types' => [
        'static' => 'Static',
        'remote' => 'Remote',
        'live'   => 'Live',
    ],


    'card' => [
        'read-more'       => 'More information',
    ],
];
