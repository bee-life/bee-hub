<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Administration Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the texts related to Nova Administration panel.
    |
    */

    'unknown' => 'Unknown',

    'groups' => [
        'cms'    => 'CMS',
        'data'   => 'Data',
        'meta'   => 'Metadata',
        'origin' => 'Origin Metadata',
    ],

    'attributes' => [
        'common' => [
            'id'             => 'ID',
            'uid'            => 'Unique identifier',
            'slug'           => 'Slug',
            'created_at'     => 'Created At',
            'updated_at'     => 'Updated At',
            'name'           => 'Name',
            'description'    => 'Description',
            'icon'           => 'Icon',
            'featured_image' => 'Featured Image',
            'public'         => 'Publicly displayed',
            'deprecated'     => 'Deprecation status',

            'website' => 'Website address',
            'email'   => 'E-mail address',
            'phone'   => 'Phone number',
        ],


        'group' => [
            'public'     => 'Public Attributes',
            'internal'   => 'Internal Attributes',
            'statistics' => 'Statistics',
        ],

        'descriptors'           => [
            'resource-name'          => 'Descriptors',
            'resource-singular-name' => 'Descriptor',
            'table_name'             => 'Table name',
            'category'               => 'Category',
            'since'                  => 'Since version',
            'data'                   => 'Data points',
            'data_count'             => 'Data count',
            'data_unique'            => 'Unique Values',
        ],
        'descriptor-categories' => [
            'resource-name'          => 'Descriptor Categories',
            'resource-singular-name' => 'Descriptor Category',
            'name'                   => 'Name',
            'description'            => 'Description',
            'parent'                 => 'Parent Category',
            'children'               => 'Children Categories',
            'descriptor-count'       => 'Number of Descriptors',
        ],
        'projects'              => [
            'resource-name'          => 'Projects',
            'resource-singular-name' => 'Project',
            'acronym'                => 'Acronym',
            'active'                 => 'Active',
            'class_name'             => 'Connector Class Name',
            'last_synced_at'         => 'Last Data Synced At',
            'sync_period'            => 'Update Period',
            'type'                   => 'Data acquisition type',
            'log_count'              => 'Total Logs',
            'descriptor_count'       => 'Total Descriptors',
            'providers'              => 'Project Data Providers',
            'logs'                   => 'Logs',
            'descriptors'            => 'Related Descriptors',
            'started_at'             => 'Project Start',
            'ended_at'               => 'Project End',
        ],
        'providers'             => [
            'resource-name'          => 'Providers',
            'resource-singular-name' => 'Provider',
            'full_name'              => 'Full Name',
            'active'                 => 'Active',
            'address'                => 'Address',
            'post'                   => 'Post address',
            'registry_number'        => 'Registration Number',
            'projects'               => 'Participating Projects',
        ],
        'logs'                  => [
            'resource-name'          => 'Logs',
            'resource-singular-name' => 'Log',
            'year'                   => 'Year',
            'date'                   => 'Date',
            'time'                   => 'Time',
            'project'                => 'Related Project',
            'apiary'                 => 'Related Apiary',
            'hive'                   => 'Related Hive',
            'data'                   => 'Data Points',
            'descriptors'            => 'Descriptors',
            'location'               => 'Location',
        ],
        'data'                  => [
            'resource-name'          => 'Data',
            'resource-singular-name' => 'Datum',
            'log'                    => 'Log',
            'descriptor'             => 'Descriptor',
            'origin'                 => 'Origin',
            'data'                   => 'Data Values',
            'value'                  => 'Value',
        ],
        'data-id'               => [
            'resource-name'          => 'Ids',
            'resource-singular-name' => 'Id',
            'value'                  => 'Name',
            'parent'                 => 'Parent',
            'children'               => 'Children',
            'project'                => 'Related Project',
            'logs'                   => 'Logs',
            'log_count'              => 'Total Logs',
            'children_count'         => 'Child Ids',
        ],
        'data-location'         => [
            'resource-name'          => 'Locations',
            'resource-singular-name' => 'Location',
            'value'                  => 'Coordinates',
            'location'               => 'Location',
            'log_count'              => 'Total Logs',
        ],

        'origin'                 => [
            'category'      => 'Category',
            'introduced_at' => 'Introduced At',
            'origin'        => 'Origin',
            'data'          => 'Data Points',
        ],
        'methodologies'          => [
            'resource-name'          => 'Methodologies',
            'resource-singular-name' => 'Methodology',
        ],
        'methodology_categories' => [
            'resource-name'          => 'Methodology Categories',
            'resource-singular-name' => 'Methodology Category',
            'methodologies'          => 'Methodologies',
        ],
        'devices'                => [
            'resource-name'          => 'Devices',
            'resource-singular-name' => 'Device',
            'vendor'                 => 'Vendor',
        ],
        'device_categories'      => [
            'resource-name'          => 'Device Categories',
            'resource-singular-name' => 'Device Category',
            'devices'                => 'Devices',
        ],
        'vendor'                 => [
            'resource-name'          => 'Device Vendors',
            'resource-singular-name' => 'Device Vendor',
            'devices'                => 'Devices'
        ],
        'publications'           => [
            'resource-name'          => 'Publications',
            'resource-singular-name' => 'Publication',

            'title'            => 'Title',
            'author'           => 'Author',
            'author-short'     => 'Short Author',
            'year'             => 'Publication Year',
            'medium'           => 'Medium',
            'medium-short'     => 'Short Medium',
            'editor'           => 'Editor',
            'editor-short'     => 'Short Editor',
            'issue'            => 'Issue Number',
            'volume'           => 'Volume Number',
            'edition'          => 'Edition Number',
            'pages'            => 'Total Pages',
            'website-date'     => 'Date of website',
            'publication-type' => 'Type of Publication',
        ],

        'locations' => [
            'regions'    => 'Regions',
            'region'     => 'Regions',
            'posts'      => 'Posts',
            'post'       => 'Post',
            'districts'  => 'Districts',
            'district'   => 'District',
            'country'    => 'Country',
            'parent'     => 'Parent Location',
            'children'   => 'Children Locations',
            'population' => 'Population',
            'surface'    => 'Surface (in square metres)',
            'map'        => 'Map display',
            'center'     => 'Center Coordinates',
            'area'       => 'Area',
        ],

        'country'  => [
            'resource-name'          => 'Countries',
            'resource-singular-name' => 'Country',
            'native_name'            => 'Native Name',
            'capital'                => 'Capital',
            'continent'              => 'Continent',
            'country_code'           => 'Country Code',
            'iso3'                   => 'ISO 3 Country Code',
            'currency_code'          => 'Currency Code',
            'currency_name'          => 'Currency Name',
            'phone_prefix'           => 'Phone prefix',
        ],
        'regions'  => [
            'resource-name'          => 'Regions',
            'resource-singular-name' => 'Region',
            'nuts_level'             => 'NUTS Level',
            'nuts_id'                => 'NUTS Number',
        ],
        'district' => [
            'resource-name'          => 'Districts',
            'resource-singular-name' => 'District',
            'nuts_id'                => 'NUTS Number',
            'lau_id'                 => 'LAU Number',
        ],
        'post'     => [
            'resource-name'          => 'Posts',
            'resource-singular-name' => 'Post',
            'number'                 => 'Number',
            'nuts_id'                => 'NUTS Number',
        ],

        'pages'   => [
            'resource-name'          => 'Pages',
            'resource-singular-name' => 'Page',
            'title'                  => 'Title',
            'meta_title'             => 'Meta Title',
            'meta_description'       => 'Meta Description',
            'content'                => 'Content',
        ],
        'news'    => [
            'resource-name'          => 'Pages',
            'resource-singular-name' => 'Page',
            'title'                  => 'Title',
            'meta_title'             => 'Meta Title',
            'meta_description'       => 'Meta Description',
            'content'                => 'Content',
            'published_at'           => 'Published At',
            'excerpt'                => 'Excerpt',
        ],
        'users'   => [
            'resource-name'          => 'Users',
            'resource-singular-name' => 'User',
            'password'               => 'Password',
            'email'                  => 'E-mail Address',
            'type'                   => 'User Type',
        ],
        'reports' => [
            'resource-name'          => 'Reports',
            'resource-singular-name' => 'Report',
            'title'                  => 'Title',
            'meta_title'             => 'Meta Title',
            'meta_description'       => 'Meta Description',
            'content'                => 'Content',
            'published_at'           => 'Published At',
            'abstract'               => 'Abstract',
            'keywords'               => 'Keywords',
            'author'                 => 'Author',
        ],
    ],

    'user_types' => [
        'administrator' => 'Administrator',
        'editor'        => 'Editor',
        'user'          => 'User',
    ],

    'actions' => [
        'delete-project' => [
            'name'    => 'Delete Projects',
            'confirm' => 'Type CONFIRM to confirm the deletion',
            'success' => '',
            'failure' => 'The confirmation was incorrect.',
        ],
    ],

    'flexible' => [
        'add-layout' => 'Add Section',
    ],

];
