<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth and registration
Auth::routes( [ 'verify' => true ] );
Route::get( '/logout', 'App\Http\Controllers\Auth\LoginController@logout' )->name( 'logout' );

Route::get( '/register/step-1', 'App\Http\Controllers\Auth\RegisterController@getStep1' )->name( 'register-step-1' );
Route::post( '/register/step-1', 'App\Http\Controllers\Auth\RegisterController@postStep1' );
Route::get( '/register/step-2', 'App\Http\Controllers\Auth\RegisterController@getStep2' )->name( 'register-step-2' );
Route::post( '/register/step-2', 'App\Http\Controllers\Auth\RegisterController@postStep2' );
Route::get( '/register/step-3', 'App\Http\Controllers\Auth\RegisterController@getStep3' )->name( 'register-step-3' );
Route::post( '/register/step-3', 'App\Http\Controllers\Auth\RegisterController@postStep3' );

Route::get( '/register/step-4', 'App\Http\Controllers\Auth\RegisterController@getStep4' )->name( 'register-step-4' );
Route::post( '/register/step-4', 'App\Http\Controllers\Auth\RegisterController@postStep4' );

Route::get( '/welcome', 'App\Http\Controllers\Auth\RegisterController@getStep5' )->name( 'register-step-5' );

Route::get( '/', 'App\Http\Controllers\PageController@index' )->name( 'frontpage' );
Route::get( '/app', 'App\Http\Controllers\AppController@index' )->name( 'app' );

if ( ! is_production() ) {
    // Various scripts to be run.
    Route::get( '/aggregate', 'App\Http\Controllers\ScriptsController@aggregate' );
    Route::get( '/live', 'App\Http\Controllers\ScriptsController@live' );
    Route::get( '/historic', 'App\Http\Controllers\ScriptsController@historic' );
    Route::get( '/scripts', 'App\Http\Controllers\ScriptsController@scripts' );
    Route::get( '/cache', 'App\Http\Controllers\ScriptsController@cache' );
    Route::get( '/debug', 'App\Http\Controllers\ScriptsController@debug' );
}

// News pages
Route::get( 'news', 'App\Http\Controllers\NewsController@index' )->name( 'news' );
Route::get( 'news/feed', 'App\Http\Controllers\NewsController@feed' )->name( 'news-feed' );
Route::get( 'news/{slug}', 'App\Http\Controllers\NewsController@single' )->where( 'slug', '[a-z0-9]+(?:-[a-z0-9]+)*' )->name( 'news-item' );

// Report pages
Route::get( 'reports', 'App\Http\Controllers\ReportController@index' )->name( 'reports' );
Route::get( 'reports/{slug}', 'App\Http\Controllers\ReportController@single' )->where( 'slug', '[a-z0-9]+(?:-[a-z0-9]+)*' )->name( 'report' );

// Meta pages
Route::get( 'projects', 'App\Http\Controllers\ProjectController@index' )->name( 'projects' );
Route::get( 'projects/{slug}', 'App\Http\Controllers\ProjectController@single' )->where( 'slug', '[a-z0-9]+(?:-[a-z0-9]+)*' )->name( 'project' );

Route::get( 'providers', 'App\Http\Controllers\ProviderController@index' )->name( 'providers' );
Route::get( 'providers/{slug}', 'App\Http\Controllers\ProviderController@single' )->where( 'slug', '[a-z0-9]+(?:-[a-z0-9]+)*' )->name( 'provider' );

Route::get( 'data', 'App\Http\Controllers\DescriptorController@index' )->name( 'descriptors' );
Route::get( 'data/{slug}', 'App\Http\Controllers\DescriptorController@single' )->where( 'slug', '[a-z0-9]+(?:-[a-z0-9]+)*' )->name( 'descriptor' );
Route::get( 'data-categories', 'App\Http\Controllers\DescriptorCategoryController@index' )->name( 'descriptor-categories' );
Route::get( 'data-categories/{slug}', 'App\Http\Controllers\DescriptorCategoryController@single' )->where( 'slug', '[a-z0-9]+(?:-[a-z0-9]+)*' )->name( 'descriptor-category' );

// Front end user management
Route::middleware( [ 'auth' ] )->group( function () {
    Route::get( '/user/account', 'App\Http\Controllers\Management\AccountController@getProfile' )->name( 'user-account' );
    Route::post( '/user/account/settings', 'App\Http\Controllers\Management\AccountController@postAccountSettings' )->name( 'user-account-settings' );
    Route::post( '/user/account/password', 'App\Http\Controllers\Management\AccountController@postPasswordSettings' )->name( 'user-account-password' );
    Route::resource( '/user/projects', 'App\Http\Controllers\Management\ProjectsController', [ 'except' => [ 'show', 'destroy' ] ] );
    Route::resource( '/user/providers', 'App\Http\Controllers\Management\ProvidersController', [ 'except' => [ 'show', 'destroy' ] ] );
} );

Route::group( [ 'prefix' => 'laravel-filemanager', 'middleware' => [ 'web', 'auth', 'editor' ] ], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
} );

// Catch all single slug route with valid characters - reduces overhead for invalid slugs, includes exceptions for Nova administration.
Route::get( '{slug}', 'App\Http\Controllers\PageController@page' )->where( 'slug', '(?!administration|nova-api)[a-z0-9]+(?:-[a-z0-9]+)*' )->name( 'page' );

