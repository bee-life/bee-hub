# The Beehub dataset sources implementation

This document describes public data providers, that are used in the Beehub software.

## Support resources

### Country, region and district boundaries with coordinates

We have imported a list of all administrative regions from NUTS_2016 database, last updated 14. 3. 2019.
Additionally, we are using Geodata (Boundaries, Regions and Labels - geographical centers) in geJSON in 1:1 Million scale.  
 
Source: [Eurostat](https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units/nuts)

Copyright information: [Website](https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units)

### Countries metadata and statistics

We have imported a list of all world countries from GeoNames.

Source: [GeoNames.org](https://www.geonames.org/)

Copyright information: [Creative Commons Attribution 4.0 License](Creative Commons Attribution 4.0 License)

### Post numbers in relation to NUTS

We have implemented a list of post numbers from most European countries with relation of NUTS_2016. Last updated 31. 7. 2019.

Source: [Eurostat](https://ec.europa.eu/eurostat/web/nuts/correspondence-tables/postcodes-and-nuts)

### Post center coordinates

We have included WGS86 coordinates of most European post code numbers.

Source: [GeoNames.org](https://www.geonames.org/)

Copyright information: [Creative Commons Attribution 4.0 License](Creative Commons Attribution 4.0 License)

### Datatype categorization

Bee data categorization is based on work by Beep fundation. 

Source: [Beep.nl](https://beep.nl/home-english)

Copyright information: [AGPL v3 licence](https://github.com/beepnl/BEEP/blob/master/LICENSE)
