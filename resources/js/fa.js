/**
 * FontAwesome 5 Pro Vue includes.
 */
import { library } from '@fortawesome/fontawesome-svg-core'
import { faMinus, faCaretDown, faCaretUp, faWindowMinimize, faArrowAltCircleLeft, faTimes, faCalendar } from '@fortawesome/pro-regular-svg-icons'
import { faSquare, faCheckSquare } from '@fortawesome/free-solid-svg-icons'
import { faChartLine } from '@fortawesome/pro-regular-svg-icons'
import { faThermometerThreeQuarters, faHumidity, faRaindrops, faSun, faWind, faWindsock, faLocationArrow, faSunrise, faSunset } from '@fortawesome/pro-regular-svg-icons'
import { faFacebookF, faTwitter, faLinkedinIn, faYoutube } from '@fortawesome/free-brands-svg-icons'

library.add( faMinus, faCaretDown, faCaretUp, faWindowMinimize, faArrowAltCircleLeft, faTimes, faCalendar );
library.add( faChartLine );
library.add( faThermometerThreeQuarters, faHumidity, faRaindrops, faSun, faWind, faWindsock, faLocationArrow, faSunrise, faSunset );
library.add( faFacebookF, faTwitter, faLinkedinIn, faYoutube );
library.add( faSquare, faCheckSquare );


