/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require( './bootstrap' );
require( './fa' );
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Loading from 'vue-loading-overlay';
import UniqueId from 'vue-unique-id';
import VModal from 'vue-js-modal'

window.Vue = require( 'vue' );

//require('./scripts/news-component.js');
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context( './', true, /\.vue$/i );
files.keys().map( key => Vue.component( key.split( '/' ).pop().split( '.' )[ 0 ], files( key ).default ) );

Vue.component( 'font-awesome-icon', FontAwesomeIcon );
//Vue.component( 'vue-trix', VueTrix );

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.prototype.$forceCompute= function(computedName, forceUpdate /* default: true */) {
    if (this._computedWatchers[computedName]) {
        this._computedWatchers[computedName].run();
        if (forceUpdate || typeof forceUpdate == 'undefined') this.$forceUpdate()
    }
}

Vue.use( Loading, {
    // props
    container: false,
    color: 'amber',
    loader: 'dots',
    width: 64,
    height: 64,
    backgroundColor: '#ffffff',
    opacity: 0.5,
    zIndex: 999,
} );
Vue.use(UniqueId);
Vue.use(VModal)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue( {
    el: '#app',
} );


