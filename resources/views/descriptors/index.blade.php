<?php /**
 * @var \App\Models\MetaData\Descriptor[] $models
 */ ?>
@extends('layouts.app')

@section('meta-title')
    {{ __('descriptors.meta.index.title') }}
@endsection

@section('meta-description')
    {{ __('descriptors.meta.index.description') }}
@endsection

@section('content')
    <div class="jumbotron bg-primary hexagon-background" style="background-image: url('images/Element 3.png');">
        <div class="container">
            <div class="row">
                <div class="col-12 text-white text-left px-2">
                    <h1 class="card-title display-4 pt-3">
                        {{ __('descriptors.meta.index.title') }}
                        <img src="{{ img('Element 5.png') }}" class="w-icon">
                    </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
                @include('descriptors.card-list')
        </div>
    </div>
@endsection
