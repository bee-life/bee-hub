<?php /**
 * @var \App\Models\MetaData\Descriptor $model
 */ ?>
{{-- Base entry point for news pages. --}}
@extends('layouts.app')

@section('meta-title')
    {{ $model->full_name }}
@endsection

@section('meta-description')
    {{ $model->about }}
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mt-3">
                        <li class="breadcrumb-item"><a href="{{ route('frontpage') }}">{{ __('static.homepage.title-short') }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('descriptors') }}">{{ __('descriptors.meta.index.title') }}</a></li>
                        @if($model->hasCategory())
                            @if($model->category->hasParent())
                                <li class="breadcrumb-item">{!! $model->category->parent->getLink()  !!}</li>
                            @endif
                            <li class="breadcrumb-item">{!! $model->category->getLink()  !!}</li>
                        @endif
                        <li class="breadcrumb-item active">{{ $model->name }}</li>
                    </ol>
                </nav>
                <h1 class="mt-4 mb-0 text-center">{{ $model->name }}</h1>
                @if($model->hasCategory())
                    <p class="text-center"><small>{!! $model->category->getLink()  !!} </small></p>
                @endif
            </div>

            <div class="col-sm-3">
                @if($model->hasFeaturedImage())
                    {!! $model->getFeaturedImage(['w-100', 'z-depth-1']) !!}
                @elseif($model->hasIcon())
                    {!! $model->getIcon(['w-100', 'z-depth-1']) !!}
                @else
                    {!! $model->getFeaturedImage(['w-100', 'z-depth-1']) !!}
                @endif
            </div>
            <div class="col-sm-9">
                <h3>{{ trans('descriptors.attributes.about') }}</h3>
                <p>{!! $model->description !!}</p>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-12">
                <h3>{{ __('descriptors.attributes.statistics') }}</h3>

                <ul>
                    <li>{{ __('descriptors.statistics.data-count') }} {{ format_number($model->dataCount(), 0) }}</li>
                    <li>{{ __('descriptors.statistics.data-count-unique') }} {{ format_number($model->dataCountUnique(), 0) }}</li>
                    <li>{{ __('descriptors.statistics.projects-count') }} {{ format_number($model->projects->count(), 0) }}</li>
                </ul>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-12">
                <h3 class="mb-4">{{ __('descriptors.attributes.projects') }}</h3>

                <div class="row">
                    @include('projects.card-list', ['models' => $model->projects])
                </div>
            </div>
        </div>
    </div>
@endsection
