<?php /**
 * @var \App\Models\MetaData\Descriptor $model
 */ ?>
<div class="card mb-4 promoting-card">
    <div class="card-body">
        <div class="d-flex flex-row">
            @if($model->hasIcon())
                <img src="{{ $model->getIconUrl() }}" class="square mr-3" height="50px" width="50px">
            @else
                <img src="{{ $model->getFeaturedImageUrl() }}" class="square mr-3" height="50px" width="50px">
            @endif
            <div>
                <h4 class="card-title mb-0">{!! $model->getLink() !!}</h4>
                @if($model->hasCategory())
                    <p><small>{!! $model->category->getLink()  !!} </small></p>
                @endif
            </div>
        </div>
        <div class="">
            <a class="btn btn-outline-primary btn-sm" href="{{ $model->getUrl() }}">{{ trans('descriptors.card.read-more') }}</a>
        </div>
    </div>
</div>
