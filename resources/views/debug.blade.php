<?php /**
 * @var string $data
 */ ?>

@extends('layouts.empty')

@section('content')
    @if (is_local() || is_development())
        {!! $data !!}
    @endif

@endsection
