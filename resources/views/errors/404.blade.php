<?php /**
 * @var \App\Models\CMS\Page $page
 */ ?>
{{-- Base entry point for news pages. --}}
@extends('layouts.app')

@section('meta-title')
    {{ trans('static.404.title') }}
@endsection

@section('meta-description')
    {{ trans('static.404.description') }}
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-header">
                    <h1 class="m-4 text-center">{{ trans('static.404.title') }}</h1>
                </div>
            </div>

            <div class="col-12 post-item post-single">
                <div class="post-content">
                    <p>{{ trans('static.404.description') }}</p>
                    <br>
                    <p>{{ trans('static.404.recommendations') }}</p>
                    <ul>
                        <li><a href="{{ url('/') }}">{{ trans('static.404.links.homepage') }}</a></li>
                        <li><a href="{{ url('/about') }}">{{ trans('static.404.links.about') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
