<?php /**
 * @var \App\Models\MetaData\Project $model
 */ ?>
{{-- Base entry point for news pages. --}}
@extends('layouts.app')

@section('meta-title')
    {{ $model->name }}
@endsection

@section('meta-description')
    {{ $model->description }}
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mt-3">
                        <li class="breadcrumb-item"><a href="{{ route('frontpage') }}">{{ trans('static.homepage.title-short') }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('projects') }}">{{ trans('connectors.meta.index.title') }}</a></li>
                        <li class="breadcrumb-item active">{{ $model->acronym }}</li>
                    </ol>
                </nav>
                <h1 class="m-4 text-center">{{ $model->name }}</h1>
            </div>

            <div class="col-sm-3">
                {!! $model->getFeaturedImage(['w-100', 'z-depth-1']) !!}
            </div>
            <div class="col-sm-9">
                <h3>{{ trans('connectors.attributes.about-project') }}</h3>
                <p>{!! $model->description !!}</p>


                @if($model->hasWebsite())
                    <p>{!! $model->getWebsiteLink() !!}</p>
                @endif
            </div>
            @if($model->hasEmail() || $model->hasPhone())
                <div class="col-md-6 my-4">
                    <h3>{{ trans('connectors.attributes.contact-details') }}</h3>
                    <ul class="list-unstyled">
                        @if($model->hasEmail())
                            <li>{!! $model->getEmail() !!}</li>@endif
                        @if($model->hasPhone())
                            <li>{!! $model->getPhoneLink() !!}</li>@endif
                    </ul>
                </div>
            @endif
        </div>
        <div class="row my-4">
            <div class="col-12">
                <h3 class="mb-4">{{ trans('connectors.attributes.about-providers') }}</h3>

                <div class="row">
                    @include('providers.card-list', ['models' => $model->providers])
                </div>
            </div>
        </div>

        <div class="row my-4">

            <div class="col-12">
                <h3>{{ trans('connectors.attributes.about-data') }}</h3>
                <p>{{ trans('connectors.attributes.data-points') }}: {{ format_number($model->logs_count, 0) }}</p>
            </div>

            @foreach($model->descriptors as $descriptor)
                @if($descriptor->public)
                    <div class="col-md-3">
                        @include('data.descriptors.list-item', ['model' => $descriptor])
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@endsection
