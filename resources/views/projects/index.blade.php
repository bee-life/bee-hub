<?php /**
 * @var \App\Models\MetaData\Project[] $projects
 */ ?>
@extends('layouts.app')

@section('meta-title')
    {{ trans('connectors.meta.index.title') }}
@endsection

@section('meta-description')
    {{ trans('connectors.meta.index.description') }}
@endsection

@section('content')
    <div class="jumbotron bg-primary hexagon-background" style="background-image: url('images/Element 3.png');">
        <div class="container">
            <div class="row">
                <div class="col-12 text-white text-left px-2">
                    <h1 class="card-title display-4 pt-3">
                        {{ trans('connectors.meta.index.title') }}
                        <img src="{{ img('Element 5.png') }}" class="w-icon">
                    </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12 mb-4">
                @include('components.promotion')
            </div>
        </div>
        <div class="row">
            @include('projects.card-list', ['models' => $projects])
        </div>
    </div>
@endsection
