<?php /**
 * @var \App\Models\MetaData\Descriptor $descriptor
 */
?>
<div class="small">
    <h6>{!! $descriptor->getLink()  !!}</h6>
    <p>{!! $descriptor->description  !!}</p>
</div>
