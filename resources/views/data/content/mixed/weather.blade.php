@php
    /**
     * @var \App\Models\Logs\Log  $log
     * @var \App\Models\MetaData\Descriptor  $descriptor
     * @var \App\Models\MetaData\Descriptor[] $descriptors
     */
    $descriptors = $log->data->keyBy('descriptor_id');
@endphp


@isset($descriptors[getDescriptorBySlug('temperature')])
    <p>{{ __('logs.weather-sensors.temperature') }}: {{ format_temperature( $descriptors[getDescriptorBySlug('temperature')]->value, 'celsius' ) }}</p>
@endisset
@isset($descriptors[getDescriptorBySlug('relative-humidity')])
    <p>{{ __('logs.weather-sensors.relative-humidity') }}: {{ format_percentage( $descriptors[getDescriptorBySlug('relative-humidity')]->value ) }}</p>
@endisset
@isset($descriptors[getDescriptorBySlug('rain')])
    <p>{{ __('logs.weather-sensors.rain') }}: {{ format_volume( $descriptors[getDescriptorBySlug('rain')]->value, 'l' ) }}</p>
@endisset
@isset($descriptors[getDescriptorBySlug('solar-radiation')])
    <p>{{ __('logs.weather-sensors.solar-radiation') }}: {{ format_volume( $descriptors[getDescriptorBySlug('solar-radiation')]->value, 'l' ) }}</p>
@endisset
@isset($descriptors[getDescriptorBySlug('wind-direction')])
    <p>{{ __('logs.weather-sensors.wind-direction') }}: {{ format_volume( $descriptors[getDescriptorBySlug('wind-direction')]->value, 'l' ) }}</p>
@endisset
@isset($descriptors[getDescriptorBySlug('wind-speed')])
    <p>{{ __('logs.weather-sensors.wind-speed') }}: {{ format_speed( $descriptors[getDescriptorBySlug('wind-speed')]->value, 'm/s' ) }}</p>
@endisset
@isset($descriptors[getDescriptorBySlug('wind-gust')])
    <p>{{ __('logs.weather-sensors.wind-gust') }}: {{ format_speed( $descriptors[getDescriptorBySlug('wind-gust')]->value, 'm/s' ) }}</p>
@endisset
