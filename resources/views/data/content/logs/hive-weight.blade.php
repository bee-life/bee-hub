@php
    /**
     * @var \App\Models\Logs\Log  $log
     * @var \App\Models\MetaData\Descriptor  $descriptor
     * @var \App\Models\MetaData\Descriptor[] $descriptors
     * @var \App\Models\Logs\Data\DataAggregate[] $aggregates
     */
    $descriptors = $log->data->keyBy('descriptor_id');
    $aggregates  = $log->external_id->aggregates->keyBy( 'count' );
@endphp
<div class=" px-4 py-2">
    <p>
        @if( $log->hasApiaryId())
            {{ __('logs.apiary-id') }}: {{ $log->apiary_id->value }},
        @endif
        {{ __('logs.hive-id') }}: {{ $log->external_id->value }}
    </p>
    <p>{{ __('logs.region') }}: {{ $log->getLocation() }}</p>
    @isset( $descriptors[ getDescriptorBySlug('hive-weight') ])
        <p>{{ __('logs.hive-sensors.hive-weight') }}: {{ format_weight( $descriptors[ getDescriptorBySlug('hive-weight') ]->value, 'kg' ) }}</p>
    @endisset
    @isset($aggregates[1])
        <p>{{ __('logs.hive-sensors.weight-change-1') }}: {{ format_weight( $aggregates[1]->change, 'kg' ) }}</p>
    @endisset
    @isset($aggregates[3])
        <p>{{ __('logs.hive-sensors.weight-change-3') }}: {{ format_weight( $aggregates[3]->change, 'kg' ) }}</p>
    @endisset
    @isset($aggregates[7])
        <p>{{ __('logs.hive-sensors.weight-change-7') }}: {{ format_weight( $aggregates[7]->change, 'kg' ) }}</p>
    @endisset

    @isset($descriptors[getDescriptorBySlug('hive-temperature')])
        <p>{{ __('logs.hive-sensors.inside-temperature') }}: {{ format_temperature( $descriptors[getDescriptorBySlug('hive-temperature')]->value, 'celsius' ) }}</p>
    @endisset

    @include('data.content.mixed.weather')
    @isset($descriptors[getDescriptorBySlug('updated-at')])
        <p>{{ __('logs.hive-sensors.updated-at') }}: {{ format_date( $descriptors[getDescriptorBySlug('updated-at')]->data->value, true ) }}</p>
    @endisset
</div>
