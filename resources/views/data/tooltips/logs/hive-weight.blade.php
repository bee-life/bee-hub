@php
    /**
     * @var \App\Models\Logs\Log  $log
     * @var \App\Models\MetaData\Descriptor  $descriptor
     *
     * TODO: Better handle Unknown apiaries.
     */
    $dataModels = $log->data->keyBy('descriptor_id');
@endphp
@if(str_contains($log->apiary->value, 'unknown_'))
    {{ $log->hive }}
@else
    {{ $log->apiary }}
@endif


