@php
    /**
     * @var \App\Models\Logs\Log  $log
     * @var \App\Models\MetaData\Descriptor  $descriptor
     */
@endphp
{{ $log->location }}: {{ format_percentage( $log->getValue( $descriptor ) ) }}
