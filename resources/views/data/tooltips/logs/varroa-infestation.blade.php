@php
    /**
     * @var \App\Models\Logs\Log  $log
     * @var \App\Models\MetaData\Descriptor  $descriptor
 *                                              {{ $log->location }}: {{ __('logs.varroa_infestation_alerts.severity_level_' . $dataModels[\App\Models\Logs\Descriptor::getIdFromUid('varroa-infestation-alerts')]) }}
     */
    $dataModels = $log->data->keyBy('descriptor_id');
@endphp

{{ __('logs.varroa-infestation.tooltip', [ 'country' => $log->location ,'value' => format_number($dataModels[$descriptor->id]->value, 2)] ) }}
