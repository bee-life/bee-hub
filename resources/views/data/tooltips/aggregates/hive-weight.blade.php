@php
    /**
     * @var \App\Models\Logs\Data\DataAggregate  $aggregate
     * @var \App\Models\MetaData\Descriptor  $descriptor
     */
@endphp
{{ $aggregate->externalId }}: {{ trans('logs.hive-sensors.change', [ 'change' => format_weight( $aggregate->change, 'kg' )]) }}
