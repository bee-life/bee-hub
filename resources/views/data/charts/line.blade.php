<?php /**
 * @var \App\Models\MetaData\Descriptor                             $descriptor
 * @var \App\Models\Logs\Log[]|\Illuminate\Support\Collection|array $log
 * @var string                                                      $id
 */ ?>
<canvas id="line-chart-{{ $id }}" width="400" height="400"></canvas>
<script>
    var ctx = document.getElementById( 'line-chart-{{ $id }}' ).getContext( '2d' );

</script>
