@php
    /**
     * @var \App\Models\Logs\Log  $log
     * @var \App\Models\MetaData\Descriptor  $descriptor
     * @var \App\Models\MetaData\DescriptorCategory  $category
     */
@endphp
@popup([ 'title' => $log->location, 'log' => $log ])
<div class="card-body">
    <h5>{{ $descriptor->name }}</h5>
    @isset ($descriptor->description)
        {!! $descriptor->description  !!}
    @endif
    <p>{{ __('logs.socio-economics.year') }}: {{ $log->year }}</p>
    @foreach($log->data as $data)
        @if ($data->descriptor_id == $descriptor->id)
            @if($data->data->reference->code == 'USD')
                <p>{{ __('logs.socio-economics.prices') }}: {!! format_currency( $data->data ) !!} per tonne {!! $data->getOrigin()  !!}</p>
            @else
                <p>{{ __('logs.socio-economics.prices-local') }}: {!! format_currency( $data->data ) !!} per tonne {!! $data->getOrigin()  !!}</p>
            @endif
        @endif
    @endforeach
</div>
@endpopup
