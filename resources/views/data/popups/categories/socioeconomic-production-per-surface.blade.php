@php
    /**
     * @var \App\Models\Logs\Log  $log
     * @var \App\Models\MetaData\Descriptor  $descriptor
     * @var \App\Models\MetaData\DescriptorCategory  $category
     */
    $descriptors = $log->data->keyBy('descriptor_id');
    $productionDescriptorId = \App\Models\MetaData\Descriptor::getIdFromUid(substr($descriptor->uid, 0, -12), false);
    //dd($descriptors[$descriptor->id]->value);
@endphp
@popup([ 'title' => $log->location, 'log' => $log ])
<div class="card-body">
    <h5>{{ $descriptor->name }}</h5>
    @isset ($descriptor->description)
        {!! $descriptor->description  !!}
    @endif
    <p>{{ __('logs.socio-economics.year') }}: {{ $log->year }}</p>
    <p>{{ __('logs.socio-economics.production') }}: {!! format_special_unit( $descriptors[$descriptor->id]->value,  'kg/km2', 2 ) !!} {!! $descriptors[$descriptor->id]->getOrigin()  !!}</p>
    @if($descriptors->has($productionDescriptorId))
        <p>{{ __('logs.socio-economics.production') }}: {!! format_weight( $descriptors[$productionDescriptorId]->value,  't', 0 ) !!} {!! $descriptors[$productionDescriptorId]->getOrigin()  !!}</p>
    @endif
</div>
@endpopup
