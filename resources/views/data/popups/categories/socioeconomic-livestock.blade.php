@php
    /**
     * @var \App\Models\Logs\Log  $log
     * @var \App\Models\MetaData\Descriptor  $descriptor
     * @var \App\Models\MetaData\DescriptorCategory  $category
     */
    $descriptors = $log->data->keyBy('descriptor_id');
@endphp
@popup([ 'title' => $log->location, 'log' => $log ])
<div class="card-body">
    <h5>{{ $descriptor->name }}</h5>
    @isset ($descriptor->description)
        {!! $descriptor->description  !!}
    @endif
    <p>{{ __('logs.socio-economics.year') }}: {{ $log->year }}</p>
    <p>{{ __('logs.socio-economics.amount') }}: {!! format_number( $descriptors[$descriptor->id]->data->value, 0 ) !!}{!! $descriptors[$descriptor->id]->getOrigin()  !!}</p>
</div>
@endpopup
