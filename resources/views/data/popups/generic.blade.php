@php
    /**
     * @var \App\Models\Logs\Log  $log
     * @var \App\Models\MetaData\Descriptor  $descriptor
     */
    $value = $log->data->where('descriptor_id', $descriptor->id)->first()->value;
@endphp
@popup([ 'title' => __('logs.hive_sensors.title', ['location' => $log->location]), 'log' => $log ])
<div class="card-body">
    <p class="card-text"><small>{!! $aggregate->descriptor->description  !!}</small></p>
</div>
<div class="list-group list-group-flush">
    <div class="list-group-item">
        <p class="card-text">{{ __('Measurements') }}: {{ $aggregate->count }}</p>
        <p class="card-text">{{ __('Average') }}: {{ $aggregate->getMedian() }}</p>
        <p class="card-text">{{ __('Median') }}: {{ $aggregate->getMedian() }}</p>
        <p class="card-text">{{ __('Smallest Value') }}: {{ $aggregate->getMin() }}</p>
        <p class="card-text">{{ __('Largests Value') }}: {{ $aggregate->getMax() }}</p>
        <p class="card-text">{{ __('Measured in') }}: {{ $aggregate->year }}</p>
    </div>
</div>
@endpopup
