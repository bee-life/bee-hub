@php
    /**
     * @var \App\Models\Logs\Log  $log
     * @var \App\Models\MetaData\Descriptor  $descriptor
     */
    $descriptors = $log->data->keyBy('descriptor_id');
@endphp
@popup(['title' => __('logs.varroa_infestation_alerts.infestation_levels', ['location' => $log->location]), 'log' => $log])
<div class="card-body">
    <p>{{ __('logs.varroa_infestation_alerts.severity') }}: {{ __('logs.varroa_infestation_alerts.severity_level_' . $descriptors[ \App\Models\MetaData\Descriptor::getIdFromUid('varroa-infestation-alerts') ]) }}</p>
    <hr>
    <p>{{ __('logs.varroa_infestation_alerts.validity', ['date_from' => $descriptors[ \App\Models\MetaData\Descriptor::getIdFromUid('valid-from') ], 'date_to' => $descriptors[ \App\Models\MetaData\Descriptor::getIdFromUid('valid-to') ]]) }}</p>
    <hr>
    <small>
        {!! $descriptors[ \App\Models\MetaData\Descriptor::getIdFromUid('content')]->data !!}
    </small>
</div>
@endpopup

