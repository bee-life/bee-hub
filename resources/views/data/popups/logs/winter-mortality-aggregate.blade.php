@php
    /**
     * @var \App\Models\Logs\Log  $log
     * @var \App\Models\MetaData\Descriptor  $descriptor
     */
@endphp
@popup([ 'title' => __('logs.winter-mortality.title', ['location' => $log->location]), 'log' => $log ])
<div class="card-body">
    <p class="card-text"><small>{!! $descriptor->description  !!}</small></p>
</div>
<div class="list-group list-group-flush">
    <div class="list-group-item">
        <p class="card-text">{{ __('logs.winter-mortality.mortality') }}: {{ format_percentage( $log->getValue( $descriptor ), 2 ) }}</p>
        @if( $log->hasData( getDescriptorIdByUid( 'data-counts' ) ) )
            <p class="card-text">{{ __('logs.winter-mortality.data-count') }}: {{ format_number( $log->getValue( getDescriptorIdByUid('data-counts') ), 0 ) }}</p>
        @endif
        <p class="card-text">{{ __('logs.winter-mortality.measured') }}: {{ $log->year }}</p>
    </div>
</div>
@endpopup
