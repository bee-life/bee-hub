@php
    /**
     * @var \App\Models\Logs\Log  $log
     * @var \App\Models\MetaData\Descriptor  $descriptor
     */
    $descriptors = $log->data->keyBy('descriptor_id');
@endphp
@popup([ 'title' => __('logs.hive_sensors.title', ['location' => $log->location]), 'log' => $log ])
<div class="card-body">
    <p>{{ __('logs.hive_sensors.hive-weight') }}: {{ format_weight( $descriptors[$descriptor->id]->value, 'kg' ) }}</p>
    <hr>
    <p>{{ __('logs.hive_sensors.updated_at') }}: {{ format_date( $descriptors[\App\Models\MetaData\Descriptor::getIdFromUid('updated-at')]->data->value, true ) }}</p>
</div>
@endpopup
