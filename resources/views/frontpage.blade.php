<?php /**
 * @var array                                                      $map
 * @var Illuminate\Support\Collection|\App\Models\CMS\News[]|array $news
 * @var Illuminate\Support\Collection|array                        $descriptors
 * @var App\Models\News|Illuminate\Database\Eloquent\Model         $newsItem
 */ ?>

@extends('layouts.app')

@section('meta-title')
    EU Bee Partnership Prototype Platform on Bee Health
@endsection

@section('content')
    <div class="frontpage container-fluid">
        <map-component :data='{!! json_encode($descriptors)  !!}'></map-component>
    </div>
@endsection
