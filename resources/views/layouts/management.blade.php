@extends('layouts.app')

@section('content')
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark primary-color mb-4">
        <div class="container">
            <!-- Collapse button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
                    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!-- Collapsible content -->
            <div class="collapse navbar-collapse" id="basicExampleNav">
                <!-- Links -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('user-account') }}">{{ __('management.nav.account') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('projects.index') }}">{{ __('management.nav.projects') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('providers.index') }}">{{ __('management.nav.providers') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register-step-1') }}">{{ __('management.nav.register') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}">{{ __('management.nav.logout') }}</a>
                    </li>
                </ul>
                <!-- Links -->
            </div>
            <!-- Collapsible content -->
        </div>
    </nav>
    <!--/.Navbar-->

    @yield('management-content')
@endsection
