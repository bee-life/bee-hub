<?php
$menus = function_exists( 'nova_get_menu' ) ? nova_get_menu( 'header' ) : null;
?><!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('meta-title', trans('static.homepage.title'))</title>
    <meta name="description" content="@yield('meta-description', trans('static.meta-description'))">
    <meta name="author" content="BeeLife European Beekeeping Coordination">

    <link rel="apple-touch-icon" sizes="57x57" href="/images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
    <link rel="manifest" href="/images/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-164789573-1"></script>
    <script>window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push( arguments );
        }

        gtag( 'js', new Date() );
        gtag( 'config', 'UA-164789573-1' );</script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,500,700,700i&display=swap&subset=cyrillic,cyrillic-ext,latin-ext"
          rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('headers')
</head>
<body class="{{ \App\Helpers\Body::print() }}">
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="/images/Prototype-logo.png" height="60">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                @isset($menus)
                    <ul class="navbar-nav ml-auto {{$menus['slug']}}">
                        @foreach ($menus['menuItems'] as $item)
                            <li class="nav-item">
                                <a class="nav-link menu-item-{{ $item['id'] }}" href="{{ $item['value'] }}"
                                   target="{{ $item['target'] }}">{{ $item['name'] }}</a>
                            </li>
                        @endforeach

                        @if(Auth::check())
                            <li class="nav-item">
                                <a class="nav-link menu-item-account" href="{{ route('user-account') }}"
                                   target="{{ $item['target'] }}">{{ __('management.nav.account') }}</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link menu-item-login" href="{{ route('login') }}"
                                   target="{{ $item['target'] }}">{{ __('auth.login.nav') }}</a>
                            </li>
                        @endif

                    </ul>
                @endif
            </div>
        </div>
    </nav>

    <main>
        @yield('content')
    </main>
    @if(! Route::is('frontpage'))
        <footer class="page-footer font-small  mdb-color lighten-3 pt-4">
            <!-- Call to action -->
            <ul class="list-unstyled list-inline text-center py-2">
                <li class="list-inline-item">
                    <h5 class="mb-1">{{ __('Join as a Data Provider') }}</h5>
                </li>
                <li class="list-inline-item">
                    <a href="{!! config('app.survey_link') !!}" class="btn btn-blue-grey btn-rounded">{{ __('Join now') }}</a>
                </li>
            </ul>
            <!-- Call to action -->

            <hr>

            <!-- Social buttons -->
            <ul class="list-unstyled list-inline text-center">
                <li class="list-inline-item">
                    <a class="btn-floating btn-fb mx-1" target="_blank" href="https://www.facebook.com/BeeLifeEU/">
                        <i>
                            <font-awesome-icon :icon="['fab', 'facebook-f']"></font-awesome-icon>
                        </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-tw mx-1" target="_blank" href="https://twitter.com/BeeLifeEU">
                        <i>
                            <font-awesome-icon :icon="['fab', 'twitter']"></font-awesome-icon>
                        </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-li mx-1" target="_blank" href="https://www.linkedin.com/company/beelife-european-beekeeping-coordination/">
                        <i>
                            <font-awesome-icon :icon="['fab', 'linkedin-in']"></font-awesome-icon>
                        </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-yt mx-1" target="_blank" href="https://www.youtube.com/channel/UCbEkE_u9ftNTc1S7RWZXEFA">
                        <i>
                            <font-awesome-icon :icon="['fab', 'youtube']"></font-awesome-icon>
                        </i>
                    </a>
                </li>
            </ul>
            <!-- Social buttons -->

            <!-- Copyright -->
            <div class="footer-copyright text-center py-3">{!!  __('© EU Bee Partnership Prototype Platform on Bee Health copyright :year: :efsa and :beelife. All data, brands and logos are property by their respective owners.', [
    'year' =>  \Carbon\Carbon::today()->year ,
    'beelife' => '<a href="https://www.bee-life.eu/">BeeLife European Beekeeping Coordination</a>',
    'efsa' => '<a href="http://efsa.europa.eu/">EFSA</a>',
]) !!}
            </div>
            <!-- Copyright -->
        </footer>
    @endif
</div>

<!-- Scripts -->
<script src="{{ asset('js/vendor.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
@stack('scripts')
</body>
</html>
