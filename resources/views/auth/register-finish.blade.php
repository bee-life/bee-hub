@extends('layouts.app')

@section('content')
    <div class="jumbotron bg-primary hexagon-background" style="background-image: url('images/Element 3.png');">
        <div class="container">
            <div class="row">
                <div class="col-12 text-white text-left px-2">
                    <h1 class="card-title display-4 pt-3">
                        {{ __('auth.registration.outro.title') }}
                        <img src="{{ img('Element 1.png') }}" class="w-icon">
                    </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 mb-2">
                {!! nl2br(__('auth.registration.outro.text'))  !!}
            </div>

            <div class="col-12 mb-5 text-center">
                <a href="/" class="btn btn-primary btn-lg">{{ __('auth.registration.outro.cta') }}</a>
            </div>
        </div>
    </div>
@endsection
