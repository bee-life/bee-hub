@extends('layouts.app')

@section('content')
    <div class="jumbotron bg-primary hexagon-background" style="background-image: url('/images/Element 3.png');">
        <div class="container">
            <div class="row">
                <div class="col-12 text-white text-left px-2">
                    <h1 class="card-title display-4 pt-3">
                        {{ __('auth.registration.project.title') }}
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <form method="POST" action="{{ url('register/step-1') }}" enctype="multipart/form-data">
        @csrf

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 mb-2">
                    @if($errors->isNotEmpty())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br />
                            @endforeach
                        </div>
                    @endif


                    {{ __('auth.registration.project.i-want-to') }}
                </div>

                <div class="col-12">
                    <div class="accordion md-accordion mb-5" id="accordionEx1" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header amber darken-1 z-depth-1" role="tab" id="headingTwo1">
                                <a {{ $expand != 'project' ? 'class="collapsed"' : null }} data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo1"
                                   aria-expanded="false" aria-controls="collapseTwo1">
                                    <h5 class="mb-0 white-text">
                                        {{ __('auth.registration.project.share-project-data') }} <i class="fas fa-angle-down rotate-icon"></i>
                                    </h5>
                                </a>
                            </div>

                            <div id="collapseTwo1" class="collapse {{ $expand == 'project' ? 'show' : null }}" role="tabpanel" aria-labelledby="headingTwo1"
                                 data-parent="#accordionEx1">
                                <div class="card-body">
                                    <div class="md-form pb-2">
                                        <input value="{{ session('project-name') }}" placeholder="{{ __('auth.registration.project.project-name-placeholder') }}" type="text"
                                               id="project-name" class="form-control" name="project-name" length="200" required>
                                        <label for="project-name">{{ __('auth.registration.project.project-name') }}*</label>
                                    </div>

                                    <div class="md-form pb-2">
                                        <input value="{{ session('project-acronym') }}" placeholder="{{ __('auth.registration.project.project-acronym-placeholder') }}" type="text"
                                               id="project-acronym" class="form-control" name="project-acronym" length="100" required>
                                        <label for="project-acronym">{{ __('auth.registration.project.project-acronym') }}*</label>
                                    </div>

                                    <div class="pb-2">
                                        <input id="project-description-input" type="hidden" name="project-description" value="{{ session('project-description') }}">
                                        <label for="project-description">{{ __('auth.registration.project.project-description') }}</label>
                                        <trix-editor input="project-description-input" placeholder="{{ __('auth.registration.project.project-description-placeholder') }}"></trix-editor>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div id="date-picker-project-start-date" class="md-form md-outline input-with-post-icon datepicker mb-0">
                                                <input placeholder="{{ __('auth.registration.select-date') }}" type="text" id="project-start-date" class="form-control"
                                                       name="project-start-date" value="{{ session('project-start-date') }}"aria-describedby="project-date-note">
                                                <label for="project-start-date">{{ __('auth.registration.project.project-start-date') }}</label>
                                                <font-awesome-icon :icon="['far', 'calendar']" class="input-prefix" tabindex="0"></font-awesome-icon>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div id="date-picker-project-end-date" class="md-form md-outline input-with-post-icon datepicker mb-0">
                                                <input placeholder="{{ __('auth.registration.select-date') }}" type="text" id="project-end-date" class="form-control"
                                                       name="project-end-date" value="{{ session('project-end-date') }}" aria-describedby="project-date-note">
                                                <label for="project-end-date">{{ __('auth.registration.project.project-end-date') }}</label>
                                                <font-awesome-icon :icon="['far', 'calendar']" class="input-prefix" tabindex="0"></font-awesome-icon>
                                            </div>
                                        </div>
                                    </div>
                                    <small id="project-date-note" class="form-text text-muted mb-3">{{ __('auth.registration.project.project-date-note') }}</small>

                                    <p>{{ __('auth.registration.ownership.logo') }}</p>


                                    <div class="md-form" style="max-width: 200px;">
                                        <div class="file-field">
                                            <div class="z-depth-1-half mb-4">
                                                <img src="{{ !empty(session('project-logo')) ?  Storage::url(session('project-logo')) : asset('images/Prototype-Platform.png') }}"
                                                     class="img-fluid"
                                                     id="image-preview" alt="example placeholder">
                                            </div>
                                            <div class="d-flex justify-content-center">
                                                <div class="btn btn-mdb-color btn-rounded float-left">
                                                    <span>{{ __('auth.registration.data.data-provided.upload-file-placeholder') }}</span>
                                                    <input type="file" name="project-logo" id="image-uploader">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card z-depth-1">
                                        <div class="card-body">
                                            <h5 class="card-title">{{ __('auth.registration.project.public.title') }}</h5>
                                            <p class="card-text">
                                                {{ __('auth.registration.project.public.description') }}
                                            </p>
                                            <div class="md-form pb-2">
                                                <input value="{{ session('project-website') }}" type="text"
                                                       id="project-website" class="form-control" name="project-website" length="100">
                                                <label for="project-website">{{ __('auth.registration.project.public.website') }}</label>
                                            </div>
                                            <div class="md-form pb-2">
                                                <input value="{{ session('project-email') }}" type="email"
                                                       id="project-email" class="form-control" name="project-email" length="50">
                                                <label for="project-email">{{ __('auth.registration.project.public.email') }}</label>
                                            </div>
                                            <div class="md-form pb-2">
                                                <input value="{{ session('project-phone') }}" type="text"
                                                       id="project-phone" class="form-control" name="project-phone" length="20">
                                                <label for="project-phone">{{ __('auth.registration.project.public.phone') }}</label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-12 my-4 text-center">
                                        <button type="submit" class="btn btn-secondary btn-lg" name="action" value="previous">{{ __('auth.registration.previous-step') }}</button>
                                        <button type="submit" class="btn btn-primary btn-lg" name="action" value="next">{{ __('auth.registration.next-step') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header amber darken-1 z-depth-1" role="tab" id="headingTwo2">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo21"
                                   aria-expanded="false" aria-controls="collapseTwo21">
                                    <h5 class="mb-0 white-text">
                                        {{ __('auth.registration.project.share-personal-data') }} <i class="fas fa-angle-down rotate-icon"></i>
                                    </h5>
                                </a>
                            </div>

                            <div id="collapseTwo21" class="collapse" role="tabpanel" aria-labelledby="headingTwo21"
                                 data-parent="#accordionEx1">
                                <div class="card-body">
                                    Sharing personal data is currently not supported. Come back soon as we build this feature.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    <script type="text/javascript">
        jQuery( document ).ready( function () {
            function readURL( input ) {
                if ( input.files && input.files[ 0 ] ) {
                    var reader = new FileReader();

                    reader.onload = function ( e ) {
                        $( '#image-preview' ).attr( 'src', e.target.result );
                    }

                    reader.readAsDataURL( input.files[ 0 ] );
                }
            }

            $( "#image-uploader" ).change( function () {
                readURL( this );
            } );
            jQuery( '.datepicker' ).datepicker(
                {
                    // Strings and translations
                    monthsFull: ['{{ __('framework.datepicker.months.1') }}', '{{ __('framework.datepicker.months.2') }}', '{{ __('framework.datepicker.months.3') }}',
                        '{{ __('framework.datepicker.months.4') }}', '{{ __('framework.datepicker.months.5') }}', '{{ __('framework.datepicker.months.6') }}',
                        '{{ __('framework.datepicker.months.7') }}', '{{ __('framework.datepicker.months.8') }}', '{{ __('framework.datepicker.months.9') }}',
                        '{{ __('framework.datepicker.months.10') }}', '{{ __('framework.datepicker.months.11') }}', '{{ __('framework.datepicker.months.12') }}'],
                    monthsShort: ['{{ __('framework.datepicker.months-short.1') }}', '{{ __('framework.datepicker.months-short.2') }}', '{{ __('framework.datepicker.months-short.3') }}',
                        '{{ __('framework.datepicker.months-short.4') }}', '{{ __('framework.datepicker.months-short.5') }}', '{{ __('framework.datepicker.months-short.6') }}',
                        '{{ __('framework.datepicker.months-short.7') }}', '{{ __('framework.datepicker.months-short.8') }}', '{{ __('framework.datepicker.months-short.9') }}',
                        '{{ __('framework.datepicker.months-short.10') }}', '{{ __('framework.datepicker.months-short.11') }}', '{{ __('framework.datepicker.months-short.12') }}'],
                    weekdaysFull: ['{{ __('framework.datepicker.weekdays.7') }}', '{{ __('framework.datepicker.weekdays.1') }}', '{{ __('framework.datepicker.weekdays.2') }}',
                        '{{ __('framework.datepicker.weekdays.3') }}', '{{ __('framework.datepicker.weekdays.4') }}', '{{ __('framework.datepicker.weekdays.5') }}',
                        '{{ __('framework.datepicker.weekdays.6') }}'],
                    weekdaysShort: ['{{ __('framework.datepicker.weekdays-short.7') }}', '{{ __('framework.datepicker.weekdays-short.1') }}', '{{ __('framework.datepicker.weekdays-short.2') }}',
                        '{{ __('framework.datepicker.weekdays-short.3') }}', '{{ __('framework.datepicker.weekdays-short.4') }}', '{{ __('framework.datepicker.weekdays-short.5') }}',
                        '{{ __('framework.datepicker.weekdays-short.6') }}'],
                    showMonthsShort: undefined,
                    showWeekdaysFull: undefined,

                    // Buttons
                    today: '{{__('framework.datepicker.today')}}',
                    clear: '{{__('framework.datepicker.clear')}}',
                    close: '{{__('framework.datepicker.close')}}',

                    // Accessibility labels
                    labelMonthNext: '{{__('framework.datepicker.next-month')}}',
                    labelMonthPrev: '{{__('framework.datepicker.previous-month')}}',
                    labelMonthSelect: '{{__('framework.datepicker.select-month')}}',
                    labelYearSelect: '{{__('framework.datepicker.select-year')}}',

                    // Formats
                    format: '{{__('framework.js-date')}}',
                    formatSubmit: 'YYYY-MM-DD',
                    hiddenPrefix: undefined,
                    hiddenSuffix: '_submit',
                }
            );
        } );
    </script>
@endpush
