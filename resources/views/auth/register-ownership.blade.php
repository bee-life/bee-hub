@php /**
 * @var $old array
 */

@endphp

@extends('layouts.app')

@section('content')
    <div class="jumbotron bg-primary hexagon-background" style="background-image: url('/images/Element 3.png');">
        <div class="container">
            <div class="row">
                <div class="col-12 text-white text-left px-2">
                    <h1 class="card-title display-4 pt-3">
                        {{ __('auth.registration.ownership.title') }}
                        <img src="{{ img('Element 1.png') }}" class="w-icon">
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <form method="POST" action="{{ url('register/step-3') }}" enctype="multipart/form-data">
        <input type="hidden" id="section-count" value="1">
        @csrf
        <div class="container mb-5">
            <div class="row justify-content-center">
                <div class="col-12 mb-2">
                    @if($errors->isNotEmpty())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br />
                            @endforeach
                        </div>
                    @endif


                    {!! nl2br(__('auth.registration.ownership.text'))  !!}

                    <div class="alert alert-primary w-100 mt-2" role="alert">
                        {{ __('auth.registration.project.public.description') }}
                    </div>
                </div>

                <div class="col-12" id="section-container">

                    @for ($i = 1; $i <= $count; $i++)
                        <section {!! $i == 1 ? 'id="clone-section"' : null  !!} class="card z-depth-1 mb-2">
                            <div class="card-body">
                                <h5 class="card-title">{{ __('auth.registration.ownership.owner') }} <span class="section-number">{{ $i }}</span></h5>

                                <div class="md-form pb-2">
                                    <input value="{{ $i != $count ?  $old[$i]['full-name'] : null}}" type="text"
                                           id="full-name{{ $i != 1 ? '-' . $count : null }}" class="form-control" name="owner[{{ $i }}][full-name]" length="200">
                                    <label for="full-name{{ $i != 1 ? '-' . $count : null }}">{{ __('auth.registration.ownership.full-name') }}</label>
                                </div>

                                <div class="md-form pb-2">
                                    <input value="{{ $i != $count ?  $old[$i]['short-name'] : null}}" type="text"
                                           id="short-name{{ $i != 1 ? '-' . $count : null }}" class="form-control" name="owner[{{ $i }}][short-name]" length="50">
                                    <label for="short-name{{ $i != 1 ? '-' . $count : null }}">{{ __('auth.registration.ownership.short-name') }}</label>
                                </div>

                                <div class="md-form pb-2">
                                    <textarea id="address{{ $i != 1 ? '-' . $count : null }}" class="md-textarea form-control" rows="5"
                                              name="owner[{{ $i }}][address]">{{ $i != $count ?  $old[$i]['address'] : null}}</textarea>
                                    <label for="address{{ $i != 1 ? '-' . $count : null }}">{{ __('auth.registration.ownership.address') }}</label>
                                </div>

                                <div class="md-form pb-2">
                                    <input value="{{ $i != $count ?  $old[$i]['registry-number'] : null}}" type="text"
                                           id="registry-number{{ $i != 1 ? '-' . $count : null }}" class="form-control" name="owner[{{ $i }}][registry-number]" length="190">
                                    <label for="registry-number{{ $i != 1 ? '-' . $count : null }}">{{ __('auth.registration.ownership.registry-number') }}</label>
                                    <small id="registry-number-details{{ $i != 1 ? '-' . $count : null }}" class="form-text text-muted">{{ __('auth.registration.ownership.registry-number-details') }}</small>
                                </div>

                                <div class="pb-2">
                                    <input id="project-description-input-{{ $i != 1 ? '-' . $count : null }}" type="hidden" name="owner[{{ $i }}][description]" value="{{ $i != $count ?  $old[$i]['description'] : null}}">
                                    <label for="project-description">{{ __('auth.registration.project.project-description') }}</label>
                                    <trix-editor input="owner[{{ $i }}][description]" ></trix-editor>
                                </div>

                                <p>{{ __('auth.registration.ownership.logo') }}</p>


                                <div class="md-form" style="max-width: 200px;">
                                    <div class="file-field">
                                        <div class="z-depth-1-half mb-4">
                                            <img src="{{ $i != $count && isset($old[$i]['logo']) ?  Storage::url($old[$i]['logo']) : asset('images/Prototype-Platform.png') }}" class="img-fluid"
                                                 alt="example placeholder">
                                        </div>
                                        <div class="d-flex justify-content-center">
                                            <div class="btn btn-mdb-color btn-rounded float-left">
                                                <span>{{ __('auth.registration.data.data-provided.upload-file-placeholder') }}</span>
                                                <input type="file" name="owner[{{ $i }}][logo]">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="md-form pb-2">
                                    <input value="{{ $i != $count ?  $old[$i]['website'] : null}}" type="text"
                                           id="website{{ $i != 1 ? '-' . $count : null }}" class="form-control" name="owner[{{ $i }}][website]" length="100">
                                    <label for="website{{ $i != 1 ? '-' . $count : null }}">{{ __('auth.registration.ownership.public.website') }}</label>
                                </div>
                                <div class="md-form pb-2">
                                    <input value="{{ $i != $count ?  $old[$i]['email'] : null}}" type="email"
                                           id="email{{ $i != 1 ? '-' . $count : null }}" class="form-control" name="owner[{{ $i }}][email]" length="50">
                                    <label for="email{{ $i != 1 ? '-' . $count : null }}">{{ __('auth.registration.ownership.public.email') }}</label>
                                </div>
                                <div class="md-form pb-2">
                                    <input value="{{ $i != $count ?  $old[$i]['phone'] : null}}" type="text"
                                           id="phone{{ $i != 1 ? '-' . $count : null }}" class="form-control" name="owner[{{ $i }}][phone]" length="20">
                                    <label for="phone{{ $i != 1 ? '-' . $count : null }}">{{ __('auth.registration.ownership.public.phone') }}</label>
                                </div>

                                <div class="my-4 text-center">
                                    <button type="submit" class="btn btn-secondary btn-lg" name="action" value="previous">{{ __('auth.registration.previous-step') }}</button>
                                    <button type="button" class="btn btn-primary btn-lg add-ownership-fields">{{ __('auth.registration.ownership.add-owner') }}</button>
                                    <button type="submit" class="btn btn-primary btn-lg" name="action" value="next">{{ __('auth.registration.next-step') }}</button>
                                </div>
                            </div>
                        </section>
                    @endfor
                </div>
            </div>
        </div>
    </form>
@endsection


@push('scripts')
    <script type="text/javascript">
        jQuery( document ).ready( function () {
            var count = parseInt( jQuery( '#section-count' ).val() );
            var template = jQuery( '#clone-section' ).clone();

            jQuery( '#section-container' ).on( 'click', '.add-ownership-fields', function () {
                count++;

                var section = template.clone().find( ':input:not([type="submit"]):not([type="button"])' ).each( function () {
                    var newId = this.id + '-' + count;
                    jQuery( this ).next().attr( 'for', newId );
                    jQuery( this ).val( '' );
                    if ( typeof jQuery( this ).attr( 'name' ) != 'undefined' ) {
                        jQuery( this ).attr( 'name', jQuery( this ).attr( 'name' ).replace( '[' + ( count - 1 ) + ']', '[' + count + ']' ) );
                    }
                    this.id = newId;

                } ).end().appendTo( '#section-container' );

                section.find( '.section-number' ).html( count );
                return false;
            } );
        } );
    </script>
@endpush
