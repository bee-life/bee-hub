@php /**
 * @var $old array
 * @var $errors \Illuminate\Support\MessageBag
 */

@endphp
@extends('layouts.app')

@section('content')
    <div class="jumbotron bg-primary hexagon-background" style="background-image: url('/images/Element 3.png');">
        <div class="container">
            <div class="row">
                <div class="col-12 text-white text-left px-2">
                    <h1 class="card-title display-4 pt-3">
                        {{ __('auth.registration.data.title') }}
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <form method="POST" action="{{ url('register/step-2') }}" enctype="multipart/form-data">
        @csrf

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 mb-2">
                    @if($errors->isNotEmpty())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br />
                            @endforeach
                        </div>
                    @endif

                    <h2>{{ __('auth.registration.data.text') }}</h2>
                </div>


                <div class="col-12">
                    <div class="card z-depth-1 w-100 mb-3">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('auth.registration.data.smart-hives.title') }}</h5>

                            <div class="row">
                                <div class="col-md-6">
                                    @foreach($data['smart-hives'] as $key => $item)
                                        <div class="form-check">
                                            <input type="checkbox" name="smart-hives[]" value="{{ $item }}" class="form-check-input" id="smart-hives-{{ $key }}"
                                                {{ (is_array(session('smart-hives')) && in_array($item, session('smart-hives'))) ? 'checked' : null }}>
                                            <label class="form-check-label" for="smart-hives-{{ $key }}">{{ __('auth.registration.data.smart-hives.' . $item) }}</label>
                                        </div>
                                        @if($key == 5)
                                </div>
                                <div class="col-md-6">
                                    @endif

                                    @endforeach
                                </div>
                                <div class="col-12">
                                    <div class="md-form">
                                        <input value="{{ session('smart-hives-other') }}" placeholder="{{ __('auth.registration.data.smart-hives.specify-other') }}" type="text"
                                               id="smart-hives-other" class="form-control" name="smart-hives-other" length="200">
                                        <label for="smart-hives-other">{{ __('auth.registration.data.smart-hives.other') }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card z-depth-1 w-100 mb-3">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('auth.registration.data.stress-factors.title') }}</h5>

                            <div class="row">
                                <div class="col-md-6">

                                    @foreach($data['stress-factors'] as $key => $item)
                                        <div class="form-check">
                                            <input type="checkbox" name="stress-factors[]" value="{{ $item }}" class="form-check-input" id="stress-factors-{{ $key }}"
                                                {{ (is_array(session('stress-factors')) && in_array($item, session('stress-factors'))) ? 'checked' : null }}>
                                            <label class="form-check-label" for="stress-factors-{{ $key }}">{{ __('auth.registration.data.stress-factors.' . $item) }}</label>
                                        </div>
                                        @if($key == 3)
                                </div>
                                <div class="col-md-6">
                                    @endif

                                    @endforeach
                                </div>
                            </div>
                            <div class="md-form">
                                <input value="{{ session('stress-factors-other') }}" placeholder="{{ __('auth.registration.data.stress-factors.specify-other') }}" type="text"
                                       id="stress-factors-other" class="form-control" name="stress-factors-other" length="200">
                                <label for="stress-factors-other">{{ __('auth.registration.data.stress-factors.other') }}</label>
                            </div>
                        </div>
                    </div>


                    <div class="card z-depth-1 w-100 mb-3">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('auth.registration.data.beekeeping-data.title') }}</h5>

                            @foreach($data['beekeeping-data'] as $key => $item)
                                <div class="form-check">
                                    <input type="checkbox" name="beekeeping-data[]" value="{{ $item }}" class="form-check-input" id="beekeeping-data-{{ $key }}"
                                        {{ (is_array(session('beekeeping-data')) && in_array($item, session('beekeeping-data'))) ? 'checked' : null }}>
                                    <label class="form-check-label" for="beekeeping-data-{{ $key }}">{{ __('auth.registration.data.beekeeping-data.' . $item) }}</label>
                                </div>
                            @endforeach

                            <div class="md-form">
                                <input value="{{ session('beekeeping-data-other') }}" placeholder="{{ __('auth.registration.data.beekeeping-data.specify-other') }}" type="text"
                                       id="beekeeping-data-other" class="form-control" name="beekeeping-data-other" length="200">
                                <label for="beekeeping-data-other">{{ __('auth.registration.data.beekeeping-data.other') }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="card z-depth-1 w-100 mb-3">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('auth.registration.data.mortality-data.title') }}</h5>

                            @foreach($data['mortality-data'] as $key => $item)
                                <div class="form-check">
                                    <input type="checkbox" name="mortality-data[]" value="{{ $item }}" class="form-check-input" id="mortality-data-{{ $key }}"
                                        {{ (is_array(session('mortality-data')) && in_array($item, session('mortality-data'))) ? 'checked' : null }}>
                                    <label class="form-check-label" for="mortality-data-{{ $key }}">{{ __('auth.registration.data.mortality-data.' . $item) }}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <h2>{{ __('auth.registration.data.data-provided.title') }}</h2>

                    <div class="accordion md-accordion mb-5" id="accordionEx1" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header amber darken-1 z-depth-1" role="tab" id="headingTwo1">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo1"
                                   aria-expanded="false" aria-controls="collapseTwo1">
                                    <h5 class="mb-0 white-text">
                                        {{ __('auth.registration.data.data-provided.upload') }} <i class="fas fa-angle-down rotate-icon"></i>
                                    </h5>
                                </a>
                            </div>

                            <div id="collapseTwo1" class="collapse" role="tabpanel" aria-labelledby="headingTwo1"
                                 data-parent="#accordionEx1">
                                <div class="card-body">
                                    <p>{{ __('auth.registration.data.data-provided.upload-details') }}</p>

                                    <div class="file-field">
                                        <div class="btn btn-primary btn-sm float-left">
                                            <span>{{ __('auth.registration.data.data-provided.upload-file') }}</span>
                                            <input type="file" name="file-upload">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="{{ __('auth.registration.data.data-provided.upload-file-placeholder') }}">
                                        </div>
                                    </div>

                                    <div class="md-form">
                                        <textarea id="form7" class="md-textarea form-control" rows="5" name="file-upload-notes">{{ session('file-upload-notes') }}</textarea>
                                        <label for="form7">{{ __('auth.registration.data.data-provided.notes') }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header amber darken-1 z-depth-1" role="tab" id="headingTwo2">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo"
                                   aria-expanded="false" aria-controls="collapseTwo21">
                                    <h5 class="mb-0 white-text">
                                        {{ __('auth.registration.data.data-provided.remote') }} <i class="fas fa-angle-down rotate-icon"></i>
                                    </h5>
                                </a>
                            </div>

                            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo1"
                                 data-parent="#accordionEx1">
                                <div class="card-body">
                                    <p>{{ __('auth.registration.data.data-provided.remote-details') }}</p>

                                    <div class="md-form pb-2">
                                        <input value="{{ session('remote-link') }}" type="url"
                                               id="remote-link" class="form-control" name="remote-link" length="500" value="{{ session('remote-link') }}">
                                        <label for="remote-link">{{ __('auth.registration.data.data-provided.remote-url') }}</label>
                                    </div>

                                    <div class="md-form">
                                        <textarea id="form7" class="md-textarea form-control" rows="5" name="remote-notes">{{ session('remote-notes') }}</textarea>
                                        <label for="form7">{{ __('auth.registration.data.data-provided.notes') }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header amber darken-1 z-depth-1" role="tab" id="headingThree">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseThree"
                                   aria-expanded="false" aria-controls="collapseThree">
                                    <h5 class="mb-0 white-text">
                                        {{ __('auth.registration.data.data-provided.api') }} <i class="fas fa-angle-down rotate-icon"></i>
                                    </h5>
                                </a>
                            </div>

                            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree"
                                 data-parent="#accordionEx1">
                                <div class="card-body">
                                    <p>{{ __('auth.registration.data.data-provided.api-details') }}</p>

                                    <div class="file-field">
                                        <div class="btn btn-primary btn-sm float-left">
                                            <span>{{ __('auth.registration.data.data-provided.upload-file') }}</span>
                                            <input type="file" name="api-upload">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="{{ __('auth.registration.data.data-provided.upload-file-placeholder') }}">
                                        </div>
                                    </div>

                                    <div class="md-form">
                                        <textarea id="form7" class="md-textarea form-control" rows="5" name="api-notes">{{ session('api-notes') }}</textarea>
                                        <label for="form7">{{ __('auth.registration.data.data-provided.notes') }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header amber darken-1 z-depth-1" role="tab" id="headingFour">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseFour"
                                   aria-expanded="false" aria-controls="collapseFour">
                                    <h5 class="mb-0 white-text">
                                        {{ __('auth.registration.data.data-provided.other') }} <i class="fas fa-angle-down rotate-icon"></i>
                                    </h5>
                                </a>
                            </div>

                            <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour"
                                 data-parent="#accordionEx1">
                                <div class="card-body">
                                    <p>{{ __('auth.registration.data.data-provided.other-details') }}</p>

                                    <div class="md-form">
                                        <textarea id="form7" class="md-textarea form-control" rows="5" name="other-notes">{{ session('other-notes') }}</textarea>
                                        <label for="form7">{{ __('auth.registration.data.data-provided.notes') }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h2>{{ __('auth.registration.data.data-uses-title') }}</h2>

                    <div class="form-check">
                        <input type="radio" class="form-check-input" id="data-use-open" name="data-use" value="open"
                            {{ session('data-use') == 'open' ? 'checked' : null }}>
                        <label class="form-check-label" for="data-use-open">{{ __('auth.registration.data.data-uses-open') }}</label>
                    </div>

                    <div class="form-check">
                        <input type="radio" class="form-check-input" id="data-use-restricted" name="data-use" value="restricted"
                            {{ session('data-use') == 'restricted' ? 'checked' : null }}>
                        <label class="form-check-label" for="data-use-restricted">{{ __('auth.registration.data.data-uses-restricted') }}</label>
                    </div>

                    <div class="form-check">
                        <input type="radio" class="form-check-input" id="data-use-closed" name="data-use" value="closed"
                            {{ session('data-use') == 'closed' ? 'checked' : null }}>
                        <label class="form-check-label" for="data-use-closed">{{ __('auth.registration.data.data-uses-closed') }}</label>
                    </div>

                    <br>
                    <h2>{{ __('auth.registration.data.data-integration-title') }}</h2>

                    <div class="md-form pb-2">
                        <input value="{{ session('contact-name') }}" type="text"
                               id="contact-name" class="form-control" name="contact-name" length="100">
                        <label for="contact-name">{{ __('auth.registration.data.contact-person') }}*</label>
                    </div>
                    <div class="md-form pb-2">
                        <input value="{{ session('contact-email') }}" type="email" aria-describedby="contact-email-details"
                               id="contact-email" class="form-control" name="contact-email" length="100">
                        <label for="contact-email">{{ __('auth.registration.data.contact-email') }}*</label>
                        <small id="contact-email-details" class="form-text text-muted">{{ __('auth.registration.data.contact-email-details') }}</small>
                    </div>

                    <div class="col-12 my-4 text-center">
                        <button type="submit" class="btn btn-secondary btn-lg" name="action" value="previous">{{ __('auth.registration.previous-step') }}</button>
                        <button type="submit" class="btn btn-primary btn-lg" name="action" value="next">{{ __('auth.registration.next-step') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
