@extends('layouts.app')

@section('content')
    <div class="jumbotron bg-primary hexagon-background" style="background-image: url('/images/Element 3.png');">
        <div class="container">
            <div class="row">
                <div class="col-12 text-white text-left px-2">
                    <h1 class="card-title display-4 pt-3">
                        {{ __('auth.registration.review.title') }}
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <form method="POST" action="{{ url('register/step-4') }}">
        @csrf

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 mb-2">
                    @if($errors->isNotEmpty())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br />
                            @endforeach
                        </div>
                    @endif

                    {{ __('auth.registration.review.text') }}
                </div>

                <div class="col-12">
                    <div class="card w-100 mb-3">
                        <div class="card-header">
                            <h2 class="float-left mb-0">{{ __('auth.registration.review.about-project') }}</h2>

                            <a href="{{ route('register-step-1') }}" class="btn btn-primary btn-sm float-right m-0">{{ __('auth.registration.review.edit') }}</a>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <b>{{ __('auth.registration.project.project-name') }}:</b>
                                </div>
                                <div class="col-md-7">
                                    {{ session('project-name') }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>{{ __('auth.registration.project.project-acronym') }}:</b>
                                </div>
                                <div class="col-md-7">
                                    @if (empty(session('project-acronym')))
                                        <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                    @else
                                        {{ session('project-acronym') }}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <b>{{ __('auth.registration.project.project-description') }}:</b>
                                </div>
                                <div class="col-12 mb-2">
                                    @if (empty(session('project-description')))
                                        <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                    @else
                                        {!! session('project-description') !!}
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>{{ __('auth.registration.project.project-start-date') }}:</b>
                                </div>
                                <div class="col-md-7">
                                    @if(!empty(session('project-start-date')))
                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d', session('project-start-date'))->format(__('framework.date')) }}
                                    @else
                                        {{ __('auth.registration.review.no-date') }}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>{{ __('auth.registration.project.project-end-date') }}:</b>
                                </div>
                                <div class="col-md-7">
                                    @if(!empty(session('project-end-date')))
                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d', session('project-end-date'))->format(__('framework.date')) }}
                                    @else
                                        {{ __('auth.registration.review.no-date') }}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>{{ __('auth.registration.project.public.website') }}:</b>
                                </div>
                                <div class="col-md-7">
                                    @if (empty(session('project-website')))
                                        <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                    @else
                                        <a href="{{ session('project-website') }}" target="_blank">{{ session('project-website') }}</a>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>{{ __('auth.registration.project.public.email') }}:</b>
                                </div>
                                <div class="col-md-7">
                                    @if (empty(session('project-email')))
                                        <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                    @else
                                        {{ session('project-email') }}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>{{ __('auth.registration.project.public.phone') }}:</b>
                                </div>
                                <div class="col-md-7">
                                    @if (empty(session('project-phone')))
                                        <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                    @else
                                        {{ session('project-phone') }}
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <b>{{ __('auth.registration.ownership.logo') }}:</b>
                                </div>
                                <div class="col-12">
                                    @if(!empty(session('project-logo')))
                                        <img src="{{ Storage::url( session('project-logo')) }}" class="img-fluid" alt="Owner Logo" style="max-width: 200px">
                                    @else
                                        <img src="{{ asset('images/Prototype-Platform.png') }}" class="img-fluid" alt="Owner Logo" style="max-width: 200px">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card w-100 mb-3">
                        <div class="card-header">
                            <h2 class="float-left mb-0">{{ __('auth.registration.review.about-data') }}</h2>

                            <a href="{{ route('register-step-2') }}" class="btn btn-primary btn-sm float-right m-0">{{ __('auth.registration.review.edit') }}</a>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12"><h4>{{ __('auth.registration.review.data-types') }}</h4></div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <b>{{ __('auth.registration.data.smart-hives.title') }}:</b>
                                </div>
                                <div class="col-md-7">
                                    @if(!empty(session('smart-hives')))
                                        @foreach(session('smart-hives') as $item)
                                            <span class="comma">{{ __('auth.registration.data.smart-hives.' . $item) }}</span>
                                        @endforeach
                                    @endif
                                    @if(!empty (session('smart-hives-other')))
                                        <br>{{ session('smart-hives-other') }}
                                    @endif
                                    @if (empty(session('smart-hives')) && empty(session('smart-hives-other')))
                                        <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>{{ __('auth.registration.data.stress-factors.title') }}:</b>
                                </div>
                                <div class="col-md-7">
                                    @if(!empty(session('stress-factors')))
                                        @foreach(session('stress-factors') as $item)
                                            <span class="comma">{{ __('auth.registration.data.stress-factors.' . $item) }}</span>
                                        @endforeach
                                    @endif
                                    @if(!empty (session('stress-factors-other')))
                                        <br>{{ session('stress-factors-other') }}
                                    @endif
                                    @if (empty(session('smart-hives')) && empty(session('smart-hives-other')))
                                        <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>{{ __('auth.registration.data.beekeeping-data.title') }}:</b>
                                </div>
                                <div class="col-md-7">
                                    @if(!empty(session('beekeeping-data')))
                                        @foreach(session('beekeeping-data') as $item)
                                            <span class="comma">{{ __('auth.registration.data.beekeeping-data.' . $item) }}</span>
                                        @endforeach
                                    @endif
                                    @if(!empty (session('beekeeping-data-other')))
                                        <br>{{ session('beekeeping-data-other') }}
                                    @endif
                                    @if (empty(session('beekeeping-data')) && empty(session('beekeeping-data-other')))
                                        <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>{{ __('auth.registration.data.mortality-data.title') }}:</b>
                                </div>
                                <div class="col-md-7">
                                    @if(!empty(session('mortality-data')))
                                        @foreach(session('mortality-data') as $item)
                                            <span class="comma">{{ __('auth.registration.data.mortality-data.' . $item) }}</span>
                                        @endforeach
                                    @else
                                        <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-12"><h4>{{ __('auth.registration.review.provided-data') }}</h4></div>
                            </div>

                            @if(!empty(session('file-upload')))
                                <div class="row">
                                    <div class="col-md-3">
                                        <b>{{ __('auth.registration.review.provided-file-upload') }}:</b>
                                    </div>
                                    <div class="col-md-7">
                                        <a href="{{ Storage::url(session('file-upload')) }}" target="_blank"
                                           class="btn btn-sm btn-outline-primary">{{ __('auth.registration.review.provided-file-upload-download') }}</a>
                                    </div>
                                    @if(session('file-upload-notes'))
                                        <div class="col-12 mb-2">
                                            <b>{{ __('auth.registration.review.provided-data-description') }}:</b><br>
                                            {{ nl2br(session('file-upload-notes')) }}
                                        </div>
                                    @endif
                                </div>
                                <hr>
                            @endif
                            @if(!empty(session('remote-link')))
                                <div class="row">
                                    <div class="col-md-3">
                                        <b>{{ __('auth.registration.review.provided-remote-link') }}:</b>
                                    </div>
                                    <div class="col-md-7">
                                        <a href="{{session('remote-link')}}" target="_blank"
                                           class="btn btn-sm btn-outline-primary">{{ __('auth.registration.review.provided-remote-link-open') }}</a>
                                    </div>
                                    @if(session('remote-notes'))
                                        <div class="col-12 mb-2">
                                            <b>{{ __('auth.registration.review.provided-data-description') }}:</b><br>
                                            {{ nl2br(session('remote-notes')) }}
                                        </div>
                                    @endif
                                </div>
                                <hr>
                            @endif
                            @if(!empty(session('api-upload') || session('api-notes')))
                                <div class="row">
                                    @if(session('api-upload'))
                                        <div class="col-md-3">
                                            <b>{{ __('auth.registration.review.provided-api-upload') }}:</b>
                                        </div>
                                        <div class="col-md-7">
                                            <a href="{{ Storage::url(session('api-upload')) }}" target="_blank"
                                               class="btn btn-sm btn-outline-primary">{{ __('auth.registration.review.provided-file-upload-download') }}</a>
                                        </div>
                                    @endif
                                    @if(session('api-notes'))
                                        <div class="col-12 mb-2">
                                            <b>{{ __('auth.registration.review.provided-api-notes') }}:</b><br>
                                            {{ nl2br(session('api-notes')) }}
                                        </div>
                                    @endif
                                </div>
                                <hr>
                            @endif
                            @if(!empty(session('other-notes')))
                                <div class="row">
                                    <div class="col-12 mb-2">
                                        <b>{{ __('auth.registration.review.provided-other-notes') }}:</b><br>
                                        {{ nl2br(session('other-notes')) }}
                                    </div>
                                </div>
                                <hr>
                            @endif

                            <div class="row">
                                <div class="col-12"><h4>{{ __('auth.registration.review.data-other') }}</h4></div>
                            </div>


                            <div class="row">
                                <div class="col-md-3">
                                    <b>{{ __('auth.registration.data.data-uses-title') }}: </b>
                                </div>
                                <div class="col-md-7">
                                    {{ __('auth.registration.data.data-uses-' .  session('data-use')) }}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <b>{{ __('auth.registration.data.contact-person') }}: </b>
                                </div>
                                <div class="col-md-7">
                                    @if (empty(session('contact-name')))
                                        <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                    @else
                                        {{ session('contact-name') }}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>{{ __('auth.registration.data.contact-email') }}: </b>
                                </div>
                                <div class="col-md-7">
                                    @if (empty(session('contact-email')))
                                        <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                    @else
                                        {{ session('contact-email') }}
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                @foreach(session('owner') as $key => $owner)
                    <div class="col-lg-6">
                        <div class="card w-100 mb-3">
                            <div class="card-header">
                                <h2 class="float-left mb-0">{{ __('auth.registration.ownership.owner') }} {{ $key  }}</h2>

                                <a href="{{ route('register-step-3') }}" class="btn btn-primary btn-sm float-right m-0">{{ __('auth.registration.review.edit') }}</a>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <b>{{ __('auth.registration.ownership.full-name') }}:</b>
                                    </div>
                                    <div class="col-md-8">
                                        {{ $owner['full-name'] }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <b>{{ __('auth.registration.ownership.short-name') }}:</b>
                                    </div>
                                    <div class="col-md-8">
                                        @if (empty($owner['short-name']))
                                            <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                        @else
                                            {{ $owner['short-name']  }}
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <b>{{ __('auth.registration.ownership.address') }}:</b>
                                    </div>
                                    <div class="col-12 border-bottom pb-1 mb-1">
                                        @if (empty($owner['address']))
                                            <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                        @else
                                            {!! nl2br($owner['address']) !!}
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <b>{{ __('auth.registration.ownership.registry-number') }}:</b>
                                    </div>
                                    <div class="col-md-8">
                                        @if (empty($owner['registry-number']))
                                            <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                        @else
                                            {{ $owner['registry-number']  }}
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <b>{{ __('auth.registration.ownership.description') }}:</b>
                                    </div>
                                    <div class="col-12 border-bottom pb-1 mb-1">
                                        @if (empty($owner['description']))
                                            <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                        @else
                                            {!! $owner['description'] !!}
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <b>{{ __('auth.registration.project.public.website') }}:</b>
                                    </div>
                                    <div class="col-md-8">
                                        @if (empty($owner['website']))
                                            <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                        @else
                                            <a href="{{ $owner['website'] }}" target="_blank">{{ $owner['website'] }}</a>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <b>{{ __('auth.registration.project.public.email') }}:</b>
                                    </div>
                                    <div class="col-md-8">
                                        @if (empty($owner['email']))
                                            <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                        @else
                                            {{ $owner['email']  }}
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <b>{{ __('auth.registration.project.public.phone') }}:</b>
                                    </div>
                                    <div class="col-md-8">
                                        @if (empty($owner['phone']))
                                            <span class="text-muted">{{ __('auth.registration.review.no-input') }}</span>
                                        @else
                                            {{ $owner['phone']  }}
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <b>{{ __('auth.registration.ownership.logo') }}:</b>
                                    </div>
                                    <div class="col-12">
                                        @if(!empty($owner['logo']))
                                            <img src="{{ Storage::url( $owner['logo']) }}" class="img-fluid" alt="Owner Logo" style="max-width: 200px">
                                        @else
                                            <img src="{{ asset('images/Prototype-Platform.png') }}" class="img-fluid" alt="Owner Logo" style="max-width: 200px">
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card w-100 mb-3">
                        <div class="card-header">
                            <h2 class="mb-0">{{ __('auth.registration.review.consent-title') }}</h2>
                        </div>
                        <div class="card-body">

                            @if(Auth::check())
                               <p><b>{{ __('auth.registration.review.user-auth') }}</b></p>

                                <div class="md-form pb-2">
                                    <input value="{{ Auth::user()->name }}" type="text" disabled
                                           id="user-name" class="form-control" name="user-name" length="200">
                                    <label for="user-name">{{ __('auth.registration.review.user-name') }}</label>
                                </div>
                                <div class="md-form pb-2">
                                    <input value="{{ Auth::user()->email }}" type="email" disabled
                                           id="user-email" class="form-control" name="user-email" length="100">
                                    <label for="user-email">{{ __('auth.registration.review.user-email') }}</label>
                                </div>
                            @else
                                <p><b>{{ __('auth.registration.review.user-email-details') }}</b></p>

                            <div class="md-form pb-2">
                                <input value="{{ old('user-name') }}" type="text" required
                                       id="user-name" class="form-control" name="user-name" length="200">
                                <label for="user-name">{{ __('auth.registration.review.user-name') }}</label>
                            </div>
                            <div class="md-form pb-2">
                                <input value="{{ old('user-email') }}" type="email" required
                                       id="user-email" class="form-control" name="user-email" length="100">
                                <label for="user-email">{{ __('auth.registration.review.user-email') }}</label>
                            </div>
                            @endif

                            <p><b>{{ __('auth.registration.review.consent-rule') }}</b></p>

                            <div class="form-check">
                                <input type="checkbox" name="ownership-consent" class="form-check-input" id="ownership-consent" required>
                                <label class="form-check-label" for="ownership-consent">{{ __('auth.registration.review.ownership-consent') }}</label>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" name="public-consent" class="form-check-input" id="public-consent" required>
                                <label class="form-check-label" for="public-consent">{{ __('auth.registration.review.public-consent') }}</label>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" name="terms-consent" class="form-check-input" id="terms-consent" required>
                                <label class="form-check-label" for="terms-consent"><a href="https://bee-ppp.eu/terms-and-conditions">{{ __('auth.registration.review.terms-consent') }}</a></label>
                            </div>

                            <div class="col-12 my-4 text-center">
                                <button type="submit" class="btn btn-primary btn-lg" name="action" value="submit">{{ __('auth.registration.review.submit') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
