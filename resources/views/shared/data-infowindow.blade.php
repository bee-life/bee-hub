@php
    /**
     * @var \App\Models\MetaData\Descriptor  $descriptor
     * @var \App\Models\Logs\Log                    $log
     * @var \App\Models\Logs\Data\DataModel $data
     */
@endphp
<div class="card card-popup">
    <h4 class="card-header">{{ $descriptor->name }}</h4>
    <div class="list-group list-group-flush">
        <div class="list-group-item">
            <p class="card-text"><small>{{ $descriptor->description }}</small></p>
            <p class="card-text">Value: {{ $data }}</p>
            <p class="card-text">Measured on: {{ $log }}</p>
        </div>
        <div class="list-group-item small">
            <h5>Data sources:</h5>
            {{ $descriptor->getSources(['w-100']) }}
        </div>
    </div>
</div>
