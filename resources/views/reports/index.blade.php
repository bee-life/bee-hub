{{-- Base entry point for news pages. --}}
@extends('layouts.app')

@section('meta-title')
    {{ trans('reports.title') }}
@endsection

@section('meta-description')
    {{ trans('reports.description') }}
@endsection


@section('content')
    <div class="jumbotron bg-primary hexagon-background" style="background-image: url('images/Element 3.png');">
        <div class="container">
            <div class="row">
                <div class="col-12 text-white text-left px-2">
                    <h1 class="card-title display-4 pt-3">
                        {{ trans('reports.title') }}
                        <img src="{{ img('Element 4.png') }}" class="w-icon">
                    </h1>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row mb-4">
            @each('reports.includes-single', $models, 'model', 'reports.includes-empty')
        </div>
    </div>

@endsection
