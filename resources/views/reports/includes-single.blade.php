<?php
/**
 * @var \App\Models\CMS\Report $model
 */
?>
@if($model->hasFeaturedImage())
    <div class="col-md-12 col-lg-4 mb-1">
        <a href="{!! $model->getUrl() !!}" class="image-news image-hd h-100 d-block" style="background-image: url('{!! $news->getFeaturedImageUrl() !!}')"></a>
    </div>
@endif
<div class="@if($model->hasFeaturedImage()) col-md-12 col-lg-8 @else col-12  @endif post-item my-3">
    <h2 class="post-title">
        <a href="{{ $model->getUrl() }}">{{ $model->title }}</a>
    </h2>
    <p class="post-date">{{ trans('reports.published_at', [ 'date' => $model->getPublishedAt()]) }}@isset($model->author){{ __('reports.by',  [ 'author' => $model->author ]) }}@endisset
    </p>
    @isset($model->abstract)
        <p>{{ $model->abstract }}</p>
    @endisset

    <a href="{!! $model->getUrl() !!}" class="btn btn-outline-primary">{{ __('reports.discover') }}</a>
</div>
