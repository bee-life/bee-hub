<?php
/**
 * @var \App\Models\CMS\Report $model
 */
?>
{{-- Base entry point for news pages. --}}
@extends('layouts.app')


@section('meta-title')
    @if(empty($model->meta_title))
        {{ $model->title }}
    @else
        {{ $model->meta_title }}
    @endif
@endsection
@isset($model->meta_description)
@section('meta-description')
    {{ $model->meta_description }}
@endsection
@endisset
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ol class="breadcrumb my-3">
                    <li class="breadcrumb-item"><a href="{{ route('reports') }}">{{ trans('reports.title') }}</a></li>
                    <li class="breadcrumb-item active">{{ $model->title }}</li>
                </ol>
            </div>
            <div class="col-12">
                <div class="page-header">
                    @if($model->hasFeaturedImage())
                        <div class="d-block text-center">
                            <div class="image-news image-hd w-75 d-inline-block" style="background-image: url('{!! $news->getFeaturedImageUrl() !!}')"></div>
                        </div>
                    @endif

                    <h1 class="mt-4 text-center">{{ $model->title }}</h1>
                    <hr />
                    <p class="post-date">{{ trans('news.published', [ 'date' => $model->getPublishedAt()]) }}@isset($model->author){{ __('reports.by',  [ 'author' => $model->author ]) }}@endisset
                    </p>
                </div>
            </div>

            @isset($model->abstract)
                <div class="col-12 report-abstract">
                    <h2>{{ __('reports.abstract') }}</h2>
                    <p>{{ $model->abstract }}</p>
                </div>
            @endisset

            <div class="col-12 post-item post-single">
                <div class="post-content">
                    {!! $model->content !!}
                </div>
            </div>
        </div>
    </div>
@endsection
