<?php
/**
 * @var \App\Models\CMS\News $news
 */
?>
{{-- Base entry point for news pages. --}}
@extends('layouts.app')


@section('meta-title')
    @if(empty($news->meta_title))
        {{ $news->title }}
    @else
        {{ $news->meta_title }}
    @endif
@endsection

@section('meta-description')
    {{ $news->meta_description }}
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ol class="breadcrumb my-3">
                    <li class="breadcrumb-item"><a href="{{ url('/news') }}">{{ trans('news.title') }}</a></li>
                    <li class="breadcrumb-item active">{{ $news->title }}</li>
                </ol>
            </div>
            <div class="col-12">
                <div class="page-header">
                    @if($news->hasFeaturedImage())
                        <div class="d-block text-center">
                            <div class="image-news image-hd w-75 d-inline-block" style="background-image: url('{!! $news->getFeaturedImageUrl() !!}')"></div>
                        </div>
                    @endif

                    <h1 class="mt-4 text-center">{{ $news->title }}</h1>
                    <hr />
                    <p class="post-date">{{ trans('news.published', [ 'date' => $news->getPublishedAt()]) }}</p>
                </div>
            </div>

            <div class="col-12 post-item post-single">
                <div class="post-content">
                    {!! $news->content !!}
                </div>
            </div>
        </div>
    </div>
@endsection
