{{-- Base entry point for news pages. --}}
@extends('layouts.app')

@section('meta-title')
    {{ trans('news.title') }}
@endsection

@section('meta-description')
    {{ trans('news.description') }}
@endsection


@section('content')
    <div class="jumbotron bg-primary hexagon-background" style="background-image: url('images/Element 3.png');">
        <div class="container">
            <div class="row">
                <div class="col-12 text-white text-left px-2">
                    <h1 class="card-title display-4 pt-3">
                        {{ trans('news.title') }}
                        <img src="{{ img('Element 4.png') }}" class="w-icon">
                    </h1>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row mb-4">
            @each('news.includes-single', $news, 'news', 'news.includes-empty')
        </div>
    </div>

@endsection
