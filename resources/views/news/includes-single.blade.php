<?php
/**
 * @var \App\Models\CMS\News $news
 */
?>
@if($news->hasFeaturedImage())
    <div class="col-md-12 col-lg-4 mb-1">
        <a href="{!! $news->getUrl() !!}" class="image-news image-hd h-100 d-block" style="background-image: url('{!! $news->getFeaturedImageUrl() !!}')"></a>
    </div>
@endif
<div class="@if($news->hasFeaturedImage()) col-md-12 col-lg-8 @else col-12  @endif post-item my-3">
    <p class="post-date">{{ trans('news.published', [ 'date' => $news->getPublishedAt()]) }}</p>
    <h2 class="post-title">
        <a href="{{ $news->getUrl() }}">{{ $news->title }}</a>
    </h2>
    <p>{{ $news->excerpt }}</p>

    <a href="{!! $news->getUrl() !!}" class="btn btn-outline-primary">{{ __('news.read-more') }}</a>
</div>
