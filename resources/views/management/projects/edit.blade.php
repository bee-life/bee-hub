<?php
/**
 * @var \App\Models\MetaData\Project $model
 */
?>
@extends('layouts.management')

@section('management-content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 mb-4">
                <h2 class="card-title">{{ $model }}</h2>

                @if(session('action') && session('action') == 'account-updated')
                    <div class="alert alert-success">
                        {{ __('management.projects.updated') }}
                    </div>
                @elseif($errors->isNotEmpty())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <form action="{{ route('projects.update', $model) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="row">
                <div class="col-12 mb-4">
                    <div class="card z-depth-1 mb-2">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('management.projects.general-attributes') }}</h5>
                            <div class="md-form pb-2">
                                <input value="{{ $model->name }}" type="text"
                                       id="project-name" class="form-control" name="project-name" length="200">
                                <label for="project-name">{{ __('management.projects.name') }}</label>
                            </div>
                            <div class="md-form pb-2">
                                <input value="{{ $model->acronym }}" type="text"
                                       id="project-acronym" class="form-control" name="project-acronym" length="100">
                                <label for="project-acronym">{{ __('management.projects.acronym') }}</label>
                            </div>

                            <div class="pb-2">
                                <input id="project-description-input" type="hidden" name="project-description" value="{{ $model->description }}">
                                <label for="project-description">{{ __('management.projects.description') }}</label>
                                <trix-editor input="project-description-input"></trix-editor>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div id="date-picker-project-start-date" class="md-form md-outline input-with-post-icon datepicker">
                                        <input placeholder="{{ __('management.common.select-date') }}" type="text" id="project-start-date" class="form-control "
                                               name="project-start-date" value="{{ $model->started_at }}">
                                        <label for="project-start-date">{{ __('auth.registration.project.project-start-date') }}</label>
                                        <font-awesome-icon :icon="['far', 'calendar']" class="input-prefix" tabindex="0"></font-awesome-icon>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div id="date-picker-project-end-date" class="md-form md-outline input-with-post-icon datepicker">
                                        <input placeholder="{{ __('auth.registration.select-date') }}" type="text" id="project-end-date" class="form-control"
                                               name="project-end-date" value="{{ $model->ended_at }}">
                                        <label for="project-end-date">{{ __('auth.registration.project.project-end-date') }}</label>
                                        <font-awesome-icon :icon="['far', 'calendar']" class="input-prefix" tabindex="0"></font-awesome-icon>
                                    </div>
                                </div>
                            </div>

                            <p>{{ __('management.projects.featured-image') }}</p>

                            <div class="md-form" style="max-width: 200px;">
                                <div class="file-field">
                                    <div class="z-depth-1-half mb-4">
                                        <img src="{{ $model->hasFeaturedImage() ?  $model->getFeaturedImageUrl() : asset('images/Prototype-Platform.png') }}" class="img-fluid"
                                             id="image-preview" alt="example placeholder">
                                    </div>
                                    <div class="d-flex justify-content-center">
                                        <div class="btn btn-mdb-color btn-rounded float-left">
                                            <span>{{ __('management.projects.replace-image') }}</span>
                                            <input type="file" name="project-logo" id="image-uploader">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card z-depth-1">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('management.common.contact-attributes') }}</h5>
                            <div class="md-form pb-2">
                                <input value="{{ $model->website }}" type="text"
                                       id="project-website" class="form-control" name="project-website" length="100">
                                <label for="project-website">{{ __('management.common.website') }}</label>
                            </div>
                            <div class="md-form pb-2">
                                <input value="{{ $model->email }}" type="email"
                                       id="project-email" class="form-control" name="project-email" length="50">
                                <label for="project-email">{{ __('management.common.email') }}</label>
                            </div>
                            <div class="md-form pb-2">
                                <input value="{{ $model->phone }}" type="text"
                                       id="project-phone" class="form-control" name="project-phone" length="20">
                                <label for="project-phone">{{ __('management.common.phone') }}</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 my-4 text-center">
                    <a href="{{ route('projects.index') }}" class="btn btn-secondary btn-lg">{{ __('management.back') }}</a>
                    <button type="submit" class="btn btn-primary btn-lg" name="action" value="update">{{ __('management.update') }}</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        jQuery( document ).ready( function () {
            function readURL( input ) {
                if ( input.files && input.files[ 0 ] ) {
                    var reader = new FileReader();

                    reader.onload = function ( e ) {
                        $( '#image-preview' ).attr( 'src', e.target.result );
                    }

                    reader.readAsDataURL( input.files[ 0 ] );
                }
            }

            $( "#image-uploader" ).change( function () {
                readURL( this );
            } );

            jQuery( '.datepicker' ).datepicker(
                {
                    // Strings and translations
                    monthsFull: ['{{ __('framework.datepicker.months.1') }}', '{{ __('framework.datepicker.months.2') }}', '{{ __('framework.datepicker.months.3') }}',
                        '{{ __('framework.datepicker.months.4') }}', '{{ __('framework.datepicker.months.5') }}', '{{ __('framework.datepicker.months.6') }}',
                        '{{ __('framework.datepicker.months.7') }}', '{{ __('framework.datepicker.months.8') }}', '{{ __('framework.datepicker.months.9') }}',
                        '{{ __('framework.datepicker.months.10') }}', '{{ __('framework.datepicker.months.11') }}', '{{ __('framework.datepicker.months.12') }}'],
                    monthsShort: ['{{ __('framework.datepicker.months-short.1') }}', '{{ __('framework.datepicker.months-short.2') }}', '{{ __('framework.datepicker.months-short.3') }}',
                        '{{ __('framework.datepicker.months-short.4') }}', '{{ __('framework.datepicker.months-short.5') }}', '{{ __('framework.datepicker.months-short.6') }}',
                        '{{ __('framework.datepicker.months-short.7') }}', '{{ __('framework.datepicker.months-short.8') }}', '{{ __('framework.datepicker.months-short.9') }}',
                        '{{ __('framework.datepicker.months-short.10') }}', '{{ __('framework.datepicker.months-short.11') }}', '{{ __('framework.datepicker.months-short.12') }}'],
                    weekdaysFull: ['{{ __('framework.datepicker.weekdays.7') }}', '{{ __('framework.datepicker.weekdays.1') }}', '{{ __('framework.datepicker.weekdays.2') }}',
                        '{{ __('framework.datepicker.weekdays.3') }}', '{{ __('framework.datepicker.weekdays.4') }}', '{{ __('framework.datepicker.weekdays.5') }}',
                        '{{ __('framework.datepicker.weekdays.6') }}'],
                    weekdaysShort: ['{{ __('framework.datepicker.weekdays-short.7') }}', '{{ __('framework.datepicker.weekdays-short.1') }}', '{{ __('framework.datepicker.weekdays-short.2') }}',
                        '{{ __('framework.datepicker.weekdays-short.3') }}', '{{ __('framework.datepicker.weekdays-short.4') }}', '{{ __('framework.datepicker.weekdays-short.5') }}',
                        '{{ __('framework.datepicker.weekdays-short.6') }}'],
                    showMonthsShort: undefined,
                    showWeekdaysFull: undefined,

                    // Buttons
                    today: '{{__('framework.datepicker.today')}}',
                    clear: '{{__('framework.datepicker.clear')}}',
                    close: '{{__('framework.datepicker.close')}}',

                    // Accessibility labels
                    labelMonthNext: '{{__('framework.datepicker.next-month')}}',
                    labelMonthPrev: '{{__('framework.datepicker.previous-month')}}',
                    labelMonthSelect: '{{__('framework.datepicker.select-month')}}',
                    labelYearSelect: '{{__('framework.datepicker.select-year')}}',

                    // Formats
                    format: '{{__('framework.js-date')}}',
                    formatSubmit: 'YYYY-MM-DD',
                    hiddenPrefix: undefined,
                    hiddenSuffix: '_submit',
                }
            );
        } );
    </script>
@endpush
