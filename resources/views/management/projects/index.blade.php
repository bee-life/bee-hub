<?php
/**
 * @var \App\Models\MetaData\Project[]|\Illuminate\Database\Eloquent\Collection[] $items
 */
?>
@extends('layouts.management')

@section('management-content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 mb-4">
                <h2 class="card-title">{{ __('management.projects.title') }}</h2>
            </div>
        </div>

        <div class="row">
            @foreach($items as $model)
                <div class="col-md-6 col-xl-4">
                    <div class="card mb-4">
                        <div class="view overlay">
                            {!! $model->getFeaturedImage(['card-img-top']) !!}
                            <a href="{{ $model->getEditUrl() }}">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>
                        <div class="card-body">
                            <h4 class="card-title mb-0">{!! $model->getEditLink() !!}</h4>
                            <p><small>{{ $model }}</small></p>
                            <a class="btn btn-outline-primary" href="{{ $model->getEditUrl() }}">{{ trans('management.edit') }}</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
