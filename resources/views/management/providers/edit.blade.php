<?php
/**
 * @var \App\Models\MetaData\Provider $model
 */
?>
@extends('layouts.management')

@section('management-content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 mb-4">
                <h2 class="card-title">{{ $model }}</h2>

                @if(session('action') && session('action') == 'account-updated')
                    <div class="alert alert-success">
                        {{ __('management.projects.updated') }}
                    </div>
                @elseif($errors->isNotEmpty())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <form action="{{ route('providers.update', $model) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="row">
                <div class="col-12 mb-4">
                    <div class="card z-depth-1 mb-2">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('management.projects.general-attributes') }}</h5>
                            <div class="md-form pb-2">
                                <input value="{{ $model->full_name }}" type="text"
                                       id="full-name" class="form-control" name="full-name" length="200">
                                <label for="full-name">{{ __('management.providers.long-name') }}</label>
                            </div>
                            <div class="md-form pb-2">
                                <input value="{{ $model->name }}" type="text"
                                       id="name" class="form-control" name="name" length="100">
                                <label for="name">{{ __('management.providers.short-name') }}</label>
                            </div>


                            <div class="md-form pb-2">
                                    <textarea id="address" class="md-textarea form-control" rows="5"
                                              name="address">{{ $model->address }}</textarea>
                                <label for="address">{{ __('management.providers.address') }}</label>
                            </div>

                            <!-- TODO: Include Country/post selection -->

                            <div class="md-form pb-2">
                                <input value="{{ $model->registry_number }}" type="text" aria-describedby="registry-number-details"
                                       id="registry-number" class="form-control" name="registry-number" length="190">
                                <label for="registry-number">{{ __('management.providers.registry-number') }}</label>
                                <small id="registry-number-details" class="form-text text-muted">{{ __('management.providers.registry-number-details') }}</small>
                            </div>

                            <div class="pb-2">
                                <input id="project-description-input" type="hidden" name="description" value="{{ $model->description }}">
                                <label for="project-description">{{ __('management.providers.description') }}</label>
                                <trix-editor input="project-description-input"></trix-editor>
                            </div>

                            <p>{{ __('management.providers.featured-image') }}</p>

                            <div class="md-form" style="max-width: 200px;">
                                <div class="file-field">
                                    <div class="z-depth-1-half mb-4">
                                        <img src="{{ $model->hasFeaturedImage() ?  $model->getFeaturedImageUrl() : asset('images/Prototype-Platform.png') }}" class="img-fluid"
                                             id="image-preview" alt="example placeholder">
                                    </div>
                                    <div class="d-flex justify-content-center">
                                        <div class="btn btn-mdb-color btn-rounded float-left">
                                            <span>{{ __('management.providers.replace-image') }}</span>
                                            <input type="file" name="logo" id="image-uploader">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card z-depth-1">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('management.common.contact-attributes') }}</h5>
                            <div class="md-form pb-2">
                                <input value="{{ $model->website }}" type="text"
                                       id="project-website" class="form-control" name="website" length="100">
                                <label for="project-website">{{ __('management.common.website') }}</label>
                            </div>
                            <div class="md-form pb-2">
                                <input value="{{ $model->email }}" type="email"
                                       id="project-email" class="form-control" name="email" length="50">
                                <label for="project-email">{{ __('management.common.email') }}</label>
                            </div>
                            <div class="md-form pb-2">
                                <input value="{{ $model->phone }}" type="text"
                                       id="project-phone" class="form-control" name="phone" length="20">
                                <label for="project-phone">{{ __('management.common.phone') }}</label>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="col-12 my-4 text-center">
                    <a href="{{ route('providers.index') }}" class="btn btn-secondary btn-lg">{{ __('management.back') }}</a>
                    <button type="submit" class="btn btn-primary btn-lg" name="action" value="update">{{ __('management.update') }}</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        jQuery( document ).ready( function () {
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#image-preview').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#image-uploader").change(function() {
                readURL(this);
            });
        } );
    </script>
@endpush
