@extends('layouts.management')

@section('management-content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 mb-4">
                <div class="card z-depth-1">
                    <div class="card-body">
                        <form action="{{ route('user-account-settings') }}" method="post">
                            @csrf
                            <h5 class="card-title">{{ __('management.profile.title') }}</h5>
                            @if(session('action') && session('action') == 'account-updated')
                                <div class="alert alert-success">
                                    {{ __('management.profile.account-updated') }}
                                </div>
                            @elseif(old('action') && old('account') == 'password' && $errors->isNotEmpty())
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br />
                                    @endforeach
                                </div>
                            @endif
                            <div class="md-form pb-2">
                                <input value="{{ $user->name }}" type="text"
                                       id="user-name" class="form-control" name="user-name" length="200">
                                <label for="user-name">{{ __('management.profile.name') }}</label>
                            </div>
                            <div class="md-form pb-2">
                                <input value="{{ $user->email }}" type="email" aria-describedby="user-email-details"
                                       id="user-email" class="form-control" name="user-email" length="100">
                                <label for="user-email">{{ __('management.profile.email') }}</label>
                                <small id="user-email-details" class="form-text text-muted">{{ __('management.profile.email-note') }}</small>
                            </div>
                            <div class="col-12 my-4 text-center">
                                <button type="submit" class="btn btn-primary btn-lg" name="action" value="account">{{ __('management.profile.account-button') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 mb-4">
                <div class="card z-depth-1">
                    <div class="card-body">
                        <form action="{{ route('user-account-password') }}" method="post">
                            @csrf
                            <h5 class="card-title">{{ __('management.profile.password-title') }}</h5>
                            @if(session('action') && session('action') == 'password-updated')
                                <div class="alert alert-success">
                                        {{ __('management.profile.password-updated') }}
                                </div>
                            @elseif(old('action') && old('action') == 'password' && $errors->isNotEmpty())
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br />
                                    @endforeach
                                </div>
                            @endif

                            <div class="md-form pb-2">
                                <input type="password" id="user-password" class="form-control" name="password">
                                <label for="user-password">{{ __('management.profile.password') }}</label>
                            </div>
                            <div class="md-form pb-2">
                                <input type="password" aria-describedby="user-email-details" id="user-password-repeat" class="form-control" name="password_confirmed">
                                <label for="user-password-repeat">{{ __('management.profile.password-repeat') }}</label>
                                <small id="user-email-details" class="form-text text-muted">{{ __('management.profile.password-note') }}</small>
                            </div>
                            <div class="col-12 my-4 text-center">
                                <button type="submit" class="btn btn-primary btn-lg" name="action" value="password">{{ __('management.profile.password-button') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
