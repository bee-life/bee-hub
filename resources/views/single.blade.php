<?php /**
 * @var \App\Models\CMS\Page $page
 */ ?>
{{-- Base entry point for news pages. --}}
@extends('layouts.app')

@section('meta-title')
    @if(empty($page->meta_title))
        {{ $page->title }}
    @else
        {{ $page->meta_title }}
    @endif
@endsection

@section('meta-description')
    {{ $page->meta_description }}
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-header">
                    <h1 class="m-4 text-center">{{ $page->title }}</h1>
                </div>
            </div>

            <div class="col-12 post-item post-single">
                <div class="post-content">
                    {!! $page->content !!}
                </div>
            </div>
        </div>
    </div>
@endsection
