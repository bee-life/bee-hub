@component('mail::message')
# {{ __('auth.emails.welcome.title') }}

{{ __('auth.emails.welcome.text') }}

@component('mail::button', ['url' => url('/password/reset', ['token' => $token])])
{{ __('auth.emails.welcome.cta') }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
