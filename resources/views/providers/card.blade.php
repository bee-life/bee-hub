<?php /**
 * @var \App\Models\MetaData\Provider $model
 */ ?>
<div class="card mb-4">
    <div class="view overlay">
        {!! $model->getFeaturedImage(['card-img-top']) !!}
        <a href="{{ $model->getUrl() }}">
            <div class="mask rgba-white-slight"></div>
        </a>
    </div>
    <div class="card-body">
        <h4 class="card-title mb-0">{!! $model->getLink() !!}</h4>
        <p><small>{{ $model->name }}</small></p>
        <a class="btn btn-outline-primary" href="{{ $model->getUrl() }}">{{ trans('connector-owners.card.read-more') }}</a>
    </div>
</div>
