<?php /**
 * @var \App\Models\MetaData\Provider[] $providers
 */ ?>
@extends('layouts.app')

@section('meta-title')
    {{ trans('connector-owners.meta.index.title') }}
@endsection

@section('meta-description')
    {{ trans('connector-owners.meta.index.description') }}
@endsection

@section('content')
    <div class="jumbotron bg-primary hexagon-background" style="background-image: url('images/Element 3.png');">
        <div class="container">
            <div class="row">
                <div class="col-12 text-white text-left px-2">
                    <h1 class="card-title display-4 pt-3">
                        {{ trans('connector-owners.meta.index.title') }}
                        <img src="{{ img('Element 1.png') }}" class="w-icon">
                    </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12 mb-4">
                @include('components.promotion')
            </div>
        </div>
        <div class="row">
            @include('providers.card-list', ['models' => $providers])
        </div>

    </div>
@endsection
