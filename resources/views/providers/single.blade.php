<?php /**
 * @var \App\Models\MetaData\Provider $model
 */ ?>
{{-- Base entry point for news pages. --}}
@extends('layouts.app')

@section('meta-title')
    {{ $model->full_name }}
@endsection

@section('meta-description')
    {{ $model->about }}
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mt-3">
                        <li class="breadcrumb-item"><a href="{{ route('frontpage') }}">{{ trans('static.homepage.title-short') }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('providers') }}">{{ trans('connector-owners.meta.index.title') }}</a></li>
                        <li class="breadcrumb-item active">{{ $model->full_name }}</li>
                    </ol>
                </nav>
                <h1 class="m-4 text-center">{{ $model->full_name }}</h1>
            </div>

            <div class="col-sm-3">
                {!! $model->getFeaturedImage(['w-100', 'z-depth-1']) !!}
            </div>
            <div class="col-sm-9">
                <h3>{{ trans('connector-owners.attributes.about-provider') }}</h3>
                <p>{!! $model->description !!}</p>

                <ul class="list-unstyled">
                    @if(! empty($model->address))
                        <li>{!! nl2br($model->address) !!}</li>@endif
                    @if(! empty($model->website))
                        <li>{!! $model->getWebsiteLink() !!}</li>@endif
                </ul>
            </div>
            @if(! empty($model->phone) || ! empty($model->email))
                <div class="col-md-6 my-4">
                    <h3>{{ trans('connectors.attributes.contact-details') }}</h3>
                    <ul class="list-unstyled">
                        @if(! empty($model->email))
                            <li>{!!  $model->getEmail() !!}</li>@endif
                        @if(! empty($model->phone))
                            <li><a href="tel:{{ preg_replace('/\s/', '', $model->phone) }}">{{ $model->phone }}</a></li>@endif
                    </ul>
                </div>
            @endif
        </div>
        <div class="row my-4">
            <div class="col-12">
                <h3 class="mb-4">{{ trans('connector-owners.attributes.about-projects') }}</h3>

                <div class="row">
                    <?php foreach ( $model->projects as  $project) : ?>
                    <div class="col-sm-6 col-md-4 mb-3">
                        @include('projects.card', ['model' => $project])
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
@endsection
