<?php /**
 * @var \App\Models\MetaData\DescriptorCategory[] $models
 */ ?>
@extends('layouts.app')

@section('meta-title')
    {{ __('descriptor-categories.meta.index.title') }}
@endsection

@section('meta-description')
    {{ __('descriptor-categories.meta.index.description') }}
@endsection

@section('content')
    <div class="jumbotron bg-primary hexagon-background" style="background-image: url('images/Element 3.png');">
        <div class="container">
            <div class="row">
                <div class="col-12 text-white text-left px-2">
                    <h1 class="card-title display-4 pt-3">
                        {{ __('descriptor-categories.meta.index.title') }}
                        <img src="{{ img('Element 5.png') }}" class="w-icon">
                    </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="{{ route('frontpage') }}">{{ trans('static.homepage.title-short') }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('descriptors') }}">{{ trans('descriptors.meta.index.title') }}</a></li>
                        <li class="breadcrumb-item active">{{ trans('descriptor-categories.meta.index.title') }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
                @include('descriptor-categories.card-list')
        </div>
    </div>
@endsection
