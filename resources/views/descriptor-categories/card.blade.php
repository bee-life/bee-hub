<?php /**
 * @var \App\Models\MetaData\DescriptorCategory $model
 */ ?>
<div class="card mb-4">
    <div class="card-body">
        <div class="d-flex flex-row">
            @if($model->hasIcon())
                <img src="{{ $model->getIconUrl() }}" class="square mr-3" height="50px" width="50px">
            @else
                <img src="{{ $model->getFeaturedImageUrl() }}" class="square mr-3" height="50px" width="50px">
            @endif
            <div>
                <h4 class="card-title mb-0">{!! $model->getLink() !!}</h4>
            </div>
        </div>
        <div class="">
            <a class="btn btn-outline-primary" href="{{ $model->getUrl() }}">{{ trans('descriptor-categories.card.read-more') }}</a>
        </div>
    </div>
</div>
