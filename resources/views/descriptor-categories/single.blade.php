<?php /**
 * @var \App\Models\MetaData\DescriptorCategory $model
 */ ?>
@extends('layouts.app')

@section('meta-title')
    {{ __('descriptors.meta.index.title') }}
@endsection

@section('meta-description')
    {{ __('descriptors.meta.index.description') }}
@endsection

@section('content')
    <div class="jumbotron bg-primary hexagon-background" style="background-image: url('images/Element 3.png');">
        <div class="container">
            <div class="row">
                <div class="col-12 text-white text-left px-2">
                    <h1 class="card-title display-4 pt-3">
                        {{ $model->name }}
                        <img src="{{ img('Element 5.png') }}" class="w-icon">
                    </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="{{ route('frontpage') }}">{{ trans('static.homepage.title-short') }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('descriptors') }}">{{ trans('descriptors.meta.index.title') }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('descriptor-categories') }}">{{ trans('descriptor-categories.meta.index.title') }}</a></li>
                        <li class="breadcrumb-item active">{{ $model->name }}</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            @if (!empty($model->description))
                <div class="col-12">
                    {!! $model->description !!}
                </div>
            @endif
        </div>
        @if ($model->hasChildren())
            <div class="row">
                <div class="col-12"><h3 class="mb-4">{{ __('descriptor-categories.attributes.children') }}</h3></div>

                @include('descriptor-categories.card-list', ['models' => $model->children])
            </div>
        @endif

        <?php if($model->hasDescriptors()) : ?>
        <div class="row">
            <div class="col-12"><h3 class="mb-4">{{ __('descriptor-categories.attributes.descriptors') }}</h3></div>

            @include('descriptors.card-list', ['models' => $model->descriptors])
        </div>
        <?php endif; ?>


    </div>
@endsection
