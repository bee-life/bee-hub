<div class="row">
   <div class="col-sm-2 hexagon-horizontal-background d-none d-sm-block py-5" style="background-image: url('{!! img('Element 2-3.png') !!}');">

   </div>
    <div class="col-sm-10 py-5">
        <h2>{{ trans('connector-owners.meta.index.promotion') }}</h2>
        <a href="{!! config('app.survey_link') !!}" class="btn btn-info">{{ trans('connector-owners.meta.index.join-now') }}</a>
    </div>
</div>
