@php
    /**
     * @var \App\Models\MetaData\Project $project
     */
@endphp
<a class="d-inline-block" href="{{ $project->getUrl() }}" title="{{ $project }}">
    <figure class="figure">
        <img src="{{ $project->getFeaturedImageUrl() }}" class="figure-img img-fluid z-depth-1">

        <figcaption class="figure-caption text-center">{{ $project->name }}</figcaption>
    </figure>
</a>
