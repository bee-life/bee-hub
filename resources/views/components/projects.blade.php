@php
    /**
     * @var \App\Models\Logs\Data\DataAggregate  $aggregate
     * @var \App\Models\Logs\Log  $log
     * @var string  $title
     * @var string  $slot
     */
@endphp

@isset($aggregate)
    @foreach($aggregate->projects as $project)
        <div class="w-50">
            @include('components.project-image', [ 'project' => $project ])
        </div>
    @endforeach
@endisset
@isset($log)
    <div class="w-50">
        @include('components.project-image', [ 'project' => $log->project ])
    </div>
@endisset
