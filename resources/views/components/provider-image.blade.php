@php
    /**
     * @var \App\Models\MetaData\Provider  $provider
     */
@endphp
<a class="d-inline-block" href="{{ $provider->getUrl() }}" title="{{ $provider }}">
    <figure class="figure">
        <img src="{{ $provider->getFeaturedImageUrl() }}" class="figure-img img-fluid z-depth-1">
        <figcaption class="figure-caption text-center">{{ $provider->name }}</figcaption>
    </figure>
</a>
