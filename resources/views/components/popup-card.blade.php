@php
    /**
     * @var \App\Models\Logs\Data\DataAggregate  $aggregate
     * @var \App\Models\Logs\Log  $log
     * @var string  $title
     * @var string  $slot
     */
@endphp
<div class="card card-popup">
    <h5 class="card-header">{{ $title }}</h5>
    {{ $slot }}
    <div class="list-group list-group-flush">
        <div class="list-group-item small ">
            <p><b>{{ __('Participating projects') }}</b></p>
            <div class="popup-connectors">
                @isset($aggregate)
                    @include('components.projects', ['aggregate' => $aggregate])
                @endisset
                @isset($log)
                    @include('components.projects', ['log' => $log])
                @endisset
            </div>
        </div>
        <div class="list-group-item small">
            <p><b>{{ __('Data providers') }}</b></p>
            <div class="popup-owners">
                @isset($aggregate)
                @include('components.providers', ['providers' => $aggregate->projects->flatMap->providers->unique()])
                @endisset
                @isset($log)
                @include('components.providers', ['providers' => $log->project->providers])
                @endisset
                </divl>
            </div>
        </div>
    </div>
</div>
