@php
    /**
     * @var \App\Models\MetaData\Provider[]|\Illuminate\Database\Eloquent\Collection|array $providers
     */
@endphp

@foreach($providers as $provider)
    @include('components.provider-image', [ 'provider' => $provider ])
@endforeach
