<?php

namespace App\Policies;

use OptimistDigital\MenuBuilder\Models\Menu;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MenuPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any posts.
     *
     * @param User $user
     *
     * @return boolean
     */
    public function viewAny( User $user ): bool
    {
        return $user->canEdit();
    }


    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Menu $model
     *
     * @return boolean
     */
    public function view( User $user, Menu $model ): bool
    {
        return $user->canEdit();
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     *
     * @return boolean
     */
    public function create( User $user ): bool
    {
        return $user->canEdit();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Menu $model
     *
     * @return boolean
     */
    public function update( User $user, Menu $model ): bool
    {
        return $user->canEdit();
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Menu $model
     *
     * @return boolean
     */
    public function delete( User $user, Menu $model ): bool
    {
        return $user->canEdit();
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User $user
     * @param Menu $model
     *
     * @return boolean
     */
    public function restore( User $user, Menu $model ): bool
    {
        return $user->canEdit();
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User $user
     * @param Menu $model
     *
     * @return boolean
     */
    public function forceDelete( User $user, Menu $model ): bool
    {
        return $user->canEdit();
    }
}
