<?php

namespace App\Policies;

use App\Models\CMS\Page;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can view any posts.
     *
     * @param User $user
     *
     * @return boolean
     */
    public function viewAny( User $user ): bool
    {
        return $user->canEdit();
    }


    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Page $model
     *
     * @return boolean
     */
    public function view( User $user, Page $model ): bool
    {
        return $user->canEdit();
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     *
     * @return boolean
     */
    public function create( User $user ): bool
    {
        return $user->canEdit();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Page $model
     *
     * @return boolean
     */
    public function update( User $user, Page $model ): bool
    {
        return $user->canEdit();
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Page $model
     *
     * @return boolean
     */
    public function delete( User $user, Page $model ): bool
    {
        return $user->canEdit();
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User $user
     * @param Page $model
     *
     * @return boolean
     */
    public function restore( User $user, Page $model ): bool
    {
        return $user->canEdit();
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User $user
     * @param Page $model
     *
     * @return boolean
     */
    public function forceDelete( User $user, Page $model ): bool
    {
        return $user->canEdit();
    }
}
