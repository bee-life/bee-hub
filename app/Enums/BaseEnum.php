<?php namespace App\Enums;

use ReflectionClass;

abstract class BaseEnum
{
    /**
     * Return array of constants for a class
     *
     * @param boolean $assoc Return associative array with constant value as key
     *
     * @return array Assoc array of constants
     */
    public static function toArray( bool $assoc = false ): array
    {
        $reflector = new ReflectionClass( static::class );
        $constants = $reflector->getConstants();

        if ( $assoc ) {
            return $constants;
        }

        $values = [];
        foreach ( $constants as $constant => $value ) {
            $values[] = $value;
        }

        return $values;
    }

    /**
     * Check if element exist as enum.
     *
     * @param mixed $element
     *
     * @return bool
     */
    public static function isValid( mixed $element ): bool
    {
        $constants = static::toArray();
        return in_array( $element, $constants );
    }
}
