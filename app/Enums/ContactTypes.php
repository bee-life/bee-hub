<?php namespace App\Enums;

class ContactTypes extends BaseEnum
{
    /**
     * Type representing email.
     *
     * @var string
     */
    const EMAIL = 0;

    /**
     * Type representing url link.
     *
     * @var string
     */
    const URL = 1;

    /**
     * Type representing phone contact type.
     *
     * @var string
     */
    const PHONE = 2;

    /**
     * Type representing address.
     *
     * @var string
     */
    const ADDRESS = 3;
}
