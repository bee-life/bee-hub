<?php

namespace App\Jobs;

use App\Data\Charts\ColonyDevelopmentCharts;
use App\Data\Charts\HiveWeightChart;
use App\Data\Charts\SocioEconomicCharts;

class CacheDataJob extends ProcessObjects
{

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $objects = [
            //new ColonyDevelopmentCharts(),
            //new SocioEconomicCharts(),
            new HiveWeightChart(),
            // new ContaminationCharts(),
        ];

        $this->dispatchEach( $objects, CacheDataItemJob::class );
    }
}
