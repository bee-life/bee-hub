<?php

namespace App\Jobs;

use App\Data\Aggregators\HiveSensorAverages;
use App\Data\Aggregators\HiveWeightAlgorithms;
use App\Data\Aggregators\SocioEconomicAlgorithms;
use App\Data\Aggregators\SocioeconomicPollinators;
use App\Data\Aggregators\WeatherAverages;
use App\Data\Aggregators\WinterMortalityCounts;

class AggregateDataJob extends ProcessObjects
{

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $objects = [
            // Always run first any averages before other algorithms and calculations
            //new HiveSensorAverages(),
            //new WeatherAverages(),

            new HiveWeightAlgorithms(),
            //new HiveWeight(),

            // These should be run only once, when data is imported
            //new WinterMortalityCounts(),
            //new SocioEconomicAlgorithms(),
            //new SocioeconomicPollinators(),
        ];

        $this->dispatchEach( $objects, AggregateDataItemJob::class );

        if ( $this->queued ) {
            CacheDataJob::dispatch();
        } else {
            CacheDataJob::dispatchSync();
        }
    }
}
