<?php

namespace App\Jobs;

use App\Data\Charts\BaseChart;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CacheDataItemJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected BaseChart $object;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( BaseChart $object )
    {
        $this->object = $object;

        stop_logging();
        bcscale( 10 );
        ini_set( 'memory_limit', '2G' );
        set_time_limit(0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->object->run();
    }
}
