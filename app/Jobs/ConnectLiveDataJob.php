<?php

namespace App\Jobs;

use App\Data\Connectors\BaseConnector;
use App\Models\MetaData\Project;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class ConnectLiveDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $force;

    /**
     * The number of seconds the job can run before timing out.
     * 14400 = 4 hours.
     *
     * @var int
     */
    public $timeout = 14400;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $force = false )
    {
        $this->force = $force;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $connectors = Project::whereActive( true )->whereNotNull( 'sync_period' )->get();
        $now        = Carbon::now();
        $madeChanges = false;

        foreach ( $connectors as $connector ) {
            if ( empty( $connector->class_name ) || ! class_exists( $connector->class_name ) ) {
                continue;
            }

            if ( ! $this->force ) {
                switch ( $connector->sync_period ) {
                    case 'hourly':
                        if ( ! empty( $connector->last_synced_at ) && $connector->last_synced_at > $now->copy()->subHour() ) {
                            continue 2;
                        }
                        break;
                    case 'daily':
                        if ( ! empty( $connector->last_synced_at ) && $connector->last_synced_at > $now->copy()->subDay() ) {
                            continue 2;
                        }
                        break;
                    case 'weekly':
                        if ( ! empty( $connector->last_synced_at ) && $connector->last_synced_at > $now->copy()->subWeek() ) {
                            continue 2;
                        }
                        break;
                    case 'monthly':
                        if ( ! empty( $connector->last_synced_at ) && $connector->last_synced_at > $now->copy()->subMonth() ) {
                            continue 2;
                        }
                        break;
                    case 'quarterly':
                        if ( ! empty( $connector->last_synced_at ) && $connector->last_synced_at > $now->copy()->subMonths( 3 ) ) {
                            continue 2;
                        }
                        break;
                    case 'yearly':
                        if ( ! empty( $connector->last_synced_at ) && $connector->last_synced_at > $now->copy()->subYear() ) {
                            continue 2;
                        }
                        break;
                    default:
                        continue 2;
                }
            }

            try {
                /** @var BaseConnector $connection */
                $connection = new $connector->class_name( $connector );
                $connection->getRemoteData();

                $connector->last_synced_at = $now;
                $connector->save();
                $madeChanges = true;
            } catch ( \Exception $e ) {
                // Log the problem
                \Sentry::captureException($e);
                Log::error( $e->getMessage() );
                Log::error( $e->getFile() . ' on line ' . $e->getLine() );
                Log::error( $e->getTraceAsString() );
            }
        }
        if ( ! is_local() && $madeChanges ) {
            AggregateDataJob::dispatch();
        }
    }
}
