<?php

namespace App\Jobs;

use App\Data\Aggregators\BaseAggregator;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AggregateDataItemJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected BaseAggregator $aggregator;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 14400;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( BaseAggregator $aggregator )
    {
        $this->aggregator = $aggregator;

        stop_logging();
        bcscale( 10 );
        ini_set( 'memory_limit', '10G' );
        set_time_limit( 0 );
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->aggregator->run();
    }
}
