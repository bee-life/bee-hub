<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

abstract class ProcessObjects implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected bool $queued;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $queue = true )
    {
        $this->queued = $queue;
    }

    /**
     * Dispatches each object in a separate job.
     *
     * @param array  $objects
     * @param string $job
     *
     * @return void
     */
    protected function dispatchEach( array $objects, string $job )
    {
        foreach ( $objects as $object ) {
            if ( $this->queued ) {
                $job::dispatch( $object );
            } else {
                $job::dispatchSync( $object );
            }
        }
    }
}
