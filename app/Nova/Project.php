<?php

namespace App\Nova;

use App\Nova\Actions\DeleteProject;
use App\Nova\Actions\ProjectDisable;
use App\Nova\Actions\ProjectEnable;
use App\Nova\Actions\PublicDisable;
use App\Nova\Actions\PublicEnable;
use App\Nova\Filters\PublicFilter;
use App\Nova\Logs\Descriptor;
use App\Nova\Logs\Log;
use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Panel;

class Project extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\MetaData\Project::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'acronym';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name',
        'acronym'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request )
    {
        return [
            ID::make( __( 'administration.attributes.common.id' ), 'id' )->sortable(),
            Avatar::make( __( 'administration.attributes.common.featured_image' ), 'featured_image' )->nullable(),
            TextWithSlug::make( __( 'administration.attributes.common.name' ), 'name' )->sortable()->rules( 'required', 'max:191' )->slug( 'slug' ),
            Slug::make( __( 'administration.attributes.common.slug' ), 'slug' )->rules( 'required', 'max:191' )->hideFromIndex()->showUrlPreview( 'https://bee-hub.org/projects' ),
            Text::make( __( 'administration.attributes.projects.acronym' ), 'acronym' )->sortable()->rules( 'required', 'max:100' ),

            Trix::make( __( 'administration.attributes.common.description' ), 'description' )->hideFromIndex()->nullable()->stacked(),

            DateTime::make( __( 'administration.attributes.projects.started_at' ), 'started_at' )->nullable()->sortable(),
            DateTime::make( __( 'administration.attributes.projects.ended_at' ), 'ended_at' )->nullable()->sortable(),
            Boolean::make( __( 'administration.attributes.common.public' ), 'public' )->sortable(),

            new Panel( __( 'administration.attributes.group.public' ), [
                Text::make( __( 'administration.attributes.common.website' ), 'website' )->hideFromIndex()->rules( 'max:191' )->nullable(),
                Text::make( __( 'administration.attributes.common.email' ), 'email' )->hideFromIndex()->rules( 'max:191' )->nullable(),
                Text::make( __( 'administration.attributes.common.phone' ), 'phone' )->hideFromIndex()->rules( 'max:191' )->nullable(),
            ] ),

            new Panel( __( 'administration.attributes.group.internal' ), [
                Boolean::make( __( 'administration.attributes.projects.active' ), 'active' )->sortable(),
                Text::make( __( 'administration.attributes.projects.class_name' ), 'class_name' )->hideFromIndex()->readonly(),
                DateTime::make( __( 'administration.attributes.projects.last_synced_at' ), 'last_synced_at' )->readonly()->nullable()->sortable(),
                Text::make( __( 'administration.attributes.projects.sync_period' ), 'sync_period' )->hideFromIndex()->readonly(),
                DateTime::make( __( 'administration.attributes.common.created_at' ), 'created_at' )->hideFromIndex()->readonly()->sortable(),
                DateTime::make( __( 'administration.attributes.common.updated_at' ), 'updated_at' )->hideFromIndex()->readonly()->sortable(),
                Text::make( __( 'administration.attributes.common.uid' ), 'uid' )->rules( 'required', 'max:191' )->readonly()->hideFromIndex(),
                Text::make( __( 'administration.attributes.projects.type' ), 'type' )->sortable()->readonly(),
            ] ),

            Panel::make( __( 'administration.attributes.group.statistics' ), [
                Text::make( __( 'administration.attributes.projects.descriptor_count' ), function () {
                    /** @var \App\Models\MetaData\Project $this */
                    return format_number( $this->descriptorCount(), 0 );
                } ),
                Text::make( __( 'administration.attributes.projects.log_count' ), function () {
                    /** @var \App\Models\MetaData\Project $this */
                    return format_number( $this->logCount(), 0 );
                } ),
            ] ),

            BelongsToMany::make( __( 'administration.attributes.projects.providers' ), 'providers', Provider::class ),
            HasMany::make( __( 'administration.attributes.projects.descriptors' ), 'descriptors', Descriptor::class )->hideFromIndex(),
            HasMany::make( __( 'administration.attributes.projects.logs' ), 'logs', Log::class )->hideFromIndex(),
        ];
    }


    /**
     * Get the cards available for the request.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function cards( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function filters( NovaRequest $request )
    {
        return [
            ( new PublicFilter() ),
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function lenses( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function actions( NovaRequest $request )
    {
        $return = [
            ( new PublicEnable() ),
            ( new PublicDisable() ),
            ( new ProjectEnable )
                ->confirmText( 'Are you sure you want to activate these connections?' )
                ->confirmButtonText( 'Activate' )
                ->cancelButtonText( "Cancel" ),
            ( new ProjectDisable )
                ->confirmText( 'Are you sure you want to deactivate these connections?' )
                ->confirmButtonText( 'Activate' )
                ->cancelButtonText( "Cancel" ),
        ];
        if ( ! is_production() ) {
            $return [] = ( new DeleteProject );
        }

        return $return;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __( 'administration.attributes.projects.resource-name' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __( 'administration.attributes.projects.resource-singular-name' );
    }

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return __( 'administration.groups.data' );
    }
}
