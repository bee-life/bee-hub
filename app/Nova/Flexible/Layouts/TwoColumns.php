<?php

namespace App\Nova\Flexible\Layouts;

use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Whitecube\NovaFlexibleContent\Layouts\Layout;

class TwoColumns extends Layout
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'twocolumns';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'Two Columns';

    /**
     * Get the fields displayed by the layout.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Select::make('Type')->options([
                'left' => 'Image on the Left side',
                'right' => 'Image on the Right side',
            ])->required(),
            Image::make('Image'),
            Text::make( 'Title' ),
            Trix::make( 'Content' )->withFiles( 'public' )->stacked(),
        ];
    }

}
