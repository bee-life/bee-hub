<?php

namespace App\Nova\Flexible\Layouts;

use Laravel\Nova\Fields\Trix;
use Whitecube\NovaFlexibleContent\Layouts\Layout;

class SimpleContent extends Layout
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'simple-content';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'SimpleContent';

    /**
     * Get the fields displayed by the layout.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Trix::make( 'Content' )->withFiles( 'public' )->stacked(),
        ];
    }

}
