<?php namespace App\Nova\Flexible\Layouts;

use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\Trix;
use Whitecube\NovaFlexibleContent\Layouts\Layout;

class HeroImage extends Layout
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'hero-image';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'Fullscreen image with optional text';

    /**
     * Get the fields displayed by the layout.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Image::make( 'Image' ),
            Text::make( 'Title' ),
            Trix::make( 'Content' )->withFiles( 'public' )->stacked(),
        ];
    }
}
