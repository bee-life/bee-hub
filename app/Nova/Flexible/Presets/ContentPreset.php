<?php

namespace App\Nova\Flexible\Presets;

use App\Nova\Flexible\Layouts\HeroImage;
use App\Nova\Flexible\Layouts\SimpleContent;
use App\Nova\Flexible\Layouts\TwoColumns;
use Whitecube\NovaFlexibleContent\Flexible;
use Whitecube\NovaFlexibleContent\Layouts\Preset;

class ContentPreset extends Preset
{
    /**
     * Execute the preset configuration
     *
     * @return void
     */
    public function handle( Flexible $field )
    {
        // You can call all available methods on the Flexible field.
        // $field->addLayout(...)
        // $field->button(...)
        // $field->resolver(...)
        $field->addLayout( SimpleContent::class )->addLayout( HeroImage::class )->addLayout( TwoColumns::class )->button( __( 'administration.flexible.add-layout' ) );
    }

}
