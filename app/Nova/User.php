<?php

namespace App\Nova;

use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\Password;

class User extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\User::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name',
        'email',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request )
    {
        return [
            ID::make( __( 'administration.attributes.common.id' ), 'id' )->sortable(),

            Text::make( __( 'administration.attributes.common.name' ), 'name' )
                ->sortable()
                ->rules( 'required', 'max:255' ),

            Text::make( __( 'administration.attributes.users.email' ), 'email' )
                ->sortable()
                ->rules( 'required', 'email', 'max:254' )
                ->creationRules( 'unique:users,email' )
                ->updateRules( 'unique:users,email,{{resourceId}}' ),

            Password::make( __( 'administration.attributes.users.password' ), 'password' )
                    ->onlyOnForms()
                    ->creationRules( 'required', 'string', 'min:8' )
                    ->updateRules( 'nullable', 'string', 'min:8' ),

            Select::make( __( 'administration.attributes.users.type' ), 'type' )->options( [
                'administrator' => __( 'administration.user_types.administrator' ),
                'editor'        => __( 'administration.user_types.editor' ),
                'user'        => __( 'administration.user_types.user' ),
            ] )->displayUsing( function ( $type ) {
                switch ( $type ) {
                    case 'administrator':
                        return __( 'administration.user_types.administrator' );
                    case 'editor':
                        return __( 'administration.user_types.editor' );
                    case 'user':
                        return __( 'administration.user_types.user' );
                    default:
                        return __( 'administration.unknown' );
                }
            } )->sortable(),

            DateTime::make( __( 'administration.attributes.common.created_at' ), 'created_at' )->readonly()->sortable(),
            DateTime::make( __( 'administration.attributes.common.updated_at' ), 'updated_at' )->readonly()->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function cards( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function filters( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function lenses( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function actions( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __( 'Users' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __( 'User' );
    }

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return __( 'CMS' );
    }
}
