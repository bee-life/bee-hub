<?php

namespace App\Nova\Dashboards;

use App\Nova\Metrics\TotalDataCount;
use Bl\BeeHubHelp\BeeHubHelp;
use Illuminate\Support\Str;
use Laravel\Nova\Cards\Help;
use Laravel\Nova\Dashboards\Main as Dashboard;
use Vink\NovaCacheCard\CacheCard;

class Main extends Dashboard
{
    /**
     * Get the cards for the dashboard.
     *
     * @return array
     */
    public function cards()
    {
        return [
            //new BeeHubHelp(),
            //new TotalDataCount(),
            //( new CacheCard() )->width( 'full' ),
            new Help,
        ];
    }

    /**
     * Get the displayable name of the dashboard.
     *
     * @return string
     */
    public function name()
    {
        return 'Dashboard';
    }

    /**
     * Get the URI key of the dashboard.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'main';
    }

}
