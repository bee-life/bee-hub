<?php

namespace App\Nova\Origins;

use App\Nova\Logs\Data;
use App\Nova\Resource;
use Benjaminhirsch\NovaSlugField\Slug;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Panel;

class Publication extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Origins\Publication::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'title',
        'author',
        'author_short',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request )
    {
        return [
            ID::make( __( 'administration.attributes.common.id' ), 'id' )->sortable(),
            Text::make( __( 'administration.attributes.publications.title' ), 'title' )->sortable(),
            Text::make( __( 'administration.attributes.publications.year' ), 'year' )->sortable(),
            Text::make( __( 'administration.attributes.publications.author' ), 'author' )->sortable(),
            Text::make( __( 'administration.attributes.publications.author-short' ), 'author_short' )->sortable(),
            Text::make( __( 'administration.attributes.publications.author' ), 'author' )->sortable()->nullable(),
            Text::make( __( 'administration.attributes.publications.editor' ), 'editor' )->sortable()->nullable(),
            Text::make( __( 'administration.attributes.publications.editor-short' ), 'editor_short' )->sortable()->nullable(),
            Text::make( __( 'administration.attributes.publications.medium' ), 'medium' )->sortable()->nullable(),
            Text::make( __( 'administration.attributes.publications.medium-short' ), 'medium_short' )->sortable()->nullable(),
            Text::make( __( 'administration.attributes.publications.issue' ), 'issue' )->sortable()->nullable(),
            Text::make( __( 'administration.attributes.publications.volume' ), 'volume' )->sortable()->nullable(),
            Text::make( __( 'administration.attributes.publications.edition' ), 'edition' )->sortable()->nullable(),
            Text::make( __( 'administration.attributes.publications.pages' ), 'pages' )->sortable()->nullable(),

            Text::make( __( 'administration.attributes.common.website' ), 'website' )->nullable()->nullable(),
            DateTime::make( __( 'administration.attributes.publications.website-date' ), 'website_date' )->nullable(),
            /**
             * TODO: Add a list of publication types.
             */
            Text::make( __( 'administration.attributes.publications.publication-type' ), 'publication_type' )->nullable()->rules( 'required', 'max:20' )->sortable(),


            Panel::make( __( 'administration.attributes.group.internal' ), [
                DateTime::make( __( 'administration.attributes.common.created_at' ), 'created_at' )->sortable(),
                DateTime::make( __( 'administration.attributes.common.updated_at' ), 'updated_at' )->sortable(),
                Text::make( __( 'administration.attributes.common.uid' ), 'uid' )->rules( 'required', 'max:191' )->readonly()->hideFromIndex(),
            ] ),

            //MorphToMany::make( __( 'administration.attributes.origin.data' ), 'data', Data::class )->nullable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function cards( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function filters( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function lenses( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function actions( NovaRequest $request )
    {
        return [];
    }


    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public static function authorizedToCreate( Request $request )
    {
        return false;
    }

    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function authorizedToDelete( Request $request )
    {
        return false;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __( 'administration.attributes.publications.resource-name' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __( 'administration.attributes.publications.resource-singular-name' );
    }

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return __( 'administration.groups.origin' );
    }
}
