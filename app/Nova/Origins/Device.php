<?php

namespace App\Nova\Origins;

use App\Nova\Origins\DeviceCategory;
use App\Nova\Logs\Data;
use App\Nova\Resource;
use Benjaminhirsch\NovaSlugField\Slug;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Panel;

class Device extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Origins\Device::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name',
        'description',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request )
    {
        return [
            ID::make( __( 'administration.attributes.common.id' ), 'id' )->sortable(),
            Avatar::make( __( 'administration.attributes.common.icon' ), 'icon' )->nullable(),
            Image::make( __( 'administration.attributes.common.featured_image' ), 'featured_image' )->nullable(),
            Text::make( __( 'administration.attributes.common.name' ), 'name' )->sortable(),
            Slug::make( __( 'administration.attributes.common.slug' ), 'slug' )->rules( 'required', 'max:191' )->hideFromIndex()->showUrlPreview( 'https://bee-hub.org/devices' ),
            Trix::make( __( 'administration.attributes.common.description' ), 'description' )->hideFromIndex()->nullable()->stacked(),
            DateTime::make( __( 'administration.attributes.origin.introduced_at' ), 'introduced_at' )->nullable(),
            BelongsTo::make( __( 'administration.attributes.origin.category' ), 'category', DeviceCategory::class ),
            BelongsTo::make( __( 'administration.attributes.devices.vendor' ), 'vendor', Vendor::class ),

            Panel::make( __( 'administration.attributes.group.internal' ), [
                DateTime::make( __( 'administration.attributes.common.created_at' ), 'created_at' )->sortable(),
                DateTime::make( __( 'administration.attributes.common.updated_at' ), 'updated_at' )->sortable(),
                Text::make( __( 'administration.attributes.common.uid' ), 'uid' )->rules( 'required', 'max:191' )->readonly()->hideFromIndex(),
            ] ),

            MorphToMany::make( __( 'administration.attributes.origin.data' ), 'data', Data::class )->nullable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function cards( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function filters( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function lenses( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function actions( NovaRequest $request )
    {
        return [];
    }

    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public static function authorizedToCreate( Request $request )
    {
        return false;
    }

    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function authorizedToDelete( Request $request )
    {
        return false;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __( 'administration.attributes.devices.resource-name' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __( 'administration.attributes.devices.resource-singular-name' );
    }

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return __( 'administration.groups.origin' );
    }
}
