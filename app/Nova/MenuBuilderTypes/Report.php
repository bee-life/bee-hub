<?php

namespace App\Nova\MenuBuilderTypes;


use Outl1ne\MenuBuilder\MenuItemTypes\MenuItemSelectType;

class Report extends MenuItemSelectType
{
    /**
     * Get the menu link identifier that can be used to tell different custom
     * links apart (ie 'page' or 'product').
     *
     * @return string
     **/
    public static function getIdentifier(): string
    {
        return 'report';
    }


    /**
     * Get menu link name shown in  a dropdown in CMS when selecting link type
     * ie ('Product Link').
     *
     * @return string
     **/
    public static function getName(): string
    {
        return 'Report Link';
    }


    /**
     * Get list of options shown in a select dropdown.
     *
     * Should be a map of [key => value, ...], where key is a unique identifier
     * and value is the displayed string.
     *
     * @return array
     **/
    public static function getOptions( $locale ): array
    {
        return \App\Models\CMS\Report::all()->pluck( 'title', 'id' )->toArray();
    }


    /**
     * Get the subtitle value shown in CMS menu items list.
     *
     * @param mixed      $value
     * @param array|null $data The data from item fields.
     * @param string     $locale
     *
     * @return string
     */
    public static function getDisplayValue( $value, ?array $data, $locale )
    {
        // Example usecase
        // return 'Page: ' . Page::find($value)->name;
        return $value;
    }


    /**
     * Get the value of the link visible to the front-end.
     *
     * Can be anything. It is up to you how you will handle parsing it.
     *
     * This will only be called when using the nova_get_menu()
     * and nova_get_menus() helpers or when you call formatForAPI()
     * on the Menu model.
     *
     * @param mixed      $value The key from options list that was selected.
     * @param array|null $data  The data from item fields.
     * @param string     $locale
     *
     * @return mixed
     */
    public static function getValue( $value, ?array $data, $locale )
    {
        return \App\Models\CMS\Report::find( $value )->getUrl();
    }

    /**
     * Get the rules for the resource.
     *
     * @return array A key-value map of attributes and rules.
     */

    public static function getRules(): array
    {
        return [
            'value' => 'required',
        ];
    }
}
