<?php

namespace App\Nova\Logs;

use App\Nova\Actions\PublicDisable;
use App\Nova\Actions\PublicEnable;
use App\Nova\Filters\PublicFilter;
use App\Nova\Project;
use App\Nova\Resource;
use Benjaminhirsch\NovaSlugField\Slug;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Panel;

class Descriptor extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static string $model = \App\Models\MetaData\Descriptor::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'slug',
        'uid',
        'name',
        'description',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request ): array
    {
        return [
            ID::make( __( 'administration.attributes.common.id' ) )->sortable(),
            Avatar::make( __( 'administration.attributes.common.icon' ), 'icon' )->nullable(),
            Image::make( __( 'administration.attributes.common.featured_image' ), 'featured_image' )->nullable(),
            Text::make( __( 'administration.attributes.common.name' ), 'name' )->rules( 'required', 'max:255' )->sortable(),
            Slug::make( __( 'administration.attributes.common.slug' ), 'slug' )->rules( 'required', 'max:191' )->onlyOnDetail()->showUrlPreview( 'https://bee-hub.org/descriptors' ),

            Trix::make( __( 'administration.attributes.common.description' ), 'description' )->rules( 'max:255' )->hideFromIndex()->nullable(),
            //  Text::make(),
            BelongsTo::make( __( 'administration.attributes.descriptors.category' ), 'category', DescriptorCategory::class )->sortable(),
            Boolean::make( __( 'administration.attributes.common.public' ), 'public' )->sortable(),


            Panel::make( __( 'administration.attributes.group.internal' ), [
                Boolean::make( __( 'administration.attributes.common.deprecated' ), 'deprecated' )->rules( 'required' )->sortable(),
                Text::make( __( 'administration.attributes.descriptors.since' ), 'since' )->rules( 'required', 'max:10' )->sortable(),
                Text::make( __( 'administration.attributes.descriptors.table_name' ), 'table_name' )->onlyOnDetail()->readonly(),
                DateTime::make( __( 'administration.attributes.common.created_at' ), 'created_at' )->readonly()->sortable(),
                DateTime::make( __( 'administration.attributes.common.updated_at' ), 'updated_at' )->readonly()->sortable(),
                Text::make( __( 'administration.attributes.common.uid' ), 'uid' )->rules( 'required', 'max:191' )->readonly()->onlyOnDetail(),
            ] ),

            Panel::make( __( 'administration.attributes.group.statistics' ), [
                Text::make( __( 'administration.attributes.descriptors.data_count' ), function () {
                    /** @var \App\Models\MetaData\Descriptor $this */
                    return format_number( $this->dataCount(), 0 );
                } ),
                Text::make( __( 'administration.attributes.descriptors.data_unique' ), function () {
                    /** @var \App\Models\MetaData\Descriptor $this */
                    return format_number( $this->dataPivot()->select( [ 'descriptor_id', 'data_type', 'data_id' ] )->distinct()->get()->count(), 0 );
                } )->onlyOnDetail(),
            ] ),

            BelongsToMany::make( __( 'administration.attributes.projects.resource-name' ), 'projects', Project::class ),
             HasMany::make( __( 'administration.attributes.descriptors.data' ), 'dataPivot', Data::class ),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function cards( NovaRequest $request ): array
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function filters( NovaRequest $request ): array
    {
        return [
            ( new PublicFilter() ),
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function lenses( NovaRequest $request ): array
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function actions( NovaRequest $request ): array
    {
        return [
            ( new PublicEnable() ),
            ( new PublicDisable() ),
        ];
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label(): string
    {
        return __( 'administration.attributes.descriptors.resource-name' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel(): string
    {
        return __( 'administration.attributes.descriptors.resource-singular-name' );
    }

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group(): string
    {
        return __( 'administration.groups.data' );
    }

}
