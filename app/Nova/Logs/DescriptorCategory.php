<?php

namespace App\Nova\Logs;

use App\Nova\Resource;
use Benjaminhirsch\NovaSlugField\Slug;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Panel;

class DescriptorCategory extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\MetaData\DescriptorCategory::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name',
        'description',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request )
    {
        return [
            ID::make()->sortable(),
            Avatar::make( __( 'administration.attributes.common.icon' ), 'icon' )->nullable(),
            Image::make( __( 'administration.attributes.common.featured_image' ), 'featured_image' )->nullable(),
            Text::make( __( 'administration.attributes.descriptor-categories.name' ), 'name' )->rules( 'required', 'max:255' ),
            Slug::make( __( 'administration.attributes.common.slug' ), 'slug' )->rules( 'required', 'max:191' )->hideFromIndex()->showUrlPreview( 'https://bee-hub.org/descriptors/categories' ),
            Trix::make( __( 'administration.attributes.descriptor-categories.description' ), 'description' )->rules( 'required', 'max:255' )->hideFromIndex()->nullable(),
            BelongsTo::make( __( 'administration.attributes.descriptor-categories.parent' ), 'parent', DescriptorCategory::class )->nullable()->sortable(),

            Panel::make( __( 'administration.attributes.group.internal' ), [
                DateTime::make( __( 'administration.attributes.common.created_at' ), 'created_at' )->readonly()->sortable(),
                DateTime::make( __( 'administration.attributes.common.updated_at' ), 'updated_at' )->readonly()->sortable(),
                Text::make( __( 'administration.attributes.common.uid' ), 'uid' )->rules( 'required', 'max:191' )->readonly()->hideFromIndex(),
            ] ),

            Panel::make( __( 'administration.attributes.group.statistics' ), [
                Text::make( __( 'administration.attributes.common.public' ) . ' ' . __( 'administration.attributes.descriptors.resource-name' ), function () {
                    /** @var \App\Models\MetaData\Descriptor $this */
                    return format_number( $this->descriptors->where( 'public', 1 )->count(), 0 );
                } ),
                Text::make( __( 'administration.attributes.descriptor-categories.descriptor-count' ), function () {
                    /** @var \App\Models\MetaData\Descriptor $this */
                    return format_number( $this->descriptors->count(), 0 );
                } ),

            ] ),
            HasMany::make( __( 'administration.attributes.descriptor-categories.children' ), 'children', DescriptorCategory::class ),
            HasMany::make( __( 'administration.attributes.descriptors.resource-name' ), 'descriptors', Descriptor::class ),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function cards( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function filters( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function lenses( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function actions( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __( 'administration.attributes.descriptor-categories.resource-name' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __( 'administration.attributes.descriptor-categories.resource-singular-name' );
    }

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return __( 'administration.groups.data' );
    }

}
