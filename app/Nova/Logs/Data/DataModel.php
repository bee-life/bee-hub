<?php

namespace App\Nova\Logs\Data;

use App\Nova\Logs\Data;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphMany;
use Laravel\Nova\Fields\MorphTo;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Text;

abstract class DataModel extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Logs\Data\DataModel::class;

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = false;

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'value'
    ];


    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return (string) $this->model();
    }


    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request )
    {
        return [
            ID::make( __( 'administration.attributes.common.id' ), 'id' )->sortable(),
           // Text::make( __( 'Value' ), 'value' ),
           // MorphMany::make(__('Pivot'), 'pivot', Data::class),

          /*
            MorphToMany::make( __( 'Logs' ), 'logs', 'App\Nova\Data\Log' )->fields( function () {
                return [
                    BelongsTo::make( __( 'Data Type' ), 'type', 'App\Nova\Data\Descriptor' )->readonly(),
                ];
            } )->readonly()->searchable(),


            MorphToMany::make( __( 'Data Type' ), 'type', 'App\Nova\Data\Descriptor' )->fields( function () {
                return [
                    BelongsTo::make( __( 'Log' ), 'event', 'App\Nova\Data\Log' )->readonly(),
                ];
            } )->readonly()->searchable(),   */

        ];
    }


    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function cards( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function filters( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function lenses( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function actions( NovaRequest $request )
    {
        return [];
    }

    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public static function authorizedToCreate( Request $request )
    {
        return false;
    }

    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function authorizedToDelete( Request $request )
    {
        return false;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __( 'Data points' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __( 'Data point' );
    }


    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return __( 'administration.groups.data' );
    }
}
