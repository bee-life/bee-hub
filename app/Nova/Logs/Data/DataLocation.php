<?php

namespace App\Nova\Logs\Data;


use App\Nova\Logs\Log;
use Davidpiesse\Map\Map;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\MorphTo;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Panel;


class DataLocation extends DataModel
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Logs\Data\DataLocation::class;

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request )
    {
        return array_merge(parent::fields($request),[
            MorphTo::make( __( 'administration.attributes.data-location.location' ), 'location' )->searchable(),

            Map::make( __( 'administration.attributes.locations.center' ), 'value' )->spatialType( 'Point' )->height( '300px' )->zoom( 8 )->nullable(),

            Panel::make( __( 'administration.attributes.group.statistics' ), [
                Text::make( __( 'administration.attributes.data-location.log_count' ), function () {
                    /** @var \App\Models\Logs\Data\DataLocation $this */
                    return format_number( $this->logCount(), 0 );
                } ),
            ] ),

            MorphToMany::make( __( 'administration.attributes.data-id.logs' ), 'logs', Log::class ),
        ]);
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __( 'administration.attributes.data-id.resource-name' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __( 'administration.attributes.data-id.resource-singular-name' );
    }
}
