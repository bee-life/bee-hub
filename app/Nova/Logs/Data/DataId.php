<?php

namespace App\Nova\Logs\Data;


use App\Nova\Logs\Log;
use App\Nova\Project;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Panel;


class DataId extends DataModel
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Logs\Data\DataId::class;

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = true;

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'value',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request )
    {
        return array_merge(parent::fields($request),[
            Text::make( __( 'administration.attributes.data-id.value' ), 'value' )->sortable(),
            BelongsTo::make( __( 'administration.attributes.data-id.parent' ), 'parent', DataId::class )->searchable()->sortable(),
            BelongsTo::make( __( 'administration.attributes.data-id.project' ), 'project', Project::class )->searchable()->sortable(),

            Panel::make( __( 'administration.attributes.group.statistics' ), [
                Text::make( __( 'administration.attributes.data-id.children_count' ), function () {
                    /** @var \App\Models\Logs\Data\DataId $this */
                    return format_number( $this->children()->count(), 0 );
                } ),
                Text::make( __( 'administration.attributes.projects.log_count' ), function () {
                    /** @var \App\Models\Logs\Data\DataId $this */
                    return format_number( $this->logCount(), 0 );
                } ),
            ] ),

            HasMany::make(__('administration.attributes.data-id.children'), 'children', DataId::class),
            MorphToMany::make( __( 'administration.attributes.data-id.logs' ), 'logs', Log::class ),
        ]);
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __( 'administration.attributes.data-id.resource-name' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __( 'administration.attributes.data-id.resource-singular-name' );
    }
}
