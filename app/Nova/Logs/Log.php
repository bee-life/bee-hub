<?php

namespace App\Nova\Logs;

use App\Nova\Logs\Data\DataId;
use App\Nova\Logs\Data\DataLocation;
use App\Nova\Project;
use App\Nova\Resource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Laraning\NovaTimeField\TimeField;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Panel;

class Log extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static string $model = \App\Models\Logs\Log::class;

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = false;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title(): string
    {
        return (string) $this->resource;
    }


    /**
     * Get the fields displayed by the resource.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request ): array
    {
        return [
            ID::make( __( 'administration.attributes.common.id' ) )->sortable(),
            Text::make( __( 'administration.attributes.logs.year' ), 'year' )->sortable()->rules( [ 'required', 'integer', 'min:1900', 'max:' . Carbon::today()->year ] ),
            Date::make( __( 'administration.attributes.logs.date' ), 'date' )->sortable()->nullable(),
            TimeField::make( __( 'administration.attributes.logs.time' ), 'time' )->nullable(),

            Panel::make( __( 'administration.attributes.group.internal' ), [
                DateTime::make( __( 'administration.attributes.common.created_at' ), 'created_at' )->readonly(),
            ] ),

            BelongsTo::make( __( 'administration.attributes.logs.project' ), 'project', Project::class )->readonly(),
            BelongsTo::make( __( 'administration.attributes.logs.apiary' ), 'apiary', DataId::class )->nullable()->readonly(),
            BelongsTo::make( __( 'administration.attributes.logs.hive' ), 'hive', DataId::class )->nullable()->readonly(),
            Text::make( __( 'administration.attributes.logs.location' ), function () {
                /** @var \App\Models\Logs\Log $this */
                $url = \Nova::path() . "/resources/data-locations/{$this->location->id}";
                return "<a class=\"no-underline dim text-primary font-bold\" href=\"{$url}\">{$this->location}</a>";
            } )->asHtml(),

            BelongsToMany::make( __( 'administration.attributes.logs.descriptors' ), 'descriptors', Descriptor::class ),
            HasMany::make( __( 'administration.attributes.logs.data' ), 'data', Data::class )->readonly()->sortable( false ),
        ];
    }


    /**
     * Get the cards available for the request.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function cards( NovaRequest $request ): array
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function filters( NovaRequest $request ): array
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function lenses( NovaRequest $request ): array
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function actions( NovaRequest $request ): array
    {
        return [];
    }

    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public static function authorizedToCreate( Request $request ): bool
    {
        return false;
    }

    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function authorizedToDelete( Request $request ): bool
    {
        return false;
    }

    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function authorizedToUpdate( Request $request ): bool
    {
        return false;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label(): string
    {
        return __( 'administration.attributes.logs.resource-name' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel(): string
    {
        return __( 'administration.attributes.logs.resource-singular-name' );
    }


    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group(): string
    {
        return __( 'administration.groups.data' );
    }
}
