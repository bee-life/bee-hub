<?php

namespace App\Nova;

use App\Nova\Actions\PublicDisable;
use App\Nova\Actions\PublicEnable;
use App\Nova\Filters\PublicFilter;
use App\Nova\Locations\Post;
use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Panel;

class Provider extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\MetaData\Provider::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name',
        'full_name',
        'registry_number',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request )
    {
        return [
            ID::make( __( 'administration.attributes.common.id' ), 'id' )->sortable(),

            Avatar::make( __( 'administration.attributes.common.featured_image' ), 'featured_image' )->nullable(),
            TextWithSlug::make( __( 'administration.attributes.providers.full_name' ), 'full_name' )->sortable()->required()->rules( 'max:191' )->slug( 'slug' ),
            Slug::make( __( 'administration.attributes.common.slug' ), 'slug' )->rules( 'required', 'max:191' )->hideFromIndex()->showUrlPreview( 'https://bee-hub.org/providers' ),
            Text::make( __( 'administration.attributes.common.name' ), 'name' )->sortable()->nullable()->rules( 'max:191' ),
            Trix::make( __( 'administration.attributes.common.description' ), 'description' )->hideFromIndex()->nullable()->stacked(),
            Text::make( __( 'administration.attributes.providers.registry_number' ), 'registry_number' )->sortable()->nullable()->rules( 'max:191' ),
            Boolean::make( __( 'administration.attributes.common.public' ), 'public' )->sortable(),

            new Panel( __( 'administration.attributes.group.public' ), [
                Textarea::make( __( 'administration.attributes.providers.address' ), 'address' )->hideFromIndex()->nullable(),
                BelongsTo::make( __( 'administration.attributes.providers.post' ), 'post', Post::class )->hideFromIndex()->nullable()->searchable(),
                Text::make( __( 'administration.attributes.common.website' ), 'website' )->hideFromIndex()->rules( 'max:191' )->nullable(),
                Text::make( __( 'administration.attributes.common.email' ), 'email' )->hideFromIndex()->rules( 'max:191' )->nullable(),
                Text::make( __( 'administration.attributes.common.phone' ), 'phone' )->hideFromIndex()->rules( 'max:191' )->nullable(),
            ] ),

            new Panel( __( 'administration.attributes.group.internal' ), [
                DateTime::make( __( 'administration.attributes.common.created_at' ), 'created_at' )->sortable(),
                DateTime::make( __( 'administration.attributes.common.updated_at' ), 'updated_at' )->sortable(),
                Text::make( __( 'administration.attributes.common.uid' ), 'uid' )->rules( 'required', 'max:191' )->readonly()->hideFromIndex(),
            ] ),

            Panel::make( __( 'administration.attributes.group.statistics' ), [
                Text::make( __( 'administration.attributes.providers.projects' ), function () {
                    /** @var \App\Models\MetaData\Provider $this */
                    return $this->projects()->count();
                } ),
            ] ),

            BelongsToMany::make( __( 'administration.attributes.providers.projects' ), 'projects', Project::class ),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function cards( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function filters( NovaRequest $request )
    {
        return [
            ( new PublicFilter() ),
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function lenses( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function actions( NovaRequest $request )
    {
        return [
            ( new PublicEnable() ),
            ( new PublicDisable() ),
        ];
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __( 'administration.attributes.providers.resource-name' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __( 'administration.attributes.providers.resource-singular-name' );
    }

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return __( 'administration.groups.data' );
    }
}
