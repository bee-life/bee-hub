<?php

namespace App\Nova\Filters;

use Illuminate\Database\Eloquent\Builder;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Filters\Filter;

class PublicFilter extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param NovaRequest $request
     * @param Builder     $query
     * @param mixed       $value
     *
     * @return Builder
     */
    public function apply( NovaRequest $request, $query, $value )
    {
        if ( isset( $value ) ) {
            return $query->where( 'public', $value );
        }
    }

    /**
     * Get the filter's available options.
     *
     * @param NovaRequest $request
     *
     * @return array
     */
    public function options( NovaRequest $request )
    {
        return [
            'Only Public'     => 1,
            'Only non-public' => 0,
        ];
    }
}
