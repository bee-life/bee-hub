<?php

namespace App\Nova\Metrics;


use App\Models\Logs\Log;
use Cache;
use Carbon\Carbon;
use Coroowicaksono\ChartJsIntegration\LineChart;

class TotalDataCount extends LineChart
{
    public function __construct( $component = null )
    {
        if ( Cache::has( 'nova_dashboard_total_data_count' ) ) {
            $data = Cache::get( 'nova_dashboard_total_data_count' );
        } else {
            $current = ( new Carbon( Log::min( 'created_at' ) ) )->startOfWeek();
            $now     = Carbon::now();

            $data = collect();

            while ( $current < $now ) {
                $key          = trans( 'Week :week in :year', [ 'week' => $current->weekOfYear, 'year' => $current->year ] );
                $data[ $key ] = Log::where( 'created_at', '<=', $current )->count();

                $current->addWeek();
            }
            Cache::set( 'nova_dashboard_total_data_count', $data, 60 * 60 * 12 );
        }

        $this->title( __( 'Total Logs in Database' ) )
             ->animations( [
                 'enabled' => true,
                 'easing'  => 'easeinout',
             ] )
             ->series( array(
                 [
                     'label'       => 'Logs',
                     'borderColor' => 'rgb(245, 87, 59)',
                     'data'        => $data->values(),
                 ],

             ) )
             ->options( [
                 'xaxis' => [
                     'categories' => $data->keys(),
                 ],
             ] );
        parent::__construct( $component );
    }
}
