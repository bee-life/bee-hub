<?php

namespace App\Nova\Actions;

use App\Models\MetaData\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Actions\DestructiveAction;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Str;

class DeleteProject extends DestructiveAction
{
    use InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Perform the action on the given models.
     *
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection    $models
     *
     * @return mixed
     */
    public function handle( ActionFields $fields, Collection $models )
    {
        if ( $fields->confirm !== "CONFIRM" ) {
            return Action::danger( __( 'administration.actions.delete-project.failure' ) );
        }

        foreach ( $models as $project ) {
            /** @var Project $project */
            $project->logs()->delete();
            $project->relatedDataIds()->delete();
            $project->descriptors()->detach();
            $project->users()->detach();
            $rnd = Str::random( 20 );

            $project->providers()->detach();

            $project->slug = $project->slug . $rnd;
            $project->uid  = $project->uid . $rnd;

            $project->save();
            $project->delete();
        }

        return Action::message( __( 'administration.actions.delete-project.success' ) );
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            Text::make( __( 'administration.actions.delete-project.confirm' ), 'confirm' ),
        ];
    }
}
