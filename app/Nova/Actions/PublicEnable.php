<?php

namespace App\Nova\Actions;

use App\Models\MetaData\Descriptor;
use App\Models\MetaData\Project;
use App\Models\MetaData\Provider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Http\Requests\NovaRequest;

class PublicEnable extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Perform the action on the given models.
     *
     * @param \Laravel\Nova\Fields\ActionFields                  $fields
     * @param \Illuminate\Database\Eloquent\Collection|Project[]|Descriptor[]|Provider[] $models
     */
    public function handle( ActionFields $fields, Collection $models )
    {
        foreach ( $models as $model ) {
            if ( ! $model->isPrivate() ) {
                $model->setPublic();
            }
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [];
    }
}
