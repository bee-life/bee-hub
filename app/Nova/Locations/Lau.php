<?php

namespace App\Nova\Locations;

use App\Nova\Resource;
use Davidpiesse\Map\Map;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Panel;

class Lau extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Locations\Lau::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'number';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'number',
        'nuts_id',
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request )
    {
        return [
            ID::make( __( 'administration.attributes.common.id' ), 'id' )->sortable(),
            Text::make( __( 'administration.attributes.common.name' ), 'name' )->required()->rules( 'max:150' )->sortable(),
            Text::make( __( 'administration.attributes.district.lau_id' ), 'lau_id' )->required()->rules( 'max:13' )->sortable(),

            Text::make( __( 'administration.attributes.district.nuts_id' ), 'nuts_id' )->required()->rules( 'max:5' )->sortable(),

            new Panel( __( 'administration.attributes.group.statistics' ), [
                Number::make(__('administration.attributes.locations.population'), 'population')->nullable(),
                Number::make(__('administration.attributes.locations.surface'), 'surface')->nullable(),
                Map::make( __( 'administration.attributes.locations.center' ), 'center' )->spatialType( 'Point' )->height( '300px' )->zoom( 6 ),
                Map::make( __( 'administration.attributes.locations.area' ), 'area' )->spatialType( 'Geometry' )->height( '300px' ),
            ] ),

            BelongsTo::make( __( 'administration.attributes.locations.country' ), 'country', Country::class )->required()->sortable(),
            BelongsTo::make( __( 'administration.attributes.locations.parent' ), 'parent', Region::class )->required()->sortable(),

            HasMany::make( __( 'administration.attributes.locations.posts' ), 'children', Post::class )->nullable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function cards( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function filters( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function lenses( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function actions( NovaRequest $request )
    {
        return [];
    }

    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public static function authorizedToCreate( Request $request )
    {
        return false;
    }

    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function authorizedToDelete( Request $request )
    {
        return false;
    }

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->name;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __( 'administration.attributes.district.resource-name' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __( 'administration.attributes.district.resource-singular-name' );
    }

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return __('administration.groups.meta');
    }
}
