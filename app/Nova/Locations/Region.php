<?php

namespace App\Nova\Locations;

use App\Nova\Resource;
use Davidpiesse\Map\Map;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Panel;

class Region extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Locations\Region::class;


    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'parent_id',
        'country_code',
        'nuts_id',
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request )
    {
        return [
            ID::make()->sortable(),

            Text::make( __( 'administration.attributes.common.name' ), 'name' )->sortable(),
            Number::make( __( 'administration.attributes.regions.nuts_level' ), 'nuts_level' )->sortable(),
            Text::make( __( 'administration.attributes.regions.nuts_idr' ), 'nuts_id' )->sortable(),
            BelongsTo::make( __( 'administration.attributes.locations.country' ), 'country', Country::class )->searchable(),
            BelongsTo::make( __( 'administration.attributes.locations.parent' ), 'parent', Region::class )->nullable()->searchable(),

            new Panel( __( 'administration.attributes.group.statistics' ), [
                Number::make( __( 'administration.attributes.locations.population' ), 'population' )->nullable(),
                Number::make( __( 'administration.attributes.locations.surface' ), 'surface' )->nullable(),
                Map::make( __( 'administration.attributes.locations.center' ), 'center' )->spatialType( 'Point' )->height( '300px' )->zoom( 6 )->nullable(),
                Map::make( __( 'administration.attributes.locations.area' ), 'area' )->spatialType( 'Geometry' )->height( '300px' )->nullable(),
            ] ),

            HasMany::make( __( 'administration.attributes.locations.children' ), 'children', Region::class )->nullable(),
            HasMany::make( __( 'administration.attributes.locations.posts' ), 'posts', Post::class )->nullable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function cards( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function filters( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function lenses( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function actions( NovaRequest $request )
    {
        return [];
    }

    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public static function authorizedToCreate( Request $request )
    {
        return false;
    }

    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function authorizedToDelete( Request $request )
    {
        return false;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __( 'administration.attributes.regions.resource-name' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __( 'administration.attributes.regions.resource-singular-name' );
    }

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return __( 'administration.groups.meta' );
    }
}
