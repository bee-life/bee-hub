<?php

namespace App\Nova\Locations;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;

class Country extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Locations\Country::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'country_code',
        'name',
        'native_name',
        'capital',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request )
    {
        return [
            ID::make( __( 'administration.attributes.common.id' ), 'id' )->sortable(),

            Text::make( __( 'administration.attributes.common.name' ), 'name' )->required()->rules( 'max:50' )->sortable(),
            Text::make( __( 'administration.attributes.country.native_name' ), 'native_name' )->required()->rules( 'max:100' )->sortable(),
            Text::make( __( 'administration.attributes.country.capital' ), 'capital' )->rules( 'max:30' )->sortable(),
            Select::make( __( 'administration.attributes.country.continent' ), 'continent' )->options( [
                'AF' => 'Africa',
                'AN' => 'Antarctica',
                'AS' => 'Asia',
                'EU' => 'Europe',
                'NA' => 'North America',
                'OC' => 'Oceania',
                'SA' => 'South America',
            ] )->required()->sortable(),

            Text::make( __( 'administration.attributes.country.country_code' ), 'country_code' )->rules( 'max:2' )->sortable(),
            Text::make( __( 'administration.attributes.country.iso3' ), 'iso3' )->rules( 'max:3' )->sortable(),
            Text::make( __( 'administration.attributes.country.currency_code' ), 'currency_code' )->rules( 'max:3' )->sortable(),
            Text::make( __( 'administration.attributes.country.currency_name' ), 'currency_name' )->rules( 'max:20' )->sortable(),
            Text::make( __( 'administration.attributes.country.phone_prefix' ), 'phone_prefix' )->rules( 'max:20' ),

            HasMany::make( __( 'administration.attributes.locations.regions' ), 'regions', Region::class ),
            HasMany::make( __( 'administration.attributes.locations.districts' ), 'laus', Lau::class ),
            HasMany::make( __( 'administration.attributes.locations.posts' ), 'posts', Post::class ),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function cards( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function filters( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function lenses( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function actions( NovaRequest $request )
    {
        return [];
    }

    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public static function authorizedToCreate( Request $request )
    {
        return false;
    }

    /**
     * Disallow resource deletion.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function authorizedToDelete( Request $request )
    {
        return false;
    }

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->name;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __( 'administration.attributes.country.resource-name' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __( 'administration.attributes.country.resource-singular-name' );
    }

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return __( 'administration.groups.meta' );
    }
}
