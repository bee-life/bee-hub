<?php

namespace App\Nova;


use App\Nova\Flexible\Layouts\HeroImage;
use App\Nova\Flexible\Presets\ContentPreset;
use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Trix;
use Whitecube\NovaFlexibleContent\Flexible;

class Page extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\CMS\Page::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'title',
        'slug',
        'content',
        'meta_title',
        'meta_description',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function fields( NovaRequest $request )
    {
        return [
            ID::make( __( 'administration.attributes.common.id' ), 'id' )->sortable(),

            TextWithSlug::make( __( 'administration.attributes.pages.title' ), 'title' )
                        ->sortable()
                        ->rules( 'required', 'max:255' )
                        ->slug( 'slug' ),

            Slug::make( __( 'administration.attributes.common.slug' ), 'slug' )
                ->hideFromIndex()
                ->hideFromDetail()
                ->rules( 'required', 'max:255' )
                ->creationRules( 'unique:news,slug' )
                ->updateRules( 'unique:news,slug,{{resourceId}}' )
                ->disableAutoUpdateWhenUpdating()->help(
                    'Unique URL representation of the News.'
                ),

            Text::make( __( 'administration.attributes.common.slug' ), function () {
                /** @var \App\Models\CMS\News $this */
                return '<a href="' . url( $this->getUrl() ) . '">' . $this->slug . '</a>';
            } )->asHtml(),

            Flexible::make( __( 'administration.attributes.pages.content' ), 'content' )
                    ->hideFromIndex()
                    ->preset( ContentPreset::class ),

            Text::make( __( 'administration.attributes.pages.meta_title' ), 'meta_title' )
                ->sortable()
                ->rules( 'max:255' )
                ->hideFromIndex(),

            Textarea::make( __( 'administration.attributes.pages.meta_description' ), 'meta_description' )
                    ->sortable()
                    ->hideFromIndex(),

            DateTime::make( __( 'administration.attributes.common.created_at' ), 'created_at' )->readonly()->sortable(),
            DateTime::make( __( 'administration.attributes.common.updated_at' ), 'updated_at' )->readonly()->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function cards( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function filters( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function lenses( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     *
     * @return array
     */
    public function actions( NovaRequest $request )
    {
        return [];
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __( 'administration.attributes.pages.resource-name' );
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __( 'administration.attributes.pages.resource-singular-name' );
    }

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return __( 'administration.groups.cms' );
    }
}
