<?php

namespace App\Data\Content;


use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use Cache;
use Illuminate\Database\Eloquent\Collection;

class ColonyDevelopmentContent extends Content
{
    /**
     * Content constructor.
     *
     * @param Descriptor      $descriptor
     * @param Log             $log
     * @param Collection|null $logs
     */
    public function __construct( Descriptor $descriptor, Log $log, ?Collection $logs = null )
    {
        $descriptors = $log->data->keyBy( 'descriptor_id' );

        $hiveWeightDescriptor       = Descriptor::find( getDescriptorIdByUid( 'hive-weight' ) );
        $additionalChartDescriptors = [
            [
                'id'    => getDescriptorIdByUid( 'hive-population' ),
                'uid'   => 'hive-population',
                'label' => __( 'logs.colony-development.hive-population' ),
                'color' => '1',
            ],
            [
                'id'    => getDescriptorIdByUid( 'brood-cells' ),
                'uid'   => 'brood-cells',
                'label' => __( 'logs.colony-development.brood-cells' ),
                'color' => '2',
            ],
            [
                'id'    => getDescriptorIdByUid( 'beebread-cells' ),
                'uid'   => 'beebread-cells',
                'label' => __( 'logs.colony-development.beebread-cells' ),
                'color' => '3',
            ],
            [
                'id'       => null,
                'uid'      => 'events',
                'label'    => __( 'logs.hive-events.management-events' ),
                'color'    => '6',
                'tertiary' => true,

            ],
        ];

        $this->addDataTypeNote( $log );
        $this->addLabeledValue( __( 'logs.region' ), $log->getLocation() );
        $this->addApiaryId( $log );

        //$this->includeChartPopup( 'ColonyDevelopmentComponent', __( 'logs.colony-development.colony-development-data' ), $hiveWeightDescriptor, $log->apiary_id, $additionalChartDescriptors, __( 'logs.apiary-id' ) . ': ' . $log->apiary->value );

        /** @var Log $hiveLog */
        foreach ( $logs as $hiveLog ) {
            if ( Cache::has( "graph-hive-population-" . $hiveLog->hive_id ) ) {
                $this->addLabeledValue( __( 'logs.hive-id' ), $hiveLog->hive->value );
                $this->includeChartPopup( 'ColonyDevelopmentComponent', __( 'logs.colony-development.colony-development-data' ), $hiveWeightDescriptor, $hiveLog->hive_id, $additionalChartDescriptors, __( 'logs.hive-id' ) . ': ' . $hiveLog->hive->value );
            }
        }
    }
}
