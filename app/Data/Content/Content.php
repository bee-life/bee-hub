<?php

namespace App\Data\Content;

use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use JsonSerializable;

abstract class Content implements JsonSerializable
{

    protected $data = array();


    /**
     * Content constructor.
     *
     * @param Descriptor      $descriptor
     * @param Log             $log
     * @param Collection|null $logs
     */
    public abstract function __construct( Descriptor $descriptor, Log $log, ?Collection $logs = null );

    /**
     * Add single row of data as label-value pair.
     *
     * @param string $label
     * @param string $value
     * @param array  $additionalProps
     */
    public function addLabeledValue( string $label, string $value, array $additionalProps = [] ): void
    {
        $this->push( 'labeled-value', array_merge( [ 'label' => $label, 'value' => $value ], $additionalProps ) );
    }

    /**
     * Add section heading, in order to group data.
     *
     * @param string $heading
     */
    public function addSection( string $heading ): void
    {
        $this->push( 'section', [ 'heading' => $heading ] );
    }

    /**
     * Add simple separator.
     */
    public function addSeparator(): void
    {
        $this->push( 'separator' );
    }

    /**
     * Add a button linking to a chart to previous line.
     *
     * @param string     $component
     * @param string     $label
     * @param Descriptor $descriptor
     * @param int        $external_id
     */
    public function includeChartPopup( string $component, string $label, Descriptor $descriptor, int $external_id, $secondaryDescriptors = null, string $title = null ): void
    {

        if ( Cache::has( "graph-{$descriptor->uid}-{$external_id}" ) ) {
            $additional = [];
            foreach ( $secondaryDescriptors as $secondaryDescriptor ) {
                if ( empty( $secondaryDescriptor['object_id'] ) ) {
                    $secondaryDescriptor['object_id'] = $external_id;
                }

                if ( Cache::has( "graph-{$secondaryDescriptor['uid']}-{$secondaryDescriptor['object_id']}" ) ) {
                    $additional[] = $secondaryDescriptor;
                }
            }

            $this->data[ count( $this->data ) - 1 ]['chart'] = [
                'component'     => $component,
                'label'         => $label,
                'title'         => isset( $title ) ? $title : $label,
                'descriptor_id' => $descriptor->id,
                'name'          => $descriptor->name,
                'object_id'     => $external_id,
                'secondary'     => $additional
            ];
        }
    }

    /**
     * Include added content in the output.
     *
     * @param string $type
     * @param array  $data
     */
    protected function push( string $type, array $data = [] ): void
    {
        $data['type'] = $type;
        $this->data[] = $data;
    }

    /**
     * Include available weather station data.
     *
     * @param Collection $descriptors
     * @param Log        $log
     *
     * @throws \App\Exceptions\ModelNotFoundByUidException
     */
    protected function addWeatherContent( $descriptors, Log $log )
    {
        $this->addSection( __( 'logs.weather-sensors.heading' ) );

        if ( isset( $descriptors[ getDescriptorIdByUid( 'temperature-daily' ) ] ) ) {
            $this->addLabeledValue( __( 'logs.weather-sensors.temperature' ), format_temperature_range( $descriptors[ getDescriptorIdByUid( 'temperature-daily' ) ]->value, 'celsius' ), [
                'icon' => [
                    'far',
                    'thermometer-three-quarters'
                ],
                'inline'
            ] );
        }
        if ( isset( $descriptors[ getDescriptorIdByUid( 'relative-humidity' ) ] ) ) {
            $this->addLabeledValue( __( 'logs.weather-sensors.relative-humidity' ), format_percentage( $descriptors[ getDescriptorIdByUid( 'relative-humidity' ) ]->value ), [
                'icon' => [
                    'far',
                    'humidity'
                ],
                'inline'
            ] );
        }
        if ( isset( $descriptors[ getDescriptorIdByUid( 'rain' ) ] ) ) {
            $this->addLabeledValue( __( 'logs.weather-sensors.rain' ), format_volume( $descriptors[ getDescriptorIdByUid( 'rain' ) ]->value, 'l' ), [ 'icon' => [ 'far', 'raindrops' ] ] );
        }
        if ( isset( $descriptors[ getDescriptorIdByUid( 'solar-radiation' ) ] ) ) {
            $this->addLabeledValue( __( 'logs.weather-sensors.solar-radiation' ), format_solar_irradiance( $descriptors[ getDescriptorIdByUid( 'solar-radiation' ) ]->value, 'wm' ), [
                'icon' => [
                    'far',
                    'sun'
                ],
                'inline'
            ] );
        }
        if ( isset( $descriptors[ getDescriptorIdByUid( 'wind-direction' ) ] ) ) {
            $this->addLabeledValue( __( 'logs.weather-sensors.wind-direction' ), format_orientation( $descriptors[ getDescriptorIdByUid( 'wind-direction' ) ]->value, 'orientation' ), [
                'icon' => [
                    'far',
                    'location-arrow'
                ],
                'inline'
            ] );
        }
        if ( isset( $descriptors[ getDescriptorIdByUid( 'wind-speed' ) ] ) ) {
            $this->addLabeledValue( __( 'logs.weather-sensors.wind-speed' ), format_speed( $descriptors[ getDescriptorIdByUid( 'wind-speed' ) ]->value, 'm/s' ), [
                'icon' => [
                    'far',
                    'windsock'
                ],
                'inline'
            ] );
        }
        if ( isset( $descriptors[ getDescriptorIdByUid( 'wind-gust' ) ] ) ) {
            $this->addLabeledValue( __( 'logs.weather-sensors.wind-gust' ), format_speed( $descriptors[ getDescriptorIdByUid( 'wind-gust' ) ]->value, 'm/s' ), [
                'icon' => [ 'far', 'wind' ],
                'inline'
            ] );
        }

        if ( $log->getCoordinates() != null ) {
            $center = $log->getCoordinates();
            if ( date_sunrise( $log->date->getTimestamp(), SUNFUNCS_RET_STRING, $center[0], $center[1] ) != false ) {
                $this->addLabeledValue( __( 'logs.weather-sensors.sunrise' ), date_sunrise( $log->date->getTimestamp(), SUNFUNCS_RET_STRING, $center[0], $center[1] ), [
                    'icon' => [
                        'far',
                        'sunrise'
                    ],
                    'inline'
                ] );
                $this->addLabeledValue( __( 'logs.weather-sensors.sunset' ), date_sunset( $log->date->getTimestamp(), SUNFUNCS_RET_STRING, $center[0], $center[1] ), [
                    'icon' => [
                        'far',
                        'sunset'
                    ],
                    'inline'
                ] );
            }
        }
    }

    /**
     * Includes valid Log Apiary Id.
     *
     * @param Log $log
     */
    protected function addApiaryId( Log $log )
    {
        if ( $log->hasApiaryId() && substr( $log->apiary->value, 0, 8 ) != 'unknown_' ) {
            $this->addLabeledValue( __( 'logs.apiary-id' ), $log->apiary->value );
        }
    }

    /**
     * Add a note displaying the source of the data.
     *
     * @param Log $log
     */
    protected function addDataTypeNote( Log $log )
    {
        if ( $log->project->hasLiveData() && $log->hasRecentData() ) {
            $this->addSection( __( 'logs.latest-data' ) );
            $this->addText( __( 'logs.live-note' ) );
        } elseif ( $log->project->isTypeProcessing() ) {
            $this->addSection( __( 'logs.processed-data' ) );
            $this->addText( __( 'logs.processed-note' ) );
        } else {
            $this->addSection( __( 'logs.historic-data' ) );
            $this->addText( __( 'logs.historic-note' ) );
        }
    }

    /**
     * Rules for content serialization.
     * Part of JsonSerializable interface.
     *
     * @return string
     */
    public function jsonSerialize(): string
    {
        return json_encode( $this->data );
    }

    /**
     *
     *
     * @param Log   $log
     * @param array $project_ids
     *
     * @return Log[]|\Illuminate\Database\Eloquent\Collection
     */
    protected function loadAdditionalLogs( Log $log, array $project_ids )
    {
        $additionalDataLogs = Log::where( 'date', $log->raw_date )
                                 ->whereNull( 'time' )
                                 ->whereIn( 'project_id', $project_ids )
                                 ->whereHiveId( $log->hive_id )
                                 ->get();

        if ( $log->hasApiaryId() ) {
            $apiaryLogs = Log::where( 'date', $log->raw_date )
                             ->whereNull( 'time' )
                             ->whereIn( 'project_id', $project_ids )
                             ->whereNull( 'hive_id' )->whereApiaryId( $log->apiary_id )
                             ->get();

            $additionalDataLogs = $additionalDataLogs->union( $apiaryLogs );
        }

        $additionalDataLogs->load( [ 'data', 'data.data' ] );

        return $additionalDataLogs;
    }

    /**
     * Add a paragraph of text.
     *
     * @param string $string
     */
    protected function addText( string $string )
    {
        $this->push( 'paragraph', [ 'value' => $string ] );
    }

    /**
     * Add a line break.
     */
    protected function addLineBreak()
    {
        $this->push( 'br' );
    }

    /**
     * Add HTML code.
     *
     * @param string $string
     */
    protected function addHtml( string $string )
    {
        $this->push( 'html', [ 'value' => $string ] );
    }
}
