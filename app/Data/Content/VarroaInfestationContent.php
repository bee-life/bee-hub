<?php

namespace App\Data\Content;


use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use Illuminate\Database\Eloquent\Collection;

class VarroaInfestationContent extends Content
{
    /**
     * Content constructor.
     *
     * @param Descriptor      $descriptor
     * @param Log             $log
     * @param Collection|null $logs
     */
    public function __construct( Descriptor $descriptor, Log $log, ?Collection $logs = null )
    {
        $descriptors = $log->data->keyBy( 'descriptor_id' );

        $this->addDataTypeNote( $log );
        $this->addLabeledValue( __( 'logs.region' ), $log->getLocation() );

        $this->addLabeledValue( __( 'logs.varroa-infestation.current-value' ), format_number( $descriptors[ $descriptor->id ]->value, 2 ) );
        $this->addLabeledValue( __( 'logs.varroa-infestation.threshold' ), format_number( $descriptors[ getDescriptorIdByUid( 'varroa-investation-threshold' ) ]->value, 2 ) . ' (±' . format_number( $descriptors[ getDescriptorIdByUid( 'varroa-investation-threshold-deviation' ) ]->value, 2 ) . ')' );

        if ( isset( $descriptors[ getDescriptorIdByUid( 'updated-at' ) ] ) ) {
            $this->addLabeledValue( __( 'logs.varroa-infestation.updated-at' ), format_date( $descriptors[ getDescriptorIdByUid( 'updated-at' ) ]->value ) );
        } else {
            $this->addLabeledValue( __( 'logs.varroa-infestation.updated-at' ), format_date( $log->date ) );
        }

        $this->addLabeledValue( __( 'logs.varroa-infestation.data-counts' ), format_number( $descriptors[ getDescriptorIdByUid( 'data-counts' ) ]->value, 0 ) );
        $this->addLabeledValue( __( 'logs.varroa-infestation.apiary-counts' ), format_number( $descriptors[ getDescriptorIdByUid( 'apiary-counts' ) ]->value, 0 ) );
        $this->addLabeledValue( __( 'logs.varroa-infestation.colony-counts' ), format_number( $descriptors[ getDescriptorIdByUid( 'colony-counts' ) ]->value, 0 ) );

        $this->addSeparator();
        $this->addHtml( $descriptors[ getDescriptorIdByUid( 'content' ) ]->value );
        $this->addSeparator();

    }
}
