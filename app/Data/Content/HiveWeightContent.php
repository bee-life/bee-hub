<?php

namespace App\Data\Content;


use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use Illuminate\Database\Eloquent\Collection;

class HiveWeightContent extends Content
{
    /**
     * Content constructor.
     *
     * @param Descriptor      $descriptor
     * @param Log             $log
     * @param Collection|null $logs
     */
    public function __construct( Descriptor $descriptor, Log $log, ?Collection $logs = null )
    {
        $descriptors = $log->data->keyBy( 'descriptor_id' );
        $additionalChartDescriptors = [
            [
                'id'    => getDescriptorIdByUid( 'temperature-daily' ),
                'uid'   => 'temperature-daily',
                'label' => __( 'logs.weather-sensors.temperature' ),
                'color' => '1',
            ],
            [
                'id'    => getDescriptorIdByUid( 'hive-temperature-daily' ),
                'uid'   => 'hive-temperature-daily',
                'label' => __( 'logs.hive-sensors.inside-temperature' ),
                'color' => '2',
            ],
            [
                'id'    => getDescriptorIdByUid( 'relative-humidity' ),
                'uid'   => 'relative-humidity',
                'label' => __( 'logs.weather-sensors.relative-humidity' ),
                'color' => '6',
            ],
            [
                'id'    => getDescriptorIdByUid( 'hive-relative-humidity' ),
                'uid'   => 'hive-relative-humidity',
                'label' => __( 'logs.hive-sensors.inside-humidity' ),
                'color' => '7',
            ],
            [
                'id'    => getDescriptorIdByUid( 'rain' ),
                'uid'   => 'rain',
                'label' => __( 'logs.weather-sensors.rain' ),
                'color' => '3',
            ],
            [
                'id'    => getDescriptorIdByUid( 'solar-radiation' ),
                'uid'   => 'solar-radiation',
                'label' => __( 'logs.weather-sensors.solar-radiation' ),
                'color' => '4',
            ],
            [
                'id'    => getDescriptorIdByUid( 'wind-speed' ),
                'uid'   => 'wind-speed',
                'label' => __( 'logs.weather-sensors.wind-speed' ),
                'color' => '5',
            ],
            [
                'id'    => getDescriptorIdByUid( 'hive-production' ),
                'uid'   => 'hive-production',
                'label' => __( 'logs.hive-sensors.hive-production' ),
                'color' => '3',
            ],
            [
                'id'    => getDescriptorIdByUid( 'hive-metabolic-resting-state' ),
                'uid'   => 'hive-metabolic-resting-state',
                'label' => __( 'logs.hive-sensors.hive-metabolic-resting-state' ),
                'color' => '4',
            ],
            [
                'id'    => getDescriptorIdByUid( 'maximum-available-flight-time' ),
                'uid'   => 'maximum-available-flight-time',
                'label' => __( 'logs.hive-sensors.maximum-available-flight-time' ),
                'color' => '5',
            ],
            [
                'id'       => null,
                'uid'      => 'events',
                'label'    => __( 'logs.hive-events.management-events' ),
                'color'    => '6',
                'tertiary' => true,

            ],
        ];

        $this->addDataTypeNote( $log );
        $this->addLabeledValue( __( 'logs.region' ), $log->getLocation() );
        $this->addApiaryId( $log );

        if ( empty( $logs ) ) {
            // Get any additional Log data we might have related to this Colony/Apiary
            $additionalDataLogs = $this->loadAdditionalLogs( $log, [ 2, 5, 6 ] );
            foreach ( $additionalDataLogs as $additionalLog ) {
                $descriptors = $additionalLog->data->keyBy( 'descriptor_id' )->union( $descriptors );
            }
            $this->addLabeledValue( __( 'logs.hive-id' ), $log->hive->value );

            if ( isset( $descriptors[ getDescriptorIdByUid( 'updated-at' ) ] ) ) {
                $this->addLabeledValue( __( 'logs.hive-sensors.updated-at' ), format_date( $descriptors[ getDescriptorIdByUid( 'updated-at' ) ]->value ) );
            } else {
                $this->addLabeledValue( __( 'logs.hive-sensors.updated-at' ), format_date( $log->date ) );
            }

            if ( isset( $descriptors[ getDescriptorIdByUid( 'hive-weight' ) ] ) ) {
                $this->addLabeledValue( __( 'logs.hive-sensors.hive-weight' ), format_weight( $descriptors[ getDescriptorIdByUid( 'hive-weight' ) ]->value, 'kg' ) );
                $this->includeChartPopup( 'HiveWeightComponent', __( 'logs.historic-data' ), $descriptor, $log->hive_id, $additionalChartDescriptors );
            }

            if ( isset( $descriptors[ getDescriptorIdByUid( 'hive-production' ) ] ) ) {
                $this->addLabeledValue( __( 'logs.hive-sensors.hive-production' ), format_weight( $descriptors[ getDescriptorIdByUid( 'hive-production' ) ]->value, 'kg' ) );
            } else {
                $this->addLabeledValue( __( 'logs.hive-sensors.hive-production' ), __( 'logs.hive-sensors.hive-production-unknown', [ 'value' => format_weight( 0, 'kg' ) ] ) );
            }

            if ( isset( $descriptors[ getDescriptorIdByUid( 'hive-temperature-daily' ) ] ) ) {
                $this->addLabeledValue( __( 'logs.hive-sensors.inside-temperature' ), format_temperature_range( $descriptors[ getDescriptorIdByUid( 'hive-temperature-daily' ) ]->value, 'c' ) );
            }

            $this->addWeatherContent( $descriptors, $log );

        } else {

            $this->includeChartPopup( 'HiveWeightComponent', __( 'logs.historic-data' ), $descriptor, $log->apiary_id, $additionalChartDescriptors, __('logs.apiary-id') . ': ' . $log->apiary->value );

            /** @var Log $hiveLog */
            foreach($logs as $hiveLog){
                $this->addLabeledValue( __( 'logs.hive-id' ), $hiveLog->hive->value );
                $this->includeChartPopup( 'HiveWeightComponent', __( 'logs.historic-data' ), $descriptor, $hiveLog->hive_id, $additionalChartDescriptors, __('logs.hive-id') . ': ' . $hiveLog->hive->value );

             }
        }
    }
}
