<?php namespace App\Data\Charts;

use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\References\Pesticide;
use App\Models\References\Pollen;

/**
 * Preload Pollen contamination and pesticide charts.
 * @TODO:
 *      - Review and test
 *      - Finish the implementation
 *
 * @see resources/js/components/charts/logs/HiveWeightComponent.vue
 */
class ContaminationCharts extends BaseChart {

    /**
     * Preload Hive Scales data for D3 graph display.
     *
     * @TODO: Evaluate and test for all sites.
     *
     * @see resources/js/components/charts/logs/HiveWeightComponent.vue
     */
    public function run(): void
    {
        /** @var Descriptor $descriptor */
        $descriptor = Descriptor::where( 'uid', 'pollen-pesticides' )->first();
        if ( ! empty( $descriptor ) ) {
            $logs = $descriptor->getLogs( 2011, true )->groupBy( function ( Log $log ) {
                return (string) $log->apiary_id;
            } );

            $pollenReference           = Pollen::all()->keyBy( 'id' );
            $pesticideReference        = Pesticide::with( [ 'type' ] )->get()->keyBy( 'id' );
            $pollenDescriptorId        = Descriptor::getIdFromUid( 'pollen-composition' );
            $contaminationDescriptorId = Descriptor::getIdFromUid( 'pollen-pesticides' );

            /** @var Log $log */
            foreach ( $logs as $apiary_id => $apiary_logs ) {
                $log = $apiary_logs[0];

                $logData     = $log->data->groupBy( 'descriptor_id' );
                $composition = collect();
                $pesticides  = collect();
                $other       = collect();

                foreach ( $logData[ $pollenDescriptorId ] as $data ) {
                    $d = [
                        'amount'    => $data->data->amount,
                        'fraction'  => $data->data->fraction,
                        'reference' => $pollenReference[ $data->data->reference_id ],
                    ];
                    if ( floatval( $data->data->fraction ) < 0.1 ) {
                        $other[] = $d;
                    } else {
                        $composition[] = $d;
                    }
                }

                if ( $other->isNotEmpty() ) {
                    $composition[] = [
                        'amount'    => $other->sum( 'amount' ),
                        'fraction'  => $other->sum( 'fraction' ),
                        'reference' => [
                            'name' => 'Other',
                        ],
                    ];
                }

                foreach ( $logData[ $contaminationDescriptorId ] as $data ) {
                    if ( floatval( $data->data->value ) > 0 ) {
                        $pesticides[] = [
                            'value'     => $data->data->value,
                            'reference' => $pesticideReference[ $data->data->reference_id ],
                        ];
                    }
                }
                $this->putCache( $descriptor->uid, $log->apiary_id->id, [
                    'id'         => $log->id . '-' . $descriptor->id,
                    'descriptor' => $descriptor,
                    'data'       => [
                        'composition' => $composition->sortByDesc( 'amount' )->values()->all(),
                        'pesticides'  => $pesticides->sortByDesc( 'value' )->values()->all(),
                    ],
                ] );
            }
        }
    }
}
