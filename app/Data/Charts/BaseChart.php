<?php namespace App\Data\Charts;


use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use Cache;
use Carbon\Carbon;
use Illuminate\Support\Collection;

abstract class BaseChart
{

    /**
     * The amount of time, we should keep data in cache.
     * We keep the cache for a week but try to refresh it every day.
     *
     * @var int
     */
    protected $ttl = 60 * 60 * 24 * 7;

    /**
     * Get value unit based on descriptor.
     *
     * @param Descriptor $descriptor
     *
     * @return string|null
     * @todo: Add localizations.
     *
     */
    protected function getUnit( Descriptor $descriptor ): ?string
    {
        switch ( $descriptor->uid ) {
            case 'hive-weight':
            case 'hive-production':
                return 'kg';
            case 'hive-temperature':
            case 'hive-temperature-daily':
            case 'temperature':
            case 'temperature-daily':
                return '°C';
            case 'relative-humidity':
            case 'hive-relative-humidity':
                return '%';
            case 'rain':
                return 'l';
            case 'solar-radiation':
                return 'watt/m²';
            case 'wind-speed':
                return 'm/s';
            case 'maximum-available-flight-time':
                return 'min/day';
            default:
                return '';
        }
    }

    /**
     * Save results to Cache using predefined keys.
     *
     * @param string     $type
     * @param int|string $id
     * @param array      $data
     */
    protected function putCache( string $type, $id, array $data ): void
    {
        if ( $this->ttl == 0 ) {
            Cache::forever( "graph-{$type}-$id", $data );
        } else {
            Cache::put( "graph-{$type}-$id", $data, $this->ttl );
        }
    }

    /**
     * Gets historic data for current location.
     *
     * @param string     $id
     * @param Descriptor $descriptor
     * @param string     $type One of 'yearly', 'monthly', 'weekly' or 'daily'.
     * @param Log[]      $logs
     *
     * @return array
     */
    protected function getHistoric( string $id, Descriptor $descriptor, string $type, Collection $logs, $continuous = false ): array
    {
        // We need to aggregate data, as we might have multiple logs per time frame.
        $grouped_logs = $logs->groupBy( function ( Log $log ) use ( $type ) {
            switch ( $type ) {
                case 'daily':
                    return $log->raw_date;
                case 'weekly':
                    return $log->date->format( 'o-W' );
                case 'monthly':
                    return $log->date->format( 'Y-m' );
                case 'yearly':
                default:
                    return $log->year;
            }
        } )->sortKeys();

        // Aggregated data based on given Descriptor.
        $labels = collect();
        $data   = collect();
        foreach ( $grouped_logs as $label => $group ) {
            $labels[] = $label;
            $data[]   = $this->aggregate( $group, $descriptor );
        }

        $sample = $data->whereNotNull();
        if ( $sample->isEmpty() ) {
            $chartType = 'empty';
        } else if ( ! is_array( $sample->first() ) ) {
            $chartType = 'line';
        } else if ( count( $sample->first() ) == 2 && is_a( $sample->first()[0], Carbon::class ) ) {
            $chartType = 'boolean';
        } else if ( count( $sample->first() ) == 2 ) {
            $chartType = 'area';
        } else if ( count( $sample->first() ) >= 3 ) {
            $chartType = 'complex';
        }
        // Add empty values for missing dates
        if ( $chartType != 'empty' ) {
            $combined = collect( array_combine( $labels->toArray(), $data->toArray() ) );
            if ( ! $continuous ) {
                $this->populateValues( $combined, $labels->first(), $labels->last(), $type );
            } else {
                $combined = $combined->whereNotNull();
            }
        } else {
            $combined = collect( [] );
        }

        return [
            'id'         => $id . '-' . $descriptor->id,
            'descriptor' => $descriptor->toArray(),
            'data'       => $combined->toArray(),
            'type'       => $chartType,
            'continuous' => $this->getContinuous( $descriptor ),
            'domains'    => [
                'x' => [
                    $labels->first(),
                    $labels->last(),
                ],
                'y' => $this->getYDomain( $data, $descriptor ),
            ],
            'unit'       => $this->getUnit( $descriptor ),
        ];
    }


    /**
     * Aggregate events based on
     *
     * @param string                  $id
     * @param Collection|Descriptor[] $descriptor
     * @param string                  $type One of 'yearly', 'monthly', 'weekly' or 'daily'.
     * @param Collection              $logs
     */
    protected function getEvents( string $id, Collection $descriptors, string $type, Collection $logs ): array
    {
        $grouped_logs = $logs->groupBy( function ( Log $log ) use ( $type ) {
            switch ( $type ) {
                case 'daily':
                    return $log->raw_date;
                case 'weekly':
                    return $log->date->format( 'o' ) . '-' . $log->date->format( 'W' );
                case 'monthly':
                    return $log->date->format( 'Y' ) . '-' . $log->date->format( 'm' );
                case 'yearly':
                default:
                    return $log->year;
            }
        } );
        $data         = [];
        // For each time frame...
        foreach ( $grouped_logs as $date => $group ) {
            $groupData = [];

            // For each Log within a day
            foreach ( $group as $log ) {
                $filteredData = $log->data->whereIn( 'descriptor_id', $descriptors->pluck( 'id' ) );
                if ( $filteredData->isEmpty() ) {
                    continue;
                }
                // For each Event within a Log
                foreach ( $filteredData as $dataItem ) {
                    $groupData[] = [
                        $dataItem->data->description,
                        empty( $dataItem->data->value ) ? null : $dataItem->data->value,
                        $dataItem->data->unit,
                    ];
                }
            }
            if ( count( $groupData ) ) {
                $data[ $date ] = $groupData;
            }
        }

        return [
            'id'      => $id . '-events',
            'data'    => $data,
            'type'    => 'events',
            'domains' => [
                'x' => [
                    $logs->first()->raw_date,
                    $logs->last()->raw_date,
                ],
            ],
        ];
    }

    /**
     * Aggregate data based on descriptor.
     *
     * @param Collection $logs
     * @param Descriptor $descriptor
     *
     * @return array|string|null
     */
    protected function aggregate( Collection $logs, Descriptor $descriptor )
    {
        $dataValues = $logs->flatMap->data->filter( function ( Data $value ) use ( $descriptor ) {
            return $value->descriptor_id == $descriptor->id;
        } )->pluck( 'data' );

        if ( $dataValues->count() > 1 ) {
            switch ( $descriptor->uid ) {
                case 'rain':
                case 'solar-radiation':
                case 'hive-metabolic-resting-state' :
                case 'maximum-available-flight-time':
                    return bcsum( $dataValues->pluck( 'value' ) );
                case 'wind-speed':
                case 'wind-direction':
                    return bcmax( $dataValues->pluck( 'value' ) );
                case 'temperature-daily':
                case 'hive-temperature-daily':
                    return [
                        bcmedian( $dataValues->pluck( 'value_from' ) ),
                        bcmedian( $dataValues->pluck( 'value_to' ) )
                    ];
                case 'hive-weight':
                    return $this->getConfidenceInterval( $dataValues->pluck( 'value' ) );
                case 'hive-production-period':
                    return [
                        $dataValues->pluck( 'value_from' )[0],
                        $dataValues->pluck( 'value_to' )->max()
                    ];
                default:
                    // Otherwise always use Median, as its less biased compared to others.
                    return bcmedian( $dataValues->pluck( 'value' ) );
            }
        } elseif ( $dataValues->count() == 1 ) {
            return $dataValues->first()->value;
        } else {
            return null;
        }
    }

    /**
     * Check if the chart should be continuous.
     *
     * @param Descriptor $descriptor
     *
     * @return bool
     */
    protected function getContinuous( Descriptor $descriptor ): bool
    {
        switch ( $descriptor->uid ) {
            case 'hive-population':
            case 'brood-cells':
            case 'beebread-cells':
                return true;
            default:
                return false;
        }
    }

    /**
     * Calculate provided values confidence interval and return lower, upper and average values.
     *
     * @param Collection $values
     *
     * @return array
     */
    protected function getConfidenceInterval( Collection $values ): array
    {
        $average    = bcaverage( $values );
        $confidence = bcadd( $average, bcmul( confidenceIntervalConstant( '95' ), bcdiv( bcstandarddeviation( $values ), sqrt( $values->count() ) ) ) );

        return [
            bcsub( $average, $confidence ),
            $average,
            bcadd( $average, $confidence ),
        ];
    }


    /**
     * Get data domain for Y axis, based on supplied descriptor.
     *
     * @param Collection $data
     * @param Descriptor $descriptor
     *
     * @return array|int[]
     */
    protected function getYDomain( Collection $data, Descriptor $descriptor ): array
    {
        if ( $descriptor->uid == 'hive-weight' && is_array( $data->whereNotNull()->first() ) ) {
            return [ 0, bcmax( $data->pluck( 2 ) ) ];
        }
        switch ( $descriptor->uid ) {
            // Min should be determined by first item in the array and max by the second item in the array.
            case 'temperature-daily':
            case 'hive-temperature-daily':
                return [ bcmin( $data->pluck( 0 )->whereNotNull() ), bcmax( $data->pluck( 1 )->whereNotNull() ) ];
            // Percentage based domain
            case 'relative-humidity':
            case 'hive-relative-humidity':
                return [ 0, 100 ];
            // Always start with 0.
            case 'hive-weight':
            case 'solar-radiation':
            case 'rain':
            case 'wind-speed':
            case 'hive-population':
            case 'brood-cells':
            case 'beebread-cells':
            case 'hive-metabolic-resting-state':
            case 'maximum-available-flight-time':
                return [ 0, bcmax( $data ) ];
            // Boolean representation, either its there or it isnt.
            case 'hive-production-period':
                return [ 0, 1 ];
            default:
                return bcextent( $data );
        }
    }


    /**
     * Iterates over the resulting data array and adds null values to missing dates between $start and $end.
     * Null values allow our chart generator to create gaps for missing data.
     * Additionally  expands Date ranges.
     *
     * @param Collection $data
     * @param string     $start
     * @param string     $end
     * @param string     $type
     */
    protected function populateValues( Collection &$data, string $start, string $end, string $type ): void
    {
        switch ( $type ) {
            case 'daily':
                $format = 'Y-m-d';
                break;
            case 'weekly':
                $format = 'o-W';
                break;
            case 'monthly':
                $format = 'Y-m';
                break;
            case 'yearly':
            default:
                $format = 'Y';
        }

        $date         = Carbon::createFromFormat( $format, $start );
        $last         = Carbon::createFromFormat( $format, $end );
        $expand_until = null;

        while ( $date != $last ) {
            $key = $date->format( $format );

            if ( $data->has( $key ) && is_array( $data[ $key ] ) && is_a( $data[ $key ][0], Carbon::class ) ) {
                $expand_until = $data[ $key ][1];
                $data[ $key ] = 1;
            } else if ( isset( $expand_until ) && $date <= $expand_until ) {
                $data[ $key ] = 1;
            } elseif ( ! $data->has( $key ) ) {
                $data[ $key ] = null;
            }

            switch ( $type ) {
                case 'daily':
                    $date->addDay();
                    break;
                case 'weekly':
                    $date->addWeek();
                    break;
                case 'monthly':
                    $date->addMonth();
                    break;
                case 'yearly':
                default:
                    $date->addYear();
            }
        }
        $data = $data->sortKeys();
    }

    /**
     * The Chart generation entry point.
     */
    abstract public function run(): void;

}
