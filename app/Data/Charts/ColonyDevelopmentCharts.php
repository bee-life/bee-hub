<?php namespace App\Data\Charts;

use App\Models\Logs\Data\DataId;
use App\Models\MetaData\Descriptor;
use App\Models\MetaData\Project;


class ColonyDevelopmentCharts extends BaseChart
{
    /**
     * The amount of time, we should keep data in cache.
     * We keep the cache for a week but try to refresh it every day.
     *
     * @var int
     */
    protected $ttl = 0;

    /**
     * Preload Colony Development data for D3 graph display.
     *
     * @see resources/js/components/charts/logs/HiveWeightComponent.vue
     */
    public function run(): void
    {
        Project::getIdsFromUids( [ 'corteva-colony-development', 'efsa-mustb-2020', 'eubeeppp' ], true );
        $projectsIds = Project::getIdsFromUids( [ 'corteva-colony-development', 'efsa-mustb-2020', 'eubeeppp' ], true );
        $ids         = DataId::whereIn( 'project_id', $projectsIds->toArray() )->get();

        $descriptors = Descriptor::whereIn( 'uid', [
            'hive-population',
            'brood-cells',
            'beebread-cells',
        ] )->with( [ 'category' ] )->get();

        /** @var DataId $id */
        foreach ( $ids as $id ) {
            // Preload datatypes for faster processing
            $logs = $id->logs()->with( [
                'data',
                'data.data' => function ( $q ) {
                    $q->without( [ 'region', 'region.parent', 'region.parent.parent', 'region.parent.parent.parent' ] );
                },
            ] )
                       ->whereNull( 'time' )
                       ->orderBy( 'date' )->get();

            if ( $id->hasParent() ) {
                $logs = $logs->concat( $id->parent->apiaryLogs()->with( [
                    'data',
                    'data.data' => function ( $q ) {
                        $q->without( [ 'region', 'region.parent', 'region.parent.parent', 'region.parent.parent.parent' ] );
                    },
                ] )
                                                  ->whereNull( 'hive_id' )
                                                  ->whereNull( 'time' )
                                                  ->orderBy( 'date' )->get() );
            }

            if ( $logs->isNotEmpty() ) {
                foreach ( $descriptors as $descriptor ) {
                    $data = $this->getHistoric( $id->id, $descriptor, 'daily', $logs, true );

                    if ( $data['type'] != 'empty' ) {
                        $this->putCache( $descriptor->uid, $id->id, $data );
                    }
                }
            }
        }
    }
}
