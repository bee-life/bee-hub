<?php namespace App\Data\Charts;

use App\Models\MetaData\Descriptor;


class SocioEconomicCharts extends BaseChart
{
    /**
     * The amount of time, we should keep data in cache.
     * We keep the cache for a week but try to refresh it every day.
     *
     * @var int
     */
    protected $ttl = 0;


    /**
     * Preload SocioEconomic charts.
     *
     * @see resources/js/components/charts/logs/HiveWeightComponent.vue
     */
    public function run(): void
    {
        $descriptors = Descriptor::whereIn( 'uid', [
            'livestock-beehives',
            'production-honey-tonnes',
            'prices-honey',
            'production-beeswax-tonnes',
            'prices-beeswax',
        ] )->with( [ 'category' ] )->get();


        foreach ( $descriptors as $descriptor ) {
            $allLogs = $descriptor->logs()->with( [ 'data', 'data.data' ] )->orderBy( 'year' )->get();

            if ( $allLogs->isEmpty() ) {
                continue;
            }

            $locations = $allLogs->groupBy( 'location.id' );

            foreach ( $locations as $logs ) {
                $location = $logs->first()->location;

                $data = $this->getHistoric( $location->id, $descriptor, 'yearly', $logs );

                if ( $data['type'] != 'empty' ) {
                    $this->putCache( $descriptor->uid, $location->id, $data );
                }

            }
        }
    }
}
