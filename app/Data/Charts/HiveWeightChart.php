<?php namespace App\Data\Charts;

use App\Models\Logs\Data\DataId;
use App\Models\MetaData\Descriptor;
use App\Models\MetaData\Project;


class HiveWeightChart extends BaseChart
{

    /**
     * The amount of time, we should keep data in cache.
     * We keep the cache for a week but try to refresh it every day.
     *
     * @var int
     */
    protected $ttl = 0;

    /**
     * Preload Hive Scales data for D3 graph display.
     * @TODO:
     *      - Implement Management Events.
     *
     * @see resources/js/components/charts/logs/HiveWeightComponent.vue
     */
    public function run(): void
    {
        $projectsIds = Project::getIdsFromUids( [ 'cari-scales', 'efsa-mustb-2020', 'eubeeppp', 'corteva-colony-development' ], true );
        $ids         = DataId::whereIn( 'project_id', $projectsIds->toArray() )->get();

        $descriptors = Descriptor::whereIn( 'uid', [
            'hive-weight',
            'hive-temperature-daily',
            'relative-humidity',
            'hive-relative-humidity',
            'temperature-daily',
            'rain',
            'solar-radiation',
            'wind-speed',
            'hive-production',
            'hive-production-period',
            'hive-metabolic-resting-state',
            'maximum-available-flight-time',
        ] )->with( [ 'category' ] )->get();

        $eventDescriptors = Descriptor::whereIn( 'uid', [
            'event-hive-management',
            'event-scale-weight',
        ] )->with( [ 'category' ] )->get();

        /** @var DataId $id */
        foreach ( $ids as $id ) {
            // Preload datatypes for faster processing
            $logs = $id->logs()->with( [
                'data',
                'data.data' => function ( $q ) {
                    $q->without( [ 'region', 'region.parent', 'region.parent.parent', 'region.parent.parent.parent' ] );
                },
            ] )
                       ->whereNull( 'time' )
                       ->orderBy( 'date' )->get();

            if ( $id->hasParent() ) {
                $logs = $logs->concat( $id->parent->apiaryLogs()->with( [
                    'data',
                    'data.data' => function ( $q ) {
                        $q->without( [ 'region', 'region.parent', 'region.parent.parent', 'region.parent.parent.parent' ] );
                    },
                ] )
                                                  ->whereNull( 'hive_id' )
                                                  ->whereNull( 'time' )
                                                  ->orderBy( 'date' )->get() );
            }

            if ( $logs->isNotEmpty() ) {
                foreach ( $descriptors as $descriptor ) {
                    $data = $this->getHistoric( $id->id, $descriptor, 'daily', $logs );

                    if ( $data['type'] != 'empty' ) {
                        $this->putCache( $descriptor->uid, $id->id, $data );
                    }
                }

                $data = $this->getEvents( $id->id, $eventDescriptors, 'daily', $logs );

                if ( ! empty( $data['data'] ) ) {
                    $this->putCache( 'events', $id->id, $data );
                }
            }
        }
    }
}
