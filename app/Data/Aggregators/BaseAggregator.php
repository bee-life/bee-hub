<?php namespace App\Data\Aggregators;

use App\Models\Logs\Data\DataAggregate;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\MetaData\DescriptorCategory;
use Carbon\Carbon;
use Illuminate\Support\Collection;

abstract class BaseAggregator
{
    protected Collection $descriptors;
    protected Collection $descriptorCategories;


    public function __construct()
    {
        $this->descriptors          = collect();
        $this->descriptorCategories = collect();
    }

    abstract public function run(): void;

    /**
     * Insert Aggregate row into database.
     *
     * @param array       $data
     * @param int[]|array $project_ids
     *
     * @return DataAggregate
     */
    public function insert( array $data, array $project_ids ): DataAggregate
    {
        $aggregate = DataAggregate::create( $data );
        $aggregate->projects()->sync( $project_ids );

        return $aggregate;
    }

    /**
     * Created a Log based on date string.
     *
     * @param int      $project_id
     * @param string   $date_string The string representing the date and/or time of logged Log.
     * @param string   $format      Datetime format of the $date_string.
     * @param bool     $time        Optional, set false of exclude time information.
     * @param int|null $apiary_id
     * @param int|null $hive_id
     *
     * @return Log
     */
    protected function insertLog( int $project_id, string $date_string, string $format, bool $time = true, int $apiary_id = null, ?int $hive_id = null ): Log
    {
        if ( $format == 'Y' ) {
            $data = [
                'year'       => $date_string,
                'project_id' => $project_id,
            ];
        } else {
            $datetime = Carbon::createFromFormat( $format, $date_string );

            $data = [
                'year'       => $datetime->year,
                'date'       => $datetime->format( 'Y-m-d' ),
                'time'       => $time ? $datetime->format( 'H:i:s' ) : null,
                'project_id' => $project_id,
            ];
        }

        $data['apiary_id'] = $apiary_id ?? null;
        $data['hive_id']   = $hive_id ?? null;

        return Log::firstOrCreate( $data );
    }

    /**
     * Create new Datatypes based on inputted array.
     * Datatypes which slug already exists is just added to the connector.
     * Caches the results.
     *
     * @param array $data
     *
     * @return Descriptor
     */
    protected function getDescriptor( array $data ): Descriptor
    {
        if ( $this->descriptors->has( $data['uid'] ) ) {
            return $this->descriptors[ $data['uid'] ];
        }

        if ( $this->descriptorCategories->isEmpty() ) {
            $this->descriptorCategories = DescriptorCategory::all()->keyBy( 'uid' );
        }

        if ( empty( $data['uid'] ) ) {
            $data['uid'] = $data['slug'];
        }

        if ( ! empty( $item['category'] ) && ! empty( $this->descriptorCategories[ $item['category'] ] ) ) {
            $item['category_id'] = $this->descriptorCategories[ $item['category'] ]->id;
            unset( $item['category'] );
        }

        return $this->descriptors[ $data['uid'] ] = Descriptor::firstOrCreate( [ 'uid' => $data['uid'] ], $data );
    }
}
