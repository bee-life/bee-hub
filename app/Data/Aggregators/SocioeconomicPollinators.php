<?php namespace App\Data\Aggregators;

use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\MetaData\DescriptorCategory;
use App\Models\MetaData\Project;
use App\Models\Origins\Publication;
use App\Models\References\CropSpecies;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Str;

class SocioeconomicPollinators extends BaseAggregator
{

    /**
     * This file contains algorithms related to Socioeconomic data on pollinators report, published on 29. 4. 2021, by Rubinigg Michael, BeeLife & Biene Österreich.
     */
    public function run(): void
    {
        $this->processData();
    }

    /**
     * This functions processes calculations:
     */
    protected function processData(): void
    {
        $targetProject = Project::where( 'uid', 'eubeeppp' )->first();
        $originProject = Project::where( 'uid', 'social-economic-statistics' )->first();

        $methodology = Publication::firstOrCreate( [
            'title'            => 'Socioeconomic data on pollinators',
            'author'           => 'Rubinigg Michael, BeeLife & Biene Österreich',
            'author_short'     => 'Rubinigg M.',
            'year'             => '2021',
            'medium'           => 'Bee Hub Report',
            'website'          => '/reports/socioeconomic-data-on-pollinators',
            'website_date'     => Carbon::today(),
            'publication_type' => 'Report',
        ] );

        if ( empty( $targetProject ) || empty( $originProject ) || empty( $methodology ) ) {
            return;
        }

        /**
         * Economic value of pollination for a crop per EU member state per year:
         */
        $EV_ixy      = collect();
        $PEVmin_ixy  = collect();
        $PEVmax_ixy  = collect();
        $PEVavg_ixy  = collect();
        $RPEVavg_ixy = collect();

        /**
         * And partial pollinated area:
         */
        $PA_xy = collect();
        $B_xy  = collect();

        $pricings   = DescriptorCategory::whereUid( 'socioeconomic-prices' )->with( [ 'descriptors' ] )->first()->descriptors->keyBy( function ( Descriptor $item ) {
            return substr( $item->uid, strpos( $item->uid, '-' ) + 1 );
        } );
        $production = DescriptorCategory::whereUid( 'socioeconomic-production' )->with( [ 'descriptors' ] )->first()->descriptors->keyBy( function ( Descriptor $item ) {
            return substr( $item->uid, strpos( $item->uid, '-' ) + 1, - 7 );
        } );

        $productionArea = DescriptorCategory::whereUid( 'socioeconomic-production-area' )->with( [ 'descriptors' ] )->first()->descriptors->keyBy( function ( Descriptor $item ) {
            $array = explode( '-', $item->uid );
            array_pop( $array );
            array_shift( $array );

            return implode( '-', $array );
        } );

        $beehives = Descriptor::whereUid( 'livestock-beehives' )->first();

        $years   = $originProject->logs()->select( 'year' )->distinct()->orderBy( 'year' )->get()->pluck( 'year' );
        $species = CropSpecies::with( [ 'dependency' ] )->whereHas( 'dependency' )->get()->keyBy( function ( CropSpecies $item ) {
            return Str::slug( $item->name );
        } );

        foreach ( $years as $year_y ) {
            $allLogs = $originProject->logs()->with( [ 'data', 'data.data', 'locations', 'locations.location' ] )->where( 'year', $year_y )
                                     ->whereHas( 'data', function ( $q ) use ( $pricings, $production, $beehives ) {
                                         $q->whereIn( 'descriptor_id', $pricings->pluck( 'id' ) )
                                           ->orWhereIn( 'descriptor_id', $production->pluck( 'id' ) )
                                           ->orWhere( 'descriptor_id', $beehives->id );
                                     } )
                                     ->get()->where( 'precision', 0 )->groupBy( 'location.location.nuts_id' );


            foreach ( $allLogs as $country_x => $logs ) {
                $data = $logs->flatmap( function ( Log $log ) {
                    return $log->data;
                } )->keyBy( 'descriptor_id' );

                foreach ( $species as $crop_i => $spec ) {
                    if ( $pricings->has( $crop_i ) && $data->has( $pricings[ $crop_i ]->id ) && $production->has( $crop_i ) && $data->has( $production[ $crop_i ]->id ) ) {
                        $EV_value = bcmul( $data[ $pricings[ $crop_i ]->id ]->value, $data[ $production[ $crop_i ]->id ]->value );
                        if ( bccomp( $EV_value, '0.0' ) == 0 ) {
                            continue;
                        }
                        $this->saveValue( $EV_ixy, $EV_value, $crop_i, $country_x, $year_y );

                        $PEVmin_value  = bcmul( $EV_value, $spec->dependency->dependency_from );
                        $PEVmax_value  = bcmul( $EV_value, $spec->dependency->dependency_to );
                        $PEVavg_value  = bcdiv( bcadd( $PEVmin_value, $PEVmax_value ), 2 );
                        $RPEVavg_value = bcdiv( bcmul( $PEVavg_value, 100 ), $EV_value );

                        $this->saveValue( $PEVmin_ixy, $PEVmin_value, $crop_i, $country_x, $year_y );
                        $this->saveValue( $PEVmax_ixy, $PEVmax_value, $crop_i, $country_x, $year_y );
                        $this->saveValue( $PEVavg_ixy, $PEVavg_value, $crop_i, $country_x, $year_y );
                        $this->saveValue( $RPEVavg_ixy, $RPEVavg_value, $crop_i, $country_x, $year_y );
                    }

                    if ( $productionArea->has( $crop_i ) && $data->has( $productionArea[ $crop_i ]->id ) && $data->has( $beehives->id ) ) {
                        $this->addValue( $PA_xy, $data[ $productionArea[ $crop_i ]->id ]->value, $country_x, $year_y );
                    }
                }
                if ( $productionArea->has( $crop_i ) && $data->has( $productionArea[ $crop_i ]->id ) && $data->has( $beehives->id ) ) {
                    $this->saveValue( $B_xy, $data[ $beehives->id ]->value, $country_x, $year_y );
                }
            }
        }

        /**
         * Economic value of pollination per crop per year
         */
        $EV_iy      = collect();
        $PEVmin_iy  = collect();
        $PEVmax_iy  = collect();
        $PEVavg_iy  = collect();
        $RPEVavg_iy = collect();
        $RPEVavg_i2 = collect();

        foreach ( $EV_ixy as $crop_i => $countries ) {
            foreach ( $countries as $country_x => $years ) {
                foreach ( $years as $year_y => $value ) {
                    $this->addValue( $EV_iy, $value, $crop_i, $year_y );
                    $this->addValue( $PEVmin_iy, $PEVmin_ixy[ $crop_i ][ $country_x ][ $year_y ], $crop_i, $year_y );
                    $this->addValue( $PEVmax_iy, $PEVmax_ixy[ $crop_i ][ $country_x ][ $year_y ], $crop_i, $year_y );
                }
            }
        }
        foreach ( $EV_iy as $crop_i => $years ) {
            foreach ( $years as $year_y => $value ) {
                $PEVavg_value = bcdiv( bcadd( $PEVmin_iy[ $crop_i ][ $year_y ], $PEVmax_iy[ $crop_i ][ $year_y ] ), 2 );
                $RPEV_value   = bcdiv( bcmul( $PEVavg_value, 100 ), $value );


                $this->saveValue( $PEVavg_iy, $PEVavg_value, $crop_i, $year_y );
                $this->saveValue( $RPEVavg_iy, $RPEV_value, $crop_i, $year_y );

                if ( $year_y >= 2014 && $year_y <= 2018 ) {
                    $this->addValue( $RPEVavg_i2, bcdiv( $RPEV_value, 5 ), $crop_i );
                }
            }
        }

        /**
         * Economic value of pollination per EU member state per year
         */
        $EV_xy      = collect();
        $PEVmin_xy  = collect();
        $PEVmax_xy  = collect();
        $PEVavg_xy  = collect();
        $RPEVavg_xy = collect();
        $RPEVavg_x2 = collect();

        foreach ( $EV_ixy as $crop_i => $countries ) {
            foreach ( $countries as $country_x => $years ) {
                foreach ( $years as $year_y => $value ) {
                    $this->addValue( $EV_xy, $value, $country_x, $year_y );
                    $this->addValue( $PEVmin_xy, $PEVmin_ixy[ $crop_i ][ $country_x ][ $year_y ], $country_x, $year_y );
                    $this->addValue( $PEVmax_xy, $PEVmax_ixy[ $crop_i ][ $country_x ][ $year_y ], $country_x, $year_y );
                }
            }
        }
        foreach ( $EV_xy as $country_x => $years ) {
            foreach ( $years as $year_y => $value ) {
                $PEVavg_value = bcdiv( bcadd( $PEVmin_xy[ $country_x ][ $year_y ], $PEVmax_xy[ $country_x ][ $year_y ] ), 2 );
                $RPEV_value   = bcdiv( bcmul( $PEVavg_value, 100 ), $value );

                $this->saveValue( $PEVavg_xy, $PEVavg_value, $country_x, $year_y );
                $this->saveValue( $RPEVavg_xy, $RPEV_value, $country_x, $year_y );

                if ( $year_y >= 2014 && $year_y <= 2018 ) {
                    $this->addValue( $RPEVavg_x2, bcdiv( $RPEV_value, 5 ), $country_x );
                }
            }
        }

        /**
         * Economic value of pollination per year
         */

        $EV_y      = collect();
        $PEVmin_y  = collect();
        $PEVmax_y  = collect();
        $PEVavg_y  = collect();
        $RPEVavg_y = collect();
        $RPEVmin_y = collect();
        $RPEVmax_y = collect();

        foreach ( $EV_ixy as $crop_i => $countries ) {
            foreach ( $countries as $country_x => $years ) {
                foreach ( $years as $year_y => $value ) {
                    $this->addValue( $EV_y, $value, $year_y );
                    $this->addValue( $PEVmin_y, $PEVmin_ixy[ $crop_i ][ $country_x ][ $year_y ], $year_y );
                    $this->addValue( $PEVmax_y, $PEVmax_ixy[ $crop_i ][ $country_x ][ $year_y ], $year_y );
                }
            }
        }
        foreach ( $EV_y as $year_y => $value ) {
            $PEVavg_value  = bcdiv( bcadd( $PEVmin_y[ $year_y ], $PEVmax_y[ $year_y ] ), 2 );
            $RPEVavg_value = bcdiv( bcmul( $PEVavg_value, 100 ), $value );
            $RPEVmin_value = bcdiv( bcmul( $PEVmin_y[ $year_y ], 100 ), $value );
            $RPEVmax_value = bcdiv( bcmul( $PEVmax_y[ $year_y ], 100 ), $value );

            $this->saveValue( $PEVavg_y, $PEVavg_value, $country_x, $year_y );
            $this->saveValue( $RPEVavg_y, $RPEVavg_value, $country_x, $year_y );
            $this->saveValue( $RPEVmin_y, $RPEVmin_value, $country_x, $year_y );
            $this->saveValue( $RPEVmax_y, $RPEVmax_value, $country_x, $year_y );
        }

        /**
         * Pollinated area:
         */
        //$PA_xy  = collect(); // Already calculated
        $BPA_xy = collect();
        $BPA_x2 = collect();
        foreach ( $PA_xy as $country_x => $years ) {
            foreach ( $years as $year_y => $value ) {
                $PA_value = bcdiv( $B_xy[ $country_x ][ $year_y ], $value );

                $this->saveValue( $BPA_xy, $PA_value, $country_x, $year_y );

                if ( $year_y >= 2014 && $year_y <= 2018 ) {
                    $this->addValue( $BPA_x2, bcdiv( $PA_value, 5 ), $country_x );
                }
            }
        }

        /**
         * TODO:
         * Implement saving of data and visualizations.
         */
    }

    /**
     * A recursive variable function to save nth sized item within an array.
     *
     * @param Collection $array
     * @param int|string $value
     * @param int|string ...$keys
     */
    protected function saveValue( Collection &$array, $value, ...$keys ): void
    {
        $this->processValue( 'save', $array, $value, ...$keys );
    }

    /**
     * A recursive variable function to add nth sized item within an array.
     *
     * @param Collection $array
     * @param int|string $value
     * @param int|string ...$keys
     */
    protected function addValue( Collection &$array, $value, ...$keys ): void
    {
        $this->processValue( 'add', $array, $value, ...$keys );
    }

    /**
     * A recursive variable function to subtract nth sized item within an array.
     *
     * @param Collection $array
     * @param int|string $value
     * @param int|string ...$keys
     */
    protected function subValue( Collection &$array, $value, ...$keys ): void
    {
        $this->processValue( 'sub', $array, $value, ...$keys );
    }


    /**
     * A recursive variable function to multiply nth sized item within an array.
     *
     * @param Collection $array
     * @param int|string $value
     * @param int|string ...$keys
     */
    protected function mulValue( Collection &$array, $value, ...$keys ): void
    {
        $this->processValue( 'mul', $array, $value, ...$keys );
    }

    /**
     * A recursive variable function to divide nth sized item within an array.
     *
     * @param Collection $array
     * @param int|string $value
     * @param int|string ...$keys
     */
    protected function divValue( Collection &$array, $value, ...$keys ): void
    {
        $this->processValue( 'div', $array, $value, ...$keys );
    }

    /**
     * A recursive variable function to either save, add, subtract, multiply or divide nth sized item within an array.
     *
     * @param string     $type One of: save, add, sub, mul, div.
     * @param Collection $array
     * @param int|string $value
     * @param int|string ...$keys
     */
    protected function processValue( $type, Collection &$array, $value, ...$keys )
    {
        if ( count( $keys ) > 1 ) {
            if ( ! $array->has( $keys[0] ) ) {
                $array[ $keys[0] ] = collect();
            }
            $this->processValue( $type, $array[ array_shift( $keys ) ], $value, ...$keys );
        } else {
            if ( $type == 'save' || ! $array->has( $keys[0] ) ) {
                $array[ $keys[0] ] = $value;
            } else {
                $func              = 'bc' . $type;
                $array[ $keys[0] ] = $func( $array[ $keys[0] ], $value );
            }
        }
    }
}
