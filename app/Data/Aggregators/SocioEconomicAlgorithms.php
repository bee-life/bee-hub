<?php namespace App\Data\Aggregators;

use App\Models\Locations\Location;
use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\MetaData\DescriptorCategory;
use App\Models\MetaData\Project;
use App\Models\Origins\Methodology;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class SocioEconomicAlgorithms extends BaseAggregator
{

    /**
     * This file contains algorithms related to Socioeconomic data, gathered from FAO.
     */
    public function run(): void
    {
        $this->processProductionData();
        $this->processLivestockData();
    }

    /**
     * This functions processes all production data:
     * - Calculates the production value per area for accurate comparison between Locations.
     * - Aggregates data to lower precisions. where applicable.
     */
    protected function processProductionData(): void
    {
        $project     = Project::where( 'uid', 'eubeeppp' )->first();
        $descriptors = Descriptor::where( 'category_id', DescriptorCategory::getIdFromUid( 'socioeconomic-production' ) )
                                 ->with( [ 'projects' ] )
                                 ->whereHas( 'projects', function ( $q ) {
                                     $q->where( 'public', true );
                                 } )
                                 ->get();

        $methodology = Methodology::where( 'uid', 'eubeeppp-production-per-area' )->first();

        if ( $descriptors->isEmpty() || empty( $project ) || empty( $methodology ) ) {
            return;
        }

        $category = DescriptorCategory::firstOrCreate( [ 'uid' => 'socioeconomic-production-per-surface' ], [
            'name'        => 'Goods Production per km²',
            'slug'        => 'goods-production-per-surface',
            'description' => 'Production in kg per km² per year',
            'parent_id'   => DescriptorCategory::getIdFromUid( 'socioeconomic' ),
        ] );


        /**
         * Preload region statistics for later calculations
         */
        $projectIds = $descriptors->pluck( 'projects' )->flatten()->pluck( 'id' )->unique();
        $years      = Log::select( [ 'year' ] )
                         ->whereIn( 'project_id', $projectIds )
                         ->whereHas( 'data', function ( Builder $query ) use ( $descriptors ) {
                             $query->whereIn( 'descriptor_id', $descriptors->pluck( 'id' ) );
                         } )->distinct()->orderBy( 'year' )->get()->pluck( 'year' );

        foreach ( $years as $year ) {
            $logs = Log::where( 'year', $year )->whereIn( 'project_id', $projectIds )->with( [ 'data', 'data.data', 'locations', 'locations.location' ] )->get();
            //$data = collect();
            foreach ( $descriptors as $descriptor ) {
                $targetDescriptor = $this->getDescriptor( [
                    'name'        => $descriptor->name . ' per surface',
                    'slug'        => $descriptor->slug . '-per-surface',
                    'uid'         => $descriptor->uid . '-per-surface',
                    'description' => 'Production in kg per km²',
                    'public'      => true,
                    'since'       => '1.2',
                    'category_id' => $category->id,
                    'table_name'  => 'log_data_decimals',
                ] );

                foreach ( $logs as $log ) {
                    /** @var Data[]|Collection $data */
                    $datatypes = $log->data->keyBy( 'descriptor_id' );

                    if ( $datatypes->has( $descriptor->id ) && ! $datatypes->has( $targetDescriptor->id ) && ! empty( $log->location->location->surface ) ) {
                        /**
                         * Divide by 1.000.000 from Square meters to square Kilometers and Multiply by 1.000 from Tones to Kilograms.
                         */
                        $value = bcdiv( bcmul( $datatypes[ $descriptor->id ]->value, '1000' ), bcdiv( $log->location->location->surface, '1000000' ) );
                        $log->attachData( $targetDescriptor, [ 'value' => $value ], $methodology );
                    }
                }
            }
        }
    }

    protected function processLivestockData()
    {
        $project              = Project::where( 'uid', 'eubeeppp' )->first();
        $descriptor           = Descriptor::whereUid( 'livestock-beehives' )->first();
        $locationDescriptor   = Descriptor::whereUid( 'colonies-location' )->first();
        $livestockMethodology = Methodology::where( 'uid', 'sum' )->first();
        $livestockData        = collect();

        $projectIds = $descriptor->projects->pluck( 'id' )->unique();
        $years      = Log::select( [ 'year' ] )
                         ->whereIn( 'project_id', $projectIds )
                         ->whereHas( 'data', function ( Builder $query ) use ( $descriptor ) {
                             $query->where( 'descriptor_id', $descriptor->id );
                         } )->distinct()->orderByDesc( 'year' )->get()->pluck( 'year' );
        foreach ( $years as $year ) {
            $precisions = Log::where( 'year', $year )->whereHas( 'data', function ( Builder $query ) use ( $descriptor ) {
                $query->where( 'descriptor_id', $descriptor->id );
            } )->whereIn( 'project_id', $projectIds )->with( [ 'data', 'data.data', 'locations', 'locations.location' ] )->get()->groupBy( 'precision' );

            if ( $precisions->count() == 1 && $precisions->keys()[0] == 0 ) {
                continue;
            }

            $all = collect();
            foreach ( $precisions as $precision => $logs ) {
                foreach ( $logs as $log ) {
                    $location = $log->location->location;
                    for ( $i = $precision; $i >= 0; $i -- ) {
                        $key = $i . '-' . $location->id;
                        if ( $all->has( $key ) ) {
                            $all[ $key ][] = $log;
                        } else {
                            $all[ $key ] = collect( [ $log ] );
                        }
                        if ( $i != 0 ) {
                            $location = $location->parent;
                        }
                    }
                }
            }

            foreach ( $all as $key => $logs ) {
                [ $precision, $id ] = explode( '-', $key );
                $sum = 0;

                // Skip single value of same precision
                if ( $logs->count() == 1 && $logs[0]->precision == $precision ) {
                    continue;
                }

                foreach ( $logs as $log ) {
                    // Skip already known value
                    if ( $log->precision == $precision ) {
                        continue 2;
                    }

                    $sum = bcadd( $sum, $log->data->where( 'descriptor_id', $descriptor->id )->first()->value );
                }
                $log = Log::create( [
                    'year'       => $year,
                    'project_id' => $project->id,
                ] );

                $log->attachData( $locationDescriptor, [
                    'location_id'   => $id,
                    'location_type' => Location::getClassFromPrecision( $precision ),
                ] );

                $log->attachData( $descriptor, [ 'value' => $sum ], $livestockMethodology );
            }
        }
    }
}
