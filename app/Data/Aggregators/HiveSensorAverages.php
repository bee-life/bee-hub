<?php namespace App\Data\Aggregators;

use App\Models\Logs\Data;
use App\Models\Logs\Data\DataId;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\MetaData\Project;
use App\Models\Origins\Methodology;
use Debugbar;
use Illuminate\Support\Collection;

class HiveSensorAverages extends BaseAggregator
{
    /**
     * Calculate the various averages of Hive sensor data for each hive for each day.
     *
     * - Daily weight (last value of [hive-weight])
     * - Lowest/Highest inside temperature (daily Min/Max of [hive-temperature]),
     * - Relative humidity (daily Median? of [hive-relative-humidity]),
     *
     * @TODO: Add parameters for optimized run only (last 2 years, only live or remote projects)
     */
    public function run(): void
    {
        // Prepare a list of all locations, that have any measured value within above list.
        // We assume, these locations are part of the projects:
        // In production, we expect only the cari-scales to update over time.
        if ( ! is_production() ) {
            $projectsIds = Project::getIdsFromUids( [ 'cari-scales', 'efsa-mustb-2020', 'corteva-colony-development' ], true );
        } else {
            $projectsIds = Project::getIdsFromUids( [ 'cari-scales' ], true );
        }

        $targetProject = Project::where( 'uid', 'eubeeppp' )->first();

        $ids         = DataId::whereIn( 'project_id', $projectsIds->toArray() )->get();
        $descriptors = Descriptor::whereIn( 'uid', [ 'hive-weight', 'hive-temperature', 'hive-relative-humidity' ] )->get()->keyBy( 'uid' );

        $methodologies = Methodology::whereIn( 'uid', [ 'min-max', 'last', 'median' ] )->get()->keyBy( 'uid' );

        // Some descriptors contain aggregated data in a different format, so we need to know
        $dailyTemperatureDescriptor = Descriptor::where( 'uid', 'hive-temperature-daily' )->first();

        // Set BC Math calculation scale
        bcscale( 5 );

        foreach ( $ids as $id ) {
            Debugbar::startMeasure( $id->value, $id->value );
            // We are only interested into Hive level data.
            // We should chunk the results per year, to avoid memory issues but keep up with query performance.
            $years = $id->hiveLogs()->select( 'year' )->whereIn( 'project_id', $projectsIds )->groupBy( 'year' )->get()->pluck( 'year' )->sort();

            // Check only last two years in production
            if ( is_production() ) {
                $years = $years->take( - 2 );
            }

            foreach ( $years as $year ) {
                $allLogs = $id->hiveLogs()->whereIn( 'project_id', $projectsIds )->where( 'year', $year )->whereNotNull( 'time' )->orderBy( 'date' )->orderBy( 'time' )->with( [
                    'data',
                    'data.data'
                ] )->get()->groupBy( 'rawDate' );

                if ( $allLogs->isEmpty() ) {
                    continue;
                }
                $sample         = $allLogs->first()->first();
                $sampleApiaryId = $sample->apiary_id;
                $sampleHiveId   = $sample->hive_id;
                $sampleLocation = $sample->data->where( 'descriptor_id', getDescriptorIdByUid( 'colonies-location' ) )->first()->data()->without( [ 'location' ] )->first();

                foreach ( $allLogs as $date => $logs ) {
                    // First check if any gathered data is valid, only then save it to the database as a new Log.
                    $futureData = [];

                    foreach ( $descriptors as $descriptor ) {
                        /**
                         * Check within each Log, if the descriptor exists and return its value.
                         * Sometimes not all Logs will have data for all descriptors as expected, mainly because of input validation errors.
                         * @var Collection $data
                         */
                        $data = $logs->map( function ( Log $log ) use ( $descriptor ) {
                            if ( empty( $log->data->where( 'descriptor_id', $descriptor->id )->first() ) ) {
                                return null;
                            } else {
                                return $log->data->where( 'descriptor_id', $descriptor->id )->first()->data->raw;
                            }
                        } )->filter( function ( $value ) {
                            return isset( $value );
                        } );

                        if ( $data->isNotEmpty() ) {
                            $desc  = null;
                            $value = null;

                            switch ( $descriptor->uid ) {
                                case 'hive-temperature':
                                    $desc        = $dailyTemperatureDescriptor;
                                    $value       = bcextent( $data );
                                    $methodology = 'min-max';
                                    break;
                                case 'hive-weight':
                                    $desc        = $descriptor;
                                    $value       = $data->last();
                                    $methodology = 'last';
                                    break;
                                default:
                                    $desc        = $descriptor;
                                    $value       = bcmedian( $data );
                                    $methodology = 'median';
                            }
                            if ( isset ( $value ) ) {
                                $futureData[] = [
                                    'descriptor'  => $desc,
                                    'value'       => $value,
                                    'methodology' => $methodologies[ $methodology ],
                                ];
                            }
                        }
                    }

                    /**
                     * Create a Log only if there is any data to include.
                     */
                    if ( ! empty( $futureData ) ) {
                        /** @var Log[] $logs */
                        $log = $this->insertLog( $targetProject->id, $date, 'Y-m-d', false, $sampleApiaryId, $sampleHiveId );

                        if ( $log->wasRecentlyCreated ) {
                            Data::insert( [
                                [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'colonies-location' ), 'data_id' => $sampleLocation->id, 'data_type' => 'l' ],
                                [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'apiary-id' ), 'data_id' => $sampleApiaryId, 'data_type' => 'id' ],
                                [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'external-id' ), 'data_id' => $sampleHiveId, 'data_type' => 'id' ]
                            ] );
                        } else {
                            $log->load( [ 'data' ] );
                        }

                        foreach ( $futureData as $data ) {
                            /**
                             * Make sure the data is not existing yet.
                             */
                            if ( $log->data->where( 'descriptor_id', $data['descriptor']->id )->isEmpty() ) {
                                $log->attachData( $data['descriptor'], [ 'value' => $data['value'] ], $data['methodology'] );
                            } elseif ( ! $log->data->where( 'descriptor_id', $data['descriptor']->id )->first()->data->isEqual( $data['value'] ) ) {
                                // $log->data->where( 'descriptor_id', $data['descriptor']->id )->first()->data->logs()->detach( $log->id );
                                // $log->attachData( $data['descriptor'], [ 'value' => $data['value'] ], $data['methodology'] );
                            }
                        }
                    }
                }
            }
            Debugbar::stopMeasure( $id->value );
        }
    }
}
