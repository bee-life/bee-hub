<?php namespace App\Data\Aggregators;


use App\Models\Locations\Location;
use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\MetaData\Project;
use App\Models\Origins\Methodology;
use Illuminate\Support\Collection;

class WinterMortalityCounts extends BaseAggregator
{
    protected Collection $locations;

    public function __construct()
    {
        $this->locations = collect();
        parent::__construct();
    }


    public function run(): void
    {
        $descriptors = Descriptor::whereIn( 'uid', [ 'colony-count-autumn', 'colony-count-spring', 'winter-mortality-percentage' ] )
                                 ->with( [ 'projects' ] )
                                 ->get();
        $project     = Project::where( 'uid', 'eubeeppp' )->first();

        if ( $descriptors->isEmpty() || empty( $project ) ) {
            return;
        }

        $projects    = $descriptors->pluck( 'projects' )->flatten()->unique( 'id' );
        $descriptors = $descriptors->merge( Descriptor::whereIn( 'uid', [
            'colonies-location',
            'data-counts',
            'winter-mortality-aggregate'
        ] )->get() )->keyBy( 'uid' );
        $methodology = Methodology::whereUid( 'average' )->first();

        /**
         * For some reason using distinct or group by skips latest year, that is why we pluck it after query.
         */
        $years = Log::select( [ 'year' ] )->whereIn( 'project_id', $projects->pluck( 'id' ) )->get()->pluck( 'year' )->unique()->sort();

        /*
         * We need to calculate Winter Mortality counts for each year.
         */
        foreach ( $years as $year ) {
            \Debugbar::startMeasure( $year, $year );
            $logs = $descriptors['colony-count-autumn']->getLogs( $year, true );

            if ( $logs->isEmpty() ) {
                $logs = $descriptors['winter-mortality-percentage']->getLogs( $year, false );
            } else {
                $logs = $logs->merge($descriptors['winter-mortality-percentage' ]->getLogs( $year, false ) );
            }

            /** @var Collection[] $data */
            $data = collect();

            foreach ( $logs as $log ) {
                /** @var Data[]|Collection $data */
                $datatypes = $log->data->keyBy( 'descriptor_id' );
                if ( $datatypes->has( $descriptors['colony-count-autumn']->id ) && $datatypes->has( $descriptors['colony-count-spring']->id ) ) {
                    /**
                     * Other time we must calculate it ourselves.
                     */
                    $value = getMortalityRate( $datatypes[ $descriptors['colony-count-autumn']->id ]->value, $datatypes[ $descriptors['colony-count-spring']->id ]->value ) * 100;
                } else if ( $datatypes->has( $descriptors['winter-mortality-percentage']->id ) ) {
                    /**
                     * Sometimes we have only already calculated Winter Mortality data available
                     */

                    $value = $datatypes[ $descriptors['winter-mortality-percentage']->id ]->value;
                } else {
                    /**
                     * If we get here, we have missing data and just skip it.
                     */
                    continue;
                }

                // Filter out invalid values
                if ( $value < 0 ) {
                    // TODO: Figure out, why do we have invalid values, might need data cleanup? The problem comes from Epilobee.
                    continue;
                }

                $this->saveValues( $data, $log, $value, $this->getLocation( $log->location->location_type, $log->location->location_id ) );
            }
            \Debugbar::startMeasure( $year . 'save', 'Saving ' . $year );
            /**
             * Uses batch insertion to optimize on speed and load existing data.
             */
            $existingLogs = Log::where( 'year', $year )->whereNull( 'date' )->whereHas( 'data', function ( $q ) use ( $descriptors, $methodology ) {
                $q->where( 'descriptor_id', $descriptors['winter-mortality-aggregate']->id )->where( 'origin_id', $methodology->id )->where( 'origin_type', '1' );
            } )->with( [ 'data', 'data.data' ] )->get()->keyBy( function ( Log $log ) {
                return $log->location->location_type . '_' . $log->location->location_id;
            } );
            foreach ( $data as $location => $yearlyData ) {
                if ( $existingLogs->has( $location ) ) {
                    $log = $existingLogs[ $location ];
                } else {
                    $log = Log::create( [
                        'year'       => $year,
                        'project_id' => $yearlyData['projects']->first()->id,
                    ] );
                    $log->attachData( $descriptors['colonies-location'], [ 'location_id' => $yearlyData['location']->id, 'location_type' => get_class( $yearlyData['location'] ) ] );
                }

                if ( ! $log->hasData( $descriptors['winter-mortality-aggregate'] ) ) {
                    $log->attachData( $descriptors['winter-mortality-aggregate'], $yearlyData['data']->average(), $methodology );
                } elseif ( ! $log->data->where( 'descriptor_id', $descriptors['winter-mortality-aggregate']->id )->first()->data->isEqual( $yearlyData['data']->average() ) ) {
                    $log->data()->whereIn( 'descriptor_id', [ $descriptors['winter-mortality-aggregate']->id, $descriptors['data-counts']->id ] )->delete();
                    $log->attachData( $descriptors['winter-mortality-aggregate'], $yearlyData['data']->average(), $methodology );
                    $log->load( [ 'data' ] );
                }

                if ( ! $log->hasData( $descriptors['data-counts'] ) ) {
                    $log->attachData( $descriptors['data-counts'], $yearlyData['data']->count() );
                }
            }
            \Debugbar::stopMeasure( $year . 'save' );
            \Debugbar::stopMeasure( $year );
        }
    }

    /**
     * Recursive function to save all precision levels of current Log.
     *
     * @param Collection $data
     * @param Log        $log
     * @param mixed      $value
     * @param Location   $location
     */
    protected function saveValues( Collection &$data, Log $log, $value, Location $location ): void
    {
        $location_string = get_class( $location ) . '_' . $location->id;

        if ( empty( $data[ $location_string ] ) ) {
            $data[ $location_string ] = [
                'location' => $location,
                'data'     => collect( [ $value ] ),
                'projects' => collect( [ $log->project_id => $log->project ] )
            ];
        } else {
            $data[ $location_string ]['data']->push( $value );

            if ( ! $data[ $location_string ]['projects']->has( $log->project_id ) ) {
                $data[ $location_string ]['projects']->push( [ $log->project_id => $log->project ] );
            }
        }

        if ( $location->precision != 0 ) {
            $this->saveValues( $data, $log, $value, $location->parent );
        }
    }

    protected function getLocation( string $location_type, int $location_id ): Location
    {
        $key = $location_type . '_' . $location_id;

        if ( $this->locations->has( $key ) ) {
            return $this->locations[ $key ];
        } else {
            return $this->locations[ $key ] = $location_type::find( $location_id );
        }
    }
}
