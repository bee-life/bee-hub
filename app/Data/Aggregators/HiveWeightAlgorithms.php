<?php namespace App\Data\Aggregators;

use App\Exceptions\ModelNotFoundByUidException;
use App\Models\Logs\Data;
use App\Models\Logs\Data\DataId;
use App\Models\Logs\Data\DataLocation;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\MetaData\Project;
use App\Models\Origins\Methodology;
use Carbon\Carbon;
use Debugbar;
use Illuminate\Support\Collection;

class HiveWeightAlgorithms extends BaseAggregator
{
    protected $managment_event_thresholds;

    public function __construct()
    {
        $this->managment_event_thresholds = [
            [
                'name'   => 'Brood frame removal',
                'type'   => 'decrease',
                'range'  => [ '0.5', '2.5' ],
                'months' => [ 3, 10 ],
                'time'   => 'day',
            ],
            [
                'name'   => 'Brood frame addition',
                'type'   => 'increase',
                'range'  => [ '0.5', '2.5' ],
                'months' => [ 3, 10 ],
                'time'   => 'day',
            ],
            [
                'name'   => 'Honey frame removal',
                'type'   => 'decrease',
                'range'  => [ '0.5', '3.3' ],
                'months' => [ 3, 10 ],
                'time'   => 'day',
            ],
            [
                'name'   => 'Honey frame addition',
                'type'   => 'increase',
                'range'  => [ '0.5', '3.3' ],
                'months' => [ 3, 10 ],
                'time'   => 'day',
            ],
            [
                'name'   => 'Empty frame insert',
                'type'   => 'increase',
                'range'  => [ '0.4', '0.5' ],
                'months' => [ 3, 10 ],
                'time'   => 'day',
            ],
            [
                'name'   => 'Honey super with wax comb frame',
                'type'   => 'increase',
                'range'  => [ '7.0', '9.0' ],
                'months' => [ 3, 7 ],
                'time'   => 'day',
            ],
            [
                'name'   => 'Honey super with laminated wax frame',
                'type'   => 'increase',
                'range'  => [ '5.0', '7.5' ],
                'months' => [ 3, 7 ],
                'time'   => 'day',
            ],
            [
                'name'   => 'Upper nest and frames',
                'type'   => 'increase',
                'range'  => [ '10.0', '13.0' ],
                'months' => [ 3, 7 ],
                'time'   => 'day',
            ],
            [
                'name'   => 'Feeding',
                'type'   => 'increase',
                'range'  => [ '1.0', '3.0' ],
                'months' => [ 9, 3 ],
                'time'   => 'day',
            ],
            [
                'name'   => 'Pollen trap removal',
                'type'   => 'decrease',
                'range'  => [ '2.0', '3.0' ],
                'months' => [ 3, 6 ],
                'time'   => 'day',
            ],
            [
                'name'   => 'Pollen trap insert',
                'type'   => 'increase',
                'range'  => [ '2.0', '3.0' ],
                'months' => [ 3, 6 ],
                'time'   => 'day',
            ],
            [
                'name'   => 'Honey collection',
                'type'   => 'decrease',
                'range'  => [ '5.0', '60.0' ],
                'months' => [ 5, 10 ],
                'time'   => 'day',
            ],
        ];

        parent::__construct();
    }


    /**
     * This file contains algorithms to calculate various things that are based on the Hive Weight Scales data.
     *
     * In order for the algorithms to work we require data from HiveSensorAverages Aggregator, so make sure that is run before this.
     *
     * Implemented algorithms are:
     *  - "Hive Production", taking any positive daily weight gains into account, excluding management events.
     *  - Colony management detection
     *  - Colony consumption during winter
     *  - Metabolic resting state of the colony
     *  - Available flight time
     *
     * @TODO: Add parameters for optimized run only (last 2 years, only live or remote projects)
     * @throws ModelNotFoundByUidException
     */
    public function run(): void
    {
        bcscale( 5 );
        $cariProject = Project::whereUid( 'cari-scales' )->first();
        $project     = Project::where( 'uid', 'eubeeppp' )->first();
        $descriptor  = Descriptor::where( 'uid', 'hive-weight' )
                                 ->whereHas( 'projects', function ( $q ) {
                                     $q->where( 'public', true );
                                 } )->with( [ 'projects', 'projects.relatedDataIds', 'projects.relatedDataIds' ] )->first();

        $targetDescriptors = Descriptor::whereIn( 'uid', [
            'event-scale-weight',
            'event-hive-management',
            'hive-production',
            'hive-production-period',
            'hive-super-production-period',
            'hive-wintering-duration',
            'hive-wintering-consumption',
            'hive-metabolic-resting-state',
            'maximum-available-flight-time',
        ] )->get()->keyBy( 'uid' );

        $targetMethodologies = Methodology::whereIn( 'uid', [
            'last',
            'eubeeppp-colony-management-detection',
            'eubeeppp-daily-colony-production',
            'eubeeppp-colony-production-period-detection',
            'eubeeppp-colony-winter-consumption',
            'eubeeppp-colony-metabolic-resting-state',
            'eubeeppp-available-flight-time'
        ] )->get()->keyBy( 'uid' );

        // TODO: Add exception when descriptors are missing and handle it properly.
        if ( ! isset( $project, $descriptor ) || $targetDescriptors->isEmpty() || $targetMethodologies->isEmpty() ) {
            return;
        }

        /**
         * Get a list of all Hive Ids, which contain any Hive Weight data.
         */
        $ids = $descriptor->projects->pluck( 'relatedDataIds' )->flatten();

        /*
         * Each location must be calculated separately.
         */
        /** @var DataId $id */
        foreach ( $ids as $id ) {
            if ( is_production() && $id->project_id != $cariProject->id ) {
                continue;
            }
            dump( $id->value );

            Debugbar::startMeasure( $id->value, $id->value );
            /**
             * Query data, separated sensor data and its aggregates.
             */
            if ( $id->hasParent() ) {
                $logQuery              = $id->hiveLogs();
                $aggregatesQuery       = $id->hiveLogs();
                $yearlyAggregatesQuery = $id->hiveLogs();
                $apiaryAggregates      = $id->parent->apiaryLogs()->where( 'project_id', $project->id )->whereNull( 'time' )->orderBy( 'date' )->with( [
                    'data',
                    'data.data'
                ] )->get()->keyBy( 'raw_date' );
            } else {
                $logQuery              = $id->apiaryLogs();
                $aggregatesQuery       = $id->apiaryLogs();
                $yearlyAggregatesQuery = $id->apiaryLogs();
                $apiaryAggregates      = collect();
            }

            $logs = $logQuery->whereNotNull( 'date' )->whereNotNull( 'time' )->orderBy( 'date' )->orderBy( 'time' )->with( [
                'data',
                'data.data'
            ] )->get()->groupBy( 'raw_date' );

            $aggregates = $aggregatesQuery->whereNotNull( 'date' )->whereNull( 'time' )->orderBy( 'date' )->with( [
                'data',
                'data.data'
            ] )->get()->keyBy( 'raw_date' );

            $yearlyAggregates = $yearlyAggregatesQuery->whereNull( 'date' )->whereNull( 'time' )->orderBy( 'year' )->with( [
                'data',
                'data.data'
            ] )->get()->keyBy( 'year' );

            if ( $logs->isEmpty() ) {
                unset( $logs, $aggregates, $yearlyAggregates, $apiaryAggregates );
                Debugbar::stopMeasure( $id->value );
                continue;
            }

            /**
             * Get a list of weights data grouped by date and keyed by time.
             */
            $weights              = collect();
            $rainHourly           = collect();
            $solarRadiationHourly = collect();
            $temperatureHourly    = collect();

            foreach ( $logs as $date => $group ) {
                /** @var Collection $group */
                $weights[ $date ] = $group->keyBy( 'time' )->map( function ( Log $log ) use ( $descriptor ) {
                    $data = $log->data->where( 'descriptor_id', $descriptor->id )->first();

                    return isset( $data ) ? $data->data->value : null;
                } )->whereNotNull();

                $rain = $group->keyBy( 'time' )->map( function ( Log $log ) use ( $descriptor ) {
                    $data = $log->data->where( 'descriptor_id', getDescriptorIdByUid( 'rain' ) )->first();

                    return isset( $data ) ? $data->data->value : null;
                } )->whereNotNull();
                if ( $rain->isNotEmpty() ) {
                    $rainHourly[ $date ] = $rain;
                }

                $solarRadiation = $group->keyBy( 'time' )->map( function ( Log $log ) use ( $descriptor ) {
                    $data = $log->data->where( 'descriptor_id', getDescriptorIdByUid( 'solar-radiation' ) )->first();

                    return isset( $data ) ? $data->data->value : null;
                } )->whereNotNull();
                if ( $solarRadiation->isNotEmpty() ) {
                    $solarRadiationHourly[ $date ] = $solarRadiation;
                }

                $temperature = $group->keyBy( 'time' )->map( function ( Log $log ) use ( $descriptor ) {
                    $data = $log->data->where( 'descriptor_id', getDescriptorIdByUid( 'temperature' ) )->first();

                    return isset( $data ) ? $data->data->value : null;
                } )->whereNotNull();
                if ( $temperature->isNotEmpty() ) {
                    $temperatureHourly[ $date ] = $temperature;
                }
            }

            /**
             * Get a list of weights for each midnight and daily rain data.
             */
            $midnightWeights = collect();
            $rain            = collect();

            foreach ( $aggregates as $date => $log ) {
                $data = $log->data->keyBy( 'descriptor_id' );

                if ( $data->has( $descriptor->id ) ) {
                    $midnightWeights[ $date ] = $data[ $descriptor->id ]->value;
                }
            }

            foreach ( $apiaryAggregates as $date => $log ) {
                $data = $log->data->keyBy( 'descriptor_id' );

                if ( $data->has( getDescriptorIdByUid( 'rain' ) ) ) {
                    $rain[ $date ] = $data[ getDescriptorIdByUid( 'rain' ) ]->value;
                }
            }

            /**
             * Get existing external id and location Datapoint to improve performance.
             * @var Log $sample
             */
            $sample         = $logs->first()->first();
            $sampleLocation = $sample->data()->where( 'descriptor_id', getDescriptorIdByUid( 'colonies-location' ) )->first()->data()->first();

            /**
             * We require available data to do any calculations. Make sure the HiveSensorAverages is run before this file!
             */
            if ( $weights->isEmpty() || $midnightWeights->isEmpty() || ! $id->hasParent() ) {
                $availableFlightTime = $this->calculateAvailableFlightTime( $sampleLocation, $temperatureHourly, $rainHourly, $solarRadiationHourly );
                $productions         = $productionPeriods = $superProductionPeriods = $colonyWinterConsumption = $metabolicRestingStateConsumption = collect();
            } else {
                $events                  = $this->getManagementEvents( $id, $weights, $sampleLocation );
                $productions             = $this->calculateColonyProduction( $midnightWeights, $events );
                $productionPeriods       = $this->calculateProductionPeriods( $productions, 2.0 );
                $superProductionPeriods  = $this->calculateProductionPeriods( $productions, 5.0 );
                $colonyWinterConsumption = $this->calculateColonyWinterConsumption( $productions, $events, $rain );
                //$metabolicRestingStateConsumption = $this->calculateMetabolicRestingState( $weights, $sampleLocation );
                $availableFlightTime = $metabolicRestingStateConsumption = collect();
            }

            foreach ( $logs as $date => $group ) {
                // We can reuse the loaded aggregates to check if data already exists for the current date.
                if ( $aggregates->has( $date ) ) {
                    $log = $aggregates[ $date ];
                } else {
                    $log = $this->insertLog( $project->id, $date, 'Y-m-d', false, $sample->apiary_id, $sample->hive_id );

                    if ( $log->wasRecentlyCreated ) {
                        $inserts = [
                            [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'colonies-location' ), 'data_id' => $sampleLocation->id, 'data_type' => 'l' ],
                            [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'external-id' ), 'data_id' => $sample->hive_id, 'data_type' => 'id' ]
                        ];
                        if ( $sample->hasApiaryId() ) {
                            $inserts[] = [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'apiary-id' ), 'data_id' => $sample->apiary_id, 'data_type' => 'id' ];
                        }
                        Data::insert( $inserts );
                    } else {
                        $log->load( [ 'data' ] );
                    }
                }

                if ( $productions->has( $date ) && $log->data->where( 'descriptor_id', $targetDescriptors['hive-production']->id )->isEmpty() ) {
                    if ( bccomp( $productions[ $date ], '0' ) === - 1 ) {
                        $value = '0';
                    } else {
                        $value = $productions[ $date ];
                    }
                    $log->attachData( $targetDescriptors['hive-production'], $value, $targetMethodologies['eubeeppp-daily-colony-production'] );
                }

                if ( $productionPeriods->has( $date ) && $log->data->where( 'descriptor_id', $targetDescriptors['hive-production-period']->id )->isEmpty() ) {
                    $log->attachData( $targetDescriptors['hive-production-period'], [ 'value' => $productionPeriods[ $date ] ], $targetMethodologies['eubeeppp-colony-production-period-detection'] );
                }
                if ( $superProductionPeriods->has( $date ) && $log->data->where( 'descriptor_id', $targetDescriptors['hive-super-production-period']->id )->isEmpty() ) {
                    $log->attachData( $targetDescriptors['hive-super-production-period'], [ 'value' => $superProductionPeriods[ $date ] ], $targetMethodologies['eubeeppp-colony-production-period-detection'] );
                }
                if ( $metabolicRestingStateConsumption->has( $date ) && $log->data->where( 'descriptor_id', $targetDescriptors['hive-metabolic-resting-state']->id )->isEmpty() ) {
                    $log->attachData( $targetDescriptors['hive-metabolic-resting-state'], [ 'value' => $metabolicRestingStateConsumption[ $date ] ], $targetMethodologies['eubeeppp-colony-metabolic-resting-state'] );
                }
                if ( $availableFlightTime->has( $date ) && $log->data->where( 'descriptor_id', $targetDescriptors['maximum-available-flight-time']->id )->isEmpty() ) {
                    $log->attachData( $targetDescriptors['maximum-available-flight-time'], [ 'value' => $availableFlightTime[ $date ] ], $targetMethodologies['eubeeppp-available-flight-time'] );
                }
            }

            foreach ( $colonyWinterConsumption as $year => $value ) {
                if ( $yearlyAggregates->has( $year ) ) {
                    $log = $yearlyAggregates[ $year ];
                } else {
                    $log = $this->insertLog( $project->id, $year, 'Y', false, $sample->apiary_id, $sample->hive_id );

                    if ( $log->wasRecentlyCreated ) {
                        $inserts = [
                            [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'colonies-location' ), 'data_id' => $sampleLocation->id, 'data_type' => 'l' ],
                            [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'external-id' ), 'data_id' => $sample->hive_id, 'data_type' => 'id' ]
                        ];
                        if ( $sample->hasApiaryId() ) {
                            $inserts[] = [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'apiary-id' ), 'data_id' => $sample->apiary_id, 'data_type' => 'id' ];
                        }
                        Data::insert( $inserts );
                    } else {
                        $log->load( [ 'data' ] );
                    }
                }

                if ( $log->data->where( 'descriptor_id', $targetDescriptors['hive-wintering-consumption']->id )->isEmpty() ) {
                    $log->attachData( $targetDescriptors['hive-wintering-consumption'], $value, $targetMethodologies['eubeeppp-colony-winter-consumption'] );
                }
            }

            unset( $logs, $aggregates, $yearlyAggregates, $apiaryAggregates );
            Debugbar::stopMeasure( $id->value );
        }
    }


    /**
     * Get a list of management events, either actual or automatically detected.
     * Implementation of algorithm 1. Colony management detection
     *
     * @param DataId            $id
     * @param Collection        $weights
     * @param Data\DataLocation $location
     *
     * @return Collection
     * @see D2.1 - BeeLife2020 - Existing data alghoritms, section 2.1 Colony management detection
     */
    protected function getManagementEvents( DataId $id, Collection $weights, Data\DataLocation $location ): Collection
    {
        $eventDescriptors = Descriptor::getIdsFromUids( [ 'event-scale-weight', 'event-hive-management' ] );

        // First we need to figure out, if we have any actual events available.
        $logs = $id->hiveLogs()->whereHas( 'data', function ( $q ) use ( $eventDescriptors ) {
            $q->whereIn( 'descriptor_id', $eventDescriptors )->with( 'data' );
        } )->orderBy( 'date' )->orderBy( 'time' )->with( [
            'data',
            'data.data',
        ] )->get()->groupBy( 'raw_date' );

        /**
         * If we have any Logs available, it means management events are available and we do not have to detect them.
         */
        if ( $logs->isNotEmpty() ) {
            $events = collect();

            foreach ( $logs as $date => $group ) {
                $data = $group->flatmap( function ( Log $log ) use ( $eventDescriptors ) {
                    $data = $log->data->whereIn( 'descriptor_id', $eventDescriptors )->map( function ( Data $data ) use ( $log ) {
                        if ( $data->data->isSet() ) {
                            return [
                                'name'       => $data->data->description,
                                'weight'     => $data->data->value,
                                'type'       => $data->data->value > 0 ? 'increase' : 'decrease',
                                'descriptor' => $data->descriptor,
                                'time'       => $log->time,
                            ];
                        } else {
                            return null;
                        }
                    } )->whereNotNull();

                    return $data->isNotEmpty() ? $data : null;
                } )->whereNotNull();

                if ( $data->isNotEmpty() ) {
                    $events[ $date ] = $data;
                }
            }

            return $events;
        }

        $events = collect();

        $allWeights = $weights->flattenWithKeys( ' ' );

        $previousDatetime = $previousKey = null;
        foreach ( $allWeights as $currentKey => $current ) {

            $currentDatetime = Carbon::createFromFormat( 'Y-m-d H:i:s', $currentKey );

            if ( isset( $previousDatetime ) ) {
                $timeChange   = $previousDatetime->diffInMinutes( $currentDatetime );
                $weightChange = $current - $allWeights[ $previousKey ];

                // We should skip checking if the changed time is less then a minute or longer then 8 hours.
                if ( $timeChange > 0 && $timeChange < ( 8 * 60 ) ) {
                    if ( $timeChange < 60 ) {
                        $hourlyWeightChange = $weightChange;
                    } else {
                        $hourlyWeightChange = $weightChange / ( $timeChange / 60 );
                    }


                    if ( abs( $hourlyWeightChange ) > 1 ) {
                        /**
                         * We have a possible event here, use the
                         */
                        $detected = $this->eventCheckThresholds( $currentDatetime, $weightChange, $location );
                        if ( $detected->isNotEmpty() && ! $this->eventFalsePositive( $currentDatetime, $location, $events, $detected ) ) {
                            $events[ $currentDatetime->format( 'Y-m-d' ) ] = $detected;
                        }
                    }
                }
            }
            $previousKey      = $currentKey;
            $previousDatetime = $currentDatetime;
        }

        return $events;
    }

    /**
     * The function for checking if we detected a false positive.
     *
     * @TODO: implement checking for repeatable decrease-increases
     *       - inclde the production periode result to reduce false positives, incrise threshold to 2kg.
     *
     * @param Carbon       $currentTime
     * @param DataLocation $location
     *
     * @return bool
     */
    protected function eventFalsePositive( Carbon $currentTime, DataLocation $location, Collection $currentEvents, Collection $detectedEvents ): bool
    {
        if ( $currentTime->isNight( $location ) ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * The function for checking if an event is plausible based on input parameters.
     *
     * @param Carbon       $currentTime
     * @param string       $weightChange
     * @param DataLocation $location
     *
     * @return Collection
     */
    protected function eventCheckThresholds( Carbon $currentTime, string $weightChange, DataLocation $location ): Collection
    {
        $matches = collect();

        foreach ( $this->managment_event_thresholds as $key => $threshold ) {
            if ( $threshold['type'] == 'increase' && $weightChange < 0 ) {
                continue;
            } elseif ( $threshold['type'] == 'decrease' && $weightChange > 0 ) {
                continue;
            }

            if ( $threshold['range'][0] > abs( $weightChange ) || $threshold['range'][1] < abs( $weightChange ) ) {
                continue;
            }

            if ( $threshold['months'][0] < $threshold['months'][1] ) {
                if ( $threshold['months'][0] > $currentTime->month || $threshold['months'][1] < $currentTime->month ) {
                    continue;
                }
            } else {
                if ( $threshold['months'][0] < $currentTime->month && $threshold['months'][1] > $currentTime->month ) {
                    continue;
                }
            }

            if ( $threshold['time'] == 'day' && ! $currentTime->isDay( $location ) ) {
                continue;
            } elseif ( $threshold['time'] == 'morning' && ! $currentTime->isMorning( $location ) ) {
                continue;
            }


            $matches[] = array_merge( [ 'weight' => $weightChange ], $threshold );
        }


        if ( $matches->isEmpty() ) {
            $matches[] = [ 'weight' => $weightChange, 'type' => 'Unknown' ];
        }

        return $matches;
    }


    /**
     * Calculate the daily production from available data.
     * Implementation of algorithm 2. Daily colony production
     *
     * @param Collection[] $weights A collection of weights at midnight, keyed by date.
     * @param Collection[] $events  A collection of events grouped by date.
     *
     * @return Collection A list of positive production days, keyed by date.
     * @see D2.1 - BeeLife2020 - Existing data alghoritms, section 2.2 Daily colony production
     */
    protected function calculateColonyProduction( Collection $weights, Collection $events ): Collection
    {
        $production = collect();

        foreach ( $weights as $today => $todayWeight ) {
            $tomorrow = Carbon::createFromFormat( 'Y-m-d', $today )->addDay()->format( 'Y-m-d' );

            if ( $weights->offsetExists( $tomorrow ) ) {
                $dailyProduction = bcsub( $weights[ $tomorrow ], $todayWeight );

                if ( $events->offsetExists( $tomorrow ) ) {
                    $dailyProduction = bcsub( $dailyProduction, bcsum( $events[ $tomorrow ]->pluck( 'weight' ) ) );
                }

                $production[ $tomorrow ] = $dailyProduction;
            }
        }

        return $production;
    }

    /**
     * Detect production periods from available data.
     * Implementation of algorithm 3. Colony Production period detection
     *
     * @param Collection $productions A collection of positive productionts, keyed by date.
     * @param string     $threshold
     *
     * @return Collection A list of date from / date to pairs designating production periods, keyed by date from.
     * @see D2.1 - BeeLife2020 - Existing data alghoritms, section 2.3 Colony Production period detection
     */
    protected function calculateProductionPeriods( Collection $productions, string $threshold ): Collection
    {
        $productionTrendTests = collect();

        if ( $productions->isEmpty() ) {
            return $productionTrendTests;
        }

        $currentDay = Carbon::createFromFormat( 'Y-m-d', $productions->keys()->first() );
        $last       = Carbon::createFromFormat( 'Y-m-d', $productions->keys()->last() );

        do {
            $current       = $currentDay->clone()->addDay( 3 );
            $productionSum = '0.0';

            for ( $i = 0; $i < 7; $i ++ ) {
                if ( $productions->offsetExists( $current->format( 'Y-m-d' ) ) && bccomp( $productions[ $current->format( 'Y-m-d' ) ], 0 ) == 1 ) {
                    $productionSum = bcadd( $productionSum, $productions[ $current->format( 'Y-m-d' ) ] );
                }
                $current->subDay();
            }

            /**
             * If production trend is higher then threshold,
             * bccomp returns 1 if left operand is higher then right.
             */
            $productionTrendTests[ $currentDay->format( 'Y-m-d' ) ] = ( bccomp( $productionSum, $threshold ) == 1 );

            $currentDay->addDay();
        } while ( $currentDay->format( 'Y-m-d' ) != $last->format( 'Y-m-d' ) );

        $start           = null;
        $productionTrend = collect();

        foreach ( $productionTrendTests as $date => $trend ) {
            if ( $trend == true && empty( $start ) ) {
                $start = $date;
            } elseif ( $trend == false && isset( $start ) ) {
                $productionTrend[ $start ] = [ $start, $date ];
                $start                     = null;
            }
        }

        return $productionTrend;
    }

    /**
     * Detect production periods from available data.
     * Implementation of algorithm 4. Colony consumption during the winter
     * @TODO: Implement
     *
     * @param Collection $productions A collection of daily production data, keyed by date.
     * @param Collection $events      A collection of events within the hive, keyed by date.
     * @param Collection $rain        A collection of daily rain amounts near the hive, keyed by date.
     *
     * @return Collection A list of date from / date to pairs designating production periods, keyed by date from.
     * @see D2.1 - BeeLife2020 - Existing data alghoritms, section 2.4 Colony consumption during the winter
     */
    protected function calculateColonyWinterConsumption( Collection $productions, Collection $events, Collection $rain ): Collection
    {
        $wintering = collect();
        if ( $productions->isEmpty() ) {
            return $wintering;
        }

        $dates     = $productions->keys();
        $startYear = Carbon::createFromFormat( 'Y-m-d', $dates->first() )->year;
        /**
         * We need data from at least two years, to determine winter consumption.
         */
        if ( Carbon::createFromFormat( 'Y-m-d', $dates->first() )->year == Carbon::createFromFormat( 'Y-m-d', $dates->last() )->year ) {
            return collect();
        }

        // First we have to find the honey extraction of the first year.
        // Group events by year for easier work
        $yearlyEvents = $events->groupBy( function ( $event, $key ) {
            return Carbon::createFromFormat( 'Y-m-d', $key )->year;
        }, true );

        $startDate   = null;
        $currentYear = $yearlyEvents->keys()->first();
        $weightSum   = 0;

        if ( $yearlyEvents->isEmpty() ) {
            return collect();
        }

        foreach ( $productions as $date => $production ) {
            if ( $startDate == null ) {
                if ( $currentYear > $yearlyEvents->keys()->max() ) {
                    break;
                }
                $startDate = $this->getLastHoneyExtractionDate( $yearlyEvents[ $currentYear ] );
                if ( $startDate == null ) {
                    $currentYear ++;
                }
            }

            if ( empty( $startDate ) || $date < $startDate ) {
                continue;
            }

            if ( Carbon::createFromFormat( 'Y-m-d', $date )->year > $currentYear ) {
                if ( bccomp( $production, '0' ) == 1
                     && $rain->has( $date ) && bccomp( $rain[ $date ], '0' ) != 1
                     && ! $events->has( $date )
                ) {
                    // We might have hit the end of wintering.
                    $wintering[ $currentYear ] = $weightSum;
                    $currentYear ++;
                    continue;
                }
            }

            if ( bccomp( $production, '0' ) != 1 ) {
                $weightSum = bcadd( $weightSum, bcabs( $production ) );
            }
        }

        return $wintering;
    }

    /**
     * Find the last honey collection date from list of events.
     *
     * @param Collection $events A collection of events within the hive, keyed by date.
     *
     * @return string|null
     */
    protected function getLastHoneyExtractionDate( Collection $events ): ?string
    {
        foreach ( $events as $date => $event ) {
            foreach ( $event as $e ) {
                if ( $e['type'] == 'decrease' && str_contains( mb_strtolower( $e['name'] ), 'honey' ) ) {
                    return $date;
                }
            }
        }

        return null;
    }

    /**
     * Implementation of algorithm 5. Metabolic resting state of the colony
     *
     * @param Collection        $weights  A collection of weights at midnight, keyed by date.
     * @param Data\DataLocation $location Apiary location to calculate correct sunrise/sunset times
     *
     * @return Collection List of daily night weight changes.
     * @see D2.1 - BeeLife2020 - Existing data alghoritms, section 2.5 Metabolic resting state of the colony
     */
    protected function calculateMetabolicRestingState( Collection $weights, Data\DataLocation $location ): Collection
    {
        $metabolicConsumptions = collect();
        if ( $weights->isEmpty() ) {
            return $metabolicConsumptions;
        }

        $timezone = get_nearest_timezone( $location->value->getLat(), $location->value->getLng(), $location->location->country->country_code );

        foreach ( $weights as $currentDay => $todayWeights ) {
            $today    = Carbon::createFromFormat( 'Y-m-d', $currentDay );
            $tomorrow = Carbon::createFromFormat( 'Y-m-d', $currentDay )->addDay();
            if ( ! $weights->offsetExists( $tomorrow->format( 'Y-m-d' ) ) ) {
                continue;
            }

            $sunset  = date_sunset( $today->timestamp, SUNFUNCS_RET_TIMESTAMP, $location->value->getLat(), $location->value->getLng() );
            $sunrise = date_sunrise( $tomorrow->timestamp, SUNFUNCS_RET_TIMESTAMP, $location->value->getLat(), $location->value->getLng() );
            if ( $sunset === false || $sunrise === false ) {
                continue;
            }

            $sunset  = Carbon::createFromTimestamp( $sunset, $timezone );
            $sunrise = Carbon::createFromTimestamp( $sunrise, $timezone );

            foreach ( $todayWeights as $time => $weight ) {
                if ( $time > $sunset->format( 'H:m:s' ) ) {
                    break;
                }
                $startValue = $weight;
            }
            foreach ( $weights[ $tomorrow->format( 'Y-m-d' ) ] as $time => $weight ) {
                if ( $time > $sunrise->format( 'H:m:s' ) ) {
                    break;
                }
                $endValue = $weight;
            }

            if ( empty( $startValue ) ) {
                continue;
            }
            $metabolicConsumption = bcsub( $startValue, $endValue );

            $metabolicConsumptions[ $currentDay ] = bccomp( $metabolicConsumption, '0.0' ) == 1 ? bcsub( $startValue, $endValue ) : '0.0';
        }

        return $metabolicConsumptions;
    }

    /**
     * Implementation of algorithm 6. Calculation of available flight time.
     * For the results to be meaningful we require at least temperature and rain data.
     *
     * @param DataLocation $location       The location we are calculating for.
     * @param Collection   $temperature    List of hourly recorded temperature sensor data.
     * @param Collection   $rain           List of hourly recorded rain sensor data.
     * @param Collection   $solarRadiation List of hourly recorded solar radiation sensor data.
     *
     * @return Collection
     */
    protected function calculateAvailableFlightTime( Data\DataLocation $location, Collection $temperature, Collection $rain, Collection $solarRadiation ): Collection
    {
        $availableFlightTime = collect();

        if ( $temperature->isEmpty() || $rain->isEmpty() ) {
            return $availableFlightTime;
        }

        $timezone = get_nearest_timezone( $location->value->getLat(), $location->value->getLng(), $location->location->country->country_code );

        foreach ( $temperature as $date => $temps ) {
            $flightTime = '0';
            $today      = Carbon::createFromFormat( 'Y-m-d', $date );
            $sunrise    = Carbon::createFromTimestamp( date_sunrise( $today->timestamp, SUNFUNCS_RET_TIMESTAMP,
                $location->value->getLat(), $location->value->getLng() ), $timezone );
            $sunset     = Carbon::createFromTimestamp( date_sunset( $today->timestamp, SUNFUNCS_RET_TIMESTAMP,
                $location->value->getLat(), $location->value->getLng() ), $timezone );

            // We need data available before noon to figure out the weather in the morning.
            $start = $end = null;

            foreach ( $temps as $time => $temp ) {
                $currentTime = Carbon::createFromFormat( 'Y-m-d H:i:s', $date . ' ' . $time, $timezone );
                // Start calculating after sunrise
                if ( $currentTime < $sunrise || ( empty( $start ) && $currentTime > $sunset ) ) {
                    continue;
                }

                if ( bccomp( $temp, '12' ) === 1 && bccomp( $temp, '55' ) === - 1
                     && $rain->has( $date ) && $rain[ $date ]->has( $time ) && bccomp( $rain[ $date ][ $time ], '0' ) === 0 ) {
                    // Use solar radiation data where available
                    if ( ! $solarRadiation->has( $date )
                         || ( $solarRadiation->has( $date ) && ! $solarRadiation[ $date ]->has( $time ) )
                         || ( $solarRadiation->has( $date ) && $solarRadiation[ $date ]->has( $time ) && bccomp( $solarRadiation[ $date ][ $time ], '0' ) === 1 ) ) {
                        if ( empty( $start ) ) {
                            $start = $currentTime;
                        }
                    } elseif ( isset( $start ) ) {
                        $end = $currentTime;
                    }
                } elseif ( isset( $start ) ) {
                    $end = $currentTime;
                }

                if ( isset( $start ) && $currentTime > $sunset ) {
                    $flightTime = bcadd( $flightTime, $sunset->diffInMinutes( $start ) );
                    $start      = $end = null;
                } elseif ( isset( $end ) ) {
                    $flightTime = bcadd( $flightTime, $end->diffInMinutes( $start ) );
                    $start      = $end = null;
                }
            }
            $availableFlightTime[ $date ] = $flightTime;
        }

        return $availableFlightTime;
    }
}
