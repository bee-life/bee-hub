<?php namespace App\Data\Aggregators;

use App\Models\Logs\Data\DataId;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use Carbon\Carbon;

class HiveWeight extends BaseAggregator
{
    /**
     * Calculate the change of hives weight from last 1, 3 amd 7 days.
     * @TODO: Do not use Aggregates.
     */
    public function run():void
    {
        $descriptor = Descriptor::whereSlug( 'hive-weight' )
                                ->whereHas( 'projects', function ( $q ) {
                                    $q->where( 'public', true );
                                } )->with( [ 'projects', 'projects.relatedDataIds' ] )->first();

        if ( empty( $descriptor ) ) {
            return;
        }

        /**
         * Get a list of all Hive Ids, which contain any Hive Weight data.
         */
        $ids = $descriptor->projects->pluck( 'relatedDataIds' )->flatten();

        /*
         * We need to calculate the change for each location separately.
         */
        /** @var DataId $id */
        foreach ( $ids as $id ) {
            /** @var Log $latest */
            $latest = $id->logs()->where( 'project_id', $id->project_id )->latest( 'date' )->with( [ 'locations' ] )->first();

            /**
             * Calculate only those, that actually had any data in past week.
             */
            if ( $latest->date < Carbon::today()->subDays( 7 ) ) {
                continue;
            }
            $events = $id->logs()->whereDate( 'date', '>=', $latest->date->subDays( 7 ) )->whereTime( 'time', null)->orderByDesc( 'date' )->with( [ 'data', 'data.data' ] )->get();

            $count      = 0;
            $eventGroup = collect();

            /** @var Log $event */
            foreach ( $events as $event ) {
                $data = $event->data->where( 'descriptor_id', $descriptor->id )->first();
                if ( empty( $data ) ) {
                    break;
                }

                $eventGroup[] = $data;
                switch ( $count ) {
                    case 1:
                    case 3:
                    case 7:
                        $this->insert( [
                            'descriptor_id' => $descriptor->id,
                            'location_id'   => $latest->location->location_id,
                            'location_type' => $latest->location->location_type,
                            'external_id'   => $id->id,
                            'year'          => $latest->year,
                            'date'          => $latest->date,
                            'count'         => $count,
                            'value'         => $eventGroup->first()->value,
                            'mean'          => $eventGroup->average( 'value' ),
                            'median'        => $eventGroup->median( 'value' ),
                            'min'           => $eventGroup->min( 'value' ),
                            'max'           => $eventGroup->max( 'value' ),
                            'change'        => $eventGroup->first()->value - $eventGroup->last()->value,
                        ], [ $event->project_id ] );
                        break;
                }
                $count ++;
            }
        }
    }
}
