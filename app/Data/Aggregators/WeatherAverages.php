<?php namespace App\Data\Aggregators;

use App\Exceptions\ModelNotFoundByUidException;
use App\Models\Logs\Data;
use App\Models\Logs\Data\DataId;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\MetaData\Project;
use App\Models\Origins\Methodology;
use Illuminate\Support\Collection;

class WeatherAverages extends BaseAggregator
{
    /**
     * Calculate the various averages of weather data for each location for each day.
     *
     * - Lowest/Highest temperature (daily Min/Max of [temperature]),
     * - Relative humidity (daily Median? of [relative-humidity]),
     * - Rain (daily Sum of [rain]),
     * - Solar radiation (daily Sum of [solar-radiation]),
     * - Wind speed (daily Max of [wind-speed])
     * - Wind gust (daily Max of [wind-gust])
     *
     * @TODO: Add parameters for optimized run only (last 2 years, only live or remote projects)
     * @throws ModelNotFoundByUidException
     */
    public function run(): void
    {
        // Prepare a list of all locations, that have any measured value within above list.
        // We assume, these locations are part of the projects:
        if(! is_production()){
            $projectsIds   = Project::getIdsFromUids( [ 'cari-scales', 'efsa-mustb-2020', 'corteva-colony-development' ], true );
        }else{
            $projectsIds   = Project::getIdsFromUids( [ 'cari-scales' ], true );
        }

        $targetProject = Project::where( 'uid', 'eubeeppp' )->first();

        $ids           = DataId::whereIn( 'project_id', $projectsIds->toArray() )->get();
        $descriptors   = Descriptor::whereIn( 'uid', [ 'temperature', 'relative-humidity', 'rain', 'solar-radiation', 'wind-speed', 'wind-direction', 'wind-gust', ] )->get()->keyBy( 'uid' );
        $methodologies = Methodology::whereIn( 'uid', [ 'min-max', 'sum', 'maximum', 'median' ] )->get()->keyBy( 'uid' );

        $dailyTemperatureDescriptor = Descriptor::where( 'uid', 'temperature-daily' )->first();
        $locationDescriptor         = Descriptor::where( 'uid', 'colonies-location' )->first();

        // Set BC Math calculation scale
        bcscale( 5 );

        foreach ( $ids as $id ) {
            // We should chunk the results per year, to avoid memory issues but keep up with query performance.
            $years = $id->logs()->select( 'year' )->whereIn( 'project_id', $projectsIds )->groupBy( 'year' )->get()->pluck( 'year' );

            // Check only last two years in production
            if ( is_production() ) {
                $years = $years->take( - 2 );
            }

            foreach ( $years as $year ) {
                $allLogs = $id->logs()->whereIn( 'project_id', $projectsIds )->where( 'year', $year )->whereNotNull( 'time' )->orderBy( 'date' )->orderBy( 'time' )->with( [
                    'data',
                    'data.data'
                ] )->get()->groupBy( 'rawDate' );

                if ( $allLogs->isEmpty() ) {
                    continue;
                }
                $sample         = $allLogs->first()->first();
                $sampleApiary   = $sample->apiary;
                $sampleLocation = $sample->data->where( 'descriptor_id', $locationDescriptor->id )->first()->data()->without( [ 'location' ] )->first();

                foreach ( $allLogs as $date => $logs ) {
                    // First check if any gathered data is valid, only then save it to the database as a new Log.
                    $futureData = [];

                    foreach ( $descriptors as $descriptor ) {
                        /**
                         * Check within each Log, if the descriptor exists and return its value.
                         * Sometimes not all Logs will have data for all descriptors as expected, mainly because of input validation errors.
                         * @var Collection $data
                         */
                        $data = $logs->map( function ( Log $log ) use ( $descriptor ) {
                            if ( empty( $log->data->where( 'descriptor_id', $descriptor->id )->first() ) ) {
                                return null;
                            } else {
                                return $log->data->where( 'descriptor_id', $descriptor->id )->first()->data->raw;
                            }
                        } )->filter( function ( $value ) {
                            return isset( $value );
                        } );

                        if ( $data->isNotEmpty() ) {
                            $desc  = null;
                            $value = null;

                            switch ( $descriptor->uid ) {
                                case 'temperature':
                                    $desc        = $dailyTemperatureDescriptor;
                                    $value       = bcextent( $data );
                                    $methodology = 'min-max';
                                    break;
                                case 'rain':
                                case 'solar-radiation':
                                    $desc        = $descriptor;
                                    $value       = bcsum( $data );
                                    $methodology = 'sum';
                                    break;
                                case 'wind-speed':
                                case 'wind-gust':
                                    $desc        = $descriptor;
                                    $value       = bcmax( $data );
                                    $methodology = 'maximum';
                                    break;
                                default:
                                    $desc        = $descriptor;
                                    $value       = bcmedian( $data );
                                    $methodology = 'median';
                            }
                            if ( isset ( $value ) ) {
                                $futureData[] = [
                                    'descriptor'  => $desc,
                                    'value'       => $value,
                                    'methodology' => $methodologies[ $methodology ],
                                ];
                            }
                        }
                    }

                    /**
                     * Create a Log only if there is any data to include.
                     */
                    if ( ! empty( $futureData ) ) {
                        /** @var Log[] $logs */
                        $log = $this->insertLog( $targetProject->id, $date, 'Y-m-d', false, $sampleApiary->id );

                        if ( $log->wasRecentlyCreated ) {
                            Data::insert( [
                                [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'colonies-location' ), 'data_id' => $sampleLocation->id, 'data_type' => 'l' ],
                                [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'apiary-id' ), 'data_id' => $sampleApiary->id, 'data_type' => 'id' ]
                            ] );
                        } else {
                            $log->load( [ 'data' ] );
                        }

                        foreach ( $futureData as $data ) {
                            /**
                             * Make sure the data is not existing yet.
                             */
                            if ( $log->data->where( 'descriptor_id', $data['descriptor']->id )->isEmpty() ) {
                                $log->attachData( $data['descriptor'], [ 'value' => $data['value'] ], $data['methodology'] );
                            }
                        }
                    }
                }

                unset($allLogs);
            }
        }
    }
}
