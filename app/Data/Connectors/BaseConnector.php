<?php

namespace App\Data\Connectors;

use App\Models\MetaData\Descriptor;
use App\Models\MetaData\Project;
use Illuminate\Support\Collection;


/**
 * Class BaseConnector
 * @package App\Connectors
 *
 * @property string $type
 */
abstract class BaseConnector
{
    /**
     * @var Project
     */
    protected $project;
    /**
     * @var \Illuminate\Support\Collection|Descriptor[]
     */
    protected $descriptors;

    public function __construct( Project $project )
    {
        $this->project     = $project;
        $this->descriptors = $project->descriptors->keyBy( 'uid' );
    }


    /**
     * Get string representation of Connector Type.
     * Must be implemented by a corresponding Trait.
     *
     * @return string
     */
    abstract public function getType();

    /**
     * Connect to remote and get latest data.
     *
     * @return Collection
     */
    abstract public function getRemoteData();

    /**
     * Connect to remote and get all available historic data.
     *
     * @return Collection
     */
    abstract public function getHistoricRemoteData();
}
