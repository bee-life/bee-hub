<?php

namespace App\Data\Connectors;

use App\Data\Connectors\Traits\Json;
use App\Data\Connectors\Traits\TypeLive;
use App\Exceptions\ConnectorNotAvailableException;
use App\Models\Locations\Country;
use App\Models\Locations\Lau;
use App\Models\Locations\Region;
use App\Models\Logs\Data;
use App\Models\Logs\Data\DataId;
use App\Models\Logs\Data\DataLocation;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\MetaData\Project;
use App\Models\Origins\Device;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Eloquent\Relations\Relation;

class CARIScalesConnector extends BaseConnector
{
    use TypeLive, Json;

    protected $source = 'http://www.cari.be/scale_api/v1/';
    protected $bearer = null;

    protected $scales = null;
    protected $device = null;

    protected $morphMap = null;

    public function __construct( Project $project )
    {
        // To protect the API endpoint, the bearer is provided via .env configuration.
        $this->bearer   = env( 'CARI_SCALES_CONNECTOR_BEARER', null );
        $this->device   = Device::where( 'uid', 'capaz-stockwaage-gsm-200' )->first();
        $this->morphMap = array_flip( Relation::morphMap() );
        parent::__construct( $project );
    }

    /**
     * @inheritDoc
     * @throws ConnectorNotAvailableException
     */
    public function getRemoteData(): void
    {
        $this->loadScales();

        // Historic data is more reliable and time accurate so we use that instead.
        // $this->getCurrentData();
        $latest = Log::whereProjectId( $this->project->id )->latest( 'date' )->first();

        if ( ! empty( $latest ) && ! $latest->date->isToday() && ! $latest->date->isYesterday() ) {
            // Maximum query is 100 days, we reduce that to 30 for safety.
            $to   = Carbon::today()->subDays( 1 );
            $from = $to->copy()->subDays( 29 );
            do {
                if ( $latest->date->gt( $from ) ) {
                    $this->getHistoricData( $from, $latest->date );
                    break;
                }

                $continue = $this->getHistoricData( $from, $to );

                $to->subDays( 30 );
                $from->subDays( 30 );
            } while ( $continue );

            $this->getHistoricData( $latest->date->addDay(), Carbon::yesterday() );
        }
    }


    /**
     * Connect to remote and get all available historic data.
     *
     * @throws ConnectorNotAvailableException
     */
    public function getHistoricRemoteData(): void
    {
        $this->loadScales();

        $to   = Carbon::yesterday();
        $from = $to->copy()->subDays( 29 );
        do {
            $continue = $this->getHistoricData( $from, $to );

            $to->subDays( 30 );
            $from->subDays( 30 );
        } while ( $continue );
    }

    /**
     * Get latest hive scale data.
     * @depreacted Currently unused, as historic data is more reliable.
     *
     * @throws ConnectorNotAvailableException
     */
    protected function getCurrentData(): void
    {
        $this->loadScales();
        $data = collect( $this->read( 'current' ) );

        foreach ( $data as $hive ) {
            if ( ! isset( $this->scales[ $hive['scale'] ] ) ) {
                // A hive could miss coordinates, skip it as its useless to us.
                continue;
            }

            $this->insert( $this->scales[ $hive['scale'] ], $hive['datetime'], $hive['weight'], $hive['outsid__humidity'], $hive['outside_temperature'], $hive['inside_temperature'], $hive['rain'] );
        }
    }

    /**
     * Get Historic data from a specific timeframe.
     * Note, maximum 60 days are allowed by API.
     *
     * @param Carbon $from
     * @param Carbon $to
     *
     * @return bool
     * @throws ConnectorNotAvailableException
     */
    protected function getHistoricData( Carbon $from, Carbon $to ): bool
    {
        $records = collect( $this->read( 'records', [
            'from' => $from->format( 'Y-m-d' ),
            'to'   => $to->format( 'Y-m-d' ),
        ] ) );

        echo "Working on: " . $from->format( 'Y-m-d' ) . ' - ' . $to->format( 'Y-m-d' ) . ' ( ' . $records->count() . ' records)<br>' . PHP_EOL;

        if ( $records->isEmpty() ) {
            return false;
        }

        foreach ( $records as $item ) {
            if ( ! isset( $this->scales[ $item['scale'] ] ) ) {
                // A hive could miss coordinates, skip it as its useless to us.
                continue;
            }
            $this->insert( $this->scales[ $item['scale'] ], $item['datetime'], $item['weight'], $item['outside_humidity'], $item['outside_temperature'], $item['inside_temperature'], $item['rain'] );
        }

        return true;
    }

    /**
     * Loads current scales data in order to associate correct location.
     *
     * @throws ConnectorNotAvailableException
     */
    protected function loadScales()
    {
        $this->scales = collect( $this->read( 'scales' ) )->keyBy( 'id' );

        $this->scales = $this->scales->filter( function ( $scale ) {
            return isset( $scale['lat'], $scale['lon'] );
        } )->transform( function ( $scale ) {
            $point = new Point( $scale['lat'], $scale['lon'] );
            if ( ( $lau = Lau::contains( 'area', $point )->where( 'country_id', Country::getIdFromCountryCode( 'BE' ) )->first() ) != null ) {
                $scale['location_object'] = $lau;
            } elseif ( ( $region = Region::contains( 'area', $point )->where( 'nuts_level', 3 )->where( 'country_id', Country::getIdFromCountryCode( 'BE' ) )->first() ) != null ) {
                $scale['location_object'] = $region;
            }
            $scale['location_array'] = [
                'value'         => $point,
                'location_id'   => $scale['location_object']->id,
                'location_type' => get_class( $scale['location_object'] ),
            ];

            $scale['apiary']   = DataId::firstOrCreate( [
                'project_id' => $this->project->id,
                'value'      => 'unknown_' . $scale['id'],
            ] );
            $scale['hive']     = DataId::firstOrCreate( [
                'project_id' => $this->project->id,
                'value'      => $scale['id'],
                'parent_id'  => $scale['apiary']->id,
            ] );
            $scale['location'] = DataLocation::firstOrCreate( $scale['location_array'] );

            return $scale;
        } );
    }

    /**
     * Insert an Log with given data.
     * Implemented optimizations:
     *  - Avoid Eloquent usage
     *  - Use mass inserts where possible
     *
     *
     * @param array      $scale
     * @param string     $datetime
     * @param float      $weight
     * @param float      $outside_humidity
     * @param float      $outside_temperature
     * @param float|null $inside_temperature
     * @param float|null $rain
     */
    protected function insert( array $scale, string $datetime, float $weight, float $outside_humidity, float $outside_temperature, float $inside_temperature = null, float $rain = null ): void
    {
        $date = Carbon::createFromTimeString( $datetime );

        $log = Log::create( [
            'year'       => $date->year,
            'date'       => $date->toDate(),
            'time'       => $date->format( 'H:i:s' ),
            'project_id' => $this->project->id,
            'apiary_id'  => $scale['apiary']->id,
            'hive_id'    => $scale['hive']->id,
        ] );

        /** @var Data\DataDecimal $hiveWeightValue */
        $hiveWeightValue = call_user_func( Descriptor::getDataModel( $this->descriptors['hive-weight']->table_name ) . '::firstOrCreate', [ 'value' => $weight ] );
        /** @var Data\DataPercentage $relativeHumidityValue */
        $relativeHumidityValue = call_user_func( Descriptor::getDataModel( $this->descriptors['relative-humidity']->table_name ) . '::firstOrCreate', [ 'value' => $outside_humidity ] );
        /** @var Data\DataDecimal $temperature */
        $temperature = call_user_func( Descriptor::getDataModel( $this->descriptors['temperature']->table_name ) . '::firstOrCreate', [ 'value' => $outside_temperature ] );

        $insert = [
            [
                'log_id'        => $log->id,
                'descriptor_id' => getDescriptorIdByUid( 'colonies-location' ),
                'data_id'       => $scale['location']->id,
                'data_type'     => 'l',
                'origin_id'     => null,
                'origin_type'   => null
            ],
            [
                'log_id'        => $log->id,
                'descriptor_id' => getDescriptorIdByUid( 'apiary-id' ),
                'data_id'       => $scale['apiary']->id,
                'data_type'     => 'id',
                'origin_id'     => null,
                'origin_type'   => null
            ],
            [
                'log_id'        => $log->id,
                'descriptor_id' => getDescriptorIdByUid( 'external-id' ),
                'data_id'       => $scale['hive']->id,
                'data_type'     => 'id',
                'origin_id'     => null,
                'origin_type'   => null
            ],
            [
                'log_id'        => $log->id,
                'descriptor_id' => $this->descriptors['hive-weight']->id,
                'data_id'       => $hiveWeightValue->id,
                'data_type'     => $this->morphMap[ $hiveWeightValue::class ],
                'origin_id'     => $this->device->id,
                'origin_type'   => $this->morphMap[ $this->device::class ]
            ],
            [
                'log_id'        => $log->id,
                'descriptor_id' => $this->descriptors['relative-humidity']->id,
                'data_id'       => $relativeHumidityValue->id,
                'data_type'     => $this->morphMap[ $relativeHumidityValue::class ],
                'origin_id'     => $this->device->id,
                'origin_type'   => $this->morphMap[ $this->device::class ]
            ],
            [
                'log_id'        => $log->id,
                'descriptor_id' => $this->descriptors['temperature']->id,
                'data_id'       => $temperature->id,
                'data_type'     => $this->morphMap[ $temperature::class ],
                'origin_id'     => $this->device->id,
                'origin_type'   => $this->morphMap[ $this->device::class ]
            ],
        ];

        if ( isset( $inside_temperature ) ) {
            /** @var Data\DataDecimal $hiveTemperature */
            $hiveTemperature = call_user_func( Descriptor::getDataModel( $this->descriptors['hive-temperature']->table_name ) . '::firstOrCreate', [ 'value' => $inside_temperature ] );
            $insert[]        = [
                'log_id'        => $log->id,
                'descriptor_id' => $this->descriptors['hive-temperature']->id,
                'data_id'       => $hiveTemperature->id,
                'data_type'     => $this->morphMap[ $hiveTemperature::class ],
                'origin_id'     => $this->device->id,
                'origin_type'   => $this->morphMap[ $this->device::class ]
            ];
        }
        if ( isset( $rain ) ) {
            /** @var Data\DataDecimal $rainValue */
            $rainValue = call_user_func( Descriptor::getDataModel( $this->descriptors['rain']->table_name ) . '::firstOrCreate', [ 'value' => $rain ] );
            $insert[]  = [
                'log_id'        => $log->id,
                'descriptor_id' => $this->descriptors['rain']->id,
                'data_id'       => $rainValue->id,
                'data_type'     => $this->morphMap[ $rainValue::class ],
                'origin_id'     => $this->device->id,
                'origin_type'   => $this->morphMap[ $this->device::class ]
            ];
        }

        Data::insert( $insert );
    }
}
