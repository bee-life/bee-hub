<?php

namespace App\Data\Connectors;

use App\Data\Connectors\Traits\Json;
use App\Data\Connectors\Traits\TypeLive;
use App\Models\Locations\Country;
use App\Models\Locations\Region;
use App\Models\Logs\Log;
use App\Models\MetaData\Project;
use Carbon\Carbon;

class VarroaAlertConnector extends BaseConnector
{
    use TypeLive, Json;

    protected $source = 'https://bienengesundheit.at/api/v1/';
    protected $bearer = null;


    public function __construct( Project $project )
    {
        $this->bearer = env( 'VARROA_CONNECTOR_BEARER', null );

        parent::__construct( $project );
    }


    /**
     * @inheritDoc
     */
    public function getRemoteData()
    {
        // Read Varroa Alerts
        $data    = $this->read( 'alerts' );
        $now     = Carbon::now();
        $regions = Country::whereCountryCode( 'at' )->first()->regions->keyBy( 'nuts_id' );

        /**
         * Make sure there is no data yet for today.
         */
        $current_logs = Log::where( 'project_id', $this->project->id )->where( 'date', $now->toDate() )->with( [ 'locations', 'locations.location' ] )->get();

        foreach ( $data as $nuts => $alert ) {
            // Skip Existing.
            if ( $current_logs->isNotEmpty() && $current_logs->where( 'location.location' ) == $regions[ $nuts ]->id ) {
                continue;
            }


            $log = Log::create( [
                'year'       => $now->year,
                'date'       => $now->toDate(),
                'time'       => null,
                'project_id' => $this->project->id,
            ] );

            $log->attachData( $this->descriptors['colonies-location'], [ 'location_id' => $regions[ $nuts ]->id, 'location_type' => Region::class ] );
            $log->attachData( $this->descriptors['varroa-infestation-alerts'], $alert['severity'] );
            $log->attachData( $this->descriptors['content'], $alert['content'] );
            $log->attachData( $this->descriptors['valid-from'], $alert['valid_from'] );
            $log->attachData( $this->descriptors['valid-to'], $alert['valid_to'] );
            $log->attachData( $this->descriptors['updated-at'], Carbon::createFromTimeString( $alert['updated_at'] ) );
            $log->attachData( $this->descriptors['varroa-infestation-measurement-time'], isset( $alert['last_measurement_at'] ) ? Carbon::createFromTimeString( $alert['last_measurement_at'] ) : null );
            $log->attachData( $this->descriptors['varroa-infestation'], $alert['value'] );
            $log->attachData( $this->descriptors['apiary-counts'], $alert['apiaries'] );
            $log->attachData( $this->descriptors['colony-counts'], $alert['colonies'] );
            $log->attachData( $this->descriptors['data-counts'], $alert['datasets'] );
            $log->attachData( $this->descriptors['varroa-investation-threshold'], $alert['threshold']['value'] );
            $log->attachData( $this->descriptors['varroa-investation-threshold-deviation'], $alert['threshold']['deviation'] );
        }
    }

    /**
     * @inheritDoc
     */
    public function getHistoricRemoteData()
    {
        // TODO: Implement getHistoricRemoteData() method.
    }
}
