<?php

namespace App\Data\Connectors\Traits;

/**
 * Crawler extension used to add RSS reader capabilities.
 *
 * Trait RSSReader
 */
trait TypeRSS
{
    protected $type = 'rss';

    /**
     * Read RSS feed and load in an object.
     *
     * @param $url string
     *
     * return SimpleXML
     */
    protected function readRSS( $url )
    {
        $return = simplexml_load_file($url);

        if($return === false){
            return null;
        }

        return $return;
    }

    /**
     * Check if a RSS date is valid.
     * It checks the number of present zeros. If more than 15 (date, time and half timezone) is zero, returns true.
     *
     * @param string $string
     *
     * @return bool
     */
    protected function isValidDate($string){
        return (substr_count($string, "0") <  16);
    }

}
