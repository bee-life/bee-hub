<?php

namespace App\Data\Connectors\Traits;

/**
 * Trait TypeStatic
 * Represents a Static Connector type:
 *      Used for data, that is in file format and is not expected to change. Usually as a result
 *      of published research or historic data. It should include raw data source.
 *
 * @package App\Connectors\Parsers
 */
trait TypeStatic{
    protected $type = 'static';

    /**
     * Get string representation of Connector Type.
     *
     * @return string
     */
    public function getType(){
        return trans('connectors.types.static');
    }

}
