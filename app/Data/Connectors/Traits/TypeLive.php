<?php

namespace App\Data\Connectors\Traits;

/**
 * Trait TypeLive
 * Represents a Live Connector type:
 *      Used for data, that is available from a remote source, regularly updated and locally saved.
 *      It must implement periodic data checks
 *      a limited time and it must have implemented fallback when remote data is unreachable.
 *
 */
trait TypeLive{

    protected $type = 'live';

    /**
     * Get string representation of Connector Type.
     *
     * @return string
     */
    public function getType(){
        return trans('connectors.types.live');
    }

}
