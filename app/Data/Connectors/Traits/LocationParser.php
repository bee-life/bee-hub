<?php

namespace App\Data\Connectors\Traits;

use App\Exceptions\InvalidValueException;
use App\Models\Locations\Lau;
use App\Models\Locations\Post;
use App\Models\Locations\Region;
use Illuminate\Database\Eloquent\Collection;

trait LocationParser
{
    /**
     * @var null|array|Collection|Region[]
     */
    protected $countries;
    /**
     * @var array|Collection|Post[]
     */
    protected $posts = [];
    /**
     * @var array|Collection|Lau[]
     */
    protected $laus = [];

    protected function loadCountries()
    {
        if ( empty( $this->countries ) ) {
            $this->countries = Region::select( [ 'id', 'nuts_id', 'name', 'country_id' ] )->where( 'nuts_level', 0 )->get()->keyBy( 'nuts_id' );
        }
    }

    /**
     * Load Laus by Country
     *
     * @param int    $country_id
     * @param string $countryCode
     */
    protected function loadLaus( $country_id, $countryCode )
    {
        if ( empty( $this->laus[ $countryCode ] ) ) {
            $lau = Lau::where( 'country_id', $country_id )->with( [
                'region',
            ] )->get();

            $this->laus[ $countryCode ] = $lau->keyby( 'lau_id' );
        }
    }

    /**
     * Load Posts by Country
     *
     * @param int    $country_id
     * @param string $countryCode
     */
    protected function loadPosts( $country_id, $countryCode )
    {
        if ( empty( $this->posts[ $countryCode ] ) ) {
            $posts = Post::where( 'country_id', $country_id )->with( [
                'parent',
            ] )->get();

            $this->posts[ $countryCode ] = $posts->keyby( 'number' );
        }
    }

    /**
     * @param string $string
     * @param bool   $translate
     *
     * @return int
     * @throws InvalidValueException
     */
    protected function parseRegionFromCountry( $string, $translate = false )
    {
        $this->loadCountries();

        if ( $translate ) {
            switch ( strtolower( $string ) ) {
                case 'austria':
                    $string = 'Österreich';
                    break;
                case 'belgium':
                    $string = 'Belgique-België';
                    break;
                case 'czechia':
                case 'czech':
                case 'czech republic':
                    $string = 'Česká Republika';
                    break;
                case 'denmark':
                    $string = 'Danmark';
                    break;
                case 'germany':
                    $string = 'Deutschland';
                    break;
                case 'estonia':
                    $string = 'Eesti';
                    break;
                case 'spain':
                    $string = 'España';
                    break;
                case 'france':
                    $string = 'France';
                    break;
                case 'croatia':
                    $string = 'Hrvatska';
                    break;
                case 'ireland':
                    $string = 'Ireland';
                    break;
                case 'italy':
                    $string = 'Italia';
                    break;
                case 'latvia':
                    $string = 'Latvija';
                    break;
                case 'liechtenstein':
                    $string = 'Liechtenstein';
                    break;
                case 'lithuania':
                    $string = 'Lietuva';
                    break;
                case 'luxemburg':
                    $string = 'Luxemburg';
                    break;
                case 'hungary':
                    $string = 'Magyarország';
                    break;
                case 'netherlands':
                    $string = 'Nederland';
                    break;
                case 'norway':
                    $string = 'Norge';
                    break;
                case 'macedonia':
                case 'fyrom':
                    $string = 'North Macedonia';
                    break;
                    break;
                case 'poland':
                    $string = 'Polska';
                    break;
                case 'portugal':
                    $string = 'Portugal';
                    break;
                case 'serbia':
                    $string = 'Republika Srbija /Република Србија';
                    break;
                case 'romania':
                    $string = 'România';
                    break;
                case 'switzerland':
                case 'schweiz':
                case 'suisse':
                case 'svizzera':
                    $string = 'Schweiz/Suisse/Svizzera';
                    break;
                case 'albania':
                    $string = 'Shqipëria';
                    break;
                case 'slovakia':
                    $string = 'Slovensko';
                    break;
                case 'slovenia':
                    $string = 'Slovenija';
                    break;
                case 'finland':
                case 'suomi':
                    $string = 'Suomi / Finland';
                    break;
                case 'sweden':
                    $string = 'Sverige';
                    break;
                case 'turkey':
                    $string = 'Türkiye';
                    break;
                case 'united kingdom':
                case 'uk':
                case 'england and wales':
                case 'england & wales':
                    $string = 'United Kingdom';
                    break;
                case 'greece':
                    $string = 'Ελλαδα';
                    break;
                case 'cyprus':
                    $string = 'Κυπροσ';
                    break;
                case 'bulgaria':
                    $string = 'България';
                    break;
                case 'montenegro':
                    $string = 'Црна Гора';
                    break;
                case 'bosnia-herzegovina':
                case 'bosnia i herzegovina':
                    $string = 'Bosnia and Herzegovina';
                    break;
            }
        }

        if ( $this->countries->where( 'name', $string )->isEmpty() ) {
            throw new InvalidValueException( $string );
        }

        return $this->countries->where( 'name', $string )->first()->id;
    }


    /**
     * Parse a post number and get closes region number from it.
     *
     * @param string $number
     *
     * @return int
     * @throws InvalidValueException
     */
    public function parseRegionFromPostNumber( string $number, string $countryCode )
    {
        return $this->parsePostNumber( $number, $countryCode )->parent->id;
    }

    /**
     * Parse a post number and return its object representation from database.
     *
     * @param string $number
     * @param string $countryCode
     *
     * @return Post
     * @throws InvalidValueException
     */
    public function parsePostNumber( string $number, string $countryCode ): Post
    {
        $this->loadCountries();

        $this->loadPosts( $this->countries[ $countryCode ]->id, $countryCode );

        if ( isset( $this->posts[ $countryCode ][ $number ] ) ) {
            return $this->posts[ $countryCode ][ $number ];
        } else {
            throw new InvalidValueException( $number );
        }
    }

}
