<?php

namespace App\Data\Connectors\Traits;

use App\Exceptions\ConnectorNotAvailableException;
use Illuminate\Support\Facades\Http;

trait Json
{

    /**
     * Read json file and load in an object.
     *
     * @see https://stackoverflow.com/questions/42094842/curl-error-60-ssl-certificate-in-laravel-5-4 if getting certificate errors on localhost.
     *
     * @param $url string
     *
     * @throws ConnectorNotAvailableException
     *
     * return array
     */
    protected function read( $url, $params = [] )
    {
        if ( ! empty( $this->bearer ) ) {
            $response = Http::withOptions( [ 'verify' => is_production() ] )->withToken( $this->bearer )->get( $this->source . $url, $params );
        } else {
            $response = Http::withOptions( [ 'verify' => is_production() ] )->get( $this->source . $url, $params );
        }

        if ( ! $response->successful() ) {
            throw new ConnectorNotAvailableException( static::class );
        }

        return $response->json();
    }
}
