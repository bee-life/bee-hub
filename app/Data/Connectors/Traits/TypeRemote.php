<?php

namespace App\Data\Connectors\Traits;

/**
 * Trait TypeRemote
 * Represents a Remote Connector type:
 *      Used for data, that is stored and accessed directly in source. Used for rapidly
 *      changing data or huge dataset (ie. weather). The requested data must be cached for
 *      a limited time and it must have implemented fallback when remote data is unreachable.
 *
 * @package App\Connectors\Parsers
 */
trait TypeRemote{
    protected $type = 'remote';

    /**
     * Get string representation of Connector Type.
     *
     * @return string
     */
    public function getType(){
        return trans('connectors.types.remote');
    }

}
