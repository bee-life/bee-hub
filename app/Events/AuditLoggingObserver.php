<?php /** @noinspection PhpDocSignatureInspection */

namespace App\Events;


use App\Models\MetaData\Project;
use App\Models\MetaData\Provider;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AuditLoggingObserver
{


    /**
     * Runs after a Model is created.
     *
     * @param Model|Project|Provider $model
     */
    public function created( Model $model ): void
    {
        $model->auditLogs()->create( [
            'field'      => 'created',
            'old_value'  => null,
            'new_value'  => null,
            'user_id'    => Auth::id(),
            'created_at' => Carbon::now()
        ] );
    }

    /**
     * Runs before a Model is updated.
     *
     * @param Model|Project|Provider $model
     */
    public function updating( Model $model ): void
    {

        if ( $model->isDirty() ) {
            $data      = $model->getDirty();
            $timestamp = Carbon::now();

            foreach ( $data as $key => $item ) {
                $old = $model->getOriginal($key);
                $new = $item;

                $model->auditLogs()->create( [
                    'field'      => $key,
                    'old_value'  => $old,
                    'new_value'  => $new,
                    'user_id'    => Auth::id(),
                    'created_at' => $timestamp
                ] );
            }
        }
    }

    /**
     * Runs before a Model is deleted.
     *
     * @param Model|Project|Provider $model
     */
    public function deleting( Model $model ): void
    {
        $model->auditLogs()->create( [
            'field'      => 'deleted',
            'old_value'  => null,
            'new_value'  => null,
            'user_id'    => Auth::id(),
            'created_at' => Carbon::now()
        ] );
    }

    /**
     * Runs before a model is restored from trash.
     *
     * @param Model|Project|Provider $model
     */
    public function restoring( Model $model ): void
    {
        $model->auditLogs()->create( [
            'field'      => 'restored',
            'old_value'  => null,
            'new_value'  => null,
            'user_id'    => Auth::id(),
            'created_at' => Carbon::now()
        ] );
    }
}
