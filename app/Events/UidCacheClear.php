<?php

namespace App\Events;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UidCacheClear
{
    use Dispatchable, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event. The model should use the \App\Models\Traits\Uid trait.
     *
     * @param  Model  $model
     * @return void
     */
    public function handle(Model $model): void
    {
        if(method_exists($model, 'clearUidCache')){
            $model::clearUidCache();
        }
    }
}
