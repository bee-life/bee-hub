<?php

use App\Exceptions\ModelNotFoundByUidException;
use App\Models\MetaData\Descriptor;
use Illuminate\Support\Collection;


if ( ! function_exists( 'get_year_day_count' ) ) {

    /**
     * Get number of days in current/set year.
     *
     * @param int|null $year Selected year. Defaults to current year.
     *
     * return int
     *
     * @return int
     */
    function get_year_day_count( int $year = null ): int
    {
        if ( isset( $year ) ) {
            return date( 'z', mktime( 0, 0, 0, 12, 31, $year ) + 1 );
        } else {
            return date( 'z', mktime( 0, 0, 0, 12, 31 ) + 1 );
        }
    }
}

if ( ! function_exists( 'getExplicitFields' ) ) {

    /**
     * Get explicit form fields for validation
     *
     * E.g. 'foo.1.bar.spark.baz' instead of 'foo.*.bar.spark.baz'
     *
     * Used in AppServiceProvider for custom validators
     *
     * @param $attribute
     * @param $parameters
     *
     * @return string
     */
    function getExplicitFields( $attribute, $parameters ): string
    {
        // Get key from attribute
        $index = substr( $attribute, strpos( preg_quote( $attribute, '/' ), "\." ) + 1, 1 );

        // Get numbered fields
        return str_replace( "*", $index, $parameters );
    }
}

if ( ! function_exists( 'get_locales' ) ) {
    /**
     * Gets a list of available locales. If no locales are set, it defaults to current locale.
     *
     * @return Collection|string[]
     */
    function get_locales(): Collection|array
    {
        return config( 'app.locales', [ config( 'app.locale' ) ] );
    }
}


if ( ! function_exists( 'my_simple_crypt' ) ) {
    /**
     * Encrypt the data by using universal key.
     *
     * @param string $string
     * @param string $action
     *
     * @return string
     */
    function my_simple_crypt( string $string, string $action = 'e' ): string
    {
        // you may change these values to your own
        $secret_key = 'jVYqYdXJeJZWmXCLSy0LjWNYWlZbg629mPEl9W1S';
        $secret_iv  = '614tfu8yDX7OGLGm05iU';

        $output         = false;
        $encrypt_method = "AES-256-CBC";
        $key            = hash( 'sha256', $secret_key );
        $iv             = substr( hash( 'sha256', $secret_iv ), 0, 16 );

        if ( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        } else if ( $action == 'd' ) {
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }

        return $output;
    }
}

if ( ! function_exists( 'img' ) ) {
    /**
     * Get url of a static image resources.
     *
     * @param string    $url
     * @param array     $parameters
     * @param bool|null $secure
     *
     * @return string
     */
    function img( string $url, array $parameters = [], bool $secure = null ): string
    {
        return url( 'images/' . $url, $parameters, $secure );
    }
}


if ( ! function_exists( 'array_pull' ) ) {
    /**
     * A backwards compatibility function definition for Arr:pull, used by third party libraries.
     *
     * @param array      $array
     * @param string     $key
     * @param mixed|null $default
     *
     * @return array
     */
    function array_pull( array &$array, string $key, mixed $default = null ): mixed
    {
        return Arr::pull( $array, $key, $default );
    }
}

if ( ! function_exists( 'getDescriptorIdByUid' ) ) {
    /**
     * Gets Model ID from UID.
     * Should be used when relating to specific descriptors, but we need to be optimized about it.
     *
     * @param string     $uid
     * @param bool|false $bAllowMissing
     *
     * @return int|null
     * @throws ModelNotFoundByUidException
     */
    function getDescriptorIdByUid( string $uid, bool $bAllowMissing = true ): ?int
    {
        return Descriptor::getIdFromUid( $uid, $bAllowMissing );
    }
}


if ( ! function_exists( 'getDescriptorIdByUid' ) ) {
    /**
     * Gets several Project IDs from UIDs.
     * Should be used when relating to specific project, but we need to be optimized about it.
     *
     * @param Collection|array $uids
     * @param bool|false       $bAllowMissing
     *
     * @return Collection
     * @throws ModelNotFoundByUidException
     */
    function getDescriptorIdsByUids( Collection|array $uids, bool $bAllowMissing = true ): Collection
    {
        return Descriptor::getIdsFromUids( $uids, $bAllowMissing );
    }
}
