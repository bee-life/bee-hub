<?php

use App\Models\Logs\Reference\ReferenceCurrency;
use Carbon\Carbon;

if ( ! function_exists( 'to_javascript' ) ) {
    /**
     * Transforms php variables into Javascript variable strings.
     *
     * @param array|string|int $arg
     *
     * @return string|false
     */
    function to_javascript( array|string|int $arg ): string|bool
    {
        return json_encode( $arg );
    }
}

if ( ! function_exists( 'format_number' ) ) {
    /**
     * Returns local based formatted number.
     *
     * @param float|string|int $value
     * @param int              $decimals
     *
     * @return string
     */
    function format_number( float|string|int $value, int $decimals = 2 ): string
    {
        return number_format( $value, $decimals, __( 'logs.formatting.decimal_separator' ), __( 'logs.formatting.thousand_separator' ) );
    }
}

if ( ! function_exists( 'format_currency' ) ) {
    /**
     * Returns local based formatted number.
     * Should be printed unescapes because of HTML entity encoding (non-breaking space).
     *
     * @param ReferenceCurrency $value
     *
     * @return string
     */
    function format_currency( ReferenceCurrency $value ): string
    {
        $number = format_number( $value->value, $value->reference->decimals );

        if ( $value->reference->symbol_prefix ) {
            return sprintf( '%s&nbsp;%s', $value->reference->getUnit(), $number );
        } else {
            return sprintf( '%s&nbsp;%s', $number, $value->reference->getUnit() );
        }
    }
}

if ( ! function_exists( 'format_percentage' ) ) {

    /**
     * Format a number as percentage according to current locale.
     *
     * @param string|float $value
     * @param int   $decimals
     *
     * @return string
     */
    function format_percentage( string|float $value, int $decimals = 2 ): string
    {
        return __( 'logs.units.percentage', [ 'value' => format_number( $value, $decimals ) ] );
    }
}

if ( ! function_exists( 'format_date' ) ) {
    /**
     * Format Carbon date
     *
     * @param Carbon|null $var
     * @param bool   $time
     *
     * @return string
     */
    function format_date( ?Carbon $var, bool $time = false ): string
    {
        if ( isset( $var ) ) {
            if ( $time ) {
                return $var->format( __( 'framework.datetime' ) );
            } else {
                return $var->format( __( 'framework.date' ) );
            }
        } else {
            return __( 'framework.no_date' );
        }
    }
}

if ( ! function_exists( 'format_special_unit' ) ) {
    /**
     * Formats value as a special unit based on locale and pluralization.
     *
     * @param int|float|string $value
     * @param string           $unit    One of 't/km2'.
     * @param int              $decimal Number of decimal places.
     *
     * @return string
     */
    function format_special_unit( int|float|string $value, string $unit, int $decimal = 2 ): string
    {
        $available_units = collect( [ 'kg/km2' ] );

        if ( ! $available_units->contains( $unit ) ) {
            throw new InvalidArgumentException( 'Passed unit must be one of these: "' . $available_units->implode( '", "' ) . "\". Received: \"$unit\"" );
        }

        return trans_choice( 'logs.units.special.' . $unit, (int) $value, [ 'value' => format_number( $value, $decimal ) ] );
    }
}

if ( ! function_exists( 'format_weight' ) ) {
    /**
     * Formats value as weight based on locale and pluralization.
     *
     * @param int|float|string $value
     * @param string           $unit    One of 'kg', 'g', 'mg' or 't'.
     * @param int              $decimal Number of decimal places.
     *
     * @return string
     */
    function format_weight( int|float|string $value, string $unit, int $decimal = 2 ): string
    {
        $available_units = collect( [ 'kg', 'g', 'mg', 't' ] );

        if ( ! $available_units->contains( $unit ) ) {
            throw new InvalidArgumentException( 'Passed unit must be one of these: "' . $available_units->implode( '", "' ) . "\". Received: \"$unit\"" );
        }

        return trans_choice( 'logs.units.weight.' . $unit, (int) $value, [ 'value' => format_number( $value, $decimal ) ] );
    }
}

if ( ! function_exists( 'format_volume' ) ) {
    /**
     * Formats value as volume based on locale and pluralization.
     *
     * @param int|float|string $value
     * @param string           $unit    One of 'hl', 'l', 'dl', 'ml', 'm', 'cm', 'mm' or 'ft'.
     * @param int              $decimal Number of decimal places.
     *
     * @return string
     */
    function format_volume( int|float|string $value, string $unit, int $decimal = 2 ): string
    {
        $available_units = collect( [ 'hl', 'l', 'dl', 'ml', 'm', 'cm', 'mm', 'ft' ] );

        if ( ! $available_units->contains( $unit ) ) {
            throw new InvalidArgumentException( 'Passed unit must be one of these: "' . $available_units->implode( '", "' ) . "\". Received: \"$unit\"" );
        }

        return trans_choice( 'logs.units.volume.' . $unit, (int) $value, [ 'value' => format_number( $value, $decimal ) ] );
    }
}

if ( ! function_exists( 'format_area' ) ) {
    /**
     * Formats value as area based on locale and pluralization.
     *
     * @param int|float|string $value
     * @param string           $unit    One of 'mm', 'cm', 'm', 'a', 'ha', 'km', 'in', 'ft', 'y', 'mile'
     * @param int              $decimal Number of decimal places.
     *
     * @return string
     */
    function format_area( int|float|string $value, string $unit, int $decimal = 2 ): string
    {
        $available_units = collect( [ 'mm', 'cm', 'm', 'a', 'ha', 'km', 'in', 'ft', 'y', 'mile' ] );

        if ( ! $available_units->contains( $unit ) ) {
            throw new InvalidArgumentException( 'Passed unit must be one of these: "' . $available_units->implode( '", "' ) . "\". Received: \"$unit\"" );
        }

        return trans_choice( 'logs.units.area.' . $unit, (int) $value, [ 'value' => format_number( $value, $decimal ) ] );
    }
}
if ( ! function_exists( 'format_length' ) ) {
    /**
     * Formats value as length based on locale and pluralization.
     *
     * @param int|float|string $value
     * @param string           $unit    One of 'mm', 'cm', 'm', 'a', 'ha', 'km', 'in', 'ft', 'y', 'mile'
     * @param int              $decimal Number of decimal places.
     *
     * @return string
     */
    function format_length( int|float|string $value, string $unit, int $decimal = 2 ): string
    {
        $available_units = collect( [ 'mm', 'cm', 'm', 'a', 'ha', 'km', 'in', 'ft', 'y', 'mile' ] );

        if ( ! $available_units->contains( $unit ) ) {
            throw new InvalidArgumentException( 'Passed unit must be one of these: "' . $available_units->implode( '", "' ) . "\". Received: \"$unit\"" );
        }

        return trans_choice( 'logs.units.length.' . $unit, (int) $value, [ 'value' => format_number( $value, $decimal ) ] );
    }
}

if ( ! function_exists( 'format_speed' ) ) {
    /**
     * Formats value as speed based on locale and pluralization.
     *
     * @param int|float|string $value
     * @param string           $unit    One of 'km/h', 'kph', 'm/s', 'mi/h', 'mph', 'knots', 'fps', 'mach'
     * @param int              $decimal Number of decimal places.
     *
     * @return string
     */
    function format_speed( int|float|string $value, string $unit, int $decimal = 2 ): string
    {
        $available_units = collect( [ 'km/h', 'kph', 'm/s', 'mi/h', 'mph', 'knots', 'fps', 'mach' ] );

        if ( ! $available_units->contains( $unit ) ) {
            throw new InvalidArgumentException( 'Passed unit must be one of these: "' . $available_units->implode( '", "' ) . "\". Received: \"$unit\"" );
        }
        $unit = match ( $unit ) {
            'km/h' => 'kph',
            'mi/h' => 'mph',
        };

        return trans_choice( 'logs.units.speed.' . $unit, (int) $value, [ 'value' => format_number( $value, $decimal ) ] );
    }
}

if ( ! function_exists( 'format_temperature' ) ) {
    /**
     * Formats value as weight based on locale and pluralization.
     *
     * @param int|float|string $value
     * @param string           $unit    One of 'c', 'celsius', 'k', 'kelvin', 'f' or 'fahrenheit'.
     * @param int              $decimal Number of decimal places.
     *
     * @return string
     */
    function format_temperature( int|float|string $value, string $unit, int $decimal = 1 ): string
    {
        $available_units = collect( [ 'c', 'celsius', 'k', 'kelvin', 'f', 'fahrenheit' ] );

        if ( ! $available_units->contains( $unit ) ) {
            throw new InvalidArgumentException( 'Passed unit must be one of these: "' . $available_units->implode( '", "' ) . "\". Received: \"$unit\"" );
        }
        $unit = match ( $unit ) {
            'c' => 'celsius',
            'k' => 'kelvin',
            'f' => 'fahrenheit',
        };

        return trans_choice( 'logs.units.temperature.' . $unit, (int) $value, [ 'value' => format_number( $value, $decimal ) ] );
    }
}

if ( ! function_exists( 'format_temperature_range' ) ) {
    /**
     * Formats value as weight based on locale and pluralization.
     *
     * @param array  $values
     * @param string $unit    One of 'c', 'celsius', 'k', 'kelvin', 'f' or 'fahrenheit'.
     * @param int    $decimal Number of decimal places.
     *
     * @return string
     */
    function format_temperature_range( array $values, string $unit, int $decimal = 1 ): string
    {
        return format_temperature( $values[0], $unit, $decimal ) . '/' . format_temperature( $values[1], $unit, $decimal );
    }
}

if ( ! function_exists( 'format_solar_irradiance' ) ) {
    /**
     * Formats value as weight based on locale and pluralization.
     *
     * @param int|float|string $value
     * @param string           $unit    One of 'watt/m', 'wm'.
     * @param int              $decimal Number of decimal places.
     *
     * @return string
     */
    function format_solar_irradiance( int|float|string $value, string $unit, int $decimal = 0 ): string
    {
        $available_units = collect( [ 'watt/m', 'wm' ] );

        if ( ! $available_units->contains( $unit ) ) {
            throw new InvalidArgumentException( 'Passed unit must be one of these: "' . $available_units->implode( '", "' ) . "\". Received: \"$unit\"" );
        }
        $unit = match ( $unit ) {
            'watt/m' => 'wm',
        };

        return trans_choice( 'logs.units.solar-irradiance.' . $unit, (int) $value, [ 'value' => format_number( $value, $decimal ) ] );
    }
}

if ( ! function_exists( 'format_orientation' ) ) {
    /**
     * Formats orientation value as based on locale and pluralization.
     *
     * @param int|float|string $value
     * @param string           $unit    One of 'orientation', 'degrees', 'deg', 'd'.
     * @param int              $decimal Number of decimal places.
     *
     * @return string
     */
    function format_orientation( int|float|string $value, string $unit, int $decimal = 0 ): string
    {
        $available_units = collect( [ 'orientation', 'degrees', 'deg', 'd' ] );

        if ( ! $available_units->contains( $unit ) ) {
            throw new InvalidArgumentException( 'Passed unit must be one of these: "' . $available_units->implode( '", "' ) . "\". Received: \"$unit\"" );
        }
        switch ( $unit ) {
            case 'orientation':
                $available_values = collect( [
                    'N',
                    'S',
                    'W',
                    'E',
                    'NE',
                    'NW',
                    'SE',
                    'SW',
                    'NORTH',
                    'SOUTH',
                    'EAST',
                    'WEST',
                    'NORTH-EAST',
                    'NORTH-WEST',
                    'SOUTH-EAST',
                    'SOUTH-WEST',
                ] );

                if ( $available_values->contains( strtoupper( $value ) ) ) {
                    switch ( strtoupper( $value ) ) {
                        case 'N':
                        case 'NORTH':
                            $unit = 'north';
                            break;
                        case 'S':
                        case 'SOUTH':
                            $unit = 'south';
                            break;
                        case 'E':
                        case 'EAST':
                            $unit = 'east';
                            break;
                        case 'W':
                        case 'WEST':
                            $unit = 'west';
                            break;
                        case 'NE':
                        case 'NORTH-EAST':
                            $unit = 'north-east';
                            break;
                        case 'NW':
                        case 'NORTH-WEST':
                            $unit = 'north-west';
                            break;
                        case 'SE':
                        case 'SOUTH-EAST':
                            $unit = 'south-east';
                            break;
                        case 'SW':
                        case 'SOUTH-WEST':
                            $unit = 'south-west';
                            break;
                    }
                } else {
                    $value = $value % 360;

                    if ( $value <= 22.5 || $value > 337.5 ) {
                        $unit = 'north';
                    } elseif ( $value > 22.5 && $value <= 67.5 ) {
                        $unit = 'north-east';
                    } elseif ( $value > 67.5 && $value <= 112.5 ) {
                        $unit = 'east';
                    } elseif ( $value > 112.5 && $value <= 157.5 ) {
                        $unit = 'south-east';
                    } elseif ( $value > 157.5 && $value <= 202.5 ) {
                        $unit = 'south';
                    } elseif ( $value > 202.5 && $value <= 247.5 ) {
                        $unit = 'south-west';
                    } elseif ( $value > 247.5 && $value <= 292.5 ) {
                        $unit = 'west';
                    } elseif ( $value > 292.5 && $value <= 337.5 ) {
                        $unit = 'north-west';
                    }
                }

                return __( "logs.units.orientation.relative-" . $unit );
            case 'deg':
            case 'd':
                $unit = 'degrees';
                break;
        }

        return trans_choice( 'logs.units.orientation.' . $unit, (int) $value, [ 'value' => format_number( $value, $decimal ) ] );
    }
}
