<?php

namespace App\Helpers;

use Debugbar;
use Illuminate\Support\Collection;
use Laravel\Telescope\Telescope;

class Body
{
    /**
     * @var Collection
     */
    static protected Collection $classes;

    /**
     * Add initial classes based on current page and debuging status.
     */
    static public function init(): void
    {
        static::$classes = new Collection();

        // Environment related classes.
        switch ( config( 'app.env' ) ) {
            case 'local':
                static::$classes->add( 'local' );
                break;
            case 'development':
                static::$classes->add( 'development' );
                break;
            case 'demo':
                static::$classes->add( 'demo' );
                break;
            case 'production':
            default:
                break;
        }

        if ( class_exists( 'Debugbar' ) && Debugbar::isEnabled() ) {
            static::$classes->add( 'debugbar' );
        }
        if ( class_exists( 'Laravel\Telescope\Telescope' ) && Telescope::isRecording() ) {
            static::$classes->add( 'telescope' );
        }
    }

    /**
     * Add single or an array of classes.
     *
     * @param string|array $classes
     */
    static public function addClass( string|array $classes ): void
    {
        if ( is_string( $classes ) ) {
            static::$classes->add( $classes );
        } elseif ( is_array( $classes ) || is_a( $classes, 'Illuminate\Support\Collection' ) ) {
            static::$classes = static::$classes->merge( $classes );
        }
    }

    /**
     * Print the classes separated by space as valid HTML within an attribute.
     *
     * @return string
     */
    static function print(): string
    {
        if ( ! isset( static::$classes ) ) {
            static::init();
        }

        return static::$classes->join( ' ' );
    }
}
