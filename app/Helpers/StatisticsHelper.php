<?php

use App\Exceptions\InvalidValueException;

if ( ! function_exists( 'confidenceIntervalConstant' ) ) {
    /**
     * Statistics helper - get the Z constant of the confidence interval based on required percentage.
     *
     * @param string $percentage Available values: 80, 85, 90, 95, 99, 99.5 or 99.9.
     * @param bool   $string     Return the value as string for BCMath calculations.
     *
     * @return float|string
     *
     * @source https://www.mathsisfun.com/data/confidence-interval-calculator.html
     * @throws InvalidValueException
     */
    function confidenceIntervalConstant( string $percentage, bool $string = false ): float|string
    {
        return match ( $percentage ) {
            '80' => $string ? '1.282' : 1.282,
            '85' => $string ? '1.440' : 1.440,
            '90' => $string ? '1.645' : 1.645,
            '95' => $string ? '1.960' : 1.960,
            '99' => $string ? '2.576' : 2.576,
            '99.5' => $string ? '2.807' : 2.807,
            '99.9' => $string ? '3.291' : 3.291,
            default => throw new InvalidValueException( $percentage ),
        };
    }
}


if ( ! function_exists( 'calculate_change' ) ) {
    /**
     * Calculate and display the change from previous period.
     *
     * @param string $current
     * @param string $last
     * @param bool   $icon
     *
     * @return string
     */
    function calculate_change( string $current, string $last, bool $icon = true ): string
    {
        $result = bcsub( $current, $last );

        if ( bccomp( $current, $last ) === 1 ) {
            if ( $icon ) {
                return '<font-awesome-icon :icon="[\'far\', \'caret-up\']" />' . $result;
            } else {
                return $result;
            }
        } elseif ( bccomp( $current, $last ) === - 1 ) {
            if ( $icon ) {
                return '<font-awesome-icon :icon="[\'far\', \'caret-down\']" />' . $result;
            } else {
                return $result;
            }
        } else {
            if ( $icon ) {
                return '<font-awesome-icon :icon="[\'far\', \'minus\']" />';
            } else {
                return $result;
            }
        }
    }
}

if ( ! function_exists( 'first_quartile' ) ) {
    /**
     * Get first quartile from collection.
     *
     * @param \Illuminate\Support\Collection $collection
     *
     * @return int|float
     */
    function first_quartile( \Illuminate\Support\Collection $collection ): int|float
    {
        $sorted = $collection->sort();
        $median = $sorted->median();

        return $sorted->filter( function ( $value ) use ( $median ) {
            return $value <= $median;
        } )->median();
    }
}

if ( ! function_exists( 'third_quartile' ) ) {
    /**
     * Get first quartile from collection.
     *
     * @param \Illuminate\Support\Collection $collection
     *
     * @return int|float
     */
    function third_quartile( \Illuminate\Support\Collection $collection ): int|float
    {
        $sorted = $collection->sort();
        $median = $sorted->median();

        return $sorted->filter( function ( $value ) use ( $median ) {
            return $value >= $median;
        } )->median();
    }
}

if ( ! function_exists( 'get_nearest_timezone' ) ) {
    /**
     * Function to determine the timezone based on location.
     *
     * @see https://stackoverflow.com/questions/3126878/get-php-timezone-name-from-latitude-and-longitude as original source
     *
     * @param float  $latitude
     * @param float  $longitude
     * @param string $country_code
     *
     * @return string
     */
    function get_nearest_timezone( float $latitude, float $longitude, string $country_code = '' ): string
    {
        $timezone_ids = ( $country_code ) ? DateTimeZone::listIdentifiers( DateTimeZone::PER_COUNTRY, $country_code )
            : DateTimeZone::listIdentifiers();

        if ( $timezone_ids && is_array( $timezone_ids ) && isset( $timezone_ids[0] ) ) {

            $time_zone   = '';
            $tz_distance = 0;

            //only one identifier?
            if ( count( $timezone_ids ) == 1 ) {
                $time_zone = $timezone_ids[0];
            } else {

                foreach ( $timezone_ids as $timezone_id ) {
                    $timezone = new DateTimeZone( $timezone_id );
                    $location = $timezone->getLocation();
                    $tz_lat   = $location['latitude'];
                    $tz_long  = $location['longitude'];

                    $theta    = $longitude - $tz_long;
                    $distance = ( sin( deg2rad( $latitude ) ) * sin( deg2rad( $tz_lat ) ) )
                                + ( cos( deg2rad( $latitude ) ) * cos( deg2rad( $tz_lat ) ) * cos( deg2rad( $theta ) ) );
                    $distance = acos( $distance );
                    $distance = abs( rad2deg( $distance ) );

                    if ( ! $time_zone || $tz_distance > $distance ) {
                        $time_zone   = $timezone_id;
                        $tz_distance = $distance;
                    }
                }
            }

            return $time_zone;
        }

        return 'unknown';
    }
}
