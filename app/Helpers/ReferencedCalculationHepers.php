<?php

if ( ! function_exists( 'getMortalityRate' ) ) {
    /**
     * Mortality rate calculation.
     * Uses externally referenced formula.
     * Returns a value between 0 and 1.
     *
     * @param int $startTotal Population number at the start of measuring period.
     * @param int $endTotal   Population number at the end of measuring period.
     *
     * @return float
     *
     * @source https://www.researchgate.net/publication/235257057_Calculating_and_Reporting_Managed_Honey_Bee_Colony_Losses
     */
    function getMortalityRate( int $startTotal, int $endTotal ): float
    {
        return ( $startTotal - $endTotal ) / $startTotal;
    }
}
