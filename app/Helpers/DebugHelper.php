<?php

use Illuminate\Database\Eloquent\Model;
use Laravel\Telescope\Telescope;

if ( ! function_exists( 'console_log' ) ) {
    /**
     * Display a debug message for Console Log.
     *
     * @param string $message
     * @param bool   $force
     */
    function console_log( string $message, bool $force = false ): void
    {
        if ( $force || config( 'app.debug' ) ) {
            echo $message . PHP_EOL;
        }
    }
}

if ( ! function_exists( 'stop_logging' ) ) {
    /**
     * Stop Query and Debug Logging.
     * Useful for processing and long running scripts.
     */
    function stop_logging() : void
    {
        Model::unguard();
        DB::disableQueryLog();
        Telescope::stopRecording();
        if ( app( 'debugbar' ) != null ) {
            app( 'debugbar' )->disable();
        }
    }
}

if ( ! function_exists( 'is_production' ) ) {
    /**
     * Check if we are in production environment.
     */
    function is_production() : bool
    {
        return config('app.env') == 'production';
    }
}

if ( ! function_exists( 'is_local' ) ) {
    /**
     * Check if we are in a local environment.
     */
    function is_local() : bool
    {
        return config('app.env') == 'local';
    }
}

if ( ! function_exists( 'is_development' ) ) {
    /**
     * Check if we are in a local environment.
     */
    function is_development() : bool
    {
        return config('app.env') == 'development';
    }
}
