<?php

if ( ! function_exists( 'bcround' ) ) {
    /**
     * BC Math extension helpers - rounding.
     *
     * @param string $value   The number to be rounded.
     * @param int    $decimal Decimal places.
     *
     * @return string
     */
    function bcround( string $value, int $decimal = 0 ): string
    {
        $e = bcpow( 10, $decimal + 1 );

        return bcdiv( bcadd( bcmul( $value, $e ), bcnegative( $value ) ? - 5 : 5 ), $e, $decimal );
    }
}

if ( ! function_exists( 'bcpositive' ) ) {
    /**
     * BC Math extension helpers - negating.
     *
     * @param string $value
     *
     * @return bool
     */
    function bcpositive( string $value ): bool
    {
        return ! str_contains( $value, '-' ); // Is the number less than 0?
    }
}

if ( ! function_exists( 'bcnegative' ) ) {
    /**
     * BC Math extension helpers - negating.
     *
     * @param string $value
     *
     * @return bool
     */
    function bcnegative( string $value ): bool
    {
        return str_starts_with( $value, '-' ); // Is the number less than 0?
    }
}

if ( ! function_exists( 'bcceil' ) ) {
    /**
     * BC Math extension helpers - round up to next largest whole number.
     *
     * @param string $value The number to be formatted.
     *
     * @return string
     */
    function bcceil( string $value ): string
    {
        return bcnegative( $value ) ? ( ( $v = bcfloor( substr( $value, 1 ) ) ) ? "-$v" : $v )
            : bcadd( strtok( $value, '.' ), strtok( '.' ) != 0 );
    }
}

if ( ! function_exists( 'bcfloor' ) ) {
    /**
     * BC Math extension helpers - round down to next largest whole number.
     *
     * @param string $value
     *
     * @return string
     */
    function bcfloor( string $value ): string
    {
        return bcnegative( $value ) ? '-' . bcceil( substr( $value, 1 ) ) : strtok( $value, '.' );
    }
}

if ( ! function_exists( 'bcextent' ) ) {
    /**
     * BC Math extension helpers - returns minimum and maximum value from a list of values.
     *
     * @param string[]|array|\Illuminate\Support\Collection $values
     *
     * @return array
     */
    function bcextent( \Illuminate\Support\Collection|array $values ): array
    {
        $min = null;
        $max = null;

        foreach ( $values as $value ) {
            if ( null === $min ) {
                $min = $value;
            } elseif ( bccomp( $min, $value ) === 1 ) {
                $min = $value;
            }
            if ( null === $max ) {
                $max = $value;
            } elseif ( bccomp( $max, $value ) === - 1 ) {
                $max = $value;
            }
        }

        return [ $min, $max ];
    }
}

if ( ! function_exists( 'bcmin' ) ) {
    /**
     * BC Math extension helpers - returns minimum value from list of values.
     *
     * @param string[]|array|\Illuminate\Support\Collection $values
     *
     * @return string|null
     */
    function bcmin( \Illuminate\Support\Collection|array $values ): ?string
    {
        $min = null;

        foreach ( $values as $value ) {
            if ( null === $min ) {
                $min = $value;
            } elseif ( bccomp( $min, $value ) === 1 ) {
                $min = $value;
            }
        }

        return $min;
    }
}

if ( ! function_exists( 'bcmax' ) ) {
    /**
     * BC Math extension helpers - returns maximum value from list of values.
     *
     * @param string[]|array|\Illuminate\Support\Collection $values
     *
     * @return string|null
     */
    function bcmax( \Illuminate\Support\Collection|array $values ): ?string
    {
        $max = null;
        foreach ( $values as $value ) {
            if ( null === $max ) {
                $max = $value;
            } elseif ( bccomp( $max, $value ) === - 1 ) {
                $max = $value;
            }
        }

        return $max;
    }
}


if ( ! function_exists( 'bcsum' ) ) {
    /**
     * BC Math extension helpers - sums all values.
     *
     * @param string[]|array|\Illuminate\Support\Collection $values
     *
     * @return string|null
     */
    function bcsum( \Illuminate\Support\Collection|array $values ): ?string
    {
        if ( is_a( $values, \Illuminate\Support\Collection::class ) ) {
            $values = $values->toArray();
        }

        $sum = '0.0';

        foreach ( $values as $value ) {
            $sum = bcadd( $value, $sum );
        }

        return $sum;
    }
}

if ( ! function_exists( 'bcmedian' ) ) {
    /**
     * BC Math extension helpers - caluclate median value.
     *
     * @param string[]|array|\Illuminate\Support\Collection $values
     * @param bool                                          $bSorted
     *
     * @return string|null
     */
    function bcmedian( \Illuminate\Support\Collection|array $values, bool $bSorted = false ): ?string
    {
        if ( is_a( $values, \Illuminate\Support\Collection::class ) ) {
            $values = $values->toArray();
        }

        if ( count( $values ) == 0 ) {
            return null;
        }

        if ( ! $bSorted ) {
            usort( $values, 'bcsort' );
        }

        $middle = count( $values ) / 2;

        if ( is_float( $middle ) ) {
            return $values[ (int) $middle ];
        } else {
            return bcdiv( bcadd( $values[ $middle - 1 ], $values[ $middle ] ), '2' );
        }
    }
}

if ( ! function_exists( 'bcaverage' ) ) {
    /**
     * BC Math extension helpers - calculate average value.
     *
     * @param string[]|array|\Illuminate\Support\Collection $values
     *
     * @return string|null
     */
    function bcaverage( \Illuminate\Support\Collection|array $values ): ?string
    {
        if ( is_a( $values, \Illuminate\Support\Collection::class ) ) {
            $values = $values->toArray();
        }

        if ( count( $values ) == 0 ) {
            return null;
        }

        return bcdiv( bcsum( $values ), count( $values ) );
    }
}
if ( ! function_exists( 'bcstandarddeviation' ) ) {
    /**
     * BC Math extension helpers - calculate standard deviation.
     *
     * @param string[]|array|\Illuminate\Support\Collection $values
     *
     * @return string|null
     */
    function bcstandarddeviation( \Illuminate\Support\Collection|array $values ): ?string
    {
        if ( is_a( $values, \Illuminate\Support\Collection::class ) ) {
            $values = $values->toArray();
        }

        if ( count( $values ) == 0 ) {
            return null;
        }
        $average     = bcaverage( $values );
        $differences = '0.0';

        foreach ( $values as $value ) {
            $differences = bcadd( $differences, bcpow( bcsub( $value, $average ), 2 ) );
        }

        return bcsqrt( bcdiv( $differences, count( $values ) ) );
    }
}


if ( ! function_exists( 'bcsort' ) ) {
    /**
     * BC Math extension helpers - quick sort value compare function.
     *
     * @param string $valueA
     * @param string $valueB
     *
     * @return int
     */
    function bcsort( string $valueA, string $valueB ): int
    {
        return bccomp( $valueA, $valueB );
    }
}


if ( ! function_exists( 'bcsortdesc' ) ) {
    /**
     * BC Math extension helpers - quick sort value compare function, descending order.
     *
     * @param string $valueA
     * @param string $valueB
     *
     * @return int
     */
    function bcsortdesc( string $valueA, string $valueB ): int
    {
        return bccomp( $valueB, $valueA );
    }
}


if ( ! function_exists( 'bcabs' ) ) {
    /**
     * BC Math extension helpers - get absolute value.
     *
     * @param string $value
     *
     * @return string
     */
    function bcabs( string $value ): string
    {
        if ( bcnegative( $value ) ) {
            return substr( $value, 1 );
        }

        return $value;
    }
}

