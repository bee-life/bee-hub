<?php

namespace App\Imports;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class CommaCSVImport implements ToCollection, WithCustomCsvSettings
{
    /**
     * @param Collection $collection
     */
    public function collection( Collection $rows )
    {
        // TODO: Implement collection() method.
    }

    /**
     * @inheritDoc
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ','
        ];
    }
}
