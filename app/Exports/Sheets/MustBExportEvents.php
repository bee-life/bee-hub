<?php namespace App\Exports\Sheets;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class MustBExportEvents implements FromCollection, WithTitle, WithHeadings
{
    private Collection $data;

    public function __construct( Collection $data )
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Events';
    }


    public function headings(): array
    {
        return [
                'Date',
                //   'Outside Temperature Minimum',
                //   'Outside Temperature maximum',
                'Hive Temperature Minimum',
                'Hive Temperature maximum',
                'Hive End Weight',
                'Average Hive Humidity',
                'Daily Production',
                'Maximum Available Flight Time',
                //'Metabolic Resting State Consumption',
        ];
    }
}
