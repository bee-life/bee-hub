<?php

namespace App\Exports;

use App\Exports\Sheets\MustBExportDaily;
use App\Exports\Sheets\MustBExportEvents;
use App\Exports\Sheets\MustBExportYearly;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class MustBExport implements WithMultipleSheets
{
    private Collection $data;

    public function __construct( Collection $data )
    {
        $this->data = $data;
    }


    public function sheets(): array
    {
        $sheets = [
            new MustBExportDaily( $this->data['daily']->sortKeys() ),
            new MustBExportYearly( $this->data['yearly']->sortKeys() ),
            new MustBExportEvents( $this->data['events']->sortKeys() ),
        ];

        return $sheets;
    }
}
