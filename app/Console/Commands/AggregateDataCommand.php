<?php

namespace App\Console\Commands;

use App\Data\Aggregators\HiveSensorAverages;
use App\Data\Aggregators\HiveWeightAlgorithms;
use App\Data\Aggregators\SocioEconomicAlgorithms;
use App\Data\Aggregators\WeatherAverages;
use App\Data\Aggregators\WinterMortalityCounts;
use App\Jobs\AggregateDataJob;
use DB;
use Debugbar;
use Illuminate\Console\Command;
use Schema;

class AggregateDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:aggregate {--queue}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Aggregate data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if ( $this->hasArgument( 'queue' ) ) {
            AggregateDataJob::dispatch();
        } else {
            AggregateDataJob::dispatchSync( false );
        }
    }
}
