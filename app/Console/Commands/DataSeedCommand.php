<?php

namespace App\Console\Commands;

use Artisan;
use Illuminate\Console\Command;

class DataSeedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:seed-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed all available local data sources.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        Artisan::call( 'db:seed', [ '--class' => 'DataSeeder' ] );
        echo Artisan::output();
    }
}
