<?php

namespace App\Console\Commands;

use App\Jobs\ConnectLiveDataJob;
use Illuminate\Console\Command;

class ConnectLiveData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:live {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Queries remote sources via connectors to load live data.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle(): void
    {
        // The script might take a while
        set_time_limit( 0 );
        ini_set('memory_limit', '1024M');
        stop_logging();

        $force = $this->option('force');

        ConnectLiveDataJob::dispatchSync($force);
    }
}
