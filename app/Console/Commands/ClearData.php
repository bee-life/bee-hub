<?php

namespace App\Console\Commands;

use Cache;
use DB;
use Illuminate\Console\Command;
use Schema;

class ClearData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:clear {--confirm}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all Bee-Hub data from the database. Can not be used in production.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {

        if(is_production() && ! $this->hasArgument('confirm')){
            $this->error("The --confirm argument is required to run this command.");
            return;
        }

        stop_logging();

        // Clear all data to allow re-seeding
        Schema::disableForeignKeyConstraints();
        DB::table( 'project_to_provider' )->truncate();
        DB::table( 'log_data_aggregate_to_project' )->truncate();
        DB::table( 'project_to_descriptor' )->truncate();

        DB::table( 'log_to_data' )->truncate();
        DB::table( 'log_data_aggregates' )->truncate();
        DB::table( 'log_data_booleans' )->truncate();
        DB::table( 'log_data_dates' )->truncate();
        DB::table( 'log_data_datetimes' )->truncate();
        DB::table( 'log_data_datetime_ranges' )->truncate();
        DB::table( 'log_data_date_ranges' )->truncate();
        DB::table( 'log_data_decimals' )->truncate();
        DB::table( 'log_data_decimal_ranges' )->truncate();
        DB::table( 'log_data_events' )->truncate();
        DB::table( 'log_data_ids' )->truncate();
        DB::table( 'log_data_integers' )->truncate();
        DB::table( 'log_data_integer_ranges' )->truncate();
        DB::table( 'log_data_locations' )->truncate();
        DB::table( 'log_data_percentages' )->truncate();
        DB::table( 'log_data_strings' )->truncate();
        DB::table( 'log_data_text' )->truncate();
        DB::table( 'log_data_times' )->truncate();
        DB::table( 'log_data_time_ranges' )->truncate();

        DB::table( 'log_reference_currencies' )->truncate();
        DB::table( 'log_reference_land_usages' )->truncate();
        DB::table( 'log_reference_pesticides' )->truncate();
        DB::table( 'log_reference_pollen' )->truncate();

        DB::table( 'origin_devices' )->truncate();
        DB::table( 'origin_methodologies' )->truncate();
        DB::table( 'origin_publications' )->truncate();
        DB::table( 'origin_sources' )->truncate();

        DB::table( 'origin_device_categories' )->truncate();
        DB::table( 'origin_methodology_categories' )->truncate();
        DB::table( 'origin_vendors' )->truncate();

        DB::table( 'reference_crop_species' )->truncate();
        DB::table( 'reference_land_usages' )->truncate();
        DB::table( 'reference_pesticides' )->truncate();
        DB::table( 'reference_pollen' )->truncate();

        DB::table( 'reference_pesticide_types' )->truncate();

        DB::table( 'descriptors' )->truncate();
        DB::table( 'logs' )->truncate();
        DB::table( 'providers' )->truncate();
        DB::table( 'projects' )->truncate();
        Schema::enableForeignKeyConstraints();

        // Clear any caches, related to data
        Cache::clear();
    }
}
