<?php

namespace App\Console\Commands;

use Artisan;
use Illuminate\Console\Command;

class EventSeedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed all available local data sources.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        Artisan::call( 'db:seed', [ '--class' => 'EventSeeder' ] );
        echo Artisan::output();

        $this->comment( 'Querying Live data' );
        //Artisan::call( 'data:live --force' );
        echo Artisan::output();

        $this->comment( 'Re-aggregating data' );
        //Artisan::call( 'data:aggregate' );
        echo Artisan::output();
    }
}
