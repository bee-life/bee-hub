<?php

namespace App\Console\Commands;

use App\Jobs\CacheDataJob;
use Illuminate\Console\Command;


/**
 * Class PreloadChartsCommand
 * Used to handle preloading of Chart data into Cache.
 *
 * @package App\Console\Commands
 */
class ChartCacheCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:cache {--queue}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache existing data for faster front-end loading';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        if ( $this->hasArgument( 'queue' ) ) {
            CacheDataJob::dispatch();
        } else {
            CacheDataJob::dispatchSync( false );
        }
    }
}
