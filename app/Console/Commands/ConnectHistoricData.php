<?php

namespace App\Console\Commands;

use App\Data\Connectors\BaseConnector;
use App\Models\MetaData\Project;
use Exception;
use Illuminate\Console\Command;
use Sentry;

class ConnectHistoricData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:historic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Queries remote sources via connectors to load any historic data not yet loaded.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        // The script might take a while
        set_time_limit( 0 );
        ini_set( 'memory_limit', '512M' );
        stop_logging();

        $connectors = Project::where( 'historic_sync', false )->get();

        foreach ( $connectors as $connector ) {
            if ( empty( $connector->class_name ) || ! class_exists( $connector->class_name ) ) {
                continue;
            }
            try {
                $this->info( 'Loading Connector class ' . $connector->class_name );
                /** @var BaseConnector $connection */
                $connection = new $connector->class_name( $connector );
                $connection->getHistoricRemoteData();

                $connector->historic_sync = true;
                $connector->save();

                $this->info( 'Connector class ' . $connector->class_name . ' finnished.' );
            } catch ( Exception $e ) {
                Sentry::captureException( $e );
                $this->error( $e->getMessage() );
                $this->info( $e->getFile() . ' on line ' . $e->getLine() );
                $this->info( $e->getTraceAsString() );
            }
        }
    }
}
