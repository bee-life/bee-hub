<?php

namespace App\Console\Commands;

use App\Models\Logs\Data\DataId;
use App\Models\Logs\Descriptor;
use App\Models\Logs\Log;
use App\Models\Project;
use Illuminate\Console\Command;
use \Excel;

class MustBExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:mustb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export data related to the MustB project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data_ids = DataId::where( 'project_id', Project::getIdFromUid( 'efsa-mustb-2020' ) )->whereNotNull( 'parent_id' )->get()->reverse();

        $basepath = base_path( '/public/exports/mustb' );

        $descriptors = Descriptor::whereIn( 'uid', [
            'event-scale-weight',
            'event-hive-management',
            'hive-production',
            'hive-production-period',
            'hive-super-production-period',
            'hive-wintering-duration',
            'hive-wintering-consumption',
            'hive-metabolic-resting-state',
            'maximum-available-flight-time',
            'hive-temperature-daily',
            'hive-weight',
        ] )->get()->keyBy( 'uid' );
        $eventDescriptors = Descriptor::whereIn( 'uid', [
               'event-monitoring',
               'event-scale-weight',
               'event-sampling',
               'event-hive-management',
        ] )->get()->keyBy( 'uid' );


        foreach ( $data_ids as $id ) {
            dump( $id->parent->value . ' ' . $id->value );

            $apiaryAggregates = $id->parent->apiaryLogs()->where( 'project_id', Project::getIdFromUid( 'eubeeppp' ) )->whereNull( 'time' )->orderBy( 'date' )->with( [
                'data',
                'data.data'
            ] )->get()->keyBy( 'raw_date' );

            $aggregates = $id->hiveLogs()->whereNotNull( 'date' )->whereNull( 'time' )->orderBy( 'date' )->with( [
                'data',
                'data.data'
            ] )->get()->keyBy( 'raw_date' );

            $yearlyAggregates = $id->hiveLogs()->whereNull( 'date' )->whereNull( 'time' )->orderBy( 'year' )->with( [
                'data',
                'data.data'
            ] )->get()->keyBy( 'year' );

            $events = $id->hiveLogs()->whereHas( 'data', function ( $q ) use ( $eventDescriptors ) {
                $q->whereIn( 'descriptor_id', $eventDescriptors->pluck('id') )->with( 'data' );
            } )->orderBy( 'date' )->orderBy( 'time' )->with( [
                'data',
                'data.data',
            ] )->get()->groupBy( 'raw_date' );;

            $data = collect( [ 'daily' => collect(), 'events' => collect(), 'yearly' => collect() ] );

            dump($events->count());
            continue;
            foreach ( $yearlyAggregates as $aggregate ) {

            }

            foreach ( $aggregates as $aggregate ) {
                /** @var Log $aggregate */

                $temperature = $aggregate->data->where( 'descriptor_id', $descriptors['hive-temperature-daily']->id )->first();
                $weight      = $aggregate->data->where( 'descriptor_id', $descriptors['hive-weight']->id )->first();
                $humidity    = $aggregate->data->where( 'descriptor_id', $descriptors['hive-relative-humidity']->id )->first();
                $production  = $aggregate->data->where( 'descriptor_id', $descriptors['hive-production']->id )->first();
                $flightTime = $aggregate->data->where( 'descriptor_id', $descriptors['maximum-available-flight-time']->id )->first();

                $data['daily'][ $aggregate->raw_date ] = collect([
                    0 => $aggregate->raw_date,
                    1 => isset( $temperature ) ? format_number( $temperature->value[0] ) : '',
                    2 => isset( $temperature ) ? format_number( $temperature->value[1] ) : '',
                    3 => $weight ? format_number( $weight->value ) : '',
                    4 => $humidity ? format_number( $humidity->value ) : '',
                    5 => $production ? format_number( $production->value ) : '',
                    6 => $flightTime ? format_number( $flightTime->value ) : ''

                ]);
            }
            foreach ( $apiaryAggregates->skip( '10' ) as $aggregate ) {
                $flightTime = $aggregate->data->where( 'descriptor_id', $descriptors['maximum-available-flight-time']->id )->first();

                if ( isset( $data['daily'][ $aggregate->raw_date ] ) ) {
                    $data['daily'][ $aggregate->raw_date ]->put( 6, $flightTime ? format_number( $flightTime->value ) : '' );
                }
            }


            Excel::store( new \App\Exports\MustBExport( $data ), $id->value . '.xlsx', null, \Maatwebsite\Excel\Excel::XLSX );
        }


        return 0;
    }
}
