<?php

namespace App\Console;

use App\Jobs\ConnectLiveDataJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Laravel\Nova\Trix\PruneStaleAttachments;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule( Schedule $schedule )
    {
        // $schedule->command('inspire')
        //          ->hourly();

        // Remove any temporary attachments from database and storage.
        // @see https://nova.laravel.com/docs/2.0/resources/fields.html#file-uploads
        $schedule->call( function () {
            ( new PruneStaleAttachments )();
        } )->daily();

        $schedule->job( new ConnectLiveDataJob() )->hourly();        //
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load( __DIR__ . '/Commands' );

        require base_path( 'routes/console.php' );
    }
}
