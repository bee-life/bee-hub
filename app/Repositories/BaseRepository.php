<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class BaseRepository
{
    /**
     * @var Model An instance of the Eloquent Model
     */
    protected $model;


    /**
     * Get a resource by id.
     *
     * @param int $id
     *
     * @return Model
     *
     * @throws ModelNotFoundException
     */
    public function find( $id )
    {
        return $this->model->findOrFail( $id );
    }


    /**
     * Return a collection of all elements of the resource.
     *
     * @return Collection|Model[]
     */
    public function all()
    {
        return $this->model->orderBy( 'created_at', 'DESC' )->get();
    }


    /**
     * Insert a new row.
     *
     * @param mixed $data
     *
     * @return Model
     */
    public function create( $data )
    {
        return $this->model->create( $data );
    }

    /**
     * @param Model $model
     * @param array $data
     *
     * @return Model
     */
    public function update( $model, $data )
    {
        $model->update( $data );

        return $model;
    }

    /**
     * Delete a resource to trash.
     *
     * @param Model|int $arg
     *
     * @return bool
     */
    public function delete( $arg )
    {
        if ( is_numeric( $arg ) ) {
            $arg = $this->model->find( (int) $arg );
        }

        return $arg->delete();
    }

    /**
     * Move a resource from trash.
     *
     * @param int $id
     *
     * @return bool
     */
    public function unDelete( $id )
    {
        $model = $this->model->onlyTrashed()->find( $id );

        //$model->deleted_by_user()->dissociate();

        return $model->restore();
    }

    /**
     * Delete a resource from database.
     *
     * @param Model|int $arg
     *
     * @return bool
     */
    public function destroy( $arg )
    {
        if ( is_numeric( $arg ) ) {
            return $this->model->where( 'id', (int) $arg )->first()->delete();
        } else {
            return $arg->forceDelete();
        }
    }


    /**
     * Find a resource by the given slug
     *
     * @param string $slug
     *
     * @return Model
     *
     * @throws ModelNotFoundException
     */
    public function findBySlug( string $slug )
    {
        return $this->model->where( 'slug', $slug )->firstOrFail();
    }

    /**
     * Find a resource by the given slug
     *
     * @param string $slug
     *
     * @return Model
     *
     * @throws ModelNotFoundException
     */
    public function findByUid( string $uid ): Model
    {
        return $this->model->where( 'uid', $uid )->firstOrFail();
    }

    /**
     * Find a resource by an array of attributes
     *
     * @param array $attributes
     *
     * @return Model
     */
    public function findByAttributes( array $attributes )
    {
        $query = $this->buildQueryByAttributes( $attributes );

        return $query->first();
    }

    /**
     * Build Query to catch resources by an array of attributes and params
     *
     * @param array       $attributes
     * @param null|string $orderBy
     * @param string      $sortOrder
     *
     * @return \Illuminate\Database\Query\Builder object
     */
    private function buildQueryByAttributes( array $attributes, $orderBy = null, $sortOrder = 'asc' )
    {
        $query = $this->model->query();

        foreach ( $attributes as $field => $value ) {
            $query = $query->where( $field, $value );
        }

        if ( null !== $orderBy ) {
            $query->orderBy( $orderBy, $sortOrder );
        }

        return $query;
    }

    /**
     * Get resources by an array of attributes
     *
     * @param array       $attributes
     * @param null|string $orderBy
     * @param string      $sortOrder
     *
     * @return Collection|Model[]
     */
    public function getByAttributes( array $attributes, $orderBy = null, $sortOrder = 'asc' )
    {
        $query = $this->buildQueryByAttributes( $attributes, $orderBy, $sortOrder );

        return $query->get();
    }

    /**
     * Return a collection of elements who's ids match
     *
     * @param array $ids
     *
     * @return Collection|Model[]
     */
    public function findByMany( array $ids )
    {
        $query = $this->model->query();

        return $query->whereIn( "id", $ids )->get();
    }
}
