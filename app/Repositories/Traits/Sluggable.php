<?php

namespace App\Repositories\Traits;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;

trait Sluggable{

    /**
     * Find a resource by the given slug
     *
     * @param  string $slug
     *
     * @return BaseModel
     *
     * @throws ModelNotFoundException
     */
    public function findBySlug( $slug )
    {
        return $this->model->where( 'slug', $slug )->firstOrFail();
    }
}
