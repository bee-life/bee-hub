<?php namespace App\Repositories;

use App\Models\AuditLog;
use Illuminate\Support\Collection;

class AuditLogsRepository extends BaseRepository
{
    /**
     * @param AuditLog $model
     */
    public function __construct(AuditLog $model)
    {
        $this->model = $model;
    }

    /**
     * Return a collection of all elements of the resource.
     *
     * @return Collection|AuditLog[]
     */
    public function all(){
        return $this->model->orderBy( 'created_at', 'DESC' )->get();
    }
}
