<?php namespace App\Repositories;

use App\Models\MetaData\Provider;
use Auth;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProviderRepository extends BaseRepository
{

    /**
     * @param Provider $model
     */
    public function __construct( Provider $model )
    {
        $this->model = $model;
    }

    /**
     * Return a collection of all elements of the resource.
     *
     * @return Collection|Provider[]
     */
    public function all()
    {
        return Auth::user()->providers()->orderBy( 'created_at', 'DESC' )->get();
    }

    /**
     * Get a resource by id.
     *
     * @param int $id
     *
     * @return Provider|Model
     *
     * @throws ModelNotFoundException
     */
    public function find( $id )
    {
        return Auth::user()->providers()->findOrFail( $id );
    }
}
