<?php

namespace App\Repositories;

use App\Models\CMS\Page;
use App\Repositories\Traits\Sluggable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PageRepository extends BaseRepository
{
    use Sluggable;

    public function __construct(Page $model)
    {
        $this->model = $model;
    }

}
