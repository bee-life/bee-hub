<?php namespace App\Repositories;

use App\Models\MetaData\Project;
use Auth;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProjectRepository extends BaseRepository
{

    /**
     * @param Project $model
     */
    public function __construct( Project $model )
    {
        $this->model = $model;
    }

    /**
     * Return a collection of all elements of the resource.
     *
     * @return Collection|Model[]
     */
    public function all()
    {
        return Auth::user()->projects()->orderBy( 'created_at', 'DESC' )->get();
    }

    /**
     * Get a resource by id.
     *
     * @param int $id
     *
     * @return Model
     *
     * @throws ModelNotFoundException
     */
    public function find( $id )
    {
        return Auth::user()->projects()->findOrFail( $id );
    }


    /**
     * Find a resource by the given slug.
     *
     * @param string  $slug
     * @param bool $admin
     *
     * @return Project
     */
    public function findBySlug( string $slug, bool $admin = false ): Project
    {
        return $this->model->where( 'slug', $slug )->where( 'admin', $admin )->firstOrFail();
    }

}
