<?php

namespace App\Repositories;

use App\Models\CMS\News;
use App\Repositories\Traits\Sluggable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class NewsRepository extends BaseRepository
{
    use Sluggable;

    public function __construct(News $model)
    {
        $this->model = $model;
    }

    public function latest( int $limit = 4 )
    {
        return $this->model->orderBy( 'published_at', 'DESC' )->limit( $limit )->get();
    }
}
