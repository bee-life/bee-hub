<?php

namespace App\Exceptions;

use Exception;

class CountryNotFoundException extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report(): void
    {
        \Log::error("Country with code '$this->message' was not found.");
    }

    /**
     * String representation of the exception
     * @link https://php.net/manual/en/exception.tostring.php
     * @return string the string representation of the exception.
     * @since 5.1.0
     */
    public function __toString(): string {
        return "Country with code '$this->message' was not found. Check spelling and make sure all seeders are run first.";
    }
}
