<?php

namespace App\Exceptions;

use Exception;

class InvalidValueException extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report(): void
    {
        \Log::error( "Unable to parse string to value. Found invalid value '$this->message' in dataset." );
    }

    /**
     * String representation of the exception
     * @link  https://php.net/manual/en/exception.tostring.php
     * @return string the string representation of the exception.
     * @since 5.1.0
     */
    public function __toString(): string
    {
        return "Unable to parse string to value. Found invalid value '$this->message' in dataset.";
    }
}
