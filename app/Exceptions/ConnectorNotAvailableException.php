<?php

namespace App\Exceptions;

use Exception;
use Log;

class ConnectorNotAvailableException extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report(): void
    {
        Log::error("The Connector $this->message seems currently unreachable. It could be temporary connection problems. If it persists, there might be a problem with the Connector.");
    }

    /**
     * String representation of the exception
     * @link https://php.net/manual/en/exception.tostring.php
     * @return string the string representation of the exception.
     * @since 5.1.0
     */
    public function __toString(): string {
        return "The Connector $this->message seems currently unreachable. It could be temporary connection problems. If it persists, there might be a problem with the Connector.";
    }
}
