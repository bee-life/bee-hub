<?php

namespace App\Exceptions\Interactions;

use Exception;

class InvalidValueException extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report(): void
    {
        \Log::error( "Supplied value is invalid." );
    }

    /**
     * String representation of the exception
     * @link  https://php.net/manual/en/exception.tostring.php
     * @return string the string representation of the exception.
     * @since 5.1.0
     */
    public function __toString(): string
    {
        return "Supplied value is invalid.";
    }
}
