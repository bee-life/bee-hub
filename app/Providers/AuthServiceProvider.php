<?php

namespace App\Providers;

use App\Models\Locations\Country;
use App\Models\Locations\Lau;
use App\Models\Locations\Post;
use App\Models\Locations\Region;
use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\MetaData\DescriptorCategory;
use App\Models\MetaData\Project;
use App\Models\MetaData\Provider;
use App\Models\CMS\News;
use App\Models\Origins\Device;
use App\Models\Origins\DeviceCategory;
use App\Models\Origins\MethodologyCategory;
use App\Models\Origins\Publication;
use App\Models\Origins\Source;
use App\Models\Origins\Vendor;
use App\Models\CMS\Page;
use App\Models\References\CropSpecies;
use App\Models\References\Currency;
use App\Models\References\LandUse;
use App\Models\References\Pesticide;
use App\Models\References\PesticideType;
use App\Models\References\Pollen;
use App\Models\User;
use App\Nova\Origins\Methodology;
use App\Policies\CountryPolicy;
use App\Policies\DataPolicy;
use App\Policies\DescriptorCategoryPolicy;
use App\Policies\DescriptorPolicy;
use App\Policies\LocationPolicy;
use App\Policies\LogPolicy;
use App\Policies\MenuPolicy;
use App\Policies\NewsPolicy;
use App\Policies\OriginCategorizationPolicy;
use App\Policies\OriginPolicy;
use App\Policies\PagePolicy;
use App\Policies\ProjectPolicy;
use App\Policies\ProviderPolicy;
use App\Policies\ReferencePolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use OptimistDigital\MenuBuilder\Models\Menu;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //Data
        Project::class             => ProjectPolicy::class,
        Provider::class            => ProviderPolicy::class,
        Descriptor::class          => DescriptorPolicy::class,
        DescriptorCategory::class  => DescriptorCategoryPolicy::class,
        Log::class                 => LogPolicy::class,
        Data::class                => DataPolicy::class,
        /* DataAggregate::class       => DataPolicy::class,
         DataBoolean::class         => DataPolicy::class,
         DataDate::class            => DataPolicy::class,
         DataDateRange::class       => DataPolicy::class,
         DataDatetime::class        => DataPolicy::class,
         DataDatetimeRange::class   => DataPolicy::class,
         DataDecimal::class         => DataPolicy::class,
         DataDecimalRange::class    => DataPolicy::class,
         DataEvent::class           => DataPolicy::class,
         DataId::class              => DataPolicy::class,
         DataInteger::class         => DataPolicy::class,
         DataIntegerRange::class    => DataPolicy::class,
         DataLocation::class        => DataPolicy::class,
         DataPercentage::class      => DataPolicy::class,
         DataString::class          => DataPolicy::class,
         DataText::class            => DataPolicy::class,
         DataTime::class            => DataPolicy::class,
         DataTimeRange::class       => DataPolicy::class,
         ReferenceCurrency::class   => DataPolicy::class,
         ReferenceLandUsage::class  => DataPolicy::class,
         ReferencePesticide::class  => DataPolicy::class,
         ReferencePollen::class     => DataPolicy::class,
     */
        // Origin
        Methodology::class         => OriginPolicy::class,
        Device::class              => OriginPolicy::class,
        Publication::class         => OriginPolicy::class,
        Source::class              => OriginPolicy::class,

        // Origin Categorization
        MethodologyCategory::class => OriginCategorizationPolicy::class,
        DeviceCategory::class      => OriginCategorizationPolicy::class,
        Vendor::class              => OriginCategorizationPolicy::class,

        // Locations
        Country::class       => CountryPolicy::class,
        Region::class        => LocationPolicy::class,
        Lau::class           => LocationPolicy::class,
        Post::class          => LocationPolicy::class,

        // References
        CropSpecies::class   => ReferencePolicy::class,
        Currency::class      => ReferencePolicy::class,
        LandUse::class       => ReferencePolicy::class,
        Pesticide::class     => ReferencePolicy::class,
        PesticideType::class => ReferencePolicy::class,
        Pollen::class        => ReferencePolicy::class,

        // CRM
        Page::class          => PagePolicy::class,
        News::class          => NewsPolicy::class,
        Menu::class          => MenuPolicy::class,
        User::class          => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
