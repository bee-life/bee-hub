<?php

namespace App\Providers;


use App\Helpers\JSMin;
use App\Models\Logs\Data\DataLocation;
use Blade;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Schema;

class CarbonServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        parent::register();

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Carbon::macro( 'isMorning', static function ( DataLocation $location ) {
            $current = self::this()->timestamp;
            $sunrise = date_sunrise( $current, SUNFUNCS_RET_TIMESTAMP, $location->value->getLat(), $location->value->getLng() );
            $noon    = self::this()->clone()->setTime( 12, 0, 0 )->timestamp;

            return $sunrise < $current && $current < $noon;
        } );

        Carbon::macro( 'isDay', static function ( DataLocation $location ) {
            $current = self::this()->timestamp;
            $sunrise = date_sunrise( $current, SUNFUNCS_RET_TIMESTAMP, $location->value->getLat(), $location->value->getLng() );
            $sunset  = date_sunset( $current, SUNFUNCS_RET_TIMESTAMP, $location->value->getLat(), $location->value->getLng() );

            return $sunrise < $current && $current < $sunset;
        } );

        Carbon::macro( 'isNight', static function ( DataLocation $location ) {
            return ! self::this()->isDay( $location );
        } );
    }
}
