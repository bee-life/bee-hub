<?php

namespace App\Providers;


use App\Events\AuditLoggingObserver;
use App\Helpers\JSMin;
use App\Models\MetaData\Project;
use App\Models\MetaData\Provider;
use Blade;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton( 'jsmin', function () {
            return new JSMin();
        } );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        bcscale( 5 );

        // Setup Audit Logging
        Project::observe(AuditLoggingObserver::class);
        Provider::observe(AuditLoggingObserver::class);

        Paginator::useBootstrap();
        Schema::defaultStringLength( 191 );

        Blade::aliasComponent( 'components.popup-card', 'popup' );

        Relation::morphMap( [
            'b'  => \App\Models\Logs\Data\DataBoolean::class,
            'da' => \App\Models\Logs\Data\DataDate::class,
            'ar' => \App\Models\Logs\Data\DataDateRange::class,
            'dt' => \App\Models\Logs\Data\DataDatetime::class,
            'tr' => \App\Models\Logs\Data\DataDatetimeRange::class,
            'd'  => \App\Models\Logs\Data\DataDecimal::class,
            'dr' => \App\Models\Logs\Data\DataDecimalRange::class,
            'id' => \App\Models\Logs\Data\DataId::class,
            'i'  => \App\Models\Logs\Data\DataInteger::class,
            'ir' => \App\Models\Logs\Data\DataIntegerRange::class,
            'l'  => \App\Models\Logs\Data\DataLocation::class,
            'p'  => \App\Models\Logs\Data\DataPercentage::class,
            's'  => \App\Models\Logs\Data\DataString::class,
            't'  => \App\Models\Logs\Data\DataText::class,
            'h'  => \App\Models\Logs\Data\DataTime::class,
            'hr' => \App\Models\Logs\Data\DataTimeRange::class,
            'e'  => \App\Models\Logs\Data\DataEvent::class,

            '10' => \App\Models\Logs\Reference\ReferencePollen::class,
            '11' => \App\Models\Logs\Reference\ReferencePesticide::class,
            '12' => \App\Models\Logs\Reference\ReferenceLandUse::class,
            '13' => \App\Models\Logs\Reference\ReferenceCurrency::class,

            '1' => \App\Models\Origins\Methodology::class,
            '2' => \App\Models\Origins\Device::class,
            '3' => \App\Models\Origins\Source::class,
            '4' => \App\Models\Origins\Publication::class,
        ] );


        Collection::macro( 'flattenWithKeys', function ( $concator = '.', $prefix = '' ) {
            $result = collect();

            foreach ( $this as $key => $value ) {
                $new_key = $prefix . ( empty( $prefix ) ? '' : $concator ) . $key;

                if ( is_a( $value, Collection::class ) ) {
                    $result = $result->merge( $value->flattenWithKeys( $concator, $new_key ) );
                } else {
                    $result[ $new_key ] = $value;
                }
            }

            return $result;
        } );
    }
}
