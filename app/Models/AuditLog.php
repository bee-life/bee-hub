<?php namespace App\Models;

use App\Models\Traits\Base;
use App\Models\Traits\Owner;
use App\Models\Traits\Timestamps;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;


/**
 * App\Models\AuditLog
 *
 * @property int                 $id
 * @property string              $field
 * @property string              $old_value
 * @property string              $new_value
 * @property int                 $user_id
 * @property Carbon              $created_at
 * @property string              $target_type
 * @property int|null            $target_id
 * @property-read Model|Eloquent $target
 * @property-read User           $user
 * @method static Builder|AuditLog whereField( $value )
 * @method static Builder|AuditLog whereId( $value )
 * @method static Builder|AuditLog whereNewValue( $value )
 * @method static Builder|AuditLog whereOldValue( $value )
 * @method static Builder|AuditLog whereTargetId( $value )
 * @method static Builder|AuditLog whereTargetType( $value )
 * @method static Builder|AuditLog newModelQuery()
 * @method static Builder|AuditLog newQuery()
 * @method static Builder|AuditLog query()
 * @method static Builder|AuditLog whereCreatedAt( $value )
 * @method static Builder|AuditLog whereUserId( $value )
 * @mixin Eloquent
 */
class AuditLog extends Model
{

    use Base, Owner, Timestamps;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'audit_logs';


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'created_at' ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'field',
        'old_value',
        'new_value',
        'user_id',
        'trigger',
        'user_id',
        'confirmation'
    ];


    /**
     * Get target object, the audit log belongs to.
     *
     * @return MorphTo
     */
    public function target(): MorphTo
    {
        return $this->morphTo();
    }


    /**
     * Get field name.
     *
     * @return string
     */
    public function getFieldName(): string
    {
        switch ( $this->field ) {
            case 'created':
            case 'trashed':
            case 'restored':
            case 'deleted':
                return trans( 'admin.audit.' . $this->field );
        }
        if ( str_contains( $this->field, '_id' ) ) {
            return trans( 'admin.attributes.' . substr( $this->field, 0, - 3 ) );
        } else {
            return trans( 'admin.attributes.' . $this->field );
        }
    }

    /**
     * Get nicer old value.
     *
     * @return string
     */
    public function getOldValue(): string
    {
        return $this->getValue( $this->old_value );
    }

    /**
     * Get nicer new value.
     *
     * @return string
     */
    public function getNewValue(): string
    {
        return $this->getValue( $this->new_value );
    }

    /**
     * Transforms Value to a more nicer Human readable format, if necesary.
     *
     * @param string|int $value
     *
     * @return string
     */
    protected function getValue( string|int $value ): string
    {
        if ( ! isset( $value ) ) {
            return trans( 'admin.audit.null' );
        }

        return match ( $this->field ) {
            'user_id' => (string) User::whereId( $value )->first(),
            default => $value,
        };
    }

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getFieldName();
    }
}
