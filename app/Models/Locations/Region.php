<?php

namespace App\Models\Locations;

use App\Models\Logs\Data\DataLocation;
use App\Models\Traits\Area;
use App\Models\Traits\Center;
use App\Models\Traits\BelongsToCountry;
use App\Models\Traits\SpatialTrait;
use Eloquent;
use Grimzy\LaravelMysqlSpatial\Eloquent\Builder as SpatialBuilder;
use Grimzy\LaravelMysqlSpatial\Types\Geometry;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Nova\Actions\ActionEvent;

/**
 * App\Models\Locations\Region
 *
 * @property int                            $id
 * @property string                         $name
 * @property int                            $country_id
 * @property string                         $nuts_id
 * @property int                            $nuts_level
 * @property int|null                       $parent_id
 * @property int|null                       $population
 * @property int|null                       $surface
 * @property Point|null                     $center
 * @property Geometry|null                  $area
 * @property-read Collection|Region[]       $children
 * @property-read int|null                  $children_count
 * @property-read Country                   $country
 * @property-read Region|null               $parent
 * @property-read Collection|Post[]         $posts
 * @property-read Collection|Lau[]          $laus
 * @property-read int|null                  $posts_count
 * @property-read Collection|ActionEvent[]  $actions
 * @property-read int|null                  $actions_count
 * @property-read int                       $precision
 * @property-read int|null                  $laus_count
 * @property-read Collection|DataLocation[] $relatedDataLocations
 * @property-read int|null                  $related_data_locations_count
 * @method static Builder|Region comparison( $geometryColumn, $geometry, $relationship )
 * @method static Builder|Region contains( $geometryColumn, $geometry )
 * @method static Builder|Region crosses( $geometryColumn, $geometry )
 * @method static Builder|Region disjoint( $geometryColumn, $geometry )
 * @method static Builder|Region distance( $geometryColumn, $geometry, $distance )
 * @method static Builder|Region distanceExcludingSelf( $geometryColumn, $geometry, $distance )
 * @method static Builder|Region distanceSphere( $geometryColumn, $geometry, $distance )
 * @method static Builder|Region distanceSphereExcludingSelf( $geometryColumn, $geometry, $distance )
 * @method static Builder|Region distanceSphereValue( $geometryColumn, $geometry )
 * @method static Builder|Region distanceValue( $geometryColumn, $geometry )
 * @method static Builder|Region doesTouch( $geometryColumn, $geometry )
 * @method static Builder|Region equals( $geometryColumn, $geometry )
 * @method static Builder|Region intersects( $geometryColumn, $geometry )
 * @method static SpatialBuilder|Region newModelQuery()
 * @method static SpatialBuilder|Region newQuery()
 * @method static Builder|Region orderByDistance( $geometryColumn, $geometry, $direction = 'asc' )
 * @method static Builder|Region orderByDistanceSphere( $geometryColumn, $geometry, $direction = 'asc' )
 * @method static Builder|Region orderBySpatial( $geometryColumn, $geometry, $orderFunction, $direction = 'asc' )
 * @method static Builder|Region overlaps( $geometryColumn, $geometry )
 * @method static SpatialBuilder|Region query()
 * @method static Builder|Region whereArea( $value )
 * @method static Builder|Region whereCenter( $value )
 * @method static Builder|Region whereCountryId( $value )
 * @method static Builder|Region whereId( $value )
 * @method static Builder|Region whereName( $value )
 * @method static Builder|Region whereNutsId( $value )
 * @method static Builder|Region whereNutsLevel( $value )
 * @method static Builder|Region whereParentId( $value )
 * @method static Builder|Region wherePopulation( $value )
 * @method static Builder|Region whereSurface( $value )
 * @method static Builder|Region within( $geometryColumn, $polygon )
 * @mixin Eloquent
 */
class Region extends Location
{
    use SpatialTrait, Center, Area, BelongsToCountry;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'country_id',
        'nuts_id',
        'name',
        'nuts_level',
        'center',
        'area',
        'population',
        'surface',
    ];

    /**
     * The attributes that are spatial fields.
     *
     * @var array
     */
    protected array $spatialFields = [
        'center',
        'area'
    ];

    /**
     * Get related parent Region.
     *
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo( static::class, 'parent_id', 'id' );
    }

    public function getParentAttribute(): Region
    {
        return $this->getCachedRelationship( 'parent', Region::class . $this->parent_id );
    }

    /**
     * Check if Location has a parent.
     *
     * @return bool
     */
    public function hasParent(): bool
    {
        return ! empty( $this->parent_id );
    }

    /**
     * Get related Region children.
     *
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany( static::class, 'parent_id', 'id' );
    }

    /**
     * Get related Region children.
     *
     * @return HasMany
     */
    public function posts(): HasMany
    {
        return $this->hasMany( Post::class, 'nuts_id', 'nuts_id' );
    }

    /**
     * Get related Region children.
     *
     * @return HasMany
     */
    public function laus(): HasMany
    {
        return $this->hasMany( Lau::class, 'nuts_id', 'nuts_id' );
    }

    /**
     * What is the precision of current location, higher number is better precision.
     *
     * @return int
     */
    public function getPrecisionAttribute(): int
    {
        return $this->nuts_level;
    }
}
