<?php

namespace App\Models\Locations;

use App\Models\Logs\Data\DataLocation;
use Eloquent;
use Grimzy\LaravelMysqlSpatial\Eloquent\Builder as SpatialBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Nova\Actions\ActionEvent;

/**
 * App\Models\Locations\Post
 *
 * @property int                            $id
 * @property string                         $number
 * @property string|null                    $name
 * @property int                            $country_id
 * @property string                         $nuts_id
 * @property string                         $lau_id
 * @property int|null                       $population
 * @property int|null                       $surface
 * @property string|null                    $center
 * @property string|null                    $area
 * @property-read Country                   $country
 * @property-read Region|Lau                $parent
 * @property-read Region                    $region
 * @property-read Lau                       $lau
 * @property-read Collection|ActionEvent[]  $actions
 * @property-read int|null                  $actions_count
 * @property-read Collection|Post[]         $children
 * @property-read int|null                  $children_count
 * @property-read int                       $precision
 * @property-read Collection|DataLocation[] $relatedDataLocations
 * @property-read int|null                  $related_data_locations_count
 * @method static Builder|Location comparison( $geometryColumn, $geometry, $relationship )
 * @method static Builder|Location contains( $geometryColumn, $geometry )
 * @method static Builder|Location crosses( $geometryColumn, $geometry )
 * @method static Builder|Location disjoint( $geometryColumn, $geometry )
 * @method static Builder|Location distance( $geometryColumn, $geometry, $distance )
 * @method static Builder|Location distanceExcludingSelf( $geometryColumn, $geometry, $distance )
 * @method static Builder|Location distanceSphere( $geometryColumn, $geometry, $distance )
 * @method static Builder|Location distanceSphereExcludingSelf( $geometryColumn, $geometry, $distance )
 * @method static Builder|Location distanceSphereValue( $geometryColumn, $geometry )
 * @method static Builder|Location distanceValue( $geometryColumn, $geometry )
 * @method static Builder|Location doesTouch( $geometryColumn, $geometry )
 * @method static Builder|Location equals( $geometryColumn, $geometry )
 * @method static Builder|Location intersects( $geometryColumn, $geometry )
 * @method static SpatialBuilder|Post newModelQuery()
 * @method static SpatialBuilder|Post newQuery()
 * @method static Builder|Location orderByDistance( $geometryColumn, $geometry, $direction = 'asc' )
 * @method static Builder|Location orderByDistanceSphere( $geometryColumn, $geometry, $direction = 'asc' )
 * @method static Builder|Location orderBySpatial( $geometryColumn, $geometry, $orderFunction, $direction = 'asc' )
 * @method static Builder|Location overlaps( $geometryColumn, $geometry )
 * @method static SpatialBuilder|Post query()
 * @method static Builder|Post whereArea( $value )
 * @method static Builder|Post whereCenter( $value )
 * @method static Builder|Post whereCountryId( $value )
 * @method static Builder|Post whereId( $value )
 * @method static Builder|Post whereLauId( $value )
 * @method static Builder|Post whereName( $value )
 * @method static Builder|Post whereNumber( $value )
 * @method static Builder|Post whereNutsId( $value )
 * @method static Builder|Post wherePopulation( $value )
 * @method static Builder|Post whereSurface( $value )
 * @method static Builder|Location within( $geometryColumn, $polygon )
 * @mixin Eloquent
 */
class Post extends Location
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number',
        'nuts_id',
        'country_id',
        'name',
        'center',
        'area',
        'population',
        'surface',
    ];

    /**
     * Get related parent.
     *
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        if ( ! empty( $this->lau_id ) ) {
            return $this->lau();
        } else {
            return $this->region();
        }
    }

    /**
     * Get related parent.
     *
     * @return Location
     */
    public function getParentAttribute(): Location
    {
        if ( ! empty( $this->lau_id ) ) {
            return $this->lau;
        } else {
            return $this->region;
        }
    }

    /**
     * Get related Lau.
     * It might not exist so make sure you check for its existence first.
     *
     * @return Lau
     */
    public function getLauAttribute(): Lau
    {
        return $this->getCachedRelationship( 'lau', Lau::class . $this->lau_id );
    }

    /**
     * Get related Region
     *
     * @return Region
     */
    public function getRegionAttribute(): Region
    {
        return $this->getCachedRelationship( 'region', Region::class . $this->nuts_id );
    }

    /**
     * Get related parent District.
     *
     * @return BelongsTo
     */
    public function lau(): BelongsTo
    {
        return $this->belongsTo( Lau::class, 'lau_id', 'lau_id' );
    }

    /**
     * Get related parent Region.
     *
     * @return BelongsTo
     */
    public function region(): BelongsTo
    {
        return $this->belongsTo( Region::class, 'nuts_id', 'nuts_id' );
    }

    /**
     * Check if Location has a parent.
     *
     * @return bool
     */
    public function hasParent(): bool
    {
        return ! empty( $this->lau_id ) || ! empty( $this->nuts_id );
    }

    /**
     * Get related Region children.
     * @TODO
     *
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany( Post::class, 'lau_id', 'lau_id' );
    }

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->number . ' ' . $this->name;
    }

    /**
     * What is the precision of current location, higher number is better precision.
     *
     * @return int
     */
    public function getPrecisionAttribute(): int
    {
        return 5;
    }
}
