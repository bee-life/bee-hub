<?php

namespace App\Models\Locations;

use App\Exceptions\CountryNotFoundException;
use App\Models\References\Currency;
use App\Models\Traits\Base;
use Cache;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Laravel\Nova\Actions\Actionable;
use Laravel\Nova\Actions\ActionEvent;

/**
 * App\Models\Country
 *
 * @property int                           $id
 * @property string                        $name
 * @property string                        $country_code
 * @property string                        $iso3
 * @property string|null                   $native_name
 * @property string|null                   $capital
 * @property string|null                   $continent
 * @property string|null                   $currency_code
 * @property string|null                   $currency_name
 * @property int|null                      $currency_id
 * @property string|null                   $phone_prefix
 * @property-read Region                   $countryRegion
 * @property-read Currency                 $currency
 * @property-read Collection|Post[]        $posts
 * @property-read int|null                 $posts_count
 * @property-read Collection|Region[]      $regions
 * @property-read int|null                 $regions_count
 * @property-read Collection|ActionEvent[] $actions
 * @property-read int|null                 $actions_count
 * @property-read Collection|Lau[]         $laus
 * @property-read int|null                 $laus_count
 * @method static Builder|Country newModelQuery()
 * @method static Builder|Country newQuery()
 * @method static Builder|Country query()
 * @method static Builder|Country whereCapital( $value )
 * @method static Builder|Country whereContinent( $value )
 * @method static Builder|Country whereCountryCode( $value )
 * @method static Builder|Country whereCurrencyCode( $value )
 * @method static Builder|Country whereCurrencyName( $value )
 * @method static Builder|Country whereId( $value )
 * @method static Builder|Country whereIso3( $value )
 * @method static Builder|Country whereName( $value )
 * @method static Builder|Country whereNativeName( $value )
 * @method static Builder|Country wherePhonePrefix( $value )
 * @method static Builder|Country whereCurrencyId( $value )
 * @mixin Eloquent
 */
class Country extends Model
{
    use Base, Actionable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_code',
        'iso3',
        'name',
        'native_name',
        'capital',
        'continent',
        'currency_code',
        'currency_name',
        'phone_prefix',
        'currency_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get related Region children.
     *
     * @return HasMany
     */
    public function regions(): HasMany
    {
        return $this->hasMany( Region::class, 'country_id', 'id' );
    }

    /**
     * Get related Region child.
     *
     * @return HasOne
     */
    public function countryRegion(): HasOne
    {
        return $this->hasOne( Region::class, 'country_id', 'id' )->where( 'nuts_level', 0 );
    }

    /**
     * Get related Region children.
     *
     * @return HasMany
     */
    public function posts(): HasMany
    {
        return $this->hasMany( Post::class, 'country_id', 'id' );
    }

    /**
     * Get related District children.
     *
     * @return HasMany
     */
    public function laus(): HasMany
    {
        return $this->hasMany( Lau::class, 'country_id', 'id' );
    }

    /**
     * Get related currency.
     *
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo( Currency::class, 'currency_id', 'id' );
    }

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * Return related country center representation, used in Leaflet.
     *
     * @return array
     */
    public function getCoordinates(): array
    {
        return $this->countryRegion->getCoordinates();
    }

    /**
     * Return related country area representation, used in Leaflet.
     *
     * @return array
     */
    public function getArea(): array
    {
        return $this->countryRegion->getArea();
    }

    /**
     * Gets Country ID from country code.
     * Should be used when relating to specific countries, but we need to be optimized about it.
     *
     * @param string $country_code
     *
     * @return int|null
     * @throws CountryNotFoundException
     */
    public static function getIdFromCountryCode( string $country_code ): ?int
    {
        if ( Cache::has( 'countries' ) ) {
            $countries = Cache::get( 'countries' );
        } else {
            $countries = static::select( [ 'id', 'country_code' ] )->get()->keyBy( 'country_code' );
            Cache::forever( 'countries', $countries );
        }

        if ( empty( $countries[ $country_code ] ) ) {
            throw new CountryNotFoundException( $country_code );
        }

        return $countries[ $country_code ]->id;
    }
}
