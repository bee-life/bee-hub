<?php

namespace App\Models\Locations;

use App\Models\Logs\Data\DataLocation;
use Eloquent;
use Grimzy\LaravelMysqlSpatial\Eloquent\Builder as SpatialBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Nova\Actions\ActionEvent;

/**
 * App\Models\Locations\Lau
 *
 * @property int                            $id
 * @property string                         $name
 * @property string                         $lau_id
 * @property int|null                       $population
 * @property int|null                       $surface
 * @property string                         $nuts_id
 * @property int                            $country_id
 * @property string|null                    $center
 * @property string|null                    $area
 * @property-read Country                   $country
 * @property-read Region                    $parent
 * @property-read Collection|ActionEvent[]  $actions
 * @property-read int|null                  $actions_count
 * @property-read Collection|Post[]         $children
 * @property-read int|null                  $children_count
 * @property-read int                       $precision
 * @property-read Collection|DataLocation[] $relatedDataLocations
 * @property-read int|null                  $related_data_locations_count
 * @method static Builder|Location comparison( $geometryColumn, $geometry, $relationship )
 * @method static Builder|Location contains( $geometryColumn, $geometry )
 * @method static Builder|Location crosses( $geometryColumn, $geometry )
 * @method static Builder|Location disjoint( $geometryColumn, $geometry )
 * @method static Builder|Location distance( $geometryColumn, $geometry, $distance )
 * @method static Builder|Location distanceExcludingSelf( $geometryColumn, $geometry, $distance )
 * @method static Builder|Location distanceSphere( $geometryColumn, $geometry, $distance )
 * @method static Builder|Location distanceSphereExcludingSelf( $geometryColumn, $geometry, $distance )
 * @method static Builder|Location distanceSphereValue( $geometryColumn, $geometry )
 * @method static Builder|Location distanceValue( $geometryColumn, $geometry )
 * @method static Builder|Location doesTouch( $geometryColumn, $geometry )
 * @method static Builder|Location equals( $geometryColumn, $geometry )
 * @method static Builder|Location intersects( $geometryColumn, $geometry )
 * @method static SpatialBuilder|Lau newModelQuery()
 * @method static SpatialBuilder|Lau newQuery()
 * @method static Builder|Location orderByDistance( $geometryColumn, $geometry, $direction = 'asc' )
 * @method static Builder|Location orderByDistanceSphere( $geometryColumn, $geometry, $direction = 'asc' )
 * @method static Builder|Location orderBySpatial( $geometryColumn, $geometry, $orderFunction, $direction = 'asc' )
 * @method static Builder|Location overlaps( $geometryColumn, $geometry )
 * @method static SpatialBuilder|Lau query()
 * @method static Builder|Lau whereArea( $value )
 * @method static Builder|Lau whereCenter( $value )
 * @method static Builder|Lau whereCountryId( $value )
 * @method static Builder|Lau whereId( $value )
 * @method static Builder|Lau whereLauId( $value )
 * @method static Builder|Lau whereName( $value )
 * @method static Builder|Lau whereNutsId( $value )
 * @method static Builder|Lau wherePopulation( $value )
 * @method static Builder|Lau whereSurface( $value )
 * @method static Builder|Location within( $geometryColumn, $polygon )
 * @mixin Eloquent
 */
class Lau extends Location
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'lau_id',
        'nuts_id',
        'country_id',
        'center',
        'area',
        'population',
        'surface',
    ];


    /**
     * Get related parent Region.
     *
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo( Region::class, 'nuts_id', 'nuts_id' );
    }

    /**
     * We can cache location object, because it rarely changes.
     *
     * @return Region
     */
    public function getParentAttribute(): Region
    {
        return $this->getCachedRelationship( 'parent', Region::class . $this->nuts_id );
    }

    /**
     * Check if Location has a parent.
     *
     * @return bool
     */
    public function hasParent(): bool
    {
        return ! empty( $this->nuts_id );
    }

    /**
     * Get related Region children.
     *
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany( Post::class, 'lau_id', 'lau_id' );
    }

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->lau_id . ' ' . $this->name;
    }

    /**
     * What is the precision of current location, higher number is better precision.
     *
     * @return int
     */
    public function getPrecisionAttribute(): int
    {
        return 4;
    }
}
