<?php

namespace App\Models\Locations;


use App\Exceptions\InvalidValueException;
use App\Models\Logs\Data\DataLocation;
use App\Models\Traits\Area;
use App\Models\Traits\BelongsToCountry;
use App\Models\Traits\Center;
use App\Models\Traits\RelationshipCache;
use App\Models\Traits\SpatialTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Laravel\Nova\Actions\Actionable;
use MatanYadaev\EloquentSpatial\Objects\Geometry;
use MatanYadaev\EloquentSpatial\Objects\Point;
use MatanYadaev\EloquentSpatial\SpatialBuilder;

/**
 * App\Models\Locations\Location
 *
 * @property int                $id
 * @property string|null        $name
 * @property int                $country_id
 * @property string             $nuts_id
 * @property int|null           $population
 * @property int                $precision
 * @property int|null           $surface
 * @property Point|null         $center
 * @property Geometry|null      $area
 * @property-read Location|null $parent
 * @property-read Country       $country
 * @method static Builder|Location comparison( $geometryColumn, $geometry, $relationship )
 * @method static Builder|Location contains( $geometryColumn, $geometry )
 * @method static Builder|Location crosses( $geometryColumn, $geometry )
 * @method static Builder|Location disjoint( $geometryColumn, $geometry )
 * @method static Builder|Location distance( $geometryColumn, $geometry, $distance )
 * @method static Builder|Location distanceExcludingSelf( $geometryColumn, $geometry, $distance )
 * @method static Builder|Location distanceSphere( $geometryColumn, $geometry, $distance )
 * @method static Builder|Location distanceSphereExcludingSelf( $geometryColumn, $geometry, $distance )
 * @method static Builder|Location distanceSphereValue( $geometryColumn, $geometry )
 * @method static Builder|Location distanceValue( $geometryColumn, $geometry )
 * @method static Builder|Location doesTouch( $geometryColumn, $geometry )
 * @method static Builder|Location equals( $geometryColumn, $geometry )
 * @method static Builder|Location intersects( $geometryColumn, $geometry )
 * @method static SpatialBuilder|Location newModelQuery()
 * @method static SpatialBuilder|Location newQuery()
 * @method static Builder|Location orderByDistance( $geometryColumn, $geometry, $direction = 'asc' )
 * @method static Builder|Location orderByDistanceSphere( $geometryColumn, $geometry, $direction = 'asc' )
 * @method static Builder|Location orderBySpatial( $geometryColumn, $geometry, $orderFunction, $direction = 'asc' )
 * @method static Builder|Location overlaps( $geometryColumn, $geometry )
 * @method static SpatialBuilder|Location query()
 * @method static Builder|Post whereArea( $value )
 * @method static Builder|Post whereCenter( $value )
 * @method static Builder|Post whereCountryId( $value )
 * @method static Builder|Post whereId( $value )
 * @method static Builder|Post whereName( $value )
 * @method static Builder|Post wherePopulation( $value )
 * @method static Builder|Post whereSurface( $value )
 * @method static Builder|Location within( $geometryColumn, $polygon )
 * @mixin Eloquent
 */
abstract class Location extends Model
{
    use SpatialTrait, Center, Area, BelongsToCountry, RelationshipCache, Actionable;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are spatial fields.
     *
     * @var array
     */
    protected array $spatialFields = [
        'center',
        'area'
    ];

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * Get all related data.
     *
     * @return MorphMany
     */
    public function relatedDataLocations(): MorphMany
    {
        return $this->morphMany( DataLocation::class, 'location' );
    }



    /**
     * Check if Model has defined Surface area.
     *
     * @return bool
     */
    public function hasSurface(): bool
    {
        return ! empty( $this->surface );
    }

    /**
     * Get human readable surface
     *
     * @return string
     */
    public function getSurface(): string
    {
        if ( $this->hasSurface() ) {
            return format_area( $this->surface / 1000000, 'km' );
        } else {
            return __( 'framework.not-available' );
        }
    }

    /**
     * Check if Model has defined population.
     *
     * @return bool
     */
    public function hasPopulation(): bool
    {
        return ! empty( $this->population );
    }

    /**
     * Get human-readable population amount.
     *
     * @return string
     */
    public function getPopulation(): string
    {
        if ( $this->hasPopulation() ) {
            return format_number( $this->population, 0 );
        } else {
            return __( 'framework.not-available' );
        }
    }


    /**
     * We can cache location object, because it rarely changes.
     *
     * @return Location
     */
    abstract public function getParentAttribute(): Location;


    /**
     * Get related parent Region.
     *
     * @return BelongsTo
     */
    abstract public function parent(): BelongsTo;

    /**
     * Get related parent Region.
     *
     * @return HasMany
     */
    abstract public function children(): HasMany;

    /**
     * Check if Location has a parent.
     *
     * @return bool
     */
    public function hasParent(): bool
    {
        return ! empty( $this->parent );
    }

    /**
     * What is the precision of current location, higher number is better precision.
     *
     * @return int
     */
    abstract public function getPrecisionAttribute(): int;


    /**
     * Get a full region name based on its parents
     *
     * @return string
     */
    public function getFullName(): string
    {
        //\Debugbar::debug(intval($this->relationLoaded('parent')) . ': ' . $this->name);
        if ( $this->hasParent() ) {
            return $this->parent->getFullName() . ', ' . $this->name;
        } else {
            return $this->name;
        }
    }

    /**
     * @param int $precision
     *
     * @return string
     * @throws InvalidValueException
     */
    public static function getClassFromPrecision(int $precision): string {
        return match ( $precision ) {
            0, 1, 2, 3 => Region::class,
            4 => Lau::class,
            5 => Post::class,
            default => throw new InvalidValueException(),
        };
    }
}
