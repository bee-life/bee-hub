<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class  Management extends Pivot
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_to_projects';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'project_id',
        'metadata',
        'status',
        'integration_contact',
        'integration_email',
        'share_conditions',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'metadata'   => 'array',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

}
