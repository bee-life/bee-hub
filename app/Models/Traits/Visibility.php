<?php namespace App\Models\Traits;

use App\Exceptions\Interactions\InvalidValueException;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait Visibility
 * @package App\Models\Traits
 *
 * @property string $status
 * @method static Builder|Model status ( $string )
 * @method static Builder|Model public ()
 * @method static Builder|Model draft ()
 * @mixin Eloquent
 */
trait Visibility
{
    /**
     * String representing public visibility.
     *
     * @var string
     */
    protected static string $visibility_public = 'public';

    /**
     * String representing draft status.
     *
     * @var string
     */
    protected static string $visibility_draft = 'draft';

    /**
     * String representing private status.
     *
     * @var string
     */
    protected static string $visibility_private = 'private';

    /**
     * String representing pending approval status.
     *
     * @var string
     */
    protected static string $visibility_pending = 'pending';

    /**
     * String representing blocked status.
     *
     * @var string
     */
    protected static string $visibility_blocked = 'blocked';

    /**
     * Check if Model is enabled.
     *
     * @return bool
     */
    public function isPublic(): bool
    {
        return $this->public;
    }

    /**
     * Enable Model. Saves the change to database.
     *
     * @param string $newValue
     */
    public function setStatusAttribute( string $newValue ): void
    {
        $this->attributes['status'] = match ( $newValue ) {
            self::$visibility_public, self::$visibility_draft, self::$visibility_pending, self::$visibility_blocked, self::$visibility_private => $newValue,
            default => throw new InvalidValueException(),
        };
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopePublic( Builder $query ): Builder
    {
        return $query->where( 'public', '=', 'public' );
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeDraft( Builder $query ): Builder
    {
        return $query->where( 'public', '=', 'draft' );
    }
}
