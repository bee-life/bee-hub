<?php namespace App\Models\Traits;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;

/**
 * Trait Activity
 * @package App\Models\Traits
 *
 * @property boolean $active
 * @method static Builder active( $boolean )
 * @method static Builder inactive()
 * @mixin Eloquent
 */
trait Activity
{
    /**
     * Check if Model is enabled.
     *
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->active;
    }
    /**
     * Check if Model is disabled.
     *
     * @return bool
     */
    public function isDisabled(): bool
    {
        return ! $this->isEnabled();
    }

    /**
     * Enable Model. Saves the change to database.
     */
    public function enable(): void
    {
        $this->active = true;
        $this->save();
    }

    /**
     * Disable Model. Saves the change to database.
     */
    public function disable(): void
    {
        $this->active = false;
        $this->save();
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param  Builder  $query
     * @param bool $active
     *
     * @return Builder
     */
    public function scopeActive(Builder $query, bool $active = true): Builder
    {
        return $query->where('active', '=', $active);
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeInactive(Builder $query): Builder
    {
        return $query->where('active', '=', false);
    }
}
