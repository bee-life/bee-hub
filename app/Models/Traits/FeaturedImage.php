<?php namespace App\Models\Traits;

use Eloquent;
use Storage;

/**
 * App\Models\Traits\FeaturedImage
 *
 * @property string $featured_image
 * @mixin Eloquent
 */
trait FeaturedImage
{
    /**
     * Return permalink to Featured image.
     *
     * @return string
     */
    public function getFeaturedImageUrl(): string
    {
        if ( $this->hasFeaturedImage() ) {
            return Storage::url( $this->featured_image );
        } else {
            return asset( 'images/Prototype-Platform.png' );
        }
    }

    /**
     * Check if Featured Image is set.
     *
     * @return string
     */
    public function hasFeaturedImage(): string
    {
        return ! empty ( $this->featured_image );
    }

    /**
     * Return img HTML element with Featured Image.
     *
     * @param array $class Optional list of classes to add to the IMG element.
     *
     * @return string
     */
    public function getFeaturedImage( $class = [] ): string
    {
        return '<img class="' . implode( ' ', $class ) . '" src="' . $this->getFeaturedImageUrl() . '" alt="' . $this . '" />';
    }

}
