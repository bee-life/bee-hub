<?php namespace App\Models\Traits;

use App\Models\Locations\Country;
use Eloquent;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Trait Country
 * @package App\Models\Traits
 *
 * @property int          $country_id
 * @property-read Country $country
 * @mixin Eloquent
 */
trait BelongsToCountry
{
    /**
     * Get Post address.
     *
     * @return BelongsTo
     */
    public function country(): BelongsTo
    {
        return $this->belongsTo( Country::class, 'country_id' );
    }
}
