<?php namespace App\Models\Traits;


use Eloquent;
use Grimzy\LaravelMysqlSpatial\Types\Geometry;
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use Grimzy\LaravelMysqlSpatial\Types\Point;

/**
 * Trait Area
 *
 * Implement functions used to represent geographical center of the Model.
 *
 * @package App\Models\Traits\Area
 *
 * @property Geometry|null $area
 * @mixin Eloquent
 */
trait Area
{
    /**
     * Check if Model has defined area.
     *
     * @return bool
     */
    public function hasArea(): bool
    {
        return ! empty( $this->area );
    }

    /**
     * Get array representation of Area, used in Leaflet.
     *
     * @return array
     */
    public function getArea(): array
    {
        $return = [];
        $line   = 0;
        foreach ( $this->area->getGeometries()[0] as $item ) {
            if ( is_a( $item, LineString::class ) ) {
                /** @var Point $point */
                foreach ( $item as $point ) {
                    $return[ $line ][] = [ $point->getLat(), $point->getLng() ];
                }
                $line ++;
            } else {
                /** @var Point $item */
                $return[0][] = [ $item->getLat(), $item->getLng() ];
            }
        }

        return array_values( $return );
    }

    /**
     * Returns simple string formatted coordinates.
     *
     * @return string
     */
    public function getStringArea(): string
    {
        return $this->center->getLat() . ', ' . $this->center->getLng();
    }

    /**
     * Returns WKT (Well Known Text) formatted coordinates.
     *
     * @return string
     */
    public function getWKTArea(): string
    {
        return $this->center->toWKT();
    }

    /**
     * Returns GeoJSON formatted coordinates.
     *
     * @return string
     */
    public function getGeoJSONArea(): string
    {
        return $this->center->toJson();
    }
}
