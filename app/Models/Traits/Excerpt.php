<?php namespace App\Models\Traits;

use Eloquent;

/**
 * App\Models\Traits\Excerpt
 *
 * @property string $excerpt
 * @property string $content
 * @mixin Eloquent
 */
trait Excerpt
{
    /**
     * Returns cleaned up excerpt using filterExcerpt function.
     *
     * @return string
     */
    public function getExcerpt(): string
    {
        return empty( $this->excerpt ) ? $this->filterExcerpt( $this->content ) : $this->excerpt;
    }

    /**
     * Returns cleaned up excerpt.
     *
     * @param string $content
     *
     * @return string
     */
    protected function filterExcerpt( string $content ): string
    {
        $striped = preg_replace( '/\\[(.*?)\\]/', '', strip_tags( $content ) );
        if ( strlen( $striped ) > 240 ) {
            $pos = strpos( $striped, ' ', 240 );

            return substr( $striped, 0, $pos ) . '&hellip;';
        } else {
            return $striped;
        }
    }
}
