<?php namespace App\Models\Traits;

use App\Exceptions\ModelNotFoundByUidException;
use Cache;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Trait Uid
 * @package App\Models\Traits
 *
 * @property string $uid
 * @method static Builder|Model whereUid( $value )
 * @mixin Eloquent
 */
trait Uid
{
    /**
     * The key that is used to cache Uid mapping.
     *
     * @var string
     */
    //protected static $uidCacheKey = 'uid';

    /**
     * The event map for the model.
     * Make sure to include this event if overriding events within a model.
     *
     * @var array
     */
    //protected $dispatchesEvents = [
    //    'created' => UidCacheClear::class,
    //];

    /**
     * Cached items for quick access.
     *
     * @var array|\Illuminate\Support\Collection|null
     */
    protected static array|\Illuminate\Support\Collection|null $cachedModels = null;

    /**
     * Gets several Project IDs from UIDs.
     * Should be used when relating to specific project but we need to be optimized about it.
     *
     *
     * @param \Illuminate\Support\Collection|array $uids
     * @param bool                                 $bAllowMissing
     *
     * @return Collection
     * @throws ModelNotFoundByUidException
     */
    public static function getIdsFromUids(  \Illuminate\Support\Collection|array $uids, bool $bAllowMissing = false ): \Illuminate\Support\Collection
    {
        $return = collect();

        foreach ( $uids as $uid ) {
            $item = static::getIdFromUid( $uid, $bAllowMissing );
            if ( isset( $item ) ) {
                $return[ $uid ] = $item;
            }
        }

        return $return;
    }

    /**
     * Gets Model ID from UID.
     * Should be used when relating to specific descriptors but we need to be optimized about it.
     *
     * @param string $uid
     * @param bool   $bAllowMissing
     *
     * @return int|null
     * @throws ModelNotFoundByUidException
     */
    public static function getIdFromUid( string $uid, bool $bAllowMissing = false ): ?int
    {
        if ( isset( static::$cachedModels ) ) {
            $models = static::$cachedModels;
        } elseif ( Cache::has( static::$uidCacheKey ) ) {
            $models = static::$cachedModels = Cache::get( static::$uidCacheKey );
        } else {
            // Bugfix: We need to include name field for DebugBar & Telescope to play nice.
            $models = static::$cachedModels = static::select( [ 'id', 'uid', 'name' ] )->get()->keyBy( 'uid' );
            Cache::forever( static::$uidCacheKey, $models );
        }

        if ( empty( $models[ $uid ] ) ) {
            if ( $bAllowMissing ) {
                return null;
            } else {
                throw new ModelNotFoundByUidException( $uid );
            }
        }

        return $models[ $uid ]->id;
    }

    /**
     * Delete the Cached Model Uid to Id mappings.
     * @throws InvalidArgumentException
     */
    public static function clearUidCache()
    {
        Cache::delete( static::$uidCacheKey );
        static::$cachedModels = null;
    }
}
