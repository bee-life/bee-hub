<?php namespace App\Models\Traits;


use Eloquent;

/**
 * Trait Localized
 * @package App\Models\Parsers
 * @todo
 *
 * @property string $locale
 * @mixin Eloquent
 */
trait Localized
{
    /**
     * Get object locale.
     *
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * Get localized name of the object locale.
     *
     * @return string
     */
    public function getLocalizedLocaleString(): string
    {
        return __( 'generic.locales.' . $this->locale );
    }
}
