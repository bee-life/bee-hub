<?php namespace App\Models\Traits;

use Eloquent;


/**
 * App\Models\Traits\Linkable
 *
 * @mixin Eloquent
 */
trait Linkable
{
    /**
     * Return base route to Model
     *
     * @return string
     */
    abstract public function getBaseRoute(): string;

    /**
     * Return permalink to Model.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return url( $this->getBaseRoute() );
    }

    /**
     * Return permalink HTML to Model.
     *
     * @param bool $external Should the link open in a new window?
     *
     * @return string
     */
    public function getLink( bool $external = false ): string
    {
        return '<a href="' . $this->getUrl() . '" ' . ( $external ? 'target="_blank"' : null ) . '>' . $this . '</a>';
    }
}
