<?php namespace App\Models\Traits;

use Eloquent;
use Str;

/**
 * Trait Email
 * @package App\Models\Traits
 *
 * @property string $email
 * @mixin Eloquent
 */
trait Email
{
    /**
     * Check if Model has a set email address.
     *
     * @return bool
     */
    public function hasEmail(): bool
    {
        return ! empty( $this->email );
    }

    /**
     * Get a link to Models email address.
     *
     * @deprecated To protect users, use getEmail() function instead.
     *
     * @return string|null
     */
    public function getEmailLink() : ?string
    {
        return $this->getEmail();
    }

    /**
     * Get encoded email to be displayed on the website.
     *
     * @return string|null
     */
    public function getEmail()  : ?string
    {
        if ( $this->hasEmail() ) {
            return '<span class="asp">' . substr_replace(  str_replace( '@', '&commat;', strrev($this->email) ), ( '<!-- ' . Str::random( rand( 1, 10 ) ) . '@' . Str::random( rand( 1, 10 ) ) . '.com -->' ), rand( 1, Str::length( $this->email ) - 2 ), 0 ) . '</span>';
        } else {
            return null;
        }
    }
}
