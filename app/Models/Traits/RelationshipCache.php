<?php namespace App\Models\Traits;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait RelationshipCache
 * @package App\Models\Traits
 *
 * @mixin Eloquent
 */
trait RelationshipCache
{
    /**
     * @var array|Model[]
     */
    protected static array $modelsCache = [];

    /**
     * Get a specific relationship from cache.
     * Useful for relationship that might be often accessed and are deeply nested, but the same.
     *
     * Note:
     * We avoid using Cache class as by using file driver it reached memory limits too fast.
     *
     * @param string $relationship
     * @param string $key
     *
     * @return mixed
     */
    protected function getCachedRelationship( string $relationship, string $key ): mixed
    {
        if ( $this->relationLoaded( $relationship ) ) {
            return $this->getRelationValue( $relationship );
        }

        if ( isset( static::$modelsCache[ $key ] ) || array_key_exists( $key, static::$modelsCache ) ) {
            $relationshipModels = static::$modelsCache[ $key ];
        } else {
            $relationshipModels = static::$modelsCache[ $key ] = $this->getRelationValue( $relationship );
        }

        $this->setRelation( $relationship, $relationshipModels );

        return $relationshipModels;
    }
}
