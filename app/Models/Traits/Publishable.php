<?php namespace App\Models\Traits;

use Carbon\Carbon;
use Eloquent;

/**
 * Trait Publishable
 * @package App\Models\Traits
 *
 * @property Carbon $published_at
 * @mixin Eloquent
 */
trait Publishable
{
    /**
     * Is the Model published?
     *
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->published_at > Carbon::now();
    }

    /**
     * Return formatted created date.
     *
     * @param bool $time Include time?
     *
     * @return string
     */
    public function getPublishedAt( bool $time = false ): string
    {
        return $this->formatDate( $this->published_at, $time );
    }
}
