<?php namespace App\Models\Traits;


use Eloquent;
use MatanYadaev\EloquentSpatial\Objects\Point;

/**
 * Trait Center
 *
 * Implements functions used to represent geographical center of the Model.
 *
 * @package App\Models\Traits\Center
 *
 * @property Point|null $center
 * @mixin Eloquent
 */
trait Center
{
    /**
     * Check if Model has defined center.
     *
     * @return bool
     */
    public function hasCenter(): bool
    {
        return ! empty( $this->center );
    }


    /**
     * Get array representation of Point, used in Leaflet.
     *
     * @return array
     */
    public function getCoordinates(): array
    {
        return $this->center->getCoordinates();
    }

    /**
     * Returns simple string formatted coordinates.
     *
     * @return string
     */
    public function getStringCoordinates(): string
    {
        return $this->center->latitude . ', ' . $this->center->longitude;
    }

    /**
     * Returns WKT (Well Known Text) formatted coordinates.
     *
     * @return string
     */
    public function getWKTCoordinates(): string
    {
        return $this->center->toWKT();
    }

    /**
     * Returns GeoJSON formatted coordinates.
     *
     * @return string
     */
    public function getGeoJSONCoordinates(): string
    {
        return $this->center->toJson();
    }
}
