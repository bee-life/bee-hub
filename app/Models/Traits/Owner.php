<?php namespace App\Models\Traits;

use App\Models\User;
use Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * App\Models\Traits\Owner
 *
 * @property int       $user_id
 * @property-read User $user
 * @method static Builder|Model owned ()
 */
trait Owner
{

    /**
     * Get the owning User.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo( 'App\Models\User', 'user_id', 'id' );
    }

    /**
     * Check if Current User is the owner of this object.
     *
     * @return bool
     */
    public function isCurrentUserOwner(): bool
    {
        return Auth::check() && $this->user_id == Auth::id();
    }

    /**
     * Add ownership scope to the query.
     *
     * @param Builder $query
     *
     * @return Builder
     * @throws HttpException
     */
    public function scopeOwned( Builder $query ): Builder
    {
        if ( Auth::check() ) {
            return $query->where( 'user_id', Auth::id() );
        }
        abort( 401 );
    }
}
