<?php namespace App\Models\Traits;


use App\Models\AuditLog;
use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Str;

/**
 * Class Admin
 *
 * Implement functions used to administrate the Model.
 *
 * @property int                        $id
 * @property string                     $resourceName
 * @property-read Collection|AuditLog[] $auditLogs
 * @mixin Eloquent
 * @package   App\Models\Traits
 */
trait Manageable
{
    /**
     * Prints an administration link to the Model.
     *
     * @return string
     */
    public function getManageLink(): string
    {
        if ( method_exists( $this, 'trashed' ) && $this->trashed() ) {
            return '<s title="' . __( 'admin.audit.deleted' ) . '">' . $this . '</s>';
        } else {
            return '<a href="' . $this->getManageUrl() . '" title="' . __( 'management.edit' ) . '">' . $this . '</a>';
        }
    }

    /**
     * Return URL to Resource show URL.
     *
     * @return string
     */
    public function getManageUrl(): string
    {
        return route( static::$resourceName . '.show', [ Str::singular( static::$resourceName ) => $this ] );
    }


    /**
     * Prints an administration link to the Model.
     *
     * @return string
     */
    public function getEditLink(): string
    {
        if ( method_exists( $this, 'trashed' ) && $this->trashed() ) {
            return '<s title="' . trans( 'admin.audit.deleted' ) . '">' . $this . '</s>';
        } else {
            return '<a href="' . $this->getEditUrl() . '">' . $this . '</a>';
        }
    }

    /**
     * Return URL to Resource edit URL.
     *
     * @return string
     */
    public function getEditUrl(): string
    {
        return route( static::$resourceName . '.edit', [ Str::singular( static::$resourceName ) => $this ] );
    }

    /**
     * Return URL to Resource list.
     *
     * @param string $suffix
     *
     * @return string
     */
    static function getListUrl( string $suffix = '' ): string
    {
        return route(static::$resourceName . '.index');
    }

    /**
     * Return URL to Resource create URL.
     *
     * @return string
     */
    static function getNewUrl(): string
    {
        return route(static::$resourceName . '.store/');
    }

    /**
     * Returns all changes, done to the object.
     *
     * @return MorphMany
     */
    public function auditLogs(): MorphMany
    {
        return $this->morphMany( AuditLog::class, 'target' )->with( 'user' )->orderBy( 'created_at', 'DESC' );
    }
}
