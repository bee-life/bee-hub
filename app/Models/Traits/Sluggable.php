<?php namespace App\Models\Traits;

use Eloquent;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Trait Sluggable, used to handle Slug related functions
 * @package App\Models\Traits
 *
 * @property string $slug
 * @mixin Eloquent
 */
trait Sluggable
{
    use \Cviebrock\EloquentSluggable\Sluggable;

    /**
     * Check if Model matches provided Slug.
     *
     * @param string $slug
     *
     * @return bool
     */
    public function hasSlug( string $slug ): bool
    {
        return $this->slug === $slug;
    }


    /**
     * Setup sluggable fields.
     *
     * @return string[][]
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ],
            'uid'  => [
                'source' => 'name'
            ],
        ];
    }
}
