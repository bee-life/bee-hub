<?php namespace App\Models\Traits;

use App\Events\UidCacheClear;
use App\Exceptions\ModelNotFoundByUidException;
use Cache;
use Eloquent;
use Illuminate\Database\Eloquent\Collection;

/**
 * Trait handling project types functions and constants.
 * @package App\Models\Traits
 *
 * @property string $type
 * @mixin Eloquent
 */
trait ProjectType
{
    /**
     * String representing static project source (file).
     *
     * @var string
     */
    protected static string $type_static = 'static';

    /**
     * String representing processing project source (other projects).
     *
     * @var string
     */
    protected static string $type_processing = 'processing';


    /**
     * String representing processing project source (other projects).
     *
     * @var string
     */
    protected static string $type_live = 'live';


    /**
     * Check if the project source is live data.
     *
     * @return bool
     */
    public function isTypeLive(): bool
    {
        return $this->type == self::$type_live;
    }

    /**
     * Check if the project source are other projects.
     *
     * @return bool
     */
    public function isTypeProcessing(): bool
    {
        return $this->type == self::$type_processing;
    }

    /**
     * Check if the project source are static files.
     *
     * @return bool
     */
    public function isTypeStatic(): bool
    {
        return $this->type == self::$type_static;
    }

    /**
     * Does the project contain live data?
     *
     * @return bool
     */
    public function hasLiveData(): bool
    {
        return $this->type == $this->isTypeLive();
    }

    /**
     * Does the project contain historical data only?
     *
     * @return bool
     */
    public function hasHistoricData(): bool
    {
        return $this->type == $this->isTypeStatic();
    }
}
