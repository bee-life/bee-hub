<?php namespace App\Models\Traits;


use App\Models\Locations\Country;
use App\Models\Locations\Post;
use App\Models\Locations\Region;
use Eloquent;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Trait BelongsToPost
 * @package App\Models\Traits
 *
 * @property int          $post_id
 * @property-read Post    $post
 * @property-read Region  $region
 * @property-read Country $country
 * @mixin Eloquent
 */
trait BelongsToPost
{
    /**
     * Get Post address.
     *
     * @return BelongsTo
     */
    public function post(): BelongsTo
    {
        return $this->belongsTo( Post::class, 'post_id' );
    }

    /**
     * Get related Region.
     *
     * @return Region
     */
    public function region(): Region
    {
        return $this->post->region;
    }

    /**
     * Get related Country.
     *
     * @return Country
     */
    public function country(): Country
    {
        return $this->post->country;
    }
}
