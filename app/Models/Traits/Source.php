<?php namespace App\Models\Traits;


use App\Models\MetaData\Project;
use Eloquent;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;

/**
 * Trait Sources
 * @package App\Models\Traits
 *
 * @property-read Project $project
 * @mixin Eloquent
 */
trait Source
{
    /**
     * Get Related Project that is using this Model.
     *
     * @return BelongsTo
     */
    abstract public function project(): BelongsTo;

    /**
     * Get image representation of Source.
     *
     * @param array $class
     *
     * @return Collection
     */
    public function getSources( array $class = [] ): Collection
    {
        return collect( $this->project->getFeaturedImage( $class ) );
    }

    /**
     * Get Model Attribution text.
     *
     * @return string
     */
    public function getTextSources(): string
    {
        return $this->project->attribution;
    }
}
