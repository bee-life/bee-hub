<?php namespace App\Models\Traits;

use Eloquent;

/**
 * Trait Website
 * @package App\Models\Traits
 *
 * @property string $website
 * @mixin Eloquent
 */
trait Website
{
    /**
     * Check if Model has a set phone number.
     *
     * @return bool
     */
    public function hasWebsite(): bool
    {
        return ! empty( $this->website );
    }

    /**
     * Get a link to Models phone number.
     *
     * @param bool $external Should the link open in new window.
     *
     * @return string|null
     */
    public function getWebsiteLink( bool $external = true ): ?string
    {
        if ( $this->hasWebsite() ) {
            return '<a href="' . $this->website . '"' . ( $external ? ' target="_blank"' : null ) . '>' . $this->website . '</a>';
        } else {
            return null;
        }
    }
}
