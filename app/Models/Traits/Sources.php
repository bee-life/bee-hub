<?php namespace App\Models\Traits;


use App\Models\MetaData\Project;
use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Trait Sources
 * @package App\Models\Traits
 *
 * @property-read Collection|Project[] $projects
 * @mixin Eloquent
 */
trait Sources
{
    /**
     * Get all Connectors that are using this Model.
     *
     * @return BelongsToMany
     */
    abstract public function projects(): BelongsToMany;

    /**
     * Get image representation of Sources.
     *
     * @param array $class
     *
     * @return Collection
     */
    public function getSources( array $class = [] ): Collection
    {
        return $this->projects->flatMap( function ( Project $project ) use ( $class ) {
            return $project->getFeaturedImage( $class );
        } );
    }

    /**
     * Get Model attributions in string format.
     *
     * @return string
     */
    public function getTextSources(): string
    {
        return $this->projects->implode( 'attribution', ', ' );
    }
}
