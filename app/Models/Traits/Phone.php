<?php namespace App\Models\Traits;

use Eloquent;

/**
 * Trait Phone
 * @package App\Models\Traits
 *
 * @property string $phone
 * @mixin Eloquent
 */
trait Phone
{
    /**
     * Check if Model has a set phone number.
     *
     * @return bool
     */
    public function hasPhone(): bool
    {
        return ! empty( $this->phone );
    }

    /**
     * Get a link to Models phone number.
     *
     * @return string|null
     */
    public function getPhoneLink(): ?string
    {
        if ( $this->hasPhone() ) {
            return '<a href="tel:' . preg_replace( '/\s/', '', $this->phone ) . '">' . $this->phone . '</a>';
        } else {
            return null;
        }
    }
}
