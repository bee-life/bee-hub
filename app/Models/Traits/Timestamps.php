<?php namespace App\Models\Traits;

use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Traits\Timestamps
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @mixin Eloquent
 */
trait Timestamps
{
    /**
     * Format Carbon date
     *
     * @param Carbon $var
     * @param bool   $time
     *
     * @return string
     */
    abstract protected function formatDate( Carbon $var, bool $time = false ): string;

    /**
     * Return formatted created date.
     *
     * @param bool $time
     *
     * @return string
     */
    public function getCreatedAt( bool $time = false ): string
    {
        if ( isset( $this->created_at ) ) {
            return $this->formatDate( $this->created_at, $time );
        } else {
            return __( 'framework.no-date' );
        }
    }

    /**
     * Return formatted created date.
     *
     * @param bool $time
     *
     * @return string
     */
    public function getUpdatedAt( bool $time = false ): string
    {
        if ( isset( $this->updated_at ) ) {
            return $this->formatDate( $this->updated_at, $time );
        } else {
            return __( 'framework.no-date' );
        }
    }

    /**
     * Quick Sort closure function to sort Logs by date.
     *
     * @param Model $a
     * @param Model $b
     *
     * @return int
     */
    public static function sortClosureByCreatedAt( Model $a, Model $b ): int
    {
        if ( $a->created_at == $b->created_at ) {
            return 0;
        }

        return $a->created_at > $b->created_at ? 1 : - 1;
    }
    /**
     * Quick Sort closure function to sort Logs by date.
     *
     * @param Model $a
     * @param Model $b
     *
     * @return int
     */
    public static function sortClosureByUpdatedAt( Model $a, Model $b ): int
    {
        if ( $a->updated_at == $b->updated_at ) {
            return 0;
        }

        return $a->updated_at > $b->updated_at ? 1 : - 1;
    }
}
