<?php

namespace App\Models\Traits;

use Carbon\Carbon;
use Eloquent;

/**
 * App\Models\Base
 *
 * @mixin Eloquent
 */
trait Base
{
    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->id;
    }

    /**
     * Format Carbon date
     *
     * @param Carbon $var
     * @param bool   $time
     *
     * @return string
     */
    protected function formatDate( Carbon $var, bool $time = false ): string
    {
        return format_date( $var, $time );
    }
}
