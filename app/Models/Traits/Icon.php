<?php namespace App\Models\Traits;

use Eloquent;
use Storage;

/**
 * App\Models\Traits\Icon
 *
 * @property string $icon
 * @mixin Eloquent
 */
trait Icon
{
    /**
     * Return permalink to Icon.
     *
     * @return string
     */
    public function getIconUrl(): string
    {
        if ( $this->hasIcon() ) {
            return Storage::url( $this->icon );
        } else {
            return asset( 'images/BeeHub.png' );
        }
    }

    /**
     * Check if Icon is set.
     *
     * @return string
     */
    public function hasIcon(): string
    {
        return ! empty ( $this->icon );
    }

    /**
     * Return img HTML element with Icon.
     *
     * @param array $class Optional list of classes to add to the IMG element.
     *
     * @return string
     */
    public function getIcon( $class = [] ): string
    {
        return '<img class="' . implode( ' ', $class ) . '" src="' . $this->getIconUrl() . '" alt="' . $this . '" />';
    }

}
