<?php

namespace App\Models\CMS;

use App\Models\Traits\Base;
use App\Models\Traits\FeaturedImage;
use App\Models\Traits\Linkable;
use App\Models\Traits\Publishable;
use App\Models\Traits\Sluggable;
use App\Models\Traits\Timestamps;
use ArrayAccess;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Nova\Actions\Actionable;
use Laravel\Nova\Actions\ActionEvent;
use Spatie\Tags\HasTags;
use Spatie\Tags\Tag;

/**
 * App\Models\CMS\Report
 *
 * @property int                           $id
 * @property string                        $title
 * @property string                        $slug
 * @property string                        $abstract
 * @property string                        $author
 * @property string                        $keywords
 * @property string                        $content
 * @property string|null                   $meta_title
 * @property string|null                   $meta_description
 * @property Carbon                        $published_at
 * @property Carbon|null                   $created_at
 * @property Carbon|null                   $updated_at
 * @property Carbon|null                   $deleted_at
 * @property Collection|Tag[]              $tags
 * @property string|null                   $featured_image
 * @property-read Collection|ActionEvent[] $actions
 * @property-read int|null                 $actions_count
 * @property-read int|null                 $tags_count
 * @method static Builder|Report findSimilarSlugs( string $attribute, array $config, string $slug )
 * @method static Builder|Report whereAbstract( $value )
 * @method static Builder|Report whereAuthor( $value )
 * @method static Builder|Report whereFeaturedImage( $value )
 * @method static Builder|Report wherePublishedAt( $value )
 * @method static Builder|Report withAllTags( ArrayAccess|Tag|array $tags, ?string $type = null )
 * @method static Builder|Report withAllTagsOfAnyType( $tags )
 * @method static Builder|Report withAnyTags( ArrayAccess|Tag|array $tags, ?string $type = null )
 * @method static Builder|Report withAnyTagsOfAnyType( $tags )
 * @method static Builder|Report withUniqueSlugConstraints( Model $model, string $attribute, array $config, string $slug )
 * @method static bool|null forceDelete()
 * @method static Builder|Page newModelQuery()
 * @method static Builder|Page newQuery()
 * @method static Builder|Page onlyTrashed()
 * @method static Builder|Page query()
 * @method static bool|null restore()
 * @method static Builder|Page whereContent( $value )
 * @method static Builder|Page whereCreatedAt( $value )
 * @method static Builder|Page whereDeletedAt( $value )
 * @method static Builder|Page whereId( $value )
 * @method static Builder|Page whereMetaDescription( $value )
 * @method static Builder|Page whereMetaTitle( $value )
 * @method static Builder|Page whereSlug( $value )
 * @method static Builder|Page whereTitle( $value )
 * @method static Builder|Page whereUpdatedAt( $value )
 * @method static Builder|Page withTrashed()
 * @method static Builder|Page withoutTrashed()
 * @mixin Eloquent
 */
class Report extends Model
{
    use Base, SoftDeletes, Timestamps, Linkable, Sluggable, Publishable, HasTags, FeaturedImage, Actionable;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'created_at', 'updated_at', 'deleted_at', 'published_at' ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'abstract',
        'author',
        'keywords',
        'content',
        'slug',
        'meta_title',
        'meta_description',
        'published_at',
    ];

    /**
     * Return base route to Model
     *
     * @return string
     */
    public function getBaseRoute(): string
    {
        return '/reports/' . $this->slug;
    }

    /**
     * Override toString method to print Model.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->title;
    }
}
