<?php

namespace App\Models\CMS;

use App\Models\Traits\Base;
use App\Models\Traits\Excerpt;
use App\Models\Traits\FeaturedImage;
use App\Models\Traits\Linkable;
use App\Models\Traits\Publishable;
use App\Models\Traits\Sluggable;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Laravel\Nova\Actions\Actionable;
use Laravel\Nova\Actions\ActionEvent;

/**
 * App\Models\CMS\News
 *
 * @property int                           $id
 * @property string                        $title
 * @property string                        $slug
 * @property string                        $excerpt
 * @property string                        $content
 * @property Carbon                        $published_at
 * @property string|null                   $featured_image
 * @property Carbon|null                   $created_at
 * @property Carbon|null                   $updated_at
 * @property Carbon|null                   $deleted_at
 * @property-read Collection|ActionEvent[] $actions
 * @property-read int|null                 $actions_count
 * @method static EloquentBuilder|News findSimilarSlugs( string $attribute, array $config, string $slug )
 * @method static EloquentBuilder|News withUniqueSlugConstraints( Model $model, string $attribute, array $config, string $slug )
 * @method static bool|null forceDelete()
 * @method static EloquentBuilder|News newModelQuery()
 * @method static EloquentBuilder|News newQuery()
 * @method static Builder|News onlyTrashed()
 * @method static EloquentBuilder|News query()
 * @method static bool|null restore()
 * @method static EloquentBuilder|News whereContent( $value )
 * @method static EloquentBuilder|News whereCreatedAt( $value )
 * @method static EloquentBuilder|News whereDeletedAt( $value )
 * @method static EloquentBuilder|News whereExcerpt( $value )
 * @method static EloquentBuilder|News whereFeaturedImage( $value )
 * @method static EloquentBuilder|News whereId( $value )
 * @method static EloquentBuilder|News wherePublishedAt( $value )
 * @method static EloquentBuilder|News whereSlug( $value )
 * @method static EloquentBuilder|News whereTitle( $value )
 * @method static EloquentBuilder|News whereUpdatedAt( $value )
 * @method static Builder|News withTrashed()
 * @method static Builder|News withoutTrashed()
 * @mixin Eloquent
 */
class News extends Model
{
    use SoftDeletes, Linkable, Excerpt, Base, FeaturedImage, Sluggable, Publishable, Actionable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'excerpt',
        'content',
        'published_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'created_at', 'updated_at', 'deleted_at', 'published_at' ];


    /**
     * Return base route to Model
     *
     * @return string
     */
    public function getBaseRoute(): string
    {
        return '/news/' . $this->slug;
    }

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->title;
    }


}
