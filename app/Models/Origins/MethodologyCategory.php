<?php

namespace App\Models\Origins;

use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Nova\Actions\ActionEvent;

/**
 * App\Models\Locations\MethodologyCategory
 *
 * @property int                           $id
 * @property string                        $name
 * @property string                        $slug
 * @property string                        $uid
 * @property string|null                   $description
 * @property string|null                   $featured_image
 * @property string|null                   $icon
 * @property Carbon|null                   $created_at
 * @property Carbon|null                   $updated_at
 * @property-read Collection|Methodology[] $items
 * @property-read int|null                 $items_count
 * @property-read Collection|ActionEvent[] $actions
 * @property-read int|null                 $actions_count
 * @method static Builder|Category findSimilarSlugs( string $attribute, array $config, string $slug )
 * @method static Builder|MethodologyCategory whereSlug( $value )
 * @method static Builder|MethodologyCategory whereUid( $value )
 * @method static Builder|Category withUniqueSlugConstraints( Model $model, string $attribute, array $config, string $slug )
 * @method static Builder|MethodologyCategory newModelQuery()
 * @method static Builder|MethodologyCategory newQuery()
 * @method static Builder|MethodologyCategory query()
 * @method static Builder|MethodologyCategory whereCreatedAt( $value )
 * @method static Builder|MethodologyCategory whereDescription( $value )
 * @method static Builder|MethodologyCategory whereFeaturedImage( $value )
 * @method static Builder|MethodologyCategory whereIcon( $value )
 * @method static Builder|MethodologyCategory whereId( $value )
 * @method static Builder|MethodologyCategory whereName( $value )
 * @method static Builder|MethodologyCategory whereUpdatedAt( $value )
 * @mixin Eloquent
 */
class MethodologyCategory extends Category
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'origin_methodology_categories';

    /**
     * The key that is used to cache Uid mapping.
     *
     * @var string
     */
    protected static string $uidCacheKey = 'uid_methodology_cat';

    /**
     * Get all devices related to this category.
     *
     * @return HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany( Methodology::class, 'category_id' );
    }

    /**
     * Return base route to Model
     *
     * @return string
     */
    public function getBaseRoute(): string
    {
        return '/methodologies/categories/' . $this->slug;
    }
}
