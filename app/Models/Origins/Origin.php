<?php

namespace App\Models\Origins;

use App\Exceptions\InvalidOriginException;
use App\Models\Logs\Data;
use App\Models\Traits\Base;
use App\Models\Traits\FeaturedImage;
use App\Models\Traits\Icon;
use App\Models\Traits\Linkable;
use App\Models\Traits\Sluggable;
use App\Models\Traits\Timestamps;
use App\Models\Traits\Uid;
use App\Models\Traits\Website;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Laravel\Nova\Actions\Actionable;
use Str;

/**
 * App\Models\Origin\Origin
 *
 * @property int         $id
 * @property string      $name
 * @property string      $slug
 * @property string      $uid
 * @property string|null $description
 * @property string|null $featured_image
 * @property string|null $icon
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Origin newModelQuery()
 * @method static Builder|Origin newQuery()
 * @method static Builder|Origin query()
 * @method static Builder|Origin whereCategoryId( $value )
 * @method static Builder|Origin whereCreatedAt( $value )
 * @method static Builder|Origin whereDescription( $value )
 * @method static Builder|Origin whereFeaturedImage( $value )
 * @method static Builder|Origin whereIcon( $value )
 * @method static Builder|Origin whereId( $value )
 * @method static Builder|Origin whereName( $value )
 * @method static Builder|Origin whereUpdatedAt( $value )
 * @method static Builder|Origin whereUid( $value )
 * @mixin Eloquent
 */
abstract class Origin extends Model
{
    use Base, FeaturedImage, Icon, Timestamps, Website, Linkable, Sluggable, Uid, Actionable;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uid',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * Return Data Pivot elements.
     *
     * @return HasMany
     */
    public function data(): HasMany
    {
        return $this->hasMany( Data::class, 'origin_id' );
    }

    /**
     * Prints tooltip icon with Origin information.
     *
     * @return string
     */
    public function getTooltip(): string
    {
        $data = [ 'origin' => $this ];
        $view = 'origin.tooltips.' . Str::lower( class_basename( static::class ) );

        if ( ! view()->exists( $view ) ) {
            $view = 'origin.tooltips.generic';
        }

        return view( $view, $data )->render();
    }

    /**
     * Get the numeric representation of the origin within the database.
     *
     * @param Origin $origin
     *
     * @return string
     * @throws InvalidOriginException
     */
    public static function getTypeFromClass( Origin $origin ): string
    {
        if ( is_a( $origin, Methodology::class ) ) {
            return '1';
        } elseif ( is_a( $origin, Device::class ) ) {
            return '2';
        } elseif ( is_a( $origin, Source::class ) ) {
            return '3';
        } elseif ( is_a( $origin, Publication::class ) ) {
            return '4';
        }

        throw new InvalidOriginException();
    }

}
