<?php

namespace App\Models\Origins;

use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Nova\Actions\ActionEvent;

/**
 * App\Models\Locations\DeviceCategory
 *
 * @property int                           $id
 * @property string                        $name
 * @property string                        $slug
 * @property string                        $uid
 * @property string|null                   $description
 * @property string|null                   $featured_image
 * @property string|null                   $icon
 * @property Carbon|null                   $created_at
 * @property Carbon|null                   $updated_at
 * @property-read Collection|Device[]      $items
 * @property-read int|null                 $items_count
 * @property-read Collection|ActionEvent[] $actions
 * @property-read int|null                 $actions_count
 * @method static Builder|Category findSimilarSlugs( string $attribute, array $config, string $slug )
 * @method static Builder|DeviceCategory whereSlug( $value )
 * @method static Builder|DeviceCategory whereUid( $value )
 * @method static Builder|Category withUniqueSlugConstraints( Model $model, string $attribute, array $config, string $slug )
 * @method static Builder|DeviceCategory newModelQuery()
 * @method static Builder|DeviceCategory newQuery()
 * @method static Builder|DeviceCategory query()
 * @method static Builder|DeviceCategory whereCreatedAt( $value )
 * @method static Builder|DeviceCategory whereDescription( $value )
 * @method static Builder|DeviceCategory whereFeaturedImage( $value )
 * @method static Builder|DeviceCategory whereIcon( $value )
 * @method static Builder|DeviceCategory whereId( $value )
 * @method static Builder|DeviceCategory whereName( $value )
 * @method static Builder|DeviceCategory whereUpdatedAt( $value )
 * @mixin Eloquent
 */
class DeviceCategory extends Category
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'origin_device_categories';

    /**
     * The key that is used to cache Uid mapping.
     *
     * @var string
     */
    protected static string $uidCacheKey = 'uid_device_cat';

    /**
     * Get all devices related to this category.
     *
     * @return HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany( Device::class, 'category_id' );
    }

    /**
     * Return base route to Model
     *
     * @return string
     */
    public function getBaseRoute(): string
    {
        return '/devices/categories/' . $this->slug;
    }
}
