<?php

namespace App\Models\Origins;

use App\Models\Logs\Data;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laravel\Nova\Actions\ActionEvent;

/**
 * App\Models\Locations\Device
 *
 * @property int                           $id
 * @property string                        $name
 * @property string                        $slug
 * @property string                        $uid
 * @property string|null                   $description
 * @property string|null                   $featured_image
 * @property string|null                   $icon
 * @property int                           $category_id
 * @property int                           $vendor_id
 * @property Carbon|null                   $created_at
 * @property Carbon|null                   $updated_at
 * @property Carbon|null                   $introduced_at
 * @property-read DeviceCategory           $category
 * @property-read Vendor                   $vendor
 * @property-read Collection|ActionEvent[] $actions
 * @property-read int|null                 $actions_count
 * @property-read Collection|Data[]        $data
 * @property-read int|null                 $data_count
 * @method static Builder|Origin findSimilarSlugs( string $attribute, array $config, string $slug )
 * @method static Builder|Device whereIntroducedAt( $value )
 * @method static Builder|Device whereSlug( $value )
 * @method static Builder|Device whereUid( $value )
 * @method static Builder|Origin withUniqueSlugConstraints( Model $model, string $attribute, array $config, string $slug )
 * @method static Builder|Device newModelQuery()
 * @method static Builder|Device newQuery()
 * @method static Builder|Device query()
 * @method static Builder|Device whereCategoryId( $value )
 * @method static Builder|Device whereCreatedAt( $value )
 * @method static Builder|Device whereDescription( $value )
 * @method static Builder|Device whereFeaturedImage( $value )
 * @method static Builder|Device whereIcon( $value )
 * @method static Builder|Device whereId( $value )
 * @method static Builder|Device whereName( $value )
 * @method static Builder|Device whereUpdatedAt( $value )
 * @method static Builder|Device whereVendorId( $value )
 * @mixin Eloquent
 */
class Device extends Origin
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'origin_devices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'uid',
        'description',
        'featured_image',
        'icon',
        'website',
        'category_id',
        'vendor_id',
        'introduced_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'introduced_at',
    ];

    /**
     * Get related Vendor.
     *
     * @return BelongsTo
     */
    public function vendor(): BelongsTo
    {
        return $this->belongsTo( Vendor::class, 'vendor_id', 'id' );
    }

    /**
     * Get related Category
     *
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo( DeviceCategory::class, 'category_id' );
    }

    /**
     * Return base route to Model
     *
     * @return string
     */
    public function getBaseRoute(): string
    {
        return '/devices/' . $this->slug;
    }
}
