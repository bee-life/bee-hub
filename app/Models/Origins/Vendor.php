<?php

namespace App\Models\Origins;

use App\Models\Traits\Website;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Nova\Actions\ActionEvent;

/**
 * App\Models\Locations\Device
 *
 * @property int                           $id
 * @property string                        $name
 * @property string                        $slug
 * @property string                        $uid
 * @property string|null                   $description
 * @property string|null                   $featured_image
 * @property string|null                   $icon
 * @property string|null                   $website
 * @property Carbon|null                   $created_at
 * @property Carbon|null                   $updated_at
 * @property-read Collection|Device[]      $items
 * @property-read int|null                 $items_count
 * @property-read Collection|ActionEvent[] $actions
 * @property-read int|null                 $actions_count
 * @method static Builder|Category findSimilarSlugs( string $attribute, array $config, string $slug )
 * @method static Builder|Vendor whereSlug( $value )
 * @method static Builder|Vendor whereUid( $value )
 * @method static Builder|Category withUniqueSlugConstraints( Model $model, string $attribute, array $config, string $slug )
 * @method static Builder|Vendor newModelQuery()
 * @method static Builder|Vendor newQuery()
 * @method static Builder|Vendor query()
 * @method static Builder|Vendor whereCreatedAt( $value )
 * @method static Builder|Vendor whereDescription( $value )
 * @method static Builder|Vendor whereFeaturedImage( $value )
 * @method static Builder|Vendor whereIcon( $value )
 * @method static Builder|Vendor whereId( $value )
 * @method static Builder|Vendor whereName( $value )
 * @method static Builder|Vendor whereUpdatedAt( $value )
 * @method static Builder|Vendor whereWebsite( $value )
 * @mixin Eloquent
 */
class Vendor extends Category
{
    use Website;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'origin_vendors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'uid',
        'description',
        'featured_image',
        'icon',
        'website',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The key that is used to cache Uid mapping.
     *
     * @var string
     */
    protected static string $uidCacheKey = 'uid_vendors';

    /**
     * Get all devices related to this category.
     *
     * @return HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany( Device::class, 'vendor_id' );
    }

    /**
     * Return base route to Model
     *
     * @return string
     */
    public function getBaseRoute(): string
    {
        return '/vendors/' . $this->slug;
    }
}
