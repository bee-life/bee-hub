<?php

namespace App\Models\Origins;

use App\Models\Traits\Base;
use App\Models\Traits\FeaturedImage;
use App\Models\Traits\Icon;
use App\Models\Traits\Linkable;
use App\Models\Traits\Sluggable;
use App\Models\Traits\Timestamps;
use App\Models\Traits\Uid;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Nova\Actions\Actionable;

/**
 * App\Models\Locations\Category
 *
 * @property int                                    $id
 * @property string                                 $name
 * @property string                                 $slug
 * @property string                                 $uid
 * @property string|null                            $description
 * @property string|null                            $featured_image
 * @property string|null                            $icon
 * @property Carbon|null                            $created_at
 * @property Carbon|null                            $updated_at
 * @property-read Collection|Methodology[]|Device[] $items
 * @property-read int|null                          $items_count
 * @method static Builder|Category newModelQuery()
 * @method static Builder|Category newQuery()
 * @method static Builder|Category query()
 * @method static Builder|Category whereCreatedAt( $value )
 * @method static Builder|Category whereDescription( $value )
 * @method static Builder|Category whereFeaturedImage( $value )
 * @method static Builder|Category whereIcon( $value )
 * @method static Builder|Category whereId( $value )
 * @method static Builder|Category whereName( $value )
 * @method static Builder|Category whereUpdatedAt( $value )
 * @method static Builder|Category whereUid( $value )
 * @mixin Eloquent
 */
abstract class Category extends Model
{
    use Base, Timestamps, FeaturedImage, Icon, Linkable, Sluggable, Uid, Actionable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'uid',
        'description',
        'slug',
        'featured_image',
        'icon',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uid',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get all related items to this category.
     *
     * @return HasMany
     */
    abstract public function items(): HasMany;

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
