<?php

namespace App\Models\Origins;

use App\Models\Logs\Data;
use App\Models\Traits\Website;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Laravel\Nova\Actions\ActionEvent;

/**
 * App\Models\Origins\Publication
 *
 * @property int $id
 * @property string $title
 * @property string $author
 * @property string $author_short
 * @property string $year
 * @property string|null $medium
 * @property string|null $medium_short
 * @property string|null $editor
 * @property string|null $editor_short
 * @property string|null $issue
 * @property string|null $volume
 * @property string|null $edition
 * @property string|null $pages
 * @property string|null $website
 * @property Carbon|null $website_date
 * @property string $publication_type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $slug
 * @property string $uid
 * @property-read Collection|ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read Collection|Data[] $data
 * @property-read int|null $data_count
 * @property-read string $description
 * @property-read string $name
 * @method static Builder|Origin findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static Builder|Publication newModelQuery()
 * @method static Builder|Publication newQuery()
 * @method static Builder|Publication query()
 * @method static Builder|Publication whereAuthor($value)
 * @method static Builder|Publication whereAuthorShort($value)
 * @method static Builder|Publication whereCreatedAt($value)
 * @method static Builder|Publication whereEdition($value)
 * @method static Builder|Publication whereEditor($value)
 * @method static Builder|Publication whereEditorShort($value)
 * @method static Builder|Publication whereId($value)
 * @method static Builder|Publication whereIssue($value)
 * @method static Builder|Publication whereMedium($value)
 * @method static Builder|Publication whereMediumShort($value)
 * @method static Builder|Publication wherePages($value)
 * @method static Builder|Publication wherePublicationType($value)
 * @method static Builder|Publication whereSlug($value)
 * @method static Builder|Publication whereTitle($value)
 * @method static Builder|Publication whereUid($value)
 * @method static Builder|Publication whereUpdatedAt($value)
 * @method static Builder|Publication whereVolume($value)
 * @method static Builder|Publication whereWebsite($value)
 * @method static Builder|Publication whereWebsiteDate($value)
 * @method static Builder|Publication whereYear($value)
 * @method static Builder|Origin withUniqueSlugConstraints( Model $model, string $attribute, array $config, string $slug)
 * @mixin Eloquent
 */
class Publication extends Origin
{
    use Website;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'origin_publications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'author',
        'author_short',
        'year',
        'medium',
        'medium_short',
        'editor',
        'editor_short',
        'issue',
        'volume',
        'edition',
        'pages',
        'website',
        'website_date',
        'publication_type',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'website_date',
        'created_at',
        'updated_at',
    ];

    /**
     * The key that is used to cache Uid mapping.
     *
     * @var string
     */
    protected static string $uidCacheKey = 'uid_publication';

    /**
     * Remapped title attribute to Name to be more inline with Origin data model.
     *
     * @return string
     */
    public function getNameAttribute(): string
    {
        return $this->title;
    }


    /**
     * Remapped title attribute to Name to be more inline with Origin data model.
     *
     * @return string
     * @todo: Create a description out of the rest of the attributes.
     */
    public function getDescriptionAttribute(): string
    {
        return $this->title;
    }

    /**
     * Return base route to Model
     *
     * @return string
     */
    public function getBaseRoute(): string
    {
        return 'publication/' . $this->id;
    }

    /**
     * Return permalink to Model.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return $this->getBaseRoute();
    }
}
