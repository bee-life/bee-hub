<?php

namespace App\Models\Origins;

use App\Models\Logs\Data;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laravel\Nova\Actions\ActionEvent;

/**
 * App\Models\Locations\Methodology
 *
 * @property int                           $id
 * @property string                        $name
 * @property string                        $slug
 * @property string                        $uid
 * @property string|null                   $description
 * @property string|null                   $featured_image
 * @property string|null                   $icon
 * @property int                           $category_id
 * @property Carbon|null                   $created_at
 * @property Carbon|null                   $updated_at
 * @property-read MethodologyCategory      $category
 * @property-read Collection|ActionEvent[] $actions
 * @property-read int|null                 $actions_count
 * @property-read Collection|Data[]        $data
 * @property-read int|null                 $data_count
 * @method static Builder|Origin findSimilarSlugs( string $attribute, array $config, string $slug )
 * @method static Builder|Methodology whereSlug( $value )
 * @method static Builder|Methodology whereUid( $value )
 * @method static Builder|Origin withUniqueSlugConstraints( Model $model, string $attribute, array $config, string $slug )
 * @method static Builder|Methodology newModelQuery()
 * @method static Builder|Methodology newQuery()
 * @method static Builder|Methodology query()
 * @method static Builder|Methodology whereCategoryId( $value )
 * @method static Builder|Methodology whereCreatedAt( $value )
 * @method static Builder|Methodology whereDescription( $value )
 * @method static Builder|Methodology whereFeaturedImage( $value )
 * @method static Builder|Methodology whereIcon( $value )
 * @method static Builder|Methodology whereId( $value )
 * @method static Builder|Methodology whereName( $value )
 * @method static Builder|Methodology whereUpdatedAt( $value )
 * @mixin Eloquent
 */
class Methodology extends Origin
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'origin_methodologies';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'uid',
        'description',
        'featured_image',
        'icon',
        'website',
        'category_id',
    ];

    /**
     * The key that is used to cache Uid mapping.
     *
     * @var string
     */
    protected static string $uidCacheKey = 'uid_methodologies';

    /**
     * Get related Category
     *
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo( MethodologyCategory::class, 'category_id' );
    }

    /**
     * Return base route to Model
     *
     * @return string
     */
    public function getBaseRoute(): string
    {
        return '/methodologies/' . $this->slug;
    }
}
