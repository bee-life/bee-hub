<?php

namespace App\Models\Origins;

use App\Models\Logs\Data;
use App\Models\Traits\Website;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Laravel\Nova\Actions\ActionEvent;


/**
 * App\Models\Origins\Source
 *
 * @property int                                     $id
 * @property string                        $name
 * @property string                        $uid
 * @property string|null                   $description
 * @property string|null                   $website
 * @property string|null                   $featured_image
 * @property string|null                   $icon
 * @property Carbon|null                   $created_at
 * @property Carbon|null                   $updated_at
 * @property-read Collection|ActionEvent[] $actions
 * @property-read int|null                 $actions_count
 * @property-read Collection|Data[]        $data
 * @property-read int|null                 $data_count
 * @method static Builder|Origin findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static Builder|Source newModelQuery()
 * @method static Builder|Source newQuery()
 * @method static Builder|Source query()
 * @method static Builder|Source whereCreatedAt($value)
 * @method static Builder|Source whereDescription($value)
 * @method static Builder|Source whereFeaturedImage($value)
 * @method static Builder|Source whereIcon($value)
 * @method static Builder|Source whereId($value)
 * @method static Builder|Source whereName($value)
 * @method static Builder|Source whereUid($value)
 * @method static Builder|Source whereUpdatedAt($value)
 * @method static Builder|Source whereWebsite($value)
 * @method static Builder|Origin withUniqueSlugConstraints( Model $model, string $attribute, array $config, string $slug)
 * @mixin Eloquent
 */
class Source extends Origin
{
    use Website;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'origin_sources';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'website',
        'description',
        'featured_image',
        'icon',
    ];

    /**
     * The key that is used to cache Uid mapping.
     *
     * @var string
     */
    protected static string $uidCacheKey = 'uid_source';

    /**
     * Return base route to Model
     *
     * @return string
     */
    public function getBaseRoute(): string
    {
        return $this->website;
    }

    /**
     * Return permalink to Model.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return $this->getBaseRoute();
    }
}
