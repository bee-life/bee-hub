<?php

namespace App\Models\Logs\Reference;

use App\Models\Logs\Data;
use App\Models\Logs\Data\DataLocation;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\Origins\Origin;
use App\Models\References\Currency;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * App\Models\Logs\Data\DataId
 *
 * @property int                          $id
 * @property string                       $value
 * @property int                          $reference_id
 * @property-read Collection|Log[]        $logs
 * @property-read int|null                $logs_count
 * @property-read Log                     $log
 * @property-read mixed                   $raw
 * @property-read Collection|Data[]       $pivot
 * @property-read int|null                $pivot_count
 * @property-read Collection|Descriptor[] $type
 * @property-read int|null                $type_count
 * @property-read Currency                $reference
 * @property-read Collection|Descriptor[] $descriptors
 * @property-read int|null                $descriptors_count
 * @property-read DataLocation            $data_location
 * @property-read Descriptor              $descriptor
 * @property-read Origin                  $origin
 * @property-read Collection|Data[]       $pivotData
 * @property-read int|null                $pivot_data_count
 * @method static Builder|ReferenceCurrency whereReferenceId( $value )
 * @method static Builder|ReferenceLandUse newModelQuery()
 * @method static Builder|ReferenceLandUse newQuery()
 * @method static Builder|ReferenceLandUse query()
 * @method static Builder|ReferenceLandUse whereProjectId( $value )
 * @method static Builder|ReferenceLandUse whereId( $value )
 * @method static Builder|ReferenceLandUse whereValue( $value )
 * @mixin Eloquent
 */
class ReferenceCurrency extends ReferenceModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_reference_currencies';


    /**
     * Return manipulated value.
     *
     * @param mixed $value
     *
     * @return string
     */
    public function getValueAttribute( $value ): string
    {
        return $value;
    }

    /**
     * Check if provided object is equal.
     *
     * @param ReferenceCurrency|string $data
     *
     * @return boolean
     */
    public function isEqual( $data ): bool
    {
        if ( is_a( $data, self::class ) ) {
            return $this->reference_id == $data->reference_id && bccomp( $this->value, $data ) == 0;
        } else {
            return bccomp( $this->value, $data ) == 0;
        }
    }

    /**
     * Check if Log Data is larger than supplied element.
     *
     * @param ReferenceCurrency|string $data
     *
     * @return boolean
     */
    public function isMore( $data ): bool
    {
        if ( is_a( $data, self::class ) ) {
            /** if ( $this->reference_id != $data->reference_id ) {
             * return null;
             * }  */

            return bccomp( $this->value, $data->value ) == 1;
        } else {
            return bccomp( $this->value, $data ) == 1;
        }
    }

    /**
     * Check if Log Data is smaller than supplied element.
     *
     * @param ReferenceCurrency|string $data
     *
     * @return boolean
     */
    public function isLess( $data ): bool
    {
        if ( is_a( $data, self::class ) ) {
            /*  if ( $this->reference_id != $data->reference_id ) {
                  return null;
              }   */

            return bccomp( $this->value, $data->value ) == - 1;
        } else {
            return bccomp( $this->value, $data ) == - 1;
        }
    }

    /**
     * Get Icon classes to render the value on map.
     *
     * @param int    $min
     * @param int    $max
     * @param string $scale
     *
     * @return array
     */
    public function getIconClasses( $min = null, $max = null, string $scale = null ): array
    {
        $classes = [
            'datatype',
            'datatype-decimal',
            'datatype-reference',
            'datatype-currency',
            'datatype-' . $this->value,
            'datatype-' . $scale,
        ];

        if ( $scale != 'invalid' ) {
            if ( isset( $min ) && $this->isEqual( $min ) ) {
                $classes[] = 'datatype-min';
            }

            if ( isset( $max ) && $this->isEqual( $max ) ) {
                $classes[] = 'datatype-max';
            }

            if ( isset( $min, $max ) ) {
                $classes[] = 'datatype-' . $this->getDiscreteValue( $min, $max );
            }
        }

        return $classes;
    }

    /**
     * Get a discrete value between 0 and 9, to be presented on the map.
     *
     * @param string $min
     * @param string $max
     * @param bool   $invalid
     *
     * @return string
     */
    public function getDiscreteValue( string $min, string $max, bool $invalid = true ): string
    {
        bcscale( 5 );
        $zeroedMax = bcsub( $max, $min );
        $zeroed    = bcsub( $this->value, $min );
        if ( $zeroedMax == "0" ) {
            $fraction = "0";
        } else {
            $fraction = bcdiv( $zeroed, $zeroedMax );
        }
        if ( $invalid ) {
            $value = bcmul( $fraction, '10' );
        } else {
            $value = bcadd( bcmul( $fraction, '9' ), '1' );
        }

        if ( bccomp( $value, 9 ) === 1 ) {
            $value = "9";
        }

        return $value;
    }

    /**
     *
     * Get the related Project, that the data belongs to.
     *
     * @return BelongsTo
     */
    public function reference(): BelongsTo
    {
        return $this->belongsTo( Currency::class, 'reference_id' );
    }
}
