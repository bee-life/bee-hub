<?php

namespace App\Models\Logs\Reference;

use App\Models\Logs\Data;
use App\Models\Logs\Data\DataLocation;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\Origins\Origin;
use App\Models\References\Pollen;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * App\Models\Logs\Data\DataId
 *
 * @property int                          $id
 * @property int                          $amount
 * @property double                       $fraction
 * @property int                          $reference_id
 * @property-read Collection|Log[]        $logs
 * @property-read int|null                $logs_count
 * @property-read Log                     $log
 * @property-read mixed                   $raw
 * @property-read Collection|Data[]       $pivot
 * @property-read int|null                $pivot_count
 * @property-read Collection|Descriptor[] $type
 * @property-read int|null                $type_count
 * @property-read Pollen                  $reference
 * @property-read Collection|Descriptor[] $descriptors
 * @property-read int|null                $descriptors_count
 * @property-read DataLocation            $data_location
 * @property-read Descriptor              $descriptor
 * @property-read Origin                  $origin
 * @property-read string                  $value
 * @property-read Collection|Data[]       $pivotData
 * @property-read int|null                $pivot_data_count
 * @method static Builder|ReferencePollen whereAmount( $value )
 * @method static Builder|ReferencePollen whereFraction( $value )
 * @method static Builder|ReferencePollen whereReferenceId( $value )
 * @method static Builder|ReferenceLandUse newModelQuery()
 * @method static Builder|ReferenceLandUse newQuery()
 * @method static Builder|ReferenceLandUse query()
 * @method static Builder|ReferenceLandUse whereProjectId( $value )
 * @method static Builder|ReferenceLandUse whereId( $value )
 * @method static Builder|ReferenceLandUse whereValue( $value )
 * @mixin Eloquent
 */
class ReferencePollen extends ReferenceModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_reference_pollen';


    /**
     * Return manipulated value.
     *
     * @param mixed $value
     *
     * @return string
     */
    public function getValueAttribute( $value ): string
    {
        return $this->amount;
    }

    /**
     * Check if provided object is equal.
     *
     * @param ReferencePollen $data
     *
     * @return boolean
     */
    public function isEqual( $data ): bool
    {
        return $this->reference_id == $data->reference_id && $this->value === $data->value;
    }

    /**
     * Check if Log Data is larger than supplied element.
     *
     * @param ReferencePollen $data
     *
     * @return boolean
     */
    public function isMore( $data ): bool
    {
        /**if ( $this->reference_id != $data->reference_id ) {
         * return null;
         * } */

        return $this->value > $data->value;
    }

    /**
     * Check if Log Data is smaller than supplied element.
     *
     * @param ReferencePollen $data
     *
     * @return boolean
     */
    public function isLess( $data ): bool
    {
        /**if ( $this->reference_id != $data->reference_id ) {
         * return null;
         * } */

        return $this->value < $data->value;
    }

    /**
     * Get Icon classes to render the value on map.
     *
     * @param int         $min
     * @param int         $max
     * @param string|null $scale
     *
     * @return array
     */
    public function getIconClasses( $min = null, $max = null, string $scale = null ): array
    {
        return [
            'datatype',
            'datatype-reference',
            'datatype-pollen',
            'datatype-' . $this->value,
        ];
    }

    /**
     *
     * Get the related Project, that the data belongs to.
     *
     * @return BelongsTo
     */
    public function reference(): BelongsTo
    {
        return $this->belongsTo( Pollen::class, 'reference_id' );
    }
}
