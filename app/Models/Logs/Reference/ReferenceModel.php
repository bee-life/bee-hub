<?php namespace App\Models\Logs\Reference;

use App\Models\Logs\Data;
use App\Models\Logs\Data\DataLocation;
use App\Models\Logs\Data\DataModel;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\References\Reference;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * App\Models\EventData\EventDataModel
 * Base Model Implementation for all Log Datatypes. It allows simple scaffolding and common architecture
 * across different Datatypes.
 *
 * @property int                          $id
 * @property mixed                        $value
 * @property int                          $reference_id
 * @property-read mixed                   $raw
 * @property-read DataLocation            $dataLocation
 * @property-read Collection|Log[]        $logs
 * @property-read int|null                $logs_count
 * @property-read Log                     $log
 * @property-read Collection|Data[]       $pivot
 * @property-read int|null                $pivot_count
 * @property-read Collection|Descriptor[] $descriptors
 * @property-read int|null                $descriptors_count
 * @property-read Reference               $reference
 * @method static Builder|ReferenceModel newModelQuery()
 * @method static Builder|ReferenceModel newQuery()
 * @method static Builder|ReferenceModel query()
 * @method static Builder|ReferenceModel whereId( $value )
 * @method static Builder|ReferenceModel whereValue( $value )
 * @method static Builder|ReferenceModel whereReferenceId( $value )
 * @mixin Eloquent
 */
abstract class ReferenceModel extends DataModel
{
    //use HasEagerLimit;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value',
        'reference_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [ 'reference' ];

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->reference;
    }

    /**
     * Get the related Project, that the data belongs to.
     *
     * @return BelongsTo
     */
    abstract public function reference(): BelongsTo;
}
