<?php

namespace App\Models\Logs\Data;

use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\Origins\Origin;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class EventDataString
 * One of the simple Datatypes representation, a string.
 *
 * @property int                          $id
 * @property Carbon                       $value
 * @property-read Collection|Log[]        $logs
 * @property-read int|null                $logs_count
 * @property-read Log                     $log
 * @property-read DataLocation            $dataLocation
 * @property-read mixed                   $raw
 * @property-read Collection|Data[]       $pivot
 * @property-read int|null                $pivot_count
 * @property-read Collection|Descriptor[] $type
 * @property-read int|null                $type_count
 * @property-read Collection|Descriptor[] $descriptors
 * @property-read int|null                $descriptors_count
 * @property-read DataLocation            $data_location
 * @property-read Descriptor              $descriptor
 * @property-read Origin                  $origin
 * @property-read Collection|Data[]       $pivotData
 * @property-read int|null                $pivot_data_count
 * @method static Builder|DataDate newModelQuery()
 * @method static Builder|DataDate newQuery()
 * @method static Builder|DataDate query()
 * @method static Builder|DataDate whereId( $value )
 * @method static Builder|DataDate whereValue( $value )
 * @mixin Eloquent
 */
class DataDate extends DataModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_data_dates';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'value'
    ];

    /**
     * Return manipulated value.
     *
     * @param string $value
     *
     * @return Carbon
     */
    public function getValueAttribute( $value ): Carbon
    {
        return new Carbon( $value );
    }

    /**
     * Check if provided object is equal.
     *
     * @param DataDate $data
     *
     * @return boolean
     */
    public function isEqual( $data ): bool
    {
        return $this->value->eq( $data->value );
    }

    /**
     * Check if Log Data is larger than supplied element.
     *
     * @param DataDate $data
     *
     * @return boolean
     */
    public function isMore( $data ): bool
    {
        return $this->value->gt( $data->value );
    }

    /**
     * Check if Log Data is smaller than supplied element.
     *
     * @param DataDate $data
     *
     * @return boolean
     */
    public function isLess( $data ): bool
    {
        return $this->value->lt( $data->value );
    }

    /**
     * Get Icon classes to render the value on map.
     *
     * @param Carbon|null $min
     * @param Carbon|null $max
     * @param string      $scale
     *
     * @return array
     */
    public function getIconClasses( $min = null, $max = null, string $scale = 'range' ): array
    {
        return [
            'datatype',
            'datatype-dates',
            'datatype-date',
            'datatype-' . $scale,
        ];
    }

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->value->format( __( 'framework.date' ) );
    }
}
