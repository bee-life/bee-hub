<?php

namespace App\Models\Logs\Data;

use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Nova\Logs\Data;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class EventDataIntegerRange
 * An Integer range representation.
 *
 * @property int                          $id
 * @property mixed|null                   $value_from
 * @property mixed|null                   $value_to
 * @property array                        $value
 * @property-read Collection|Log[]        $logs
 * @property-read int|null                $logs_count
 * @property-read Log                     $log
 * @property-read DataLocation            $dataLocation
 * @property-read array                   $raw
 * @property-read Collection|Data[]       $pivot
 * @property-read int|null                $pivot_count
 * @property-read Collection|Descriptor[] $type
 * @property-read int|null                $type_count
 * @method static Builder|DataModelRange newModelQuery()
 * @method static Builder|DataModelRange newQuery()
 * @method static Builder|DataModelRange query()
 * @method static Builder|DataModelRange whereId( $value )
 * @method static Builder|DataModelRange whereValueFrom( $value )
 * @method static Builder|DataModelRange whereValueTo( $value )
 * @mixin Eloquent
 */
abstract class DataModelRange extends DataModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_id',
        'log_id',
        'value_from',
        'value_to',
    ];

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->value_from . '-' . $this->value_to;
    }

    /**
     * Return manipulated value.
     *
     * @param void $value
     *
     * @return array
     */
    public function getValueAttribute( $value ): array
    {
        return [ $this->value_from, $this->value_to ];
    }

    /**
     * Set manipulated value.
     *
     * @param array $value
     */
    public function setValueAttribute( array $value ): void
    {
        $this->value_from = $value[0];
        $this->value_to   = $value[1];
    }

    /**
     * Return raw value from database.
     *
     * @return array
     */
    public function getRawAttribute(): array
    {
        return [ $this->value_from, $this->value_to ];
    }

    /**
     * Check if provided object is equal.
     *
     * @param DataIntegerRange|array $data
     *
     * @return boolean
     * @todo: Test and probably improve.
     */
    public function isEqual( $data ): bool
    {
        if ( is_array( $data ) ) {
            return $this->value_to === $data[0] && $this->value_from === $data[1];
        } else {
            return $this->value_to === $data->value_from && $this->value_from === $data->value_to;
        }
    }

    /**
     * Check if Log Data is larger than supplied element.
     *
     * @param DataIntegerRange $data
     *
     * @return boolean
     * @todo: Test and probably improve.
     */
    public function isMore( $data ): bool
    {
        return $this->value > $data->value;
    }

    /**
     * Check if Log Data is smaller than supplied element.
     *
     * @param DataIntegerRange $data
     *
     * @return boolean
     * @todo: Test and probably improve.
     */
    public function isLess( $data ): bool
    {
        return $this->value < $data->value;
    }
}
