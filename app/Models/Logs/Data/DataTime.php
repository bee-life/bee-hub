<?php

namespace App\Models\Logs\Data;

use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\Origins\Origin;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class EventDataString
 * One of the simple Datatypes representation, a string.
 *
 * @property int                          $id
 * @property Carbon                       $value
 * @property-read Collection|Log[]        $logs
 * @property-read int|null                $logs_count
 * @property-read Log                     $log
 * @property-read DataLocation            $dataLocation
 * @property-read mixed                   $raw
 * @property-read Collection|Data[]       $pivot
 * @property-read int|null                $pivot_count
 * @property-read Collection|Descriptor[] $type
 * @property-read int|null                $type_count
 * @property-read Collection|Descriptor[] $descriptors
 * @property-read int|null                $descriptors_count
 * @property-read DataLocation            $data_location
 * @property-read Descriptor              $descriptor
 * @property-read Origin                  $origin
 * @property-read Collection|Data[]       $pivotData
 * @property-read int|null                $pivot_data_count
 * @method static Builder|DataTime newModelQuery()
 * @method static Builder|DataTime newQuery()
 * @method static Builder|DataTime query()
 * @method static Builder|DataTime whereId( $value )
 * @method static Builder|DataTime whereValue( $value )
 * @mixin Eloquent
 */
class DataTime extends DataModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_data_times';

    /**
     * The attributes that should be mutated to date.
     *
     * @var array
     */
    protected $dates = [
        'value'
    ];

    /**
     * Return manipulated value.
     *
     * @param string $value
     *
     * @return string
     */
    public function getValueAttribute( $value ): string
    {
        return (string) $value;
    }

    /**
     * Check if provided object is equal.
     *
     * @param Carbon|DataTime $data
     *
     * @return boolean
     */
    public function isEqual( $data ): bool
    {
        if ( is_a( $data, self::class ) ) {
            return $this->value->eq( $data->value );
        } else {
            return $this->value->eq( $data );
        }
    }

    /**
     * Check if Log Data is larger than supplied element.
     *
     * @param Carbon|DataTime $data
     *
     * @return boolean
     */
    public function isMore( $data ): bool
    {
        if ( is_a( $data, self::class ) ) {
            return $this->value->gt( $data->value );
        } else {
            return $this->value->gt( $data );
        }
    }

    /**
     * Check if Log Data is smaller than supplied element.
     *
     * @param Carbon|DataTime $data
     *
     * @return boolean
     */
    public function isLess( $data ): bool
    {
        if ( is_a( $data, self::class ) ) {
            return $this->value->lt( $data->value );
        } else {
            return $this->value->lt( $data );
        }
    }

    /**
     * Get Icon classes to render the value on map.
     *
     * @param Carbon|DataTime|null $min
     * @param Carbon|DataTime|null $max
     * @param string               $scale
     *
     * @return array
     */
    public function getIconClasses( $min = null, $max = null, string $scale = 'range' ): array
    {
        $classes = [
            'datatype',
            'datatype-dates',
            'datatype-time',
            'datatype-' . $scale,
            //'datatype-' . $this->value,
        ];

        if ( isset( $min ) && $this->isEqual( $min ) ) {
            $classes[] = 'datatype-min';
        }

        if ( isset( $max ) && $this->isEqual( $max ) ) {
            $classes[] = 'datatype-max';
        }

        return $classes;
    }
}
