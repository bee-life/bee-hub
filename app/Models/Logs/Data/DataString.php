<?php

namespace App\Models\Logs\Data;

use App\Exceptions\DatatypeTooLargeException;
use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\Origins\Origin;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class EventDataString
 * One of the simple Datatypes representation, a string.
 *
 * @property int                          $id
 * @property string                       $value
 * @property-read Collection|Log[]        $logs
 * @property-read int|null                $logs_count
 * @property-read Log                     $log
 * @property-read DataLocation            $dataLocation
 * @property-read mixed                   $raw
 * @property-read Collection|Data[]       $pivot
 * @property-read int|null                $pivot_count
 * @property-read Collection|Descriptor[] $type
 * @property-read int|null                $type_count
 * @property-read Collection|Descriptor[] $descriptors
 * @property-read int|null                $descriptors_count
 * @property-read DataLocation            $data_location
 * @property-read Descriptor              $descriptor
 * @property-read Origin                  $origin
 * @property-read Collection|Data[]       $pivotData
 * @property-read int|null                $pivot_data_count
 * @method static Builder|DataString newModelQuery()
 * @method static Builder|DataString newQuery()
 * @method static Builder|DataString query()
 * @method static Builder|DataString whereId( $value )
 * @method static Builder|DataString whereValue( $value )
 * @mixin Eloquent
 */
class DataString extends DataModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_data_strings';

    /**
     * Return manipulated value.
     *
     * @param string $value
     *
     * @return string
     */
    public function getValueAttribute( $value ): string
    {
        return (string) $value;
    }

    /**
     * Set the user's first name.
     *
     * @param string $value
     *
     * @return void
     * @throws DatatypeTooLargeException
     */
    public function setValueAttribute( string $value ): void
    {
        if ( mb_strlen( $value ) > 192 ) {
            throw new DatatypeTooLargeException();
        }

        $this->attributes['value'] = $value;
    }

    /**
     * Check if provided object is equal.
     *
     * @param DataString $data
     *
     * @return boolean
     */
    public function isEqual( $data ): bool
    {
        return $this->value === $data->value;
    }

    /**
     * Check if Log Data is larger than supplied element.
     *
     * @param DataString $data
     *
     * @return boolean
     */
    public function isMore( $data ): bool
    {
        return $this->value > $data->value;
    }

    /**
     * Check if Log Data is smaller than supplied element.
     *
     * @param DataString $data
     *
     * @return boolean
     */
    public function isLess( $data ): bool
    {
        return $this->value < $data->value;
    }

    /**
     * Get Icon classes to render the value on map.
     *
     * @param null   $min
     * @param null   $max
     * @param string $scale
     *
     * @return array
     */
    public function getIconClasses( $min = null, $max = null, string $scale = 'range' ): array
    {
        return [
            'datatype',
            'datatype-string',
            'datatype-' . $scale,
        ];
    }
}
