<?php

namespace App\Models\Logs\Data;

use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\Origins\Origin;
use DB;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;


/**
 * App\Models\EventData\EventDataModel
 * Base Model Implementation for all Log Datatypes. It allows simple scaffolding and common architecture
 * across different Datatypes.
 *
 * @property int                          $id
 * @property mixed                        $value
 * @property-read mixed                   $raw
 * @property-read DataLocation            $dataLocation
 * @property-read Collection|Log[]        $logs
 * @property-read int|null                $logs_count
 * @property-read Log                     $log
 * @property-read Collection|Data[]       $pivotData
 * @property-read int|null                $pivotData_count
 * @property-read Collection|Descriptor[] $descriptors
 * @property-read int|null                $descriptors_count
 * @property-read Collection|Origin[]     $origins
 * @property-read int|null                $origins_count
 * @property-read DataLocation            $data_location
 * @property-read Descriptor              $descriptor
 * @property-read Origin                  $origin
 * @property-read int|null                $pivot_data_count
 * @method static Builder|DataModel newModelQuery()
 * @method static Builder|DataModel newQuery()
 * @method static Builder|DataModel query()
 * @method static Builder|DataModel whereId( $value )
 * @method static Builder|DataModel whereValue( $value )
 * @mixin Eloquent
 */
abstract class DataModel extends Model
{
    //use HasEagerLimit;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    /**
     * Get related Log entry.
     *
     * @return MorphToMany
     */
    public function logs(): MorphToMany
    {
        return $this->morphToMany( Log::class, 'data', 'log_to_data', 'data_id', 'log_id' )
                    ->using( Data::class )->withPivot( [ 'origin_id', 'origin_type', 'descriptor_id' ] );
    }

    /**
     * Get number of Logs created by this Model.
     *
     * @return int
     */
    public function logCount(): int
    {
        if ( $this->relationLoaded( 'logs' ) ) {
            return $this->logs->count();
        } else {
            return $this->logs()->count();
        }
    }

    /**
     * Returns latest log of given data.
     *
     * @return Log|null
     */
    public function latestLog(): ?Log
    {
        return $this->logs()->from( DB::raw( '`logs` FORCE INDEX (logs_date_time_index)' ) )->orderBy( 'date', 'desc' )->orderBy( 'time', 'desc' )->first();
    }


    /**
     * Returns earliest log of given data.
     *
     * @return Log|null
     */
    public function earliestLog(): ?Log
    {
        return $this->logs()->orderBy( 'date' )->orderBy( 'time' )->first();
    }

    /**
     * Get related Descriptor.
     *
     * @return MorphToMany
     */
    public function descriptors(): MorphToMany
    {
        return $this->morphToMany( Descriptor::class, 'data', 'log_to_data', 'data_id', 'descriptor_id' )
                    ->using( Data::class )->withPivot( [ 'log_id', 'origin_id' ] );
    }

    /**
     * Get related Origin metadata.
     *
     * @return MorphToMany
     */
    public function origins(): MorphToMany
    {
        return $this->morphToMany( Origin::class, 'data', 'log_to_data', 'data_id', 'origin_id' )
                    ->using( Data::class )->withPivot( [ 'log_id', 'descriptor_id' ] );
    }

    /**
     * Get related Pivot.
     *
     * @return MorphMany
     */
    public function pivotData(): MorphMany
    {
        return $this->morphMany( Data::class, 'data' );
    }

    /**
     * Return Log.
     *
     * @return Log
     */
    public function getLogAttribute(): Log
    {
        return $this->logs->first();
    }

    /**
     * Return Descriptor metadata.
     *
     * @return Descriptor
     */
    public function getDescriptorAttribute(): Descriptor
    {
        return $this->descriptors->first();
    }

    /**
     * Return Origin metadata.
     *
     * @return Origin
     */
    public function getOriginAttribute(): Origin
    {
        return $this->origins->first();
    }

    /**
     * Get related Location data.
     *
     * @return DataLocation
     */
    public function getDataLocationAttribute(): DataLocation
    {
        return $this->log->location;
    }

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->value;
    }

    /**
     * Return raw value from database.
     *
     * @return mixed
     */
    public function getRawAttribute(): mixed
    {
        return $this->attributes['value'];
    }

    /**
     * Check if Data is set. Most Data Models do not exist if value was null,
     * but some compound models can use this feature.
     *
     * @return bool
     */
    public function isSet(): bool
    {
        return isset( $this->attributes['value'] );
    }

    /**
     * Return manipulated value.
     *
     * @param mixed $value
     *
     * @return mixed
     */
    abstract public function getValueAttribute( $value ): mixed;

    /**
     * Check if provided object is equal.
     *
     * @param DataModel $data
     *
     * @return boolean
     */
    abstract public function isEqual( $data ): bool;

    /**
     * Check if provided object is not equal.
     *
     * @param DataModel $data
     *
     * @return boolean
     */
    public function isNotEqual( $data ): bool
    {
        return ! $this->isEqual( $data );
    }

    /**
     * Get Icon classes to render the value on map.
     *
     * @param mixed|null $min
     * @param mixed|null $max
     * @param string     $scale
     *
     * @return array
     */
    abstract public function getIconClasses( $min = null, $max = null, string $scale = 'range' ): array;

    /**
     * Check if Log Data is larger than supplied element.
     *
     * @param DataModel $data
     *
     * @return boolean
     */
    abstract public function isMore( $data ): bool;

    /**
     * Check if Log Data is smaller than supplied element.
     *
     * @param DataModel $data
     *
     * @return boolean
     */
    abstract public function isLess( $data ): bool;

    /**
     * The comparison function for sorting Log Data.
     *
     * @param DataModel $data_a
     * @param DataModel $data_b
     *
     * @return int
     */
    static public function uSort( DataModel $data_a, DataModel $data_b ): int
    {
        if ( $data_a->isEqual( $data_b ) ) {
            return 0;
        }

        return $data_a->isMore( $data_b ) ? 1 : - 1;
    }

    /**
     * The comparison function for sorting Log Data.
     *
     * @param DataModel $data_a
     * @param DataModel $data_b
     *
     * @return int
     */
    static public function uSortDesc( DataModel $data_a, DataModel $data_b ): int
    {
        if ( $data_a->isEqual( $data_b ) ) {
            return 0;
        }

        return $data_a->isLess( $data_b ) ? 1 : - 1;
    }
}
