<?php

namespace App\Models\Logs\Data;

use App\Models\Locations\Lau;
use App\Models\Locations\Location;
use App\Models\Locations\Post;
use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\Origins\Origin;
use App\Models\Traits\SpatialTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use MatanYadaev\EloquentSpatial\Objects\Point;

/**
 * Class EventDataLocation
 * A Location representation.
 *
 * @uses Laravel MySQL Spatial extension:  https://github.com/grimzy/laravel-mysql-spatial
 * @uses geoPHP: https://geophp.net/
 * @uses geoPHP Laravel wrapper: https://github.com/spinen/laravel-geometry
 * @property int                          $id
 * @property Point                        $value
 * @property int|null                     $location_id
 * @property string|null                  $location_type
 * @property-read Collection|Log[]        $logs
 * @property-read int|null                $logs_count
 * @property-read Log                     $log
 * @property-read DataLocation            $dataLocation
 * @property-read Location|null           $location
 * @property-read float[]                 $raw
 * @property-read Collection|Data[]       $pivot
 * @property-read int|null                $pivot_count
 * @property-read Collection|Descriptor[] $type
 * @property-read int|null                $type_count
 * @property-read int                     $precision
 * @property-read Collection|Descriptor[] $descriptors
 * @property-read int|null                $descriptors_count
 * @property-read DataLocation            $data_location
 * @property-read Descriptor              $descriptor
 * @property-read Origin                  $origin
 * @property-read Collection|Data[]       $pivotData
 * @property-read int|null                $pivot_data_count
 * @mixin Eloquent
 */
class DataLocation extends DataModel
{
    use SpatialTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_data_locations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_id',
        'log_id',
        'region_id',
    ];

    /**
     * The attributes that are spatial representations.
     *
     * @var array
     */
    protected array $spatialFields = [
        'value',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'location'
    ];

    /**
     * Get related Location.
     *
     * @return MorphTo
     */
    public function location(): MorphTo
    {
        return $this->morphTo( 'location' );
    }

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return isset( $this->location ) ? (string) $this->location : $this->value;
    }

    /**
     * Return manipulated value.
     *
     * @param Point|null $value
     *
     * @return Point
     */
    public function getValueAttribute( $value ): Point
    {
        if ( ! empty( $value ) ) {
            return $value;
        } else {
            return $this->location->center;
        }
    }

    /**
     * Get the location precision based on available data.
     * Vales range between 0 and 6; 0 representing a Country and 6 GPS coordinates.
     *
     * @return int
     */
    public function getPrecisionAttribute(): int
    {
        // For certain precision levels we can avoid loading the Location model.
        if ( $this->hasCoordinates() ) {
            return 6;
        } elseif ( $this->location_type == Post::class ) {
            return 5;
        } elseif ( $this->location_type == Lau::class ) {
            return 4;
        }

        return $this->location->precision;
    }

    /**
     * Get array representation of Point, used in Leaflet.
     *
     * @return array|null
     */
    public function getCoordinates(): ?array
    {
        if ( ! empty( $this->value ) ) {
            return $this->value->getCoordinates();
        } else {
            return null;
        }
    }

    /**
     * Return raw value from database.
     *
     * @return float[]|null
     */
    public function getRawAttribute(): ?array
    {
        if ( ! empty( $this->value ) ) {
            return $this->value->getCoordinates();
        } else {
            return null;
        }
    }

    /**
     * Checks if this location contains Coordinates value.
     *
     * @return bool
     */
    public function hasCoordinates(): bool
    {
        return ! empty( $this->attributes['value'] );
    }

    /**
     * Check if provided object is equal.
     *
     * @param DataLocation $data
     *
     * @return boolean
     */
    public function isEqual( $data ): bool
    {
        if ( empty( $this->value ) && empty( $data->value ) ) {
            return $this->location_id == $data->location_id && $this->location_type == $data->location_type;
        } elseif ( isset( $this->value, $data->value ) ) {
            return $this->value[0] == $data->value[0] && $this->value[1]== $data->value[1];
        } else {
            return false;
        }
    }

    /**
     * Check if Log Data is larger than supplied element.
     *
     * @param DataIntegerRange $data
     *
     * @return boolean
     * @todo: What to do?
     */
    public function isMore( $data ): bool
    {
        return false;
    }

    /**
     * Check if Log Data is smaller than supplied element.
     *
     * @param DataIntegerRange $data
     *
     * @return boolean
     * @todo: What to do?
     */
    public function isLess( $data ): bool
    {
        return false;
    }

    /**
     * Get related Location data.
     * If its correct datatype, should return itself.
     *
     * @return DataLocation
     */
    public function getDataLocationAttribute(): DataLocation
    {
        return $this->pivotData->type_id == 9 ? $this : $this->log->location;
    }

    /**
     * Get Icon classes to render the value on map.
     *
     * @param null   $min
     * @param null   $max
     * @param string $scale
     *
     * @return array
     */
    public function getIconClasses( $min = null, $max = null, string $scale = 'range' ): array
    {
        return [
            'datatype',
            'datatype-location',
            'datatype-' . $scale,
        ];
    }

}
