<?php

namespace App\Models\Logs\Data;

use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\Origins\Origin;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class EventDataBoolean
 * One of the simple Datatypes - a Boolean representation.
 *
 * @property int                          $id
 * @property bool                         $value
 * @property-read Collection|Log[]        $logs
 * @property-read int|null                $logs_count
 * @property-read Log                     $log
 * @property-read DataLocation            $dataLocation
 * @property-read mixed                   $raw
 * @property-read Collection|Data[]       $pivot
 * @property-read int|null                $pivot_count
 * @property-read Collection|Descriptor[] $type
 * @property-read int|null                $type_count
 * @property-read Collection|Descriptor[] $descriptors
 * @property-read int|null                $descriptors_count
 * @property-read DataLocation            $data_location
 * @property-read Descriptor              $descriptor
 * @property-read Origin                  $origin
 * @property-read Collection|Data[]       $pivotData
 * @property-read int|null                $pivot_data_count
 * @method static Builder|DataBoolean newModelQuery()
 * @method static Builder|DataBoolean newQuery()
 * @method static Builder|DataBoolean query()
 * @method static Builder|DataBoolean whereId( $value )
 * @method static Builder|DataBoolean whereValue( $value )
 * @mixin Eloquent
 */
class DataBoolean extends DataModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_data_booleans';

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->value ? trans( 'logs.boolean.true' ) : trans( 'logs.boolean.false' );
    }

    /**
     * Return manipulated value.
     *
     * @param mixed $value
     *
     * @return boolean
     */
    public function getValueAttribute( $value ): bool
    {
        return (boolean) $value;
    }

    /**
     * Check if provided object is equal.
     *
     * @param DataBoolean $data
     *
     * @return boolean
     */
    public function isEqual( $data ): bool
    {
        return $this->value === $data->value;
    }

    /**
     * Check if Log Data is larger than supplied element.
     *
     * @param DataBoolean $data
     *
     * @return boolean
     */
    public function isMore( $data ): bool
    {
        return $this->value > $data->value;
    }

    /**
     * Check if Log Data is smaller than supplied element.
     *
     * @param DataBoolean $data
     *
     * @return boolean
     */
    public function isLess( $data ): bool
    {
        return $this->value < $data->value;
    }

    /**
     * Get Icon classes to render the value on map.
     *
     * @param int    $min
     * @param int    $max
     * @param string $scale
     *
     * @return array
     */
    public function getIconClasses( $min = 0, $max = 1, string $scale = 'range' ): array
    {
        return [
            'datatype',
            'datatype-boolean',
            'datatype-' . $scale,
            'datatype-' . $this->value,
            'datatype-' . ( $this->value === true ? 'max' : 'min' ),
        ];
    }
}
