<?php

namespace App\Models\Logs\Data;

use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\Origins\Origin;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class EventDataInteger
 * One of the simple Datatypes - an Integer representation.
 *
 * @property int                          $id
 * @property int                          $value
 * @property-read Collection|Log[]        $logs
 * @property-read int|null                $logs_count
 * @property-read Log                     $log
 * @property-read DataLocation            $dataLocation
 * @property-read mixed                   $raw
 * @property-read Collection|Data[]       $pivot
 * @property-read int|null                $pivot_count
 * @property-read Collection|Descriptor[] $type
 * @property-read int|null                $type_count
 * @property-read Collection|Descriptor[] $descriptors
 * @property-read int|null                $descriptors_count
 * @property-read DataLocation            $data_location
 * @property-read Descriptor              $descriptor
 * @property-read Origin                  $origin
 * @property-read Collection|Data[]       $pivotData
 * @property-read int|null                $pivot_data_count
 * @method static Builder|DataInteger newModelQuery()
 * @method static Builder|DataInteger newQuery()
 * @method static Builder|DataInteger query()
 * @method static Builder|DataInteger whereId( $value )
 * @method static Builder|DataInteger whereValue( $value )
 * @mixin Eloquent
 */
class DataInteger extends DataModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_data_integers';

    /**
     * Return manipulated value.
     *
     * @param mixed $value
     *
     * @return integer
     */
    public function getValueAttribute( $value ): int
    {
        return (integer) $value;
    }

    /**
     * Check if provided object is equal.
     *
     * @param DataInteger|int $data
     *
     * @return boolean
     */
    public function isEqual( $data ): bool
    {
        if ( is_a( static::class, $data ) ) {
            return $this->value === $data->value;
        } else {
            return $this->value === $data;
        }
    }

    /**
     * Check if Log Data is larger than supplied element.
     *
     * @param DataInteger|int $data
     *
     * @return boolean
     */
    public function isMore( $data ): bool
    {
        if ( is_a( static::class, $data ) ) {
            return $this->value > $data->value;
        } else {
            return $this->value > $data;
        }
    }

    /**
     * Check if Log Data is smaller than supplied element.
     *
     * @param DataInteger|int $data
     *
     * @return boolean
     */
    public function isLess( $data ): bool
    {
        if ( is_a( static::class, $data ) ) {
            return $this->value < $data->value;
        } else {
            return $this->value < $data;
        }
    }

    /**
     * Get Icon classes to render the value on map.
     *
     * @param int|null $min
     * @param int|null $max
     * @param string   $scale
     *
     * @return array
     */
    public function getIconClasses( $min = null, $max = null, string $scale = 'range' ): array
    {
        $classes = [
            'datatype',
            'datatype-integer',
            'datatype-' . $scale,
            'datatype-' . $this->value,
        ];

        if ( $scale != 'invalid' ) {
            if ( isset( $min ) && $this->isEqual( $min ) ) {
                $classes[] = 'datatype-min';
            }

            if ( isset( $max ) && $this->isEqual( $max ) ) {
                $classes[] = 'datatype-max';
            }

            if ( isset( $min, $max ) ) {
                $classes[] = 'datatype-' . $this->getDiscreteValue( $min, $max );
            }
        }

        return $classes;
    }

    /**
     * Get a discrete value between 0 and 9, to be presented on the map.
     *
     * @param int $min
     * @param int $max
     *
     * @return string
     */
    public function getDiscreteValue( int $min, int $max ): string
    {
        if ( $this->value == $min ) {
            $value = 0;
        } elseif ( $this->value < $min + 0.10 * $max ) {
            $value = 1;
        } elseif ( $this->value < $min + 0.20 * $max ) {
            $value = 2;
        } elseif ( $this->value < $min + 0.30 * $max ) {
            $value = 3;
        } elseif ( $this->value < $min + 0.40 * $max ) {
            $value = 4;
        } elseif ( $this->value < $min + 0.50 * $max ) {
            $value = 5;
        } elseif ( $this->value < $min + 0.60 * $max ) {
            $value = 6;
        } elseif ( $this->value < $min + 0.70 * $max ) {
            $value = 7;
        } elseif ( $this->value < $min + 0.90 * $max ) {
            $value = 8;
        } else {
            $value = 9;
        }

        return (string) $value;
    }
}
