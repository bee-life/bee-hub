<?php

namespace App\Models\Logs\Data;

use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class EventDataPercentage
 * A Descriptor representing a percentage. Uses special handling.
 *
 * @property int                                     $id
 * @property float                                   $value
 * @property-read Collection|Log[]                   $logs
 * @property-read int|null                           $logs_count
 * @property-read Log                                $log
 * @property-read DataLocation                       $dataLocation
 * @property-read mixed                              $raw
 * @property-read Collection|Data[]                  $pivot
 * @property-read int|null                           $pivot_count
 * @property-read Collection|Descriptor[]            $type
 * @property-read int|null                           $type_count
 * @method static Builder|DataPercentage newModelQuery()
 * @method static Builder|DataPercentage newQuery()
 * @method static Builder|DataPercentage query()
 * @method static Builder|DataPercentage whereId( $value )
 * @method static Builder|DataPercentage whereValue( $value )
 * @mixin Eloquent
 * @property-read Collection|Descriptor[]            $descriptors
 * @property-read int|null                           $descriptors_count
 * @property-read \App\Models\Logs\Data\DataLocation $data_location
 * @property-read Descriptor                         $descriptor
 * @property-read \App\Models\Origins\Origin         $origin
 * @property-read Collection|Data[]                  $pivotData
 * @property-read int|null                           $pivot_data_count
 */
class DataPercentage extends DataModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_data_percentages';

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return number_format( $this->value, 2, trans( 'logs.formatting.thousand_separator' ), trans( 'logs.formatting.decimal_separator' ) ) . '%';
    }

    /**
     * Return manipulated value.
     *
     * @param mixed $value
     *
     * @return string
     */
    public function getValueAttribute( $value ): string
    {
        return (string) $value;
    }

    /**
     * Check if provided object is equal.
     *
     * @param DataPercentage|string $data
     *
     * @return boolean
     */
    public function isEqual( $data ): bool
    {
        $value = is_a( $data, static::class ) ? $data->value : $data;

        return bccomp( $this->value, $value ) == 0;
    }

    /**
     * Check if Log Data is larger than supplied element.
     *
     * @param DataPercentage|string $data
     *
     * @return boolean
     */
    public function isMore( $data ): bool
    {
        $value = is_a( $data, static::class ) ? $data->value : $data;

        return bccomp( $this->value, $value ) > 0;
    }

    /**
     * Check if Log Data is smaller than supplied element.
     *
     * @param DataPercentage|string $data
     *
     * @return boolean
     */
    public function isLess( $data ): bool
    {
        $value = is_a( $data, static::class ) ? $data->value : $data;

        return bccomp( $this->value, $value ) < 0;
    }

    /**
     * Get Icon classes to render the value on map.
     *
     * @param int    $min
     * @param int    $max
     * @param string $scale
     *
     * @return array
     */
    public function getIconClasses( $min = 0, $max = 100, string $scale = 'range' ): array
    {
        $value  = $this->getDiscreteValue( $min, $max );
        $return = [
            'datatype',
            'datatype-percentage',
            'datatype-' . $scale,
            'datatype-' . $value,
        ];

        if ( $value == 0 ) {
            $return[] = 'datatype-min';
        } elseif ( $value == 9 ) {
            $return[] = 'datatype-max';
        }

        return $return;
    }

    /**
     * Get a discrete value between 0 and 9, to be presented on the map.
     *
     * @param string $min
     * @param string $max
     *
     * @return string
     */
    public function getDiscreteValue( string $min = '0.0', string $max = '100.0' ): string
    {
        if ( bccomp( $this->value, $min ) == 0 ) {
            $value = 0;
        } elseif ( $this->value < $min + 0.10 * $max ) {
            $value = 1;
        } elseif ( $this->value < $min + 0.20 * $max ) {
            $value = 2;
        } elseif ( $this->value < $min + 0.30 * $max ) {
            $value = 3;
        } elseif ( $this->value < $min + 0.40 * $max ) {
            $value = 4;
        } elseif ( $this->value < $min + 0.50 * $max ) {
            $value = 5;
        } elseif ( $this->value < $min + 0.60 * $max ) {
            $value = 6;
        } elseif ( $this->value < $min + 0.70 * $max ) {
            $value = 7;
        } elseif ( $this->value < $min + 0.90 * $max ) {
            $value = 8;
        } else {
            $value = 9;
        }

        return (string) $value;
    }
}
