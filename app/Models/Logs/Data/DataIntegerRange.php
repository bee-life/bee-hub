<?php

namespace App\Models\Logs\Data;

use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\Origins\Origin;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class EventDataIntegerRange
 * An Integer range representation.
 *
 * @property int                          $id
 * @property int|null                     $value_from
 * @property int|null                     $value_to
 * @property integer[]                    $value
 * @property-read Collection|Log[]        $logs
 * @property-read int|null                $logs_count
 * @property-read Log                     $log
 * @property-read DataLocation            $dataLocation
 * @property-read array                   $raw
 * @property-read Collection|Data[]       $pivot
 * @property-read int|null                $pivot_count
 * @property-read Collection|Descriptor[] $type
 * @property-read int|null                $type_count
 * @property-read Collection|Descriptor[] $descriptors
 * @property-read int|null                $descriptors_count
 * @property-read DataLocation            $data_location
 * @property-read Descriptor              $descriptor
 * @property-read Origin                  $origin
 * @property-read Collection|Data[]       $pivotData
 * @property-read int|null                $pivot_data_count
 * @method static Builder|DataIntegerRange newModelQuery()
 * @method static Builder|DataIntegerRange newQuery()
 * @method static Builder|DataIntegerRange query()
 * @method static Builder|DataIntegerRange whereId( $value )
 * @method static Builder|DataIntegerRange whereValueFrom( $value )
 * @method static Builder|DataIntegerRange whereValueTo( $value )
 * @mixin Eloquent
 */
class DataIntegerRange extends DataModelRange
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_data_integer_ranges';

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return format_number( $this->value_from, 0 )
               . '-' .
               format_number( $this->value_to, 0 );
    }

    /**
     * Return manipulated value.
     *
     * @param void $value
     *
     * @return integer[]
     */
    public function getValueAttribute( $value = null ): array
    {
        return [ (int) $this->value_from, (int) $this->value_to ];
    }

    /**
     * Get Icon classes to render the value on map.
     *
     * @param null   $min
     * @param null   $max
     * @param string $scale
     *
     * @return array
     * @todo add min/max and range values.
     */
    public function getIconClasses( $min = null, $max = null, string $scale = 'range' ): array
    {
        return [
            'datatype',
            'datatype-integer-range',
            'datatype-' . $scale,
        ];
    }
}
