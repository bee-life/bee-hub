<?php

namespace App\Models\Logs\Data;

use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\Origins\Origin;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class EventDataDecimal
 * One of the simple Datatypes representation - a Decimal number.
 *
 * Special care must be taken when doing calculations. In order to keep up with any kind of precisions,
 * BC_Math functions must be used.
 *
 * @see https://www.php.net/manual/en/book.bc.php
 * @property int                          $id
 * @property string                       $value
 * @property-read Collection|Log[]        $logs
 * @property-read int|null                $logs_count
 * @property-read Log                     $log
 * @property-read DataLocation            $dataLocation
 * @property-read mixed                   $raw
 * @property-read Collection|Data[]       $pivot
 * @property-read int|null                $pivot_count
 * @property-read Collection|Descriptor[] $type
 * @property-read int|null                $type_count
 * @property-read Collection|Descriptor[] $descriptors
 * @property-read int|null                $descriptors_count
 * @property-read DataLocation            $data_location
 * @property-read Descriptor              $descriptor
 * @property-read Origin                  $origin
 * @property-read Collection|Data[]       $pivotData
 * @property-read int|null                $pivot_data_count
 * @method static Builder|DataDecimal newModelQuery()
 * @method static Builder|DataDecimal newQuery()
 * @method static Builder|DataDecimal query()
 * @method static Builder|DataDecimal whereId( $value )
 * @method static Builder|DataDecimal whereValue( $value )
 * @mixin Eloquent
 */
class DataDecimal extends DataModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_data_decimals';

    /**
     * Print object.
     *
     * @TODO: figure out what to do with decimal places!
     * @return string
     */
    public function __toString(): string
    {
        return number_format( $this->value, 2, trans( 'logs.formatting.thousand_separator' ), trans( 'logs.formatting.decimal_separator' ) );
    }

    /**
     * Return manipulated value.
     *
     * @param string $value
     *
     * @return string
     */
    public function getValueAttribute( $value ): string
    {
        return (string) $value;
    }

    /**
     * Check if provided object is equal.
     *
     * @param string|DataDecimal $data
     *
     * @return boolean
     */
    public function isEqual( $data ): bool
    {
        if ( is_a( $data, self::class ) ) {
            return bccomp( $this->value, $data->value ) === 0;
        } else {
            return bccomp( $this->value, $data ) === 0;
        }
    }


    /**
     * Check if Log Data is larger than supplied element.
     *
     * @param string|DataDecimal $data
     *
     * @return boolean
     */
    public function isMore( $data ): bool
    {
        if ( is_a( $data, self::class ) ) {
            return bccomp( $this->value, $data->value ) === 1;
        } else {
            return bccomp( $this->value, $data ) === 1;
        }
    }

    /**
     * Check if Log Data is smaller than supplied element.
     *
     * @param string|DataDecimal $data
     *
     * @return boolean
     */
    public function isLess( $data ): bool
    {
        if ( is_a( $data, self::class ) ) {
            return bccomp( $this->value, $data->value ) === - 1;
        } else {
            return bccomp( $this->value, $data ) === - 1;
        }
    }

    /**
     * Get Icon classes to render the value on map.
     *
     * @param string|DataDecimal|null $min
     * @param string|DataDecimal|null $max
     * @param string                  $scale
     *
     * @return array
     * @todo add min/max and range values.
     */
    public function getIconClasses( $min = null, $max = null, string $scale = 'range' ): array
    {
        $classes = [
            'datatype',
            'datatype-decimal',
            'datatype-' . $scale,
            'datatype-' . $this->value,
        ];

        if ( $scale != 'invalid' ) {
            if ( isset( $min ) && $this->isEqual( $min ) ) {
                $classes[] = 'datatype-min';
            }

            if ( isset( $max ) && $this->isEqual( $max ) ) {
                $classes[] = 'datatype-max';
            }

            if ( isset( $min, $max ) ) {
                $classes[] = 'datatype-' . $this->getDiscreteValue( $min, $max );
            }
        }

        return $classes;
    }

    /**
     * Get a discrete value between 0 and 9, to be presented on the map.
     *
     * @param string $min
     * @param string $max
     *
     * @return string
     */
    public function getDiscreteValue( string $min, string $max ): string
    {
        bcscale( 5 );
        $zeroedMax = bcsub( $max, $min );
        $zeroed    = bcsub( $this->value, $min );
        if ( $zeroedMax == "0" ) {
            $fraction = "0";
        } else {
            $fraction = bcdiv( $zeroed, $zeroedMax );
        }


        $value = bcmul( $fraction, '10', 0 );

        if ( bccomp( $value, 9 ) === 1 ) {
            $value = "9";
        }

        return $value;
    }
}
