<?php

namespace App\Models\Logs\Data;

use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\MetaData\Project;
use App\Models\Origins\Origin;
use App\Models\Traits\Source;
use DB;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;


/**
 * App\Models\Logs\Data\DataId
 *
 * @property int                             $id
 * @property int                             $project_id
 * @property int                             $parent_id
 * @property int                             $children_count
 * @property string                          $value
 * @property-read Collection|Log[]           $logs
 * @property-read int|null                   $logs_count
 * @property-read Log                        $log
 * @property-read DataLocation               $dataLocation
 * @property-read DataId|null                $parent
 * @property-read mixed                      $raw
 * @property-read Collection|Data[]          $pivot
 * @property-read Collection|DataId[]        $children
 * @property-read Collection|DataAggregate[] $aggregates
 * @property-read Collection|Log[]           $hiveLogs
 * @property-read Collection|Log[]           $apiaryLogs
 * @property-read int|null                   $pivot_count
 * @property-read Collection|Descriptor[]    $type
 * @property-read int|null                   $type_count
 * @property-read int|null                   $aggregates_count
 * @property-read int|null                   $apiary_logs_count
 * @property-read Collection|Descriptor[]    $descriptors
 * @property-read int|null                   $descriptors_count
 * @property-read DataLocation               $data_location
 * @property-read Descriptor                 $descriptor
 * @property-read Origin                     $origin
 * @property-read int|null                   $hive_logs_count
 * @property-read Collection|Data[]          $pivotData
 * @property-read int|null                   $pivot_data_count
 * @property-read Project|null               $project
 * @method static Builder|DataId newModelQuery()
 * @method static Builder|DataId newQuery()
 * @method static Builder|DataId query()
 * @method static Builder|DataId whereProjectId( $value )
 * @method static Builder|DataId whereParentId( $value )
 * @method static Builder|DataId whereId( $value )
 * @method static Builder|DataId whereValue( $value )
 * @mixin Eloquent
 */
class DataId extends DataModel
{
    use Source;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_data_ids';

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * Return manipulated value.
     *
     * @param mixed $value
     *
     * @return string
     */
    public function getValueAttribute( $value ): string
    {
        return $value;
    }

    /**
     * Check if provided object is equal.
     *
     * @param DataId $data
     *
     * @return boolean
     */
    public function isEqual( $data ): bool
    {
        return $this->value === $data->value;
    }

    /**
     * Check if Log Data is larger than supplied element.
     *
     * @param DataId $data
     *
     * @return boolean
     */
    public function isMore( $data ): bool
    {
        return $this->value > $data->value;
    }

    /**
     * Check if Log Data is smaller than supplied element.
     *
     * @param DataId $data
     *
     * @return boolean
     */
    public function isLess( $data ): bool
    {
        return $this->value < $data->value;
    }

    /**
     * Get Icon classes to render the value on map.
     *
     * @param int         $min
     * @param int         $max
     * @param string|null $scale
     *
     * @return array
     */
    public function getIconClasses( $min = null, $max = null, string $scale = null ): array
    {
        return [
            'datatype',
            'datatype-id',
            'datatype-' . $this->value,
            'datatype-' . $scale,
        ];
    }

    /**
     * Get the related Project, that the data belongs to.
     */
    public function project(): BelongsTo
    {
        return $this->belongsTo( Project::class, 'project_id' );
    }

    /**
     * Get the related parent Id.
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo( DataId::class, 'parent_id' );
    }

    /**
     * Get the related parent Id.
     */
    public function children(): HasMany
    {
        return $this->hasMany( DataId::class, 'parent_id' );
    }

    /**
     * Get the related parent Id.
     */
    public function hiveLogs(): HasMany
    {
        return $this->hasMany( Log::class, 'hive_id' );
    }

    /**
     * Get the related parent Id.
     */
    public function apiaryLogs(): HasMany
    {
        return $this->hasMany( Log::class, 'apiary_id' );
    }

    /**
     * Check if Id has a parent.
     *
     * @return bool
     */
    public function hasParent(): bool
    {
        return isset( $this->parent_id );
    }

    /**
     * Check if Id has any children.
     *
     * @return bool
     */
    public function hasChildren(): bool
    {
        return $this->children->count() > 0;
    }


    /**
     * Get any related aggregates.
     */
    public function aggregates(): HasMany
    {
        return $this->hasMany( DataAggregate::class, 'external_id' );
    }

    /**
     * Returns latest log of given data.
     *
     * @return Log|null
     */
    public function latestLog(): ?Log
    {
        /**
         * After investigating the length of the query, it seems to be an issue with mySql selecting incorrect index.
         * Instead of custom made ones its using the Primary index, which makes the order clauses a lot slower.
         *
         * The most performance gain is with logs that have a lot of data, so usually those querying apiary id.
         */
        if ( $this->hasParent() ) {
            return $this->logs()->orderBy( 'date', 'desc' )->orderBy( 'time', 'desc' )->first();
        } else {
            return $this->logs()->from( DB::raw( '`logs` FORCE INDEX (logs_date_time_index)' ) )->orderBy( 'date', 'desc' )->orderBy( 'time', 'desc' )->first();
        }
    }
}
