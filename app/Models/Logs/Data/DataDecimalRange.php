<?php

namespace App\Models\Logs\Data;

use App\Models\Logs\Data;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use App\Models\Origins\Origin;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class EventDataDecimalRange
 * An Decimal range representation.
 *
 * Special care must be taken when doing calculations. In order to keep up with any kind of precisions,
 * BC_Math functions must be used.
 *
 * @see https://www.php.net/manual/en/book.bc.php
 * @property int                          $id
 * @property float|null                   $value_from
 * @property float|null                   $value_to
 * @property string[]                     $value
 * @property-read Collection|Log[]        $logs
 * @property-read int|null                $logs_count
 * @property-read Log                     $log
 * @property-read DataLocation            $dataLocation
 * @property-read string[]                $raw
 * @property-read Collection|Data[]       $pivot
 * @property-read int|null                $pivot_count
 * @property-read Collection|Descriptor[] $type
 * @property-read int|null                $type_count
 * @property-read Collection|Descriptor[] $descriptors
 * @property-read int|null                $descriptors_count
 * @property-read DataLocation            $data_location
 * @property-read Descriptor              $descriptor
 * @property-read Origin                  $origin
 * @property-read Collection|Data[]       $pivotData
 * @property-read int|null                $pivot_data_count
 * @method static Builder|DataDecimalRange newModelQuery()
 * @method static Builder|DataDecimalRange newQuery()
 * @method static Builder|DataDecimalRange query()
 * @method static Builder|DataDecimalRange whereId( $value )
 * @method static Builder|DataDecimalRange whereValueFrom( $value )
 * @method static Builder|DataDecimalRange whereValueTo( $value )
 * @mixin Eloquent
 */
class DataDecimalRange extends DataModelRange
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_data_decimal_ranges';

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return format_number( $this->value_from, 2 )
               . '-' .
               format_number( $this->value_to, 2 );
    }

    /**
     * Return manipulated value.
     *
     * @param void $value
     *
     * @return string[]
     */
    public function getValueAttribute( $value = null ): array
    {
        return [ (string) $this->value_from, (string) $this->value_to ];
    }

    /**
     * Get Icon classes to render the value on map.
     *
     * @param int    $min
     * @param int    $max
     * @param string $scale
     *
     * @return array
     * @todo add min/max and range values.
     *
     */
    public function getIconClasses( $min = null, $max = null, string $scale = 'range' ): array
    {
        return [
            'datatype',
            'datatype-decimal-range',
            'datatype-' . $scale,
        ];
    }

    /**
     * Check if provided object is equal.
     *
     * @param DataDecimalRange|array $data
     *
     * @return boolean
     */
    public function isEqual( $data ): bool
    {
        if ( is_array( $data ) ) {
            return bccomp( $this->value_to, $data[0] ) === 0 && bccomp( $this->value_from, $data[1] ) === 0;
        } else {
            return bccomp( $this->value_to, $data->value_from ) === 0 && bccomp( $this->value_from, $data->value_to ) === 0;
        }
    }
}
