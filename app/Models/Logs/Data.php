<?php

namespace App\Models\Logs;

use App\Models\Logs\Data\DataModel;
use App\Models\MetaData\Descriptor;
use App\Models\Origins\Origin;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphPivot;
use Illuminate\Database\Eloquent\Relations\MorphTo;


/**
 * App\Models\EventData\EventData
 *
 * Represent a single data value, in relation with DataType and Log.
 *
 * @property int             $log_id
 * @property int             $descriptor_id
 * @property int             $data_id
 * @property string          $data_type
 * @property int             $origin_id
 * @property string          $origin_type
 * @property-read DataModel  $data
 * @property-read Log        $log
 * @property-read mixed      $value
 * @property-read Descriptor $descriptor
 * @property-read Origin     $origin
 * @method static Builder|Data newModelQuery()
 * @method static Builder|Data newQuery()
 * @method static Builder|Data query()
 * @method static Builder|Data whereDataId( $value )
 * @method static Builder|Data whereDataType( $value )
 * @method static Builder|Data whereEventId( $value )
 * @method static Builder|Data whereDescriptorId( $value )
 * @method static Builder|Data whereLogId( $value )
 * @method static Builder|Data whereOriginId( $value )
 * @method static Builder|Data whereOriginType( $value )
 * @mixin Eloquent
 */
class Data extends MorphPivot
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_to_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'log_id',
        'descriptor_id',
        'data_id',
        'data_type',
        'origin_id',
        'origin_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        //  'data'
    ];

    /**
     * Get related Data.
     *
     * @return MorphTo
     */
    public function data(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * Get related Log.
     *
     * @return BelongsTo
     */
    public function log(): BelongsTo
    {
        return $this->belongsTo( Log::class, 'log_id' );
    }

    /**
     * Get Descriptor meta data.
     *
     * @return BelongsTo
     */
    public function descriptor(): BelongsTo
    {
        return $this->belongsTo( Descriptor::class, 'descriptor_id', 'id' );
    }

    /**
     * Get Origin meta data.
     *
     * @return MorphTo
     */
    public function origin(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * Get the related value from EventDataModel.
     *
     * @return mixed
     */
    public function getValueAttribute(): mixed
    {
        return $this->data->value;
    }

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->data;
    }

    public function getOrigin(): ?string
    {
        return empty( $this->origin_id ) ? null : $this->origin->getTooltip();
    }
}
