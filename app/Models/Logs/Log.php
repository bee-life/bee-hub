<?php

namespace App\Models\Logs;

use App\Exceptions\InvalidOriginException;
use App\Exceptions\ModelNotFoundByUidException;
use App\Models\Logs\Data\DataId;
use App\Models\Logs\Data\DataLocation;
use App\Models\Logs\Data\DataModel;
use App\Models\MetaData\Descriptor;
use App\Models\MetaData\Project;
use App\Models\Origins\Origin;
use App\Models\Traits\Area;
use App\Models\Traits\Center;
use App\Models\Traits\Source;
use Carbon\Carbon;
use Eloquent;
use Grimzy\LaravelMysqlSpatial\Types\Geometry;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use JetBrains\PhpStorm\ArrayShape;
use Throwable;

/**
 * App\Models\Log
 *
 * @property int                            $id
 * @property string                         $year
 * @property Carbon|null                    $date
 * @property string|null                    $raw_date
 * @property string|null                    $time
 * @property int                            $project_id
 * @property int                            $apiary_id
 * @property int                            $hive_id
 * @property Carbon                         $created_at
 * @property-read Project                   $project
 * @property-read Collection|Data[]         $data
 * @property-read int|null                  $data_count
 * @property-read Collection|Descriptor[]   $descriptors
 * @property-read int|null                  $descriptors_count
 * @property-read Geometry|null             $area
 * @property-read Point|null                $center
 * @property-read DataId                    $hive
 * @property-read DataId                    $apiary
 * @property-read DataLocation              $location
 * @property-read Collection|DataLocation[] $locations
 * @property-read int|null                  $locations_count
 * @property-read int                       $precision
 * @method static Builder|Log newModelQuery()
 * @method static Builder|Log newQuery()
 * @method static Builder|Log query()
 * @method static Builder|Log whereProjectId( $value )
 * @method static Builder|Log whereHiveId( $value )
 * @method static Builder|Log whereApiaryId( $value )
 * @method static Builder|Log whereCreatedAt( $value )
 * @method static Builder|Log whereDate( $value )
 * @method static Builder|Log whereId( $value )
 * @method static Builder|Log whereTime( $value )
 * @method static Builder|Log whereYear( $value )
 * @mixin Eloquent
 */
class Log extends Model
{
    use Center, Area, Source;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'year',
        'date',
        'time',
        'project_id',
        'hive_id',
        'apiary_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to date.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'date'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Allow Center Trait to interact with this Model as it has the center attribute.
     *
     * @return Point
     */
    public function getCenterAttribute(): Point
    {
        return $this->location->value ?? $this->location->location->center;
    }

    /**
     * Allow Area Trait to interact with this Model as it has the area attribute.
     *
     * @return Geometry|null
     */
    public function getAreaAttribute(): ?Geometry
    {
        return $this->location->location->area;
    }

    /**
     * Return raw date to circumvent Carbon.
     *
     * @return string
     */
    public function getRawDateAttribute(): string
    {
        return $this->attributes['date'];
    }

    /**
     * Print object.
     *
     * @return string
     * @todo
     */
    public function __toString(): string
    {

        if ( isset( $this->date ) ) {
            $date = $this->date->format( 'j. n. Y' );

            if ( isset( $this->time ) ) {
                $date .= ', ' . $this->time;
            }

            return $date;
        } else {
            return (string) $this->year;
        }
    }

    /**
     * Get related Connector.
     *
     * @return BelongsTo
     */
    public function project(): BelongsTo
    {
        return $this->belongsTo( Project::class, 'project_id' );
    }

    /**
     * Return LogData Pivot elements.
     *
     * @return HasMany
     */
    public function data(): HasMany
    {
        return $this->hasMany( Data::class, 'log_id' );
    }

    /**
     * Get Log Location.
     *
     * @return MorphToMany
     * @throws ModelNotFoundByUidException
     */
    public function locations(): MorphToMany
    {
        return $this->morphedByMany( DataLocation::class, 'data', 'log_to_data', 'log_id', 'data_id' )
                    ->using( Data::class )->withPivot( [ 'descriptor_id', 'origin_id' ] )
                    ->where( 'descriptor_id', Descriptor::getIdFromUid( 'colonies-location' ) );
    }

    /**
     * Get all logs related to datatype.
     *
     * @return BelongsToMany
     */
    public function descriptors(): BelongsToMany
    {
        return $this->belongsToMany( Descriptor::class, 'log_to_data', 'log_id', 'descriptor_id' )
                    ->using( Data::class )->withPivot( [ 'origin_id', 'data_id' ] );
    }

    /**
     * Get related Location data.
     *
     * @return DataLocation|null
     * @throws ModelNotFoundByUidException
     */
    protected function getLocationAttribute(): ?DataLocation
    {
        $descriptor_id = Descriptor::getIdFromUid( 'colonies-location' );

        if ( ! $this->relationLoaded( 'colonies-location' ) && $this->relationLoaded( 'data' ) && $this->data->where( 'descriptor_id', $descriptor_id )->isNotEmpty() ) {
            return $this->data->where( 'descriptor_id', $descriptor_id )->first()->data;
        } else {
            return $this->locations->first();
        }
    }

    /**
     * Get Precision that this Log represent.
     *
     * @return int
     */
    protected function getPrecisionAttribute(): int
    {
        return $this->location->precision;
    }

    /**
     * Get related External Id.
     *
     * @return BelongsTo
     */
    public function hive(): BelongsTo
    {
        return $this->belongsTo( DataId::class, 'hive_id' );
    }

    /**
     * Get Apiary Id.
     *
     * @return BelongsTo
     */
    public function apiary(): BelongsTo
    {
        return $this->belongsTo( DataId::class, 'apiary_id' );
    }

    /**
     * Check if Log ir related to an Apiary
     *
     * @return bool
     */
    public function hasApiaryId(): bool
    {
        return isset( $this->apiary_id );
    }

    /**
     * Check if Log is related to a Hive.
     *
     * @return bool
     */
    public function hasHiveId(): bool
    {
        return isset( $this->hive_id );
    }

    /**
     * Render Popup text based on provided Descriptor.
     *
     * @param Descriptor|null $descriptor
     *
     * @return string
     * @throws Throwable
     */
    public function getPopupText( Descriptor $descriptor = null ): string
    {
        $data = [ 'log' => $this ];

        if ( isset( $descriptor ) && view()->exists( 'data.popups.logs.' . $descriptor->uid ) ) {
            $view               = 'data.popups.logs.' . $descriptor->uid;
            $data['descriptor'] = $descriptor;
        } elseif ( isset( $descriptor, $descriptor->category ) && view()->exists( 'data.popups.categories.' . $descriptor->category->uid ) ) {
            $view               = 'data.popups.categories.' . $descriptor->category->uid;
            $data['descriptor'] = $descriptor;
            $data['category']   = $descriptor->category;
        } else {
            $view = 'data.popups.generic';
        }

        return view( $view, $data )->render();
    }

    /**
     * Render tooltip text.
     *
     * @param Descriptor|mixed $value
     *
     * @return string
     */
    public function getTooltipText( Descriptor|string $value ): string
    {
        if ( is_a( $value, Descriptor::class ) && view()->exists( 'data.tooltips.logs.' . $value->uid ) ) {
            return view( 'data.tooltips.logs.' . $value->uid, [ 'log' => $this, 'descriptor' => $value ] )->render();
        } else {
            return $this->location . ': ' . $value;
        }
    }

    /**
     * Render HTML text representing the value of the currently queried datatype.
     *
     * @param Descriptor|null $descriptor
     *
     * @return string
     * @throws Throwable
     */
    public function getContent( Descriptor $descriptor = null ): string
    {
        $data = [ 'log' => $this ];

        if ( isset( $descriptor ) && view()->exists( 'data.content.logs.' . $descriptor->uid ) ) {
            $view               = 'data.content.logs.' . $descriptor->uid;
            $data['descriptor'] = $descriptor;
        } else {
            $view = 'data.content.logs.generic';
        }

        return view( $view, $data )->render();
    }

    /**
     * Generate full Region name based on parent Regions.
     *
     * @return string
     */
    public function getLocation(): string
    {
        if ( isset( $this->location, $this->location->location ) ) {
            return $this->location->location->getFullName();
        } else {
            return __( 'logs.location-unknown' );
        }
    }

    /**
     * Render Connector image.
     *
     * @return string
     */
    public function getProject(): string
    {
        return view( 'components.project-image', [ 'project' => $this->project ] )->render();
    }

    /**
     * Render Connector Owner image.
     *
     * @return string
     */
    public function getProviders(): string
    {
        return view( 'components.providers', [ 'providers' => $this->project->providers ] )->render();
    }

    /**
     * Check if Log contains provided Descriptor Data Model.
     *
     * @param Descriptor|int $descriptor
     *
     * @return bool
     */
    public function hasData( Descriptor|int $descriptor ): bool
    {
        return $this->data->where( 'descriptor_id', is_a( $descriptor, Descriptor::class ) ? $descriptor->id : $descriptor )->isNotEmpty();
    }

    /**
     * Get Data Model by specified descriptor.
     *
     * @param Descriptor|int $descriptor
     *
     * @return Data
     */
    public function getData( Descriptor|int $descriptor ): Data
    {
        return $this->data->where( 'descriptor_id', is_a( $descriptor, Descriptor::class ) ? $descriptor->id : $descriptor )->first();
    }

    /**
     * Get Data Model value by specified descriptor.
     *
     * @param Descriptor|int $descriptor
     *
     * @return mixed
     */
    public function getValue( Descriptor|int $descriptor ): mixed
    {
        return $this->getData( $descriptor )->value;
    }

    /**
     * Gets historic data for current location.
     *
     * @param Descriptor      $descriptor
     * @param string          $type    One of 'yearly', 'monthly', 'weekly' or 'daily'.
     * @param Collection|null $logData Optional preloaded log data.
     *
     * @return array
     */
    #[ArrayShape( [ 'id' => "string", 'descriptor' => "\App\Models\Logs\Descriptor", 'data' => "array" ] )]
    public function getHistoric( Descriptor $descriptor, string $type, Collection $logData = null ): array
    {
        if ( empty( $logData ) ) {
            // TODO
            /*$logData = Data::with( [
                'log',
                'log.data' => function ( $q ) use ( $descriptor ) {
                    $q->with( [ 'data' ] )->where( 'descriptor_id', $descriptor->id );
                }
            ] )
                           ->where( [
                                 'descriptor_id' => 1,
                                 'data_id'       => $this->externalId->id,
                                 'data_type'     => 'id'
                             ] )->whereHas( 'log', function ( $q ) {
                    $q->whereTime( 'time', '15:00:00' );
                } )->get(); */
        }

        // Sort by date, so we have correct order for graph.
        $logs = $logData->pluck( 'log' )->sort( function ( Log $a, Log $b ) {
            if ( $a->date == $b->date ) {
                return 0;
            }

            return $a->date > $b->date ? 1 : 0;
        } );

        // We need to aggregate data if we are not looking into daily data.
        if ( $type != 'daily' ) {
            $grouped_logs = $logs->groupBy( function ( Log $log ) use ( $type ) {
                return match ( $type ) {
                    'weekly' => $log->date->format( 'o' ) . '-' . $log->date->format( 'W' ),
                    'monthly' => $log->date->format( 'Y' ) . '-' . $log->date->format( 'm' ),
                    default => $log->date->format( 'Y' ), // Yearly
                };
            } );

            $labels = [];
            $data   = [];
            // Calculate mean based on given grouped Logs.
            foreach ( $grouped_logs as $label => $group ) {
                $labels[] = $label;
                // Use Sum for special cases
                if ( $descriptor->uid == 'rain' ) {
                    $data[] = bcsum( $group->flatMap->data->filter( function ( Data $value ) use ( $descriptor ) {
                        return $value->descriptor_id == $descriptor->id;
                    } )->pluck( 'data' )->pluck( 'value' ) );
                } else {
                    // Otherwise always use Median, as its less biased compared to others.
                    $data[] = bcmedian( $group->flatMap->data->filter( function ( Data $value ) use ( $descriptor ) {
                        return $value->descriptor_id == $descriptor->id;
                    } )->pluck( 'data' )->pluck( 'value' ) );
                }
            }
        } else {
            // Daily, keep everything as it is
            $labels = $logs->map( function ( Log $log ) {
                return format_date( $log->date );
            } )->values();
            $data   = $logs->flatMap->data->filter( function ( Data $value ) use ( $descriptor ) {
                return $value->descriptor_id == $descriptor->id;
            } )->pluck( 'data' )->pluck( 'value' );
        }

        return [
            'id'         => $this->id . '-' . $descriptor->id,
            'descriptor' => $descriptor,
            'data'       => array_combine( $labels, $data ),
        ];
    }

    /**
     * Attach a value to a datatype.
     *
     * @param Descriptor  $descriptor
     * @param mixed|array $value
     * @param Origin|null $origin
     *
     * @return DataModel|null
     * @throws InvalidOriginException
     */
    public function attachData( Descriptor $descriptor, mixed $value, Origin $origin = null ): ?DataModel
    {
        // Do not insert null values.
        if ( ! isset( $value ) ) {
            return null;
        }

        /** @var DataModel $dataModel */
        if ( ! is_array( $value ) ) {
            $value = [ 'value' => $value ];
        }

        if ( $descriptor->uid == 'external-id' || $descriptor->uid == 'apiary-id' ) {
            /**
             * Special case: DataID for Apiary id or Hive id
             */
            if ( empty( $value['project_id'] ) ) {
                $value['project_id'] = $this->project_id;
            }
            if ( $descriptor->uid == 'external-id' && $this->hasApiaryId() ) {
                $value['parent_id'] = $this->apiary_id;
            }

            $dataModel = DataId::firstOrCreate( $value );

            if ( $descriptor->uid == 'external-id' ) {
                $this->hive_id = $dataModel->id;
                $this->save();
            } else if ( $descriptor->uid == 'apiary-id' ) {
                $this->apiary_id = $dataModel->id;
                $this->save();
            }
        } elseif ( $descriptor->uid == 'colonies-location' ) {
            /**
             * Special case: Location
             */
            $q = DataLocation::without( [ 'location' ] );

            // Check if it exists already
            if ( isset( $value['value'] ) ) {
                $q->equals( 'value', $value['value'] );
            } else {
                $q->whereNull( 'value' );
            }
            if ( isset( $value['location_id'] ) ) {
                $q->whereLocationId( $value['location_id'] );
                $q->whereLocationType( $value['location_type'] );
            } else {
                $q->whereNull( 'location_id' );
            }
            $dataModel = $q->first();
            if ( empty( $dataModel ) ) {
                $dataModel = DataLocation::create( $value );
            }
        } else {
            if ( isset( $value['value'] ) && is_array( $value['value'] ) ) {
                // Separate handle for data ranges
                // Ignore when both values are null.
                if ( ! isset( $value['value'][0], $value['value'][1] ) ) {
                    return null;
                }
                $dataModel = call_user_func( $descriptor::getDataModel( $descriptor->table_name ) . '::firstOrCreate', [
                    'value_from' => $value['value'][0],
                    'value_to'   => $value['value'][1]
                ] );
            } else {
                $dataModel = call_user_func( $descriptor::getDataModel( $descriptor->table_name ) . '::firstOrCreate', $value );
            }
        }

        $data = [ 'log_id' => $this->id ];

        if ( isset( $origin ) ) {
            $data['origin_id']   = $origin->id;
            $data['origin_type'] = Origin::getTypeFromClass( $origin );
        }

        $descriptor->data()->attach( $dataModel->id, $data );

        return $dataModel;
    }

    /**
     * Check if the Log Date is recent.
     * The threshold of recent data is within last 7 days.
     *
     * @return bool
     */
    public function hasRecentData(): bool
    {
        return $this->date > Carbon::today()->subWeek();
    }

    /**
     * Quick Sort closure function to sort Logs by date.
     *
     * @param Log $a
     * @param Log $b
     *
     * @return int
     */
    public static function sortClosureByDate( Log $a, Log $b ): int
    {
        if ( $a->raw_date == $b->raw_date ) {
            return 0;
        }

        return $a->raw_date > $b->raw_date ? 1 : - 1;
    }

    /**
     * Quick Sort closure function to sort Logs by date and time.
     *
     * @param Log $a
     * @param Log $b
     *
     * @return int
     */
    public static function sortClosureByDateTime( Log $a, Log $b ): int
    {
        $byDate = static::sortClosureByDate( $a, $b );
        if ( $byDate != 0 ) {
            return $byDate;
        }
        if ( $a->time == $b->time ) {
            return 0;
        }

        return $a->time > $b->time ? 1 : - 1;
    }
}
