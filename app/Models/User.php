<?php

namespace App\Models;

use App\Models\MetaData\Project;
use App\Models\MetaData\Provider;
use App\Models\Traits\Base;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Laravel\Nova\Actions\Actionable;
use Laravel\Nova\Actions\ActionEvent;
use Str;

/**
 * App\Models\User
 *
 * @property int                                                        $id
 * @property string                                                     $name
 * @property string                                                     $email
 * @property string                                                     $new_email
 * @property string                                                     $type
 * @property Carbon|null                                                $email_verified_at
 * @property string                                                     $password
 * @property string|null                                                $remember_token
 * @property Carbon|null                                                $created_at
 * @property Carbon|null                                                $updated_at
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null                                              $notifications_count
 * @property-read Collection|ActionEvent[]                              $actions
 * @property-read int|null                                              $actions_count
 * @property-read Collection|Project[]                                  $projects
 * @property-read int|null                                              $projects_count
 * @property-read Collection|Provider[]                                 $providers
 * @property-read int|null                                              $providers_count
 * @method static Builder|User whereNewEmail( $value )
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereCreatedAt( $value )
 * @method static Builder|User whereEmail( $value )
 * @method static Builder|User whereEmailVerifiedAt( $value )
 * @method static Builder|User whereId( $value )
 * @method static Builder|User whereName( $value )
 * @method static Builder|User wherePassword( $value )
 * @method static Builder|User whereRememberToken( $value )
 * @method static Builder|User whereType( $value )
 * @method static Builder|User whereUpdatedAt( $value )
 * @mixin Eloquent
 */
class User extends Authenticatable
{
    use Notifiable, Base, Actionable;

    public static string $administrator = 'administrator';
    public static string $editor = 'editor';
    public static string $provider = 'provider';
    public static string $user = 'user';


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'new_email',
        'password',
        'type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();
        // TODO: CHECK EMAIL VERIFICATION PROCESS NOT RUNNING
        static::updating( function ( User $user ) {
            if ( in_array( 'new_email', $user->getChanges() ) ) {
                $user->email_verified_at = null;
                $user->sendEmailVerificationNotification();
            }
        } );
    }

    /**
     * Get the email address that should be used for verification.
     *
     * @return string
     */
    public function getEmailForVerification(): string
    {
        return $this->new_email;
    }

    /**
     * Get Projects managed by this User.
     *
     * @return BelongsToMany
     */
    public function projects(): BelongsToMany
    {
        return $this->belongsToMany( Project::class, 'users_to_projects', 'user_id', 'project_id' )->using( Management::class )->withTimestamps();
    }

    /**
     * Get Providers managed by this User..
     *
     * @return BelongsToMany
     */
    public function providers(): BelongsToMany
    {
        return $this->belongsToMany( Provider::class, 'users_to_providers', 'user_id', 'provider_id' )->withPivot( 'status' )->withTimestamps()->as( 'management' );
    }

    /**
     * Check if User is Administrator.
     *
     * @return bool
     */
    public function isAdministrator(): bool
    {
        return $this->type == self::$administrator;
    }

    /**
     * Check if User is Editor.
     *
     * @return bool
     */
    public function isEditor(): bool
    {
        return $this->type == self::$editor;
    }

    /**
     * Check if User is standard User.
     *
     * @return bool
     */
    public function isUser(): bool
    {
        return $this->type == self::$user;
    }

    /**
     * Check if User is standard User.
     *
     * @return bool
     */
    public function isProvider(): bool
    {
        return $this->type == self::$provider;
    }


    /**
     * Check if User can Administer.
     *
     * @return bool
     */
    public function canAdminister(): bool
    {
        return $this->isAdministrator();
    }

    /**
     * Check if User can Edit Content.
     *
     * @return bool
     */
    public function canEdit(): bool
    {
        return $this->isEditor() || $this->isAdministrator();
    }


    /**
     * Generate a random password string.
     *
     * @return string
     */
    public static function generatePassword(): string
    {
        return bcrypt( Str::random( 35 ) );
    }
}
