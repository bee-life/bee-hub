<?php

namespace App\Models\MetaData;

use App\Exceptions\ModelNotFoundByUidException;
use App\Models\Logs\Data;
use App\Models\Logs\Data\DataId;
use App\Models\Logs\Data\DataModel;
use App\Models\Logs\Log;
use App\Models\Traits\Base;
use App\Models\Traits\FeaturedImage;
use App\Models\Traits\Icon;
use App\Models\Traits\Linkable;
use App\Models\Traits\Sluggable;
use App\Models\Traits\Sources;
use App\Models\Traits\Timestamps;
use App\Models\Traits\Uid;
use App\Models\Traits\Visibility;
use Carbon\Carbon;
use DB;
use Debugbar;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Laravel\Nova\Actions\Actionable;
use Laravel\Nova\Actions\ActionEvent;
use Str;
use function App\Models\Logs\str_contains;
use function App\Models\Logs\str_ends_with;
use function collect;

/**
 * App\Models\Logs\Descriptor
 *
 * @property int                           $id
 * @property string                        $table_name
 * @property string                        $name
 * @property string                        $slug
 * @property string                        $uid
 * @property string                        $description
 * @property string                        $since
 * @property int                           $deprecated
 * @property int|null                      $category_id
 * @property string|null                   $featured_image
 * @property string|null                   $icon
 * @property int                           $public
 * @property Carbon|null                   $created_at
 * @property Carbon|null                   $updated_at
 * @property-read DescriptorCategory|null  $category
 * @property-read int|null                 $projects_count
 * @property-read Collection|Data[]        $dataPivot
 * @property-read int|null                 $data_pivot_count
 * @property-read Collection|Log[]         $logs
 * @property-read int|null                 $logs_count
 * @property-read Collection|DataModel[]   $data
 * @property-read Collection|Project[]     $projects
 * @property-read Collection|ActionEvent[] $actions
 * @property-read int|null                 $actions_count
 * @method static Builder|Descriptor findSimilarSlugs( string $attribute, array $config, string $slug )
 * @method static Builder|Descriptor private ()
 * @method static Builder|Descriptor public ( $public = true )
 * @method static Builder|Descriptor withUniqueSlugConstraints( \Illuminate\Database\Eloquent\Model $model, string $attribute, array $config, string $slug )
 * @method static Builder|Descriptor newModelQuery()
 * @method static Builder|Descriptor newQuery()
 * @method static Builder|Descriptor query()
 * @method static Builder|Descriptor whereDeprecated( $value )
 * @method static Builder|Descriptor whereDescription( $value )
 * @method static Builder|Descriptor whereId( $value )
 * @method static Builder|Descriptor whereName( $value )
 * @method static Builder|Descriptor whereSince( $value )
 * @method static Builder|Descriptor whereTableName( $value )
 * @method static Builder|Descriptor whereCategoryId( $value )
 * @method static Builder|Descriptor whereCreatedAt( $value )
 * @method static Builder|Descriptor whereFeaturedImage( $value )
 * @method static Builder|Descriptor whereIcon( $value )
 * @method static Builder|Descriptor wherePublic( $value )
 * @method static Builder|Descriptor whereSlug( $value )
 * @method static Builder|Descriptor whereUpdatedAt( $value )
 * @method static Builder|Descriptor whereIn( $column, $values )
 * @method static Builder|Descriptor whereUid( $value )
 * @method static Collection|Descriptor[] get()
 * @mixin Eloquent
 */
class Descriptor extends Model
{
    use Base, Sources, Linkable, Sluggable, Uid, FeaturedImage, Icon, Timestamps, Visibility, Actionable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'descriptors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'table_name',
        'name',
        'slug',
        'description',
        'since',
        'public',
        'deprecated',
        'category_id',
        'featured_image',
        'icon',
        'uid',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'table_name',
        'public',
        'deprecated',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'created_at', 'updated_at' ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The key that is used to cache Uid mapping.
     *
     * @var string
     */
    protected static string $uidCacheKey = 'uid_descriptors';


    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * Return base route to Model
     *
     * @return string
     */
    public function getBaseRoute(): string
    {
        return 'data/' . $this->slug;
    }


    /**
     * Get all Connectors that are using this Descriptor.
     *
     * @return BelongsToMany
     */
    public function projects(): BelongsToMany
    {
        return $this->belongsToMany( Project::class, 'project_to_descriptor', 'descriptor_id', 'project_id' );
    }

    /**
     * Get all related data.
     *
     * @return MorphToMany
     */
    public function data(): MorphToMany
    {
        return $this->morphedByMany( static::getDataModel( $this->table_name ), 'data', 'log_to_data', 'descriptor_id' )
                    ->using( Data::class )->withPivot( 'log_id', 'origin_id', 'origin_type' );
    }

    /**
     * Get all related data.
     *
     * @return HasMany
     */
    public function dataPivot(): HasMany
    {
        return $this->hasMany( Data::class, 'descriptor_id' );
    }

    /**
     * Get all logs related to datatype.
     *
     * @return BelongsToMany
     */
    public function logs(): BelongsToMany
    {
        return $this->belongsToMany( Log::class, 'log_to_data', 'descriptor_id', 'log_id' );
    }

    /**
     * Get all logs related to datatype.
     *
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo( DescriptorCategory::class, 'category_id' );
    }

    /**
     * Check if the Descriptor is related to a Category
     *
     * @return bool
     */
    public function hasCategory(): bool
    {
        return isset( $this->category_id );
    }


    /**
     * Get related logs based on provided year.
     *
     * @param int|null $year
     * @param bool     $bLoadAllDescriptors
     *
     * @return Collection|Log[]
     * @throws ModelNotFoundByUidException
     */
    public function getLogs( int $year = null, bool $bLoadAllDescriptors = false ): Collection|array
    {
        Debugbar::startMeasure( 'Logs_load', 'Load Descriptor Log Data: ' . $this->name );
        if ( $year == 0 ) {
            // Get Latest, could be single or multiple at the same time, also it could have different times so we have to check by external id (or its ID)
            // TODO: Optimize
            $logDataIds = DataId::whereIn( 'project_id', $this->projects->pluck( 'id' ) )->get();
            $logs       = collect();

            // Rather use separate queries for each event, then query them by ID and load related data.
            // In the end its much faster, uses less memory and is more clear.
            foreach ( $logDataIds as $eventId ) {
                $logs[] = DB::table( 'logs' )->join( 'log_to_data', function ( $q ) use ( $eventId ) {
                    $q->on( 'logs.id', '=', 'log_to_data.log_id' )
                      ->where( 'log_to_data.descriptor_id', self::getIdFromUid( 'external-id' ) )
                      ->where( 'log_to_data.data_id', $eventId->id )
                      ->where( 'log_to_data.data_type', 'id' );
                } )->orderByDesc( 'logs.date' )->orderByDesc( 'logs.time' )->first( 'logs.id' );
            }

            $logs = Log::whereIn( 'id', $logs->pluck( 'id' ) )->with( [
                'project',
                'descriptors',
                'data' => function ( $q ) use ( $bLoadAllDescriptors ) {
                    $q->when( ! $bLoadAllDescriptors, function ( $q ) {
                        return $q->where( 'descriptor_id', $this->id )->with( [ 'data' ] );
                    } );
                    $q->when( $bLoadAllDescriptors, function ( $q ) {
                        return $q->with( [ 'data' ] );
                    } );
                }
            ] )->when( ! $bLoadAllDescriptors, function ( $q ) {
                return $q->with( [ 'locations' ] );
            } )->get();

            // Works fine with \Staudenmeir\EloquentEagerLimit\HasEagerLimit but takes too long.
            /*
             $logs = EventDataId::whereIn( 'connector_id', $this->connectors->pluck( 'id' ) )->with( [
                 'logs' => function ( $q ) use ( $bLoadAllDatatypes ) {
                     $q->whereHas( 'data', function ( $q ) use ( $bLoadAllDatatypes ) {
                         $q->where( 'type_id', $this->id );
                     } )->with( [
                         'data' => function ( $q ) use ( $bLoadAllDatatypes ) {
                             $q->when( ! $bLoadAllDatatypes, function ( $q ) {
                                 return $q->where( 'type_id', $this->id )->with( [ 'data' ] );
                             } );
                             $q->when( $bLoadAllDatatypes, function ( $q ) {
                                 return $q->with( [ 'data' ] );
                             } );
                         }
                     ] )->orderByDesc( 'date' )->orderByDesc( 'time' )->limit( 1 );
                 }
             ] )->get()->flatMap->logs;*/
        } else {
            $logs = $this->logs()->when( isset( $year ), function ( Builder $q ) use ( $year, $bLoadAllDescriptors ) {
                $q->where( 'year', $year );

                return $q->whereIn( 'project_id', $this->projects->pluck( 'id' ) )
                         ->with( [
                             'data' => function ( $q ) use ( $bLoadAllDescriptors ) {
                                 $q->when( ! $bLoadAllDescriptors, function ( $q ) {
                                     return $q->where( 'descriptor_id', $this->id )->with( [ 'data' ] );
                                 } );
                                 $q->when( $bLoadAllDescriptors, function ( $q ) {
                                     return $q->with( [ 'data' ] );
                                 } );
                             },
                             'project',
                         ] )->when( ! $bLoadAllDescriptors, function ( $q ) {
                        return $q->with( [ 'locations' ] );
                    } );
            } )->get();
        }
        Debugbar::stopMeasure( 'Logs_load' );

        return $logs;
    }

    /**
     * Count related Data.
     *
     * @return int
     */
    public function dataCount(): int
    {
        return $this->dataPivot()->count();
    }

    /**
     * Count Unique Data points.
     *
     * @return int
     */
    public function dataCountUnique(): int
    {
        return $this->dataPivot()->select( [ 'descriptor_id', 'data_type', 'data_id' ] )->distinct()->get()->count();
    }


    /**
     * Does the Descriptor represent ranged data (min/max, from/to, etc.).
     *
     * @return bool
     */
    public function isRange(): bool
    {
        return str_ends_with( $this->table_name, 'ranges' );
    }

    /**
     * Get Log Data Model based on given database table.
     *
     * @param string $table
     *
     * @return string
     */
    public static function getDataModel( string $table ): string
    {
        if ( str_contains( $table, '_reference_' ) ) {
            return 'App\Models\Logs\Reference\\' . Str::studly( Str::singular( substr( $table, 4 ) ) );
        } else {
            return 'App\Models\Logs\Data\\' . Str::studly( Str::singular( substr( $table, 4 ) ) );
        }
    }

}
