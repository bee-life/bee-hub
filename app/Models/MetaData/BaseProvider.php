<?php

namespace App\Models\MetaData;

use App\Models\Traits\Activity;
use App\Models\Traits\Base;
use App\Models\Traits\Email;
use App\Models\Traits\FeaturedImage;
use App\Models\Traits\Linkable;
use App\Models\Traits\Manageable;
use App\Models\Traits\Owner;
use App\Models\Traits\Phone;
use App\Models\Traits\ProjectType;
use App\Models\Traits\Sluggable;
use App\Models\Traits\Timestamps;
use App\Models\Traits\Uid;
use App\Models\Traits\Visibility;
use App\Models\Traits\Website;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Nova\Actions\Actionable;

/**
 * App\Models\Provider\BaseProvider class.
 *
 * @property int    $id
 * @property string $name
 * @property string $slug
 * @property string $uid
 * @property string $featured_image
 * @property string $description
 */
abstract class BaseProvider extends Model
{
    use Base, FeaturedImage, Linkable, Activity, Actionable, Timestamps, Notifiable, Sluggable, Uid, Manageable, SoftDeletes, Visibility, Owner;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'featured_image',
        'active',
        'visibility',
        'uid',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'active',
        'public',
        'uid',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get all contact data related to this Provider.
     *
     * @return MorphMany
     */
    public function contacts(): MorphMany
    {
        return $this->morphMany( Contact::class, 'provider' );
    }
}
