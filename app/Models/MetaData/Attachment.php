<?php

namespace App\Models\MetaData;

use App\Models\Traits\Base;
use App\Models\Traits\Sluggable;
use App\Models\Traits\Timestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Attachment extends Model
{
    use Base, HasFactory, Timestamps, Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'attachments';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'created_at', 'updated_at' ];

    /**
     * The attributes that are mass assignable.@abstract
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'slug',
        'mimetype',
        'attached_id',
        'attached_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get related Model.
     *
     * @return MorphTo
     */
    public function attached_to(): MorphTo
    {
        return $this->morphTo();
    }
}
