<?php namespace App\Models\MetaData;

use App\Models\Traits\Base;
use App\Models\Traits\Owner;
use App\Models\Traits\Sluggable;
use App\Models\Traits\Timestamps;
use App\Models\Traits\Uid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dataset extends Model
{
    use Base, HasFactory, Timestamps, SoftDeletes, Sluggable, Uid, Owner;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'datasets';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'uid',
        'description',
        'featured_image',
        'active',
        'visibility',
        'type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


}
