<?php

namespace App\Models\MetaData;

use App\Models\Traits\Base;
use App\Models\Traits\Timestamps;
use App\Models\Traits\Uid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Licence extends Model
{
    use Base, HasFactory, Timestamps, SoftDeletes, Uid;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'licences';

    /**
     * The key that is used to cache Uid mapping.
     *
     * @var string
     */
    protected static string $uidCacheKey = 'uid_licence';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'uid',
        'url',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get related Datasets.
     *
     * @return HasMany
     */
    public function datasets(): HasMany
    {
        return $this->hasMany('datasets', 'licence_id', 'id');
    }

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
