<?php

namespace App\Models\MetaData;

use App\Models\AuditLog;
use App\Models\Locations\Post;
use App\Models\Traits\Base;
use App\Models\Traits\BelongsToPost;
use App\Models\Traits\Email;
use App\Models\Traits\FeaturedImage;
use App\Models\Traits\Linkable;
use App\Models\Traits\Manageable;
use App\Models\Traits\Phone;
use App\Models\Traits\Sluggable;
use App\Models\Traits\Timestamps;
use App\Models\Traits\Uid;
use App\Models\Traits\Visibility;
use App\Models\Traits\Website;
use App\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Laravel\Nova\Actions\Actionable;
use Laravel\Nova\Actions\ActionEvent;

/**
 * App\Models\Providers
 *
 * @property int                           $id
 * @property string                        $full_name
 * @property string                        $slug
 * @property string                        $uid
 * @property string|null                   $name
 * @property string|null                   $address
 * @property string|null                   $registry_number
 * @property string|null                   $description
 * @property string|null                   $featured_image
 * @property string|null                   $website
 * @property string|null                   $phone
 * @property string|null                   $email
 * @property bool                          $public
 * @property int|null                      $post_id
 * @property Carbon|null                   $created_at
 * @property Carbon|null                   $updated_at
 * @property string|null                   $type
 * @property Carbon|null                   $deleted_at
 * @property-read Collection|ActionEvent[] $actions
 * @property-read int|null                 $actions_count
 * @property-read Collection|Project[]     $projects
 * @property-read int|null                 $projects_count
 * @property-read Post|null                $post
 * @property-read Collection|AuditLog[]    $auditLogs
 * @property-read int|null                 $audit_logs_count
 * @property-read Collection|User[]        $users
 * @property-read int|null                 $users_count
 * @method static Builder|Provider findSimilarSlugs( string $attribute, array $config, string $slug )
 * @method static Builder|Provider onlyTrashed()
 * @method static Builder|Provider private ()
 * @method static Builder|Provider public ( $public = true )
 * @method static Builder|Provider whereDeletedAt( $value )
 * @method static Builder|Provider whereDescription( $value )
 * @method static Builder|Provider whereType( $value )
 * @method static Builder|Provider withTrashed()
 * @method static Builder|Provider withUniqueSlugConstraints( Model $model, string $attribute, array $config, string $slug )
 * @method static Builder|Provider withoutTrashed()
 * @method static Builder|Provider newModelQuery()
 * @method static Builder|Provider newQuery()
 * @method static Builder|Provider query()
 * @method static Builder|Provider whereAbout( $value )
 * @method static Builder|Provider whereAddress( $value )
 * @method static Builder|Provider whereCreatedAt( $value )
 * @method static Builder|Provider whereEmail( $value )
 * @method static Builder|Provider whereFeaturedImage( $value )
 * @method static Builder|Provider whereFullName( $value )
 * @method static Builder|Provider whereId( $value )
 * @method static Builder|Provider whereName( $value )
 * @method static Builder|Provider wherePhone( $value )
 * @method static Builder|Provider wherePostId( $value )
 * @method static Builder|Provider wherePublic( $value )
 * @method static Builder|Provider whereRegistryNumber( $value )
 * @method static Builder|Provider whereSlug( $value )
 * @method static Builder|Provider whereUpdatedAt( $value )
 * @method static Builder|Provider whereWebsite( $value )
 * @method static Builder|Provider whereUid( $value )
 * @mixin Eloquent
 */
class Provider extends Model
{
    use Base, FeaturedImage, Linkable, Actionable, Timestamps, Email, Phone, Website, BelongsToPost, Sluggable, Uid, Manageable, SoftDeletes, Visibility;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'providers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',
        'name',
        'address',
        'registry_number',
        'description',
        'featured_image',
        'status',
        'uid',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'active',
        'public',
        'uid',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'last_synced_at' => 'datetime',
        'active'         => 'boolean',
        'public'         => 'boolean',
    ];

    /**
     * The key that is used to cache Uid mapping.
     *
     * @var string
     */
    protected static string $uidCacheKey = 'uid_providers';


    /**
     * The Administration Route associated with the Model.
     *
     * @var string
     */
    static string $resourceName = 'providers';


    /**
     * Get Connectors managed by this Owner.
     *
     * @return BelongsToMany
     */
    public function projects(): BelongsToMany
    {
        return $this->belongsToMany( Project::class, 'project_to_provider', 'provider_id', 'project_id' );
    }

    /**
     * Get User that managed this Owner.
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany( User::class, 'users_to_providers', 'provider_id', 'user_id' )->withPivot( 'status' )->withTimestamps()->as( 'management' );
    }

    /**
     * Setup sluggable fields.
     *
     * @return string[][]
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'full_name'
            ],
            'uid'  => [
                'source' => 'full_name'
            ],
        ];
    }

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->full_name;
    }

    /**
     * Return base route to Model
     *
     * @return string
     */
    public function getBaseRoute(): string
    {
        return 'providers/' . $this->slug;
    }
}
