<?php

namespace App\Models\MetaData;

use App\Models\Traits\Base;
use App\Models\Traits\FeaturedImage;
use App\Models\Traits\Icon;
use App\Models\Traits\Linkable;
use App\Models\Traits\Sluggable;
use App\Models\Traits\Timestamps;
use App\Models\Traits\Uid;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Nova\Actions\Actionable;
use Laravel\Nova\Actions\ActionEvent;

/**
 * App\Models\EventData\EventDataTypeCategory
 *
 * @property int                                  $id
 * @property string                               $name
 * @property string                               $slug
 * @property string                               $uid
 * @property string|null                          $description
 * @property int|null                             $parent_id
 * @property string|null                          $featured_image
 * @property string|null                          $icon
 * @property Carbon|null                          $created_at
 * @property Carbon|null                          $updated_at
 * @property-read Collection|DescriptorCategory[] $children
 * @property-read int|null                        $children_count
 * @property-read Collection|Descriptor[]         $descriptors
 * @property-read int|null                        $descriptors_count
 * @property-read DescriptorCategory|null         $parent
 * @property-read Collection|ActionEvent[]        $actions
 * @property-read int|null                        $actions_count
 * @method static Builder|DescriptorCategory findSimilarSlugs( string $attribute, array $config, string $slug )
 * @method static Builder|DescriptorCategory withUniqueSlugConstraints( Model $model, string $attribute, array $config, string $slug )
 * @method static Builder|DescriptorCategory newModelQuery()
 * @method static Builder|DescriptorCategory newQuery()
 * @method static Builder|DescriptorCategory query()
 * @method static Builder|DescriptorCategory whereCreatedAt( $value )
 * @method static Builder|DescriptorCategory whereDescription( $value )
 * @method static Builder|DescriptorCategory whereFeaturedImage( $value )
 * @method static Builder|DescriptorCategory whereIcon( $value )
 * @method static Builder|DescriptorCategory whereId( $value )
 * @method static Builder|DescriptorCategory whereName( $value )
 * @method static Builder|DescriptorCategory whereParentId( $value )
 * @method static Builder|DescriptorCategory whereSlug( $value )
 * @method static Builder|DescriptorCategory whereUpdatedAt( $value )
 * @method static Builder|DescriptorCategory whereUid( $value )
 * @mixin Eloquent
 */
class DescriptorCategory extends Model
{
    use Base, Timestamps, FeaturedImage, Icon, Sluggable, Uid, Linkable, Actionable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'descriptor_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'parent_id',
        'featured_image',
        'icon',
        'uid',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uid',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'created_at', 'updated_at' ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The key that is used to cache Uid mapping.
     *
     * @var string
     */
    protected static string $uidCacheKey = 'uid_descriptor_cat';

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * Return base route to Model
     *
     * @return string
     */
    public function getBaseRoute(): string
    {
        return 'data-categories/' . $this->slug;
    }


    /**
     * Get all Datatypes that belong to this Category.
     *
     * @return HasMany
     */
    public function descriptors(): HasMany
    {
        return $this->hasMany( Descriptor::class, 'category_id' );
    }

    /**
     * Get the Category parent.
     *
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo( DescriptorCategory::class, 'parent_id' );
    }

    /**
     * Get all child Categories.
     *
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany( DescriptorCategory::class, 'parent_id' );
    }

    /**
     * Check if the Model has a parent.
     *
     * @return bool
     */
    public function hasParent(): bool
    {
        return ! empty( $this->parent_id );
    }

    /**
     * Check if the Model has any children.
     *
     * @return bool
     */
    public function hasChildren(): bool
    {
        return $this->children->isNotEmpty();
    }

    /**
     * Check if the Model has any related Descriptors.
     *
     * @return bool
     */
    public function hasDescriptors(): bool
    {
        return $this->descriptors->isNotEmpty();
    }
}
