<?php

namespace App\Models\MetaData;

use App\Enums\ContactTypes;
use App\Exceptions\Interactions\InvalidValueException;
use App\Models\Traits\Base;
use App\Models\Traits\Timestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Contact extends Model
{
    use Base, HasFactory, Timestamps;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'provider_contacts';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'created_at', 'updated_at' ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value',
        'type',
        'related_id',
        'related_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Make sure the set type is always valid.
     *
     * @throws InvalidValueException
     */
    public function setTypeAttribute( $newValue )
    {
        if ( ContactTypes::isValid( $newValue ) ) {
            $this->attributes['type'] = $newValue;
        } else {
            throw  new InvalidValueException();
        }
    }

    /**
     * Get related Model.
     *
     * @return MorphTo
     */
    public function related(): MorphTo
    {
        return $this->morphTo();
    }
}
