<?php

namespace App\Models\MetaData;

use App\Models\AuditLog;
use App\Models\Logs\Data\DataAggregate;
use App\Models\Logs\Data\DataId;
use App\Models\Logs\Log;
use App\Models\Management;
use App\Models\Traits\ProjectType;
use App\Models\User;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Laravel\Nova\Actions\ActionEvent;


/**
 * App\Models\Project
 *
 * @property int                                                        $id
 * @property string                                                     $name
 * @property string                                                     $acronym
 * @property string                                                     $slug
 * @property string                                                     $uid
 * @property string                                                     $type
 * @property string|null                                                $description
 * @property string|null                                                $website
 * @property string|null                                                $phone
 * @property string|null                                                $email
 * @property string|null                                                $featured_image
 * @property string|null                                                $class_name
 * @property bool                                                       $active
 * @property bool                                                       $public
 * @property Carbon|null                                                $last_synced_at
 * @property string|null                                                $sync_period
 * @property int                                                        $historic_sync
 * @property Carbon|null                                                $created_at
 * @property Carbon|null                                                $updated_at
 * @property Carbon|null                                                $started_at
 * @property Carbon|null                                                $ended_at
 * @property string|null                                                $sharing_type
 * @property Carbon|null                                                $deleted_at
 * @property-read Collection|ActionEvent[]                              $actions
 * @property-read int|null                                              $actions_count
 * @property-read Collection|DataAggregate[]                            $aggregates
 * @property-read int|null                                              $aggregates_count
 * @property-read Collection|Descriptor[]                               $descriptors
 * @property-read int|null                                              $descriptors_count
 * @property-read Collection|Log[]                                      $logs
 * @property-read int|null                                              $logs_count
 * @property-read string                                                $attribution
 * @property-read Collection|Provider[]                                 $providers
 * @property-read int|null                                              $providers_count
 * @property-read Collection|AuditLog[]                                 $auditLogs
 * @property-read int|null                                              $audit_logs_count
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null                                              $notifications_count
 * @property-read Collection|DataId[]                                   $relatedDataIds
 * @property-read int|null                                              $related_data_ids_count
 * @property-read Collection|User[]                                     $users
 * @property-read int|null                                              $users_count
 * @method static Builder|Project active( bool $active = true )
 * @method static Builder|Project findSimilarSlugs( string $attribute, array $config, string $slug )
 * @method static Builder|Project inactive()
 * @method static Builder|Project onlyTrashed()
 * @method static Builder|Project private ()
 * @method static Builder|Project public ( $public = true )
 * @method static Builder|Project whereDeletedAt( $value )
 * @method static Builder|Project whereEndedAt( $value )
 * @method static Builder|Project whereSharingType( $value )
 * @method static Builder|Project whereStartedAt( $value )
 * @method static Builder|Project withTrashed()
 * @method static Builder|Project withUniqueSlugConstraints( Model $model, string $attribute, array $config, string $slug )
 * @method static Builder|Project withoutTrashed()
 * @method static Builder|Project newModelQuery()
 * @method static Builder|Project newQuery()
 * @method static Builder|Project query()
 * @method static Builder|Project whereAcronym( $value )
 * @method static Builder|Project whereActive( $value )
 * @method static Builder|Project whereClassName( $value )
 * @method static Builder|Project whereCreatedAt( $value )
 * @method static Builder|Project whereDescription( $value )
 * @method static Builder|Project whereEmail( $value )
 * @method static Builder|Project whereFeaturedImage( $value )
 * @method static Builder|Project whereHistoricSync( $value )
 * @method static Builder|Project whereId( $value )
 * @method static Builder|Project whereLastSyncedAt( $value )
 * @method static Builder|Project whereName( $value )
 * @method static Builder|Project wherePhone( $value )
 * @method static Builder|Project wherePublic( $value )
 * @method static Builder|Project whereSlug( $value )
 * @method static Builder|Project whereSyncPeriod( $value )
 * @method static Builder|Project whereType( $value )
 * @method static Builder|Project whereUpdatedAt( $value )
 * @method static Builder|Project whereWebsite( $value )
 * @method static Builder|Project whereUid( $value )
 * @mixin Eloquent
 */
class Project extends BaseProvider
{
    use ProjectType;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'display_name',
        'description',
        'started_at',
        'ended_at',
        'featured_image',
        'type',
        'class_name',
        'active',
        'public',
        'last_synced_at',
        'uid',
        'slug',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'type',
        'class_name',
        'active',
        'public',
        'sync_period',
        'historic_sync',
        'last_synced_at',
        'uid',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'last_synced_at' => 'datetime',
        'started_at'     => 'datetime',
        'ended_at'       => 'datetime',
        'active'         => 'boolean',
        'public'         => 'boolean',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [ 'attribution' ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The key that is used to cache Uid mapping.
     *
     * @var string
     */
    protected static string $uidCacheKey = 'uid_projects';

    /**
     * The Administration Route associated with the Model.
     *
     * @var string
     */
    static string $resourceName = 'projects';

    /**
     * Get all Logs created by this Connector.
     *
     * @return HasMany
     */
    public function logs(): HasMany
    {
        return $this->hasMany( Log::class, 'project_id' );
    }

    /**
     * Get number of Logs created by this Connector.
     *
     * @return int
     */
    public function logCount(): int
    {
        if ( $this->relationLoaded( 'logs' ) ) {
            return $this->logs->count();
        } else {
            return $this->logs()->count();
        }
    }

    /**
     * Get number of Descriptors created by this Connector.
     *
     * @return int
     */
    public function descriptorCount(): int
    {
        if ( $this->relationLoaded( 'descriptors' ) ) {
            return $this->descriptors->count();
        } else {
            return $this->descriptors()->count();
        }
    }

    /**
     * Get all datatypes used by Connector.
     */
    public function descriptors(): BelongsToMany
    {
        return $this->belongsToMany( Descriptor::class, 'project_to_descriptor', 'project_id', 'descriptor_id' )->orderBy( 'name' );
    }

    /**
     * The connectors that belong to the model.
     *
     * @deprecated
     */
    public function aggregates(): BelongsToMany
    {
        return $this->belongsToMany( DataAggregate::class, 'log_data_aggregate_to_project', 'project_id', 'aggregate_id' );
    }

    /**
     * Get Owners that managed this Project.
     *
     * @return BelongsToMany
     */
    public function providers(): BelongsToMany
    {
        return $this->belongsToMany( Provider::class, 'project_to_provider', 'project_id', 'provider_id' );
    }

    /**
     * Get all related data Ids.
     *
     * @return HasMany
     */
    public function relatedDataIds(): HasMany
    {
        return $this->hasMany( DataId::class, 'project_id', 'id' );
    }

    /**
     * Get User that managed this Project.
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany( User::class, 'users_to_projects', 'project_id', 'user_id' )->using( Management::class )->withTimestamps();
    }

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * Return base route to Model
     *
     * @return string
     */
    public function getBaseRoute(): string
    {
        return 'projects/' . $this->slug;
    }

    /**
     * Get Attribution link as an Attribute.
     * Used in array export.
     *
     * @return string
     */
    public function getAttributionAttribute(): string
    {
        if ( $this->exists ) {
            return $this->getLink( true );
        } else {
            return '';
        }
    }
}
