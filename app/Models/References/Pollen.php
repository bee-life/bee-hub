<?php namespace App\Models\References;

use App\Models\Traits\Base;
use App\Models\Traits\FeaturedImage;
use App\Models\Traits\Icon;
use App\Models\Traits\Timestamps;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\References\Pollen
 *
 * @property int         $id
 * @property string      $name
 * @property string      $type
 * @property string      $family
 * @property string      $code
 * @property string|null $description
 * @property string|null $featured_image
 * @property string|null $icon
 * @property string|null $p
 * @property string|null $q
 * @property string|null $vol
 * @property string|null $r
 * @property string|null $vol_r
 * @property int         $deprecated
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Pollen newModelQuery()
 * @method static Builder|Pollen newQuery()
 * @method static Builder|Pollen query()
 * @method static Builder|Pollen whereCode( $value )
 * @method static Builder|Pollen whereCreatedAt( $value )
 * @method static Builder|Pollen whereDeprecated( $value )
 * @method static Builder|Pollen whereDescription( $value )
 * @method static Builder|Pollen whereFamily( $value )
 * @method static Builder|Pollen whereFeaturedImage( $value )
 * @method static Builder|Pollen whereIcon( $value )
 * @method static Builder|Pollen whereId( $value )
 * @method static Builder|Pollen whereName( $value )
 * @method static Builder|Pollen whereP( $value )
 * @method static Builder|Pollen whereQ( $value )
 * @method static Builder|Pollen whereR( $value )
 * @method static Builder|Pollen whereType( $value )
 * @method static Builder|Pollen whereUpdatedAt( $value )
 * @method static Builder|Pollen whereVol( $value )
 * @method static Builder|Pollen whereVolR( $value )
 * @mixin Eloquent
 */
class Pollen extends Reference
{
    use Base, FeaturedImage, Icon, Timestamps;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reference_pollen';


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to date.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];


    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
