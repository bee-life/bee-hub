<?php namespace App\Models\References;

use App\Models\Traits\Base;
use App\Models\Traits\FeaturedImage;
use App\Models\Traits\Icon;
use App\Models\Traits\Timestamps;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\References\PesticideType
 *
 * @property int                         $id
 * @property string                      $name
 * @property string                      $abbreviation
 * @property string|null                 $description
 * @property string|null                 $featured_image
 * @property string|null                 $icon
 * @property int                         $deprecated
 * @property Carbon|null                 $created_at
 * @property Carbon|null                 $updated_at
 * @property-read Collection|Pesticide[] $pesticides
 * @property-read int|null               $pesticides_count
 * @method static Builder|PesticideType newModelQuery()
 * @method static Builder|PesticideType newQuery()
 * @method static Builder|PesticideType query()
 * @method static Builder|PesticideType whereAbbreviation( $value )
 * @method static Builder|PesticideType whereCreatedAt( $value )
 * @method static Builder|PesticideType whereDeprecated( $value )
 * @method static Builder|PesticideType whereDescription( $value )
 * @method static Builder|PesticideType whereFeaturedImage( $value )
 * @method static Builder|PesticideType whereIcon( $value )
 * @method static Builder|PesticideType whereId( $value )
 * @method static Builder|PesticideType whereName( $value )
 * @method static Builder|PesticideType whereUpdatedAt( $value )
 * @mixin Eloquent
 */
class PesticideType extends Model
{
    use Base, FeaturedImage, Icon, Timestamps;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reference_pesticide_types';


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to date.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];


    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * Get a list of all pesticides of this type.
     *
     * @return HasMany
     */
    public function pesticides(): HasMany
    {
        return $this->hasMany( Pesticide::class, 'type_id' );
    }
}
