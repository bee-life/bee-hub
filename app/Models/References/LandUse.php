<?php namespace App\Models\References;

use App\Models\Traits\Base;
use App\Models\Traits\FeaturedImage;
use App\Models\Traits\Icon;
use App\Models\Traits\Timestamps;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\References\LandUse
 *
 * @property int         $id
 * @property string      $name
 * @property string      $group
 * @property int|null    $sigec_code
 * @property string|null $description
 * @property string|null $featured_image
 * @property string|null $icon
 * @property string|null $dim_min_res
 * @property string|null $dim_max_res
 * @property string|null $pyr_min_res
 * @property string|null $pyr_max_res
 * @property string|null $bos_min_res
 * @property string|null $bos_max_res
 * @property string      $source
 * @property int         $deprecated
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|LandUse newModelQuery()
 * @method static Builder|LandUse newQuery()
 * @method static Builder|LandUse query()
 * @method static Builder|LandUse whereBosMaxRes( $value )
 * @method static Builder|LandUse whereBosMinRes( $value )
 * @method static Builder|LandUse whereCreatedAt( $value )
 * @method static Builder|LandUse whereDeprecated( $value )
 * @method static Builder|LandUse whereDescription( $value )
 * @method static Builder|LandUse whereDimMaxRes( $value )
 * @method static Builder|LandUse whereDimMinRes( $value )
 * @method static Builder|LandUse whereFeaturedImage( $value )
 * @method static Builder|LandUse whereGroup( $value )
 * @method static Builder|LandUse whereIcon( $value )
 * @method static Builder|LandUse whereId( $value )
 * @method static Builder|LandUse whereName( $value )
 * @method static Builder|LandUse wherePyrMaxRes( $value )
 * @method static Builder|LandUse wherePyrMinRes( $value )
 * @method static Builder|LandUse whereSigecCode( $value )
 * @method static Builder|LandUse whereSource( $value )
 * @method static Builder|LandUse whereUpdatedAt( $value )
 * @mixin Eloquent
 */
class LandUse extends Reference
{
    use Base, FeaturedImage, Icon, Timestamps;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reference_land_usages';


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];


    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
