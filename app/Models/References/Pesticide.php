<?php namespace App\Models\References;

use App\Models\Logs\Reference\ReferenceLandUse;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\References\Pesticide
 *
 * @property int                                $id
 * @property string                             $substance
 * @property string                             $name
 * @property string|null                        $description
 * @property string|null                        $featured_image
 * @property string|null                        $icon
 * @property int                                $type_id
 * @property string|null                        $loq_wax
 * @property string|null                        $loq_beebread
 * @property string|null                        $loq_honey
 * @property string|null                        $loq_pollen
 * @property int                                $deprecated
 * @property Carbon|null                        $created_at
 * @property Carbon|null                        $updated_at
 * @property-read Collection|ReferenceLandUse[] $relatedData
 * @property-read int|null                      $related_data_count
 * @property-read PesticideType|null            $type
 * @method static Builder|Pesticide newModelQuery()
 * @method static Builder|Pesticide newQuery()
 * @method static Builder|Pesticide query()
 * @method static Builder|Pesticide whereCreatedAt( $value )
 * @method static Builder|Pesticide whereDeprecated( $value )
 * @method static Builder|Pesticide whereDescription( $value )
 * @method static Builder|Pesticide whereFeaturedImage( $value )
 * @method static Builder|Pesticide whereIcon( $value )
 * @method static Builder|Pesticide whereId( $value )
 * @method static Builder|Pesticide whereLoqBeebread( $value )
 * @method static Builder|Pesticide whereLoqHoney( $value )
 * @method static Builder|Pesticide whereLoqPollen( $value )
 * @method static Builder|Pesticide whereLoqWax( $value )
 * @method static Builder|Pesticide whereName( $value )
 * @method static Builder|Pesticide whereSubstance( $value )
 * @method static Builder|Pesticide whereTypeId( $value )
 * @method static Builder|Pesticide whereUpdatedAt( $value )
 * @mixin Eloquent
 */
class Pesticide extends Reference
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reference_pesticides';


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to date.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];


    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * Get the type of this Model.
     *
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo( PesticideType::class, 'type_id' );
    }

    /**
     * Get all related data Ids.
     *
     * @return HasMany
     */
    public function relatedData(): HasMany
    {
        return $this->hasMany( ReferenceLandUse::class, 'reference_id', 'id' );
    }
}
