<?php

namespace App\Models\References;

use App\Models\Logs\Reference\ReferenceModel;
use App\Models\Traits\Base;
use App\Models\Traits\FeaturedImage;
use App\Models\Traits\Icon;
use App\Models\Traits\Timestamps;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Reference\Reference
 *
 * @property int                            $id
 * @property string|null                    $name
 * @property-read Collection|ReferenceModel $relatedData
 * @method static Builder|Reference whereId( $value )
 * @method static Builder|Reference whereName( $value )
 * @mixin Eloquent
 */
abstract class Reference extends Model
{
    use Base, FeaturedImage, Icon, Timestamps;


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to date.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * Get all related data Ids.
     *
     * @return HasMany
     */
    public function relatedData(): HasMany
    {
        return $this->hasMany( ReferenceModel::class, 'reference_id', 'id' );
    }
}
