<?php namespace App\Models\References;

use App\Models\Traits\Base;
use App\Models\Traits\FeaturedImage;
use App\Models\Traits\Icon;
use App\Models\Traits\Timestamps;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\References\CropPollinationDependency
 *
 * @property int              $id
 * @property int              $crop_species_id
 * @property string           $impact
 * @property string|null      $dependency_from
 * @property string|null      $dependency_to
 * @property Carbon|null      $created_at
 * @property Carbon|null      $updated_at
 * @property-read int|null    $related_data_count
 * @property-read CropSpecies $references
 * @method static Builder|CropPollinationDependency newModelQuery()
 * @method static Builder|CropPollinationDependency newQuery()
 * @method static Builder|CropPollinationDependency query()
 * @method static Builder|CropPollinationDependency whereCreatedAt( $value )
 * @method static Builder|CropPollinationDependency whereCropSpeciesId( $value )
 * @method static Builder|CropPollinationDependency whereDependencyFrom( $value )
 * @method static Builder|CropPollinationDependency whereDependencyTo( $value )
 * @method static Builder|CropPollinationDependency whereId( $value )
 * @method static Builder|CropPollinationDependency whereImpact( $value )
 * @method static Builder|CropPollinationDependency whereUpdatedAt( $value )
 * @mixin Eloquent
 */
class CropPollinationDependency extends Model
{
    use Base, FeaturedImage, Icon, Timestamps;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reference_crop_pollination_dependency';


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];


    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->id;
    }

    /**
     * Get all related data Ids.
     *
     * @return BelongsTo
     */
    public function references(): BelongsTo
    {
        return $this->belongsTo( CropSpecies::class, 'crop_species_id', 'id' );
    }
}
