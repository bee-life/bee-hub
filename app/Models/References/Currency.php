<?php namespace App\Models\References;

use App\Models\Locations\Country;
use App\Models\Traits\Base;
use App\Models\Traits\Timestamps;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\References\Currency
 *
 * @property int                       $id
 * @property string                    $name
 * @property string                    $code
 * @property string|null               $symbol
 * @property int                       $number
 * @property boolean                   $symbol_prefix
 * @property int|null                  $decimals
 * @property Carbon|null               $created_at
 * @property Carbon|null               $updated_at
 * @property-read Collection|Country[] $countries
 * @property-read int|null             $countries_count
 * @method static Builder|Currency whereSymbolPrefix( $value )
 * @method static Builder|Currency newModelQuery()
 * @method static Builder|Currency newQuery()
 * @method static Builder|Currency query()
 * @method static Builder|Currency whereCode( $value )
 * @method static Builder|Currency whereCreatedAt( $value )
 * @method static Builder|Currency whereDecimals( $value )
 * @method static Builder|Currency whereId( $value )
 * @method static Builder|Currency whereName( $value )
 * @method static Builder|Currency whereNumber( $value )
 * @method static Builder|Currency whereSymbol( $value )
 * @method static Builder|Currency whereUpdatedAt( $value )
 * @mixin Eloquent
 */
class Currency extends Reference
{
    use Base, Timestamps;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reference_currencies';


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];


    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * Get currency unit or symbol.
     *
     * @return string
     */
    public function getUnit(): string
    {
        return $this->symbol ?? $this->code;
    }


    /**
     * Get a list of countries with this currency.
     *
     * @return HasMany
     */
    public function countries(): HasMany
    {
        return $this->hasMany( Country::class, 'currency_id', 'id' );
    }
}
