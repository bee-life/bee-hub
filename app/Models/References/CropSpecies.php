<?php namespace App\Models\References;

use App\Models\Traits\Base;
use App\Models\Traits\FeaturedImage;
use App\Models\Traits\Icon;
use App\Models\Traits\Timestamps;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;

/**
 * App\Models\References\CropSpecies
 *
 * @property int                                 $id
 * @property string                              $name
 * @property string|null                         $description
 * @property string|null                         $featured_image
 * @property string|null                         $icon
 * @property string|null                         $species_name
 * @property string|null                         $gbif_id
 * @property int|null                            $parent_id
 * @property Carbon|null                         $created_at
 * @property Carbon|null                         $updated_at
 * @property-read CropPollinationDependency|null $dependency
 * @method static Builder|CropSpecies newModelQuery()
 * @method static Builder|CropSpecies newQuery()
 * @method static Builder|CropSpecies query()
 * @method static Builder|CropSpecies whereCreatedAt( $value )
 * @method static Builder|CropSpecies whereDescription( $value )
 * @method static Builder|CropSpecies whereFeaturedImage( $value )
 * @method static Builder|CropSpecies whereGbifId( $value )
 * @method static Builder|CropSpecies whereIcon( $value )
 * @method static Builder|CropSpecies whereId( $value )
 * @method static Builder|CropSpecies whereName( $value )
 * @method static Builder|CropSpecies whereParentId( $value )
 * @method static Builder|CropSpecies whereSpeciesName( $value )
 * @method static Builder|CropSpecies whereUpdatedAt( $value )
 * @mixin Eloquent
 */
class CropSpecies extends Reference
{
    use Base, FeaturedImage, Icon, Timestamps;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reference_crop_species';


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];


    /**
     * Print object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }


    /**
     * Get crop dependency for pollination value.
     *
     * @return HasOne
     */
    public function dependency(): HasOne
    {
        return $this->hasOne( CropPollinationDependency::class, 'crop_species_id' );
    }
}
