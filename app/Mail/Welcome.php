<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Welcome extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The token for password reset.
     *
     * @var string
     */
    public string $token;

    /**
     * The order instance.
     *
     * @var User
     */
    public User $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( User $user, string $token )
    {
        $this->token = $token;
        $this->user  = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->to( $this->user->email, $this->user->name )
                    ->subject( __( 'auth.emails.welcome.subject' ) )
                    ->markdown( 'emails.auth.welcome' );
    }
}
