<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;

class Editor
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle( Request $request, Closure $next )
    {
        return ( ! Auth::check() || ! Auth::user()->isEditor() ) ? redirect( '/' ) : $next( $request );
    }
}
