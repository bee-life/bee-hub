<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;

class Administrator
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle( Request $request, Closure $next ): mixed
    {
        return ( ! Auth::check() || ! Auth::user()->isAdministrator() ) ? redirect( '/' ) : $next( $request );
    }
}
