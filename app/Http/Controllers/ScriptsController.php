<?php

namespace App\Http\Controllers;

use App\Data\Aggregators\HiveSensorAverages;
use App\Jobs\AggregateDataItemJob;
use Artisan;
use Illuminate\View\View;

class ScriptsController extends Controller
{
    public function debug(): string
    {
        die();
        echo "Running Debug Mode<br>";
        //stop_logging();
        set_time_limit( 0 );


        return 'Complete';
    }

    public function aggregate(): View
    {
        echo "Aggregating data. See DebugBar for statistics.";
        //Artisan::call( 'data:aggregate' );

        (new AggregateDataItemJob(new HiveSensorAverages()))->handle();

        return view( 'debug', [ 'data' => nl2br( Artisan::output() ) ] );
    }

    public function live(): View
    {
        echo "Fetching live data. See DebugBar for statistics.";
        Artisan::call( 'data:live' );

        return view( 'debug', [ 'data' => nl2br( Artisan::output() ) ] );
    }

    public function historic(): View
    {
        echo "Fetching historic live data. See DebugBar for statistics.";
        Artisan::call( 'data:historic' );

        return view( 'debug', [ 'data' => nl2br( Artisan::output() ) ] );
    }

    public function cache(): View
    {
        echo "Creating Graph cache. See DebugBar for statistics.";

        Artisan::call( 'data:cache --queue' );

        return view( 'debug', [ 'data' => nl2br( Artisan::output() ) ] );
    }

    public function scripts(): View
    {
        set_time_limit( 0 );
        ini_set( 'memory_limit', '2048M' );
        stop_logging();

        Artisan::call( 'migrate --force' );
        echo nl2br( Artisan::output() );
        //Artisan::call( 'db:seed-data' );
        echo nl2br( Artisan::output() );
        Artisan::call( 'cache:clear' );
        echo nl2br( Artisan::output() );
        Artisan::call( 'view:cache' );
        echo nl2br( Artisan::output() );
        //Artisan::call( 'config:cache' );
        echo nl2br( Artisan::output() );
        //Artisan::call( 'route:cache' );
        echo nl2br( Artisan::output() );
        //Artisan::call( 'data:cache' );
        echo nl2br( Artisan::output() );
        return view( 'debug', [ 'data' => nl2br( Artisan::output() ) ] );
    }

}
