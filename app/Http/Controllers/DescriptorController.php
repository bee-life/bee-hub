<?php

namespace App\Http\Controllers;

use App\Models\MetaData\Descriptor;
use Illuminate\View\View;

class DescriptorController extends Controller
{
    /**
     * View index of all Descriptors.
     *
     * @return View
     */
    public function index(): View
    {
        return view( 'descriptors.index', [
            'models' => Descriptor::with( [ 'category' ] )->withCount( [
                'projects',
            ] )->public()->where( 'deprecated', false )->orderBy( 'name' )->get()
        ] );
    }

    /**
     * View a single Descriptor page.
     *
     * @param string $slug
     *
     * @return View
     */
    public function single( string $slug ): View
    {
        return view( 'descriptors.single', [
            'model' => Descriptor::with( [
                'projects',
                'category'
            ] )->public()->where( 'deprecated', false )->where( 'slug', $slug )->firstOrFail()
        ] );
    }
}
