<?php

namespace App\Http\Controllers;

use App\Models\MetaData\DescriptorCategory;
use Illuminate\View\View;

class DescriptorCategoryController extends Controller
{
    /**
     * View index of all Categories.
     *
     * @return View
     */
    public function index(): View
    {
        return view( 'descriptor-categories.index', [
            'models' => DescriptorCategory::with( [ 'descriptors' ] )->withCount( [
                'descriptors',
            ] )->whereNull( 'parent_id' )->orderBy( 'name' )->has( 'descriptors', '>', 0 )->orHas( 'children.descriptors', '>', 0 )->get()
        ] );
    }

    /**
     * View a single Category Page.
     *
     * @param string $slug
     *
     * @return View
     */
    public function single( string $slug ): View
    {
        return view( 'descriptor-categories.single', [
            'model' => DescriptorCategory::with( [
                'descriptors' => function ( $q ) {
                    $q->where( 'public', true );
                },
                'children'    => function ( $q ) {
                    $q->withCount( 'descriptors' )->has( 'descriptors', '>', 0 );
                }
            ] )->where( 'slug', $slug )->firstOrFail()
        ] );
    }
}
