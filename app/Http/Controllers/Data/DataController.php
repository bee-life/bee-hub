<?php

namespace App\Http\Controllers\Data;

use App\Models\Logs\Data\DataAggregate;
use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

abstract class DataController
{
    /**
     * The scale to be displayed on the map and within the legend.
     * Should return either null or one of these:
     *      - danger: a green to red scale where high values represent "dangerous" (red) area.
     *      - danger_inverted: a red to green scale where low values represent "dangerous" (red) area.
     *      - range: A simple range from one colour to another. Not necessary starts at zero.
     *      - range_zeroed: A range from one colour to another. Starts from zero.
     *      - range_diverging: A range from negative numbers to zero as the middle and then to positive numbers.
     *
     * @var string|null
     */
    public ?string $scale = null;


    /**
     * The Descriptor this Controller is currently handling.
     * Some controllers are able to handle different descriptors at a time.
     *
     * @var Descriptor
     */
    protected Descriptor $descriptor;

    /**
     * Minimum value within current dataset.
     *
     * @var mixed|null
     */
    protected $min = null;

    /**
     * Maximum value within current dataset.
     *
     * @var mixed|null
     */
    protected $max = null;


    /**
     * A list of filters, that have been requested with the Controllers default values, if they are missing..
     *
     * @var array<string, mixed>
     */
    public array $filters;

    public function __construct( Descriptor $descriptor, array $filters )
    {
        $this->descriptor = $descriptor;
        $this->filters    = $this->getFilters( $filters );
    }


    /**
     * Returns Legend data to be displayed by controller.
     * By default legend is not displayed (empty array).
     *
     * Structure:
     * @return array<string, string> {
     * @type string $title  The title to be display. If omitted the legend is not rendered.
     * @type string $min    The minimum value displayed on left side of the legend.
     * @type string $middle The middle value displayed at the center. Used to display for example value 0 in -/+ range.
     * @type string $max    The maximum value displayed on right side of the legend.
     * }
     */
    public function getLegend(): array
    {
        return [];
    }

    /**
     * Returns any filters, that should be display and available to the User.
     *
     * @param array<string, mixed> $currentFilters
     *
     * @return array<string, array>
     */
    public function getFilters( array $currentFilters ): array
    {
        return [];
    }


    /**
     * Return Data.
     *
     * @return array<string,mixed>
     */
    abstract public function get(): array;

    /**
     * Get all available years of descriptor aggregates.
     *
     * @param int $descriptor_id
     *
     * @return Collection
     */
    protected function loadAggregateYears( int $descriptor_id ): Collection
    {
        return DataAggregate::select( 'year' )->where( 'descriptor_id', $descriptor_id )->distinct()->orderBy( 'year' )->get()->pluck( 'year' );
    }

    /**
     * Get all available location resolutions.
     *
     * @param int $descriptor_id
     *
     * @return Collection
     */
    protected function loadAggregateResolution( int $descriptor_id ): Collection
    {
        return DataAggregate::select( [
            'location_type',
            'location_id'
        ] )->where( 'descriptor_id', $descriptor_id )->with( 'location' )->distinct()->get()->pluck( 'precision' )->unique();
    }

    /**
     * Get all available years of descriptor logs.
     *
     * @param int $descriptor_id
     *
     * @return Collection
     */
    protected function loadLogYears( int $descriptor_id ): Collection
    {
        return Log::select( [ 'year' ] )->whereHas( 'data', function ( Builder $query ) use ( $descriptor_id ) {
            $query->where( 'descriptor_id', $descriptor_id );
        } )->orderBy( 'year' )->distinct()->get()->pluck( 'year' );
    }

    /**
     * @param int $descriptor_id
     *
     * @return Collection
     */
    protected function loadLogResolution( int $descriptor_id ): Collection
    {
        return Log::select( [ 'id' ] )->whereHas( 'data', function ( Builder $query ) use ( $descriptor_id ) {
            $query->where( 'descriptor_id', $descriptor_id );
        } )->with( [
            'locations',
            'locations.location'
        ] )->get()->pluck( 'locations.*.precision' )->flatten()->unique()->sort();
    }

    /**
     * Get the name of the resolution.
     *
     * @param int $resolution
     *
     * @return string
     */
    protected function getResolutionName( int $resolution ): string
    {
        return __( 'logs.resolution.' . $resolution );
    }

    /**
     * Get current value of a filter.
     *
     * @param string $key
     *
     * @return mixed|null
     */
    protected function getFilter( string $key )
    {
        if ( empty( $this->filters[ $key ] ) ) {
            return null;
        }

        $current = $this->filters[ $key ]['data'][ $this->filters[ $key ]['current'] ];

        return is_array( $current ) ? $current['value'] : $current;
    }
}
