<?php

namespace App\Http\Controllers\Data;

use App\Data\Content\VarroaInfestationContent;
use App\Models\Logs\Data;
use App\Models\Logs\Log;
use Cache;
use Carbon\Carbon;

class VarroaInfestationController extends DataController
{
    /**
     * The scale to be displayed on the map and within the legend.
     * Should return either null or one of these:
     *      - null: to display no scale.
     *      - danger: a green to red scale where high values represent "dangerous" (red) area.
     *      - danger_inverted: a red to green scale where low values represent "dangerous" (red) area.
     *      - range: A simple range from one colour to another. Not necessary starts at zero.
     *      - range_zeroed: A range from one colour to another. Starts from zero.
     *      - range_diverging: A range from negative numbers to zero as the middle and then to positive numbers.
     *
     * @var string|null
     */
    public ?string $scale = 'danger';

    /**
     * Returns Legend data to be displayed by controller.
     * By default legend is not displayed (empty array).
     *
     * Structure:
     * @return array<string, string> {
     * @type string $title  The title to be display. If omitted the legend is not rendered.
     * @type string $min    The minimum value displayed on left side of the legend.
     * @type string $middle The middle value displayed at the center. Used to display for example value 0 in -/+ range.
     * @type string $max    The maximum value displayed on right side of the legend.
     * }
     */
    public function getLegend(): array
    {
        return [
            'title'  => __( 'logs.varroa-infestation.full-title' ),
            'min'    => __('logs.varroa-infestation.severity-level-1-short'),
            'middle' => __('logs.varroa-infestation.severity-level-2-short'),
            'max'    => __('logs.varroa-infestation.severity-level-3-short'),
        ];
    }


    /**
     * Returns any filters, that should be display and available to the User.
     *
     * @param array<string, mixed> $currentFilters
     *
     * @return array<string, array>
     */
    public function getFilters( array $currentFilters ): array
    {
        $resolutions = Cache::remember( 'varroa-infestation-resolution-' . $this->descriptor->id, 3600, function () {
            return $this->loadLogResolution( $this->descriptor->id );
        } );

        $array = collect();

        foreach ( $resolutions as $resolution ) {
            if ( $resolution > 4 ) {
                continue;
            }
            $array[ $resolution ] = $this->getResolutionName( $resolution );
        }
        $resolutions = $array;
        $filters     = [
            'resolution' => [
                'name'    => 'Data Resolution',
                'type'    => 'range-field',
                'data'    => $resolutions,
                'current' => isset( $currentFilters['resolution'] ) ? $currentFilters['resolution'] : $resolutions->keys()->first(),

                'start' => [
                    'value' => $resolutions->keys()->first(),
                    'label' => $resolutions->first(),
                ],
                'end'   => [
                    'value' => $resolutions->keys()->last(),
                    'label' => $resolutions->last(),
                ],
            ],
        ];

        return $filters;
    }


    /**
     * Return Data. Queries all Apiaries that are part of the projects, collecting data on Hive Weight.
     *
     * @return array<string,mixed>
     */
    public function get(): array
    {
        $today = Carbon::now();
        $logs = Log::whereIn( 'project_id', $this->descriptor->projects->pluck( 'id' ) )->whereDate( 'date', $today )->get();

        /**
         * If today entry does not exist, get the latest one possible.
         */
        if ( $logs->isEmpty() ) {
            /** @var Log $latestLog */
            $latestLog = Log::select( [ 'date' ] )->whereIn( 'project_id', $this->descriptor->projects->pluck( 'id' ) )->orderBy( 'date', 'desc' )->first();

            if ( isset( $latestLog ) ) {
                $logs = Log::whereIn( 'project_id', $this->descriptor->projects->pluck( 'id' ) )->whereDate( 'date', $latestLog->date )->get();
            }
        }

        $logs->load( [
            'data',
            'data.data',
        ] );
        $data = [];

        foreach ( $logs as $log ) {
            /** @var Log $log */
            if ( $log->location->precision != $this->filters['resolution']['current'] ) {
                continue;
            }
            \Debugbar::startMeasure( $log->id, $log->location );

            /** @var Data $dataItem */
            $severityLevel = $log->data->where( 'descriptor_id', getDescriptorIdByUid( 'varroa-infestation-alerts' ) )->first();

            $data[] = [
                'id'          => $log->project_id . '_' . $log->id,
                'position'    => $log->getCoordinates(),
                'content'     => new VarroaInfestationContent( $this->descriptor, $log ),
                'tooltip'     => $log->getTooltipText( $this->descriptor ),
                'icon'        => implode( ' ', $severityLevel->data->getIconClasses( 0, 3, 'danger' ) ),
                'area'        => $log->getArea(),
                'attribution' => $log->getTextSources(),
                'color'       => $severityLevel->data->getDiscreteValue( 0, 3 ),
                'projects'    => $log->getProject(),
                'providers'   => $log->getProviders(),
            ];
            \Debugbar::stopMeasure( $log->id );
        }

        return $data;
    }
}
