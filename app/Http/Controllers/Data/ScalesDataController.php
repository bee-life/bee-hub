<?php

namespace App\Http\Controllers\Data;

use App\Data\Content\HiveWeightContent;
use App\Models\Logs\Data;
use App\Models\Logs\Data\DataId;

class ScalesDataController extends DataController
{
    /**
     * The scale to be displayed on the map and within the legend.
     * Should return either null or one of these:
     *      - null: to display no scale.
     *      - danger: a green to red scale where high values represent "dangerous" (red) area.
     *      - danger_inverted: a red to green scale where low values represent "dangerous" (red) area.
     *      - range: A simple range from one colour to another. Not necessary starts at zero.
     *      - range_zeroed: A range from one colour to another. Starts from zero.
     *      - range_diverging: A range from negative numbers to zero as the middle and then to positive numbers.
     *
     * @var string|null
     */
    public ?string $scale = 'range_diverging';

    /**
     * Returns Legend data to be displayed by controller.
     * By default legend is not displayed (empty array).
     *
     * Structure:
     * @return array<string, string> {
     * @type string $title  The title to be display. If omitted the legend is not rendered.
     * @type string $min    The minimum value displayed on left side of the legend.
     * @type string $middle The middle value displayed at the center. Used to display for example value 0 in -/+ range.
     * @type string $max    The maximum value displayed on right side of the legend.
     * }
     */
    public function getLegend(): array
    {
        return [
            'title'  => __( 'logs.hive-sensors.weight-change-3' ),
            'min'    => '<' . format_weight( - 10, 'kg', 0 ),
            'middle' => format_weight( 0, 'kg', 0 ),
            'max'    => '>' . format_weight( 10, 'kg', 0 ),
        ];
    }

    /**
     * Return Data. Queries all Apiaries that are part of the projects, collecting data on Hive Weight.
     *
     * @return array<string,mixed>
     */
    public function get(): array
    {
        $projectids = $this->descriptor->projects->pluck( 'id' );
        $dataIds    = DataId::whereIn( 'project_id', $projectids )->whereHas( 'pivotData', function ( $q ) {
            $q->where( [
                'descriptor_id' => getDescriptorIdByUid( 'apiary-id' ),
            ] );
        } )->with( [ 'project' ] )->distinct()->withCount( [ 'children' ] )->get();

        $data = [];

        /**
         * There is a difference, if an Apiary has data only for single hive, or there are multiple hives available.
         * Additionally, we can have historic or live data, depending on the related project.
         *
         * @var DataId $id
         */
        foreach ( $dataIds as $id ) {
            \Debugbar::startMeasure( $id->value, $id->value );
            if ( $id->hasChildren() && $id->children_count > 1 ) {
                $latestLog = $id->latestLog();
                $logs      = $id->children->map->latestLog();
                $logs->loadMissing( [
                    'data',
                    'data.data',
                    'hive',
                    'apiary',
                    'project.providers'
                ] );

                $logs = $logs->sortBy( 'hive.value', SORT_NATURAL );

                $data[] = [
                    'id'          => $id->project_id . '_' . $id->id,
                    'position'    => $latestLog->getCoordinates(),
                    'content'     => new HiveWeightContent( $this->descriptor, $latestLog, $logs ),
                    'tooltip'     => $latestLog->getTooltipText( $this->descriptor ),
                    'icon'        => implode( ' ', $id->getIconClasses( 0, 0, 'invalid' ) ),
                    'attribution' => $latestLog->getTextSources(),
                    'projects'    => $latestLog->getProject(),
                    'providers'   => $latestLog->getProviders(),
                ];
            } else {
                $log = $id->latestLog();
                $log->loadMissing( [
                    'data',
                    'data.data',
                    'hive',
                    'apiary',
                    'project.providers'
                ] );
                if ( empty( $log->location ) || empty( $log->location->location ) ) {
                    continue;
                }

                /** @var Data $dataItem */
                $dataItem = $log->data->where( 'descriptor_id', $this->descriptor->id )->first();
                $min      = - 10;
                $max      = 10;

                if ( empty( $dataItem ) ) {
                    $icon = 'datatype datatype-unknown';
                } else {
                    $icon = implode( ' ', isset( $aggregates[3] ) ? $aggregates[3]->getIconClasses( $min, $max, 'range_diverging' ) : $dataItem->data->getIconClasses( 0, 0, 'invalid' ) );
                }

                $data[] = [
                    'id'          => $log->project_id . '_' . $log->id,
                    'position'    => $log->getCoordinates(),
                    'content'     => new HiveWeightContent( $this->descriptor, $log ),
                    'tooltip'     => isset( $aggregates[3] ) ? $aggregates[3]->getTooltipText() : $log->getTooltipText( $this->descriptor ),
                    'icon'        => $icon,
                    'attribution' => $log->getTextSources(),
                    'projects'    => $log->getProject(),
                    'providers'   => $log->getProviders(),
                ];
            }
            \Debugbar::stopMeasure( $id->value );
        }

        return $data;
    }
}
