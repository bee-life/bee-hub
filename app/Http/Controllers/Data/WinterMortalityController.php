<?php

namespace App\Http\Controllers\Data;

use App\Models\Logs\Log;
use App\Models\MetaData\Descriptor;
use Cache;
use Illuminate\Database\Eloquent\Builder;

class WinterMortalityController extends DataController
{
    /**
     * The scale to be displayed on the map and within the legend.
     * Should return either null or one of these:
     *      - null: to display no scale.
     *      - danger: a green to red scale where high values represent "dangerous" (red) area.
     *      - danger_inverted: a red to green scale where low values represent "dangerous" (red) area.
     *      - range: A simple range from one colour to another. Not necessary starts at zero.
     *      - range_zeroed: A range from one colour to another. Starts from zero.
     *      - range_diverging: A range from negative numbers to zero as the middle and then to positive numbers.
     *
     * @var string|null
     */
    public ?string $scale = 'danger';

    /**
     * Minimum value within current dataset.
     *
     * @var mixed|null
     */
    protected $min = 0;

    /**
     * Maximum value within current dataset.
     *
     * @var mixed|null
     */
    protected $max = 50;


    /**
     * Returns Legend data to be displayed by controller.
     * By default legend is not displayed (empty array).
     *
     * Structure:
     * @return array<string, string> {
     * @type string $title  The title to be display. If omitted the legend is not rendered.
     * @type string $min    The minimum value displayed on left side of the legend.
     * @type string $middle The middle value displayed at the center. Used to display for example value 0 in -/+ range.
     * @type string $max    The maximum value displayed on right side of the legend.
     * }
     */
    public function getLegend(): array
    {
        return [
            'title' => __( 'logs.winter-mortality.full-title' ),
            'min'   => format_percentage( $this->min, 0 ),
            'max'   => '>' . format_percentage( $this->max, 0 ),
        ];
    }

    /**
     * Returns any filters, that should be display and available to the User.
     *
     * @param array<string, mixed> $currentFilters
     *
     * @return array<string, array>
     */
    public function getFilters( array $currentFilters ): array
    {
        $years       = Cache::remember( 'winter-mortality-years-' . $this->descriptor->id, 3600, function () {
            return $this->loadLogYears( $this->descriptor->id );
        } );
        $resolutions = Cache::remember( 'winter-mortality-resolution-' . $this->descriptor->id, 3600, function () {
            return $this->loadLogResolution( $this->descriptor->id );
        } );

        $array = collect();

        foreach ( $resolutions as $resolution ) {
            if ( $resolution > 4 ) {
                continue;
            }
            $array[] = [ 'value' => $resolution, 'label' => $this->getResolutionName( $resolution ) ];
        }
        $resolutions = $array->sortBy( 'value' )->values();

        $data = collect();

        foreach($years as $year){
            $data[] = $year . ' - ' . $year + 1;
        }

        $filters = [
            'year'       => [
                'name'    => 'Winter',
                'type'    => 'range-field',
                'data'    => $data,
                'current' => isset( $currentFilters['year'] ) ? $currentFilters['year'] : $years->keys()->last(),

                'start' => $years->first(),
                'end'   => $years->last(),

            ],
            'resolution' => [
                'name'    => 'Data Resolution',
                'type'    => 'range-field',
                'data'    => $resolutions,
                'current' => isset( $currentFilters['resolution'] ) ? $currentFilters['resolution'] : $resolutions->keys()->first(),

                'start' => [
                    'key'   => $resolutions->keys()->first(),
                    'value' => $resolutions->first()['value'],
                    'label' => $resolutions->first()['label'],
                ],
                'end'   => [
                    'key'   => $resolutions->keys()->last(),
                    'value' => $resolutions->last()['value'],
                    'label' => $resolutions->last()['label'],
                ],
            ],
        ];

        return $filters;
    }

    /**
     * Return Data.
     *
     * @return array<string,mixed>
     */
    public function get(): array
    {
        $descriptor = Descriptor::whereUid('winter-mortality-aggregate')->first();
        $logs = Log::whereIn('project_id', $descriptor->projects()->select('id')->get()->pluck('id') )->where( 'year', $this->getFilter( 'year' ) )->whereHas( 'data', function ( Builder $query ) {
            $query->where( 'descriptor_id', $this->descriptor->id );
        } )->with( [ 'data', 'data.data', 'data.origin', 'project', 'project.providers' ] )->get();

        $data = [];
        /** @var Log $log */
        foreach ( $logs as $log ) {
            if ( $log->precision != $this->getFilter( 'resolution' ) ) {
                continue;
            }

            $data[] = [
                'id'          => $log->descriptor_id . '_' . $log->id,
                'popup'       => $log->getPopupText( $this->descriptor ),
                'tooltip'     => $log->getTooltipText( $this->descriptor ),
                'area'        => $log->getArea(),
                'attribution' => $log->getTextSources(),
                'color'       => $log->getData( $this->descriptor )->data->getDiscreteValue( 0, 50 ),
            ];
        }

        return $data;
    }
}
