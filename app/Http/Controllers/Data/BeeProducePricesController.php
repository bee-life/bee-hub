<?php

namespace App\Http\Controllers\Data;

use App\Models\Logs\Log;
use App\Models\References\Currency;
use Cache;
use Illuminate\Database\Eloquent\Builder;

class BeeProducePricesController extends DataController
{
    /**
     * The scale to be displayed on the map and within the legend.
     * Should return either null or one of these:
     *      - null: to display no scale.
     *      - danger: a green to red scale where high values represent "dangerous" (red) area.
     *      - danger_inverted: a red to green scale where low values represent "dangerous" (red) area.
     *      - range: A simple range from one colour to another. Not necessary starts at zero.
     *      - range_zeroed: A range from one colour to another. Starts from zero.
     *      - range_diverging: A range from negative numbers to zero as the middle and then to positive numbers.
     *
     * @var string|null
     */
    public ?string $scale = 'range_zeroed';

    /**
     * Returns Legend data to be displayed by controller.
     * By default legend is not displayed (empty array).
     *
     * Structure:
     * @return array<string, string> {
     * @type string $title  The title to be display. If omitted the legend is not rendered.
     * @type string $min    The minimum value displayed on left side of the legend.
     * @type string $middle The middle value displayed at the center. Used to display for example value 0 in -/+ range.
     * @type string $max    The maximum value displayed on right side of the legend.
     * }
     */
    public function getLegend(): array
    {
        if ( $this->descriptor->category->uid == 'socioeconomic-prices' ) {
            return [
                'title' => __( 'logs.socio-economics.prices' ),
                'min'   => format_number( $this->min, 0 ),
                'max'   => format_number( $this->max, 0 ),
            ];
        } elseif($this->descriptor->category->uid == 'socioeconomic-livestock') {
            return [
                'title' => __( 'logs.socio-economics.livestock' ),
                'min'   => format_number( $this->min, 0 ),
                'max'   => format_number( $this->max, 0 ),
            ];
        } else {
            return [
                'title' => __( 'logs.socio-economics.production' ),
                'min'   => format_number( $this->min, 0 ),
                'max'   => format_number( $this->max, 0 ),
            ];
        }
    }

    /**
     * Returns any filters, that should be display and available to the User.
     *
     * @param array<string, mixed> $currentFilters
     *
     * @return array<string, array>
     */
    public function getFilters( array $currentFilters ): array
    {
        $years = Cache::remember( 'socioeconomic-years-' . $this->descriptor->id, 1440, function () {
             return $this->loadLogYears( $this->descriptor->id );
        } );
        $resolutions = Cache::remember( 'socioeconomic-resolution-' . $this->descriptor->id, 1440, function () {
             return $this->loadLogResolution( $this->descriptor->id );
        } );

        $array       = collect();

        foreach ( $resolutions as $resolution ) {
            $array[] = [ 'value' => $resolution, 'label' => $this->getResolutionName( $resolution )];
        }
        $resolutions = $array;

        $filters     = [
            'year'       => [
                'name'    => 'Year',
                'type'    => 'range-field',
                'data'    => $years,
                'current' => isset( $currentFilters['year'] ) ? $currentFilters['year'] : $years->keys()->last(),

                'start' => $years->first(),
                'end'   => $years->last(),

            ],
            'resolution' => [
                'name'    => 'Data Resolution',
                'type'    => 'range-field',
                'data'    => $resolutions,
                'current' => isset( $currentFilters['resolution'] ) ? $currentFilters['resolution'] : $resolutions->keys()->first(),

                'start' => [
                    'key' => $resolutions->keys()->first(),
                    'value' => $resolutions->first()['value'],
                    'label' => $resolutions->first()['label'],
                ],
                'end'   => [
                    'key' => $resolutions->keys()->last(),
                    'value' => $resolutions->last()['value'],
                    'label' => $resolutions->last()['label'],
                ],
            ],
        ];

        return $filters;
    }

    /**
     * Return Data. Queries all Apiaries that are part of the projects, collecting data on Hive Weight.
     *
     * @return array<string,mixed>
     */
    public function get(): array
    {
        $logs = Log::whereHas( 'data', function ( Builder $query ) {
            $query->where( 'descriptor_id', $this->descriptor->id );
        } )->with( [ 'data', 'data.data', 'data.origin', 'project', 'project.providers' ] )->where( 'year', $this->getFilter( 'year' ) )->get();

        if ( $this->descriptor->category->uid == 'socioeconomic-prices' ) {
            $currency = Currency::where( 'code', 'USD' )->first();
            $values   = $logs->pluck( 'data' )->flatten()->where( 'descriptor_id', $this->descriptor->id )->where( 'data.reference_id', $currency->id )->pluck( 'value' );
        } else {
            $values = $logs->pluck( 'data' )->flatten()->where( 'descriptor_id', $this->descriptor->id )->pluck( 'value' );
        }

        $this->min = bcmin( $values );
        $this->max = bcmax( $values );

        $data = [];

        foreach ( $logs as $log ) {
            if ( $log->location->precision != $this->getFilter( 'resolution' ) ) {
                continue;
            }

            $descriptorData = $log->data->where( 'descriptor_id', $this->descriptor->id )->first()->data;

            $data[] = [
                'id'          => $log->project_id . '_' . $log->id,
                'popup'       => $log->getPopupText( $this->descriptor ),
                'tooltip'     => $log->getTooltipText( $this->descriptor ),
                'attribution' => $log->getTextSources(),
                'projects'    => $log->getProject(),
                'providers'   => $log->getProviders(),

                'position' => $log->getCoordinates(),
                'icon'     => implode( ' ', $descriptorData->getIconClasses( $this->min, $this->max, $this->scale ) ),

                'area'  => $log->getArea(),
                'color' => $descriptorData->getDiscreteValue( $this->min, $this->max, false ),
            ];
        }

        return $data;
    }
}
