<?php

namespace App\Http\Controllers\Data;

use App\Data\Content\ColonyDevelopmentContent;
use App\Models\Logs\Data\DataId;

class ColonyDevelopmentController extends DataController
{
    /**
     * The scale to be displayed on the map and within the legend.
     * Should return either null or one of these:
     *      - null: to display no scale.
     *      - danger: a green to red scale where high values represent "dangerous" (red) area.
     *      - danger_inverted: a red to green scale where low values represent "dangerous" (red) area.
     *      - range: A simple range from one colour to another. Not necessary starts at zero.
     *      - range_zeroed: A range from one colour to another. Starts from zero.
     *      - range_diverging: A range from negative numbers to zero as the middle and then to positive numbers.
     *
     * @var string|null
     */
    public ?string $scale = 'null';

    /**
     * Returns Legend data to be displayed by controller.
     * By default legend is not displayed (empty array).
     *
     * Structure:
     * @return array<string, string> {
     * @type string $title  The title to be display. If omitted the legend is not rendered.
     * @type string $min    The minimum value displayed on left side of the legend.
     * @type string $middle The middle value displayed at the center. Used to display for example value 0 in -/+ range.
     * @type string $max    The maximum value displayed on right side of the legend.
     * }
     */
    public function getLegend(): array
    {
        return [];
    }

    /**
     * Return Data. Queries all Apiaries that are part of the projects, collecting data on Hive Weight.
     *
     * @return array<string,mixed>
     */
    public function get(): array
    {
        $projectids = $this->descriptor->projects->pluck( 'id' );
        $dataIds    = DataId::whereIn( 'project_id', $projectids )->whereHas( 'pivotData', function ( $q ) {
            $q->where( [
                'descriptor_id' => getDescriptorIdByUid( 'apiary-id' ),
            ] );
        } )->with( [ 'project' ] )->distinct()->withCount( [ 'children' ] )->get();

        $data = [];

        /**
         * @var DataId $id
         */
        foreach ( $dataIds as $id ) {
            \Debugbar::startMeasure( $id->value, $id->value );

            $latestLog = $id->latestLog();
            $logs      = $id->children->map->latestLog();
            $logs->loadMissing( [
                'data',
                'data.data',
                'hive',
                'apiary',
                'project.providers'
            ] );

            $logs = $logs->sortBy( 'hive.value', SORT_NATURAL );

            $data[] = [
                'id'          => $id->project_id . '_' . $id->id,
                'position'    => $latestLog->getCoordinates(),
                'content'     => new ColonyDevelopmentContent( $this->descriptor, $latestLog, $logs ),
                'tooltip'     => $latestLog->getTooltipText( $this->descriptor ),
                'icon'        => implode( ' ', $id->getIconClasses( 0, 0, 'invalid' ) ),
                'attribution' => $latestLog->getTextSources(),
                'projects'    => $latestLog->getProject(),
                'providers'   => $latestLog->getProviders(),
            ];

            \Debugbar::stopMeasure( $id->value );
        }

        return $data;
    }
}
