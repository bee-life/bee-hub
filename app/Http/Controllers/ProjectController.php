<?php

namespace App\Http\Controllers;

use App\Models\MetaData\Project;
use Illuminate\View\View;

class ProjectController extends Controller
{
    /**
     * View index of all Connectors.
     *
     * @return View
     */
    public function index(): View
    {
        return view( 'projects.index', [ 'projects' => Project::with( [ 'descriptors', 'providers' ] )->where( 'public', true )->orderBy( 'name' )->get() ] );
    }

    /**
     * View a single Connector information.
     *
     * @param string $slug
     *
     * @return View
     */
    public function single( string $slug ): View
    {
        return view( 'projects.single', [
            'model' => Project::with( [
                'descriptors',
                'providers'
            ] )->withCount( [ 'logs' ] )->where( 'public', true )->where( 'slug', $slug )->firstOrFail()
        ] );
    }
}
