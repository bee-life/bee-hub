<?php

namespace App\Http\Controllers;

use App\Models\MetaData\Descriptor;
use App\Repositories\NewsRepository;
use Illuminate\View\View;

class AppController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( protected NewsRepository $news )
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return View
     */
    public function index(): View
    {

        $descriptors = [
            [
                'name'        => __( 'descriptors.groups.monitoring' ),
                'descriptors' => Descriptor::whereIn( 'uid', [
                    'hive-weight',
                ] )->with( [
                    'projects' => function ( $q ) {
                        $q->where( 'public', true );
                    }
                ] )->orderBy( 'name' )->get()
            ],
            [
                'name'        => __( 'descriptors.groups.demography-biology' ),
                'descriptors' => Descriptor::whereIn( 'uid', [
                    'hive-population',
                ] )->with( [
                    'projects' => function ( $q ) {
                        $q->where( 'public', true );
                    }
                ] )->orderBy( 'name' )->get()
            ],
            [
                'name'        => __( 'descriptors.groups.health' ),
                'descriptors' => Descriptor::whereIn( 'uid', [
                    'varroa-infestation',
                   // 'pollen-pesticides',
                    'winter-mortality-aggregate',
                ] )->with( [
                    'projects' => function ( $q ) {
                        $q->where( 'public', true );
                    }
                ] )->orderBy( 'name' )->get()
            ],
            [
                'name'        => __( 'descriptors.groups.socio-economic' ),
                'descriptors' => Descriptor::whereIn( 'uid', [
                    'prices-honey',
                    'prices-beeswax',
                    'production-honey-tonnes-per-surface',
                    'production-beeswax-tonnes-per-surface',
                    'livestock-beehives',
                ] )->with( [
                    'projects' => function ( $q ) {
                        $q->where( 'public', true );
                    }
                ] )->orderBy( 'name' )->get()
            ],
        ];


        return view( 'frontpage', [ 'news' => $this->news->latest(), 'descriptors' => $descriptors ] );
    }
}
