<?php

namespace App\Http\Controllers;

use App\Repositories\NewsRepository;
use App;
use Illuminate\View\View;

class NewsController extends Controller
{

    /**
     * NewsController constructor.
     * Laravel Framework autoloads these repositories. Add them here, if you need any other.
     *
     * @param NewsRepository $news
     */
    public function __construct( protected NewsRepository $news )
    {
    }


    /**
     * Process News index page.
     *
     * @return View
     */
    public function index(): View
    {
        $data['news'] = $this->news->latest(12);

        return view( 'news.index', $data );
    }

    /**
     * Process News single page.
     *
     * @param string $slug
     *
     * @return View
     */
    public function single( string $slug ): View
    {
        $data['news'] = $this->news->findBySlug( $slug );

        return view( 'news.single', $data );
    }

    /**
     * Display News RSS feed.
     * @todo
     *
     * @return string
     */
    public function feed(): string{
        $feed = App::make("Feed");


        // cache the feed for 60 minutes (second parameter is optional)
        $feed->setCache(30, 'news_feed');

        if (!$feed->isCached())
        {
            $news = $this->news->published();

            // set your feed's title, description, link, pubdate and language
            $feed->title = 'Varroawetter News Feed';
            $feed->description = 'Varroawetter News Feed';
            $feed->logo = ' http://www.bienengesundheit.at/wp-content/themes/BGWP/img/Logo_BOE_Small.png';
            $feed->link = lang_url('news/feed');
            $feed->setDateFormat('datetime'); // 'datetime', 'timestamp' or 'carbon'
            $feed->pubdate = $news[0]->published_at;
            $feed->lang = 'de';
            $feed->setShortening(true); // true or false
            $feed->setTextLimit(100); // maximum length of description text

            foreach ($news as $news_item)
            {
                // set item's title, author, url, pubdate, description, content, enclosure (optional)*
                $feed->add($news_item->title, 'Varroawetter', $news_item->url(), $news_item->published_at, $news_item->excerpt, $news_item->content);
            }

        }

        // first param is the feed format
        // optional: second param is cache duration (value of 0 turns off caching)
        // optional: you can set custom cache key with 3rd param as string
        return $feed->render('rss');
    }
}
