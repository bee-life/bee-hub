<?php

namespace App\Http\Controllers;

use App\Models\CMS\Report;
use Carbon\Carbon;
use Illuminate\View\View;

class ReportController extends Controller
{
    /**
     * View index of all Connectors.
     *
     * @return View
     */
    public function index(): View
    {
        return view( 'reports.index', [ 'models' => Report::with( [  ] )->whereDate( 'published_at', '<', Carbon::now())->orderBy( 'published_at', 'desc' )->get() ] );
    }

    /**
     * View a single Connector information.
     *
     * @param string $slug
     *
     * @return View
     */
    public function single( string $slug ): View
    {
        return view( 'reports.single', [ 'model' => Report::with( [ ] )->whereDate( 'published_at', '<', Carbon::now())->where( 'slug', $slug )->firstOrFail() ] );
    }
}
