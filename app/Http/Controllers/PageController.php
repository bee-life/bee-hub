<?php

namespace App\Http\Controllers;

use App\Repositories\PageRepository;
use Illuminate\View\View;

class PageController extends Controller
{

    /**
     * PageController constructor.
     * Laravel Framework autoloads these repositories. Add them here, if you need any other.
     *
     * @param PageRepository $pages
     */
    function __construct( protected PageRepository $pages )
    {
    }


    /**
     * View Sitemap page
     *
     * @return View
     */
    public function sitemap(): View
    {
        return view( 'static.sitemap' );
    }

    /**
     * View a static page, defined in database
     *
     * @param string $slug
     *
     * @return  View
     */
    public function index( string $slug ): View
    {
        $data['page'] = $this->pages->findBySlug( 'frontpage' );

        return view( 'single', $data );
    }


    /**
     * View a static page, defined in database
     *
     * @param string $slug
     *
     * @return  View
     */
    public function page( string $slug ): View
    {
        $data['page'] = $this->pages->findBySlug( $slug );

        return view( 'single', $data );
    }
}
