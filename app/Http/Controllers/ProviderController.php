<?php

namespace App\Http\Controllers;

use App\Models\MetaData\Provider;
use Illuminate\View\View;

class ProviderController extends Controller
{
    /**
     * View index of all Connectors.
     *
     * @return View
     */
    public function index(): View
    {
        return view( 'providers.index', [ 'providers' => Provider::with( [ 'projects' ] )->where( 'public', true )->orderBy( 'full_name' )->get() ] );
    }

    /**
     * View a single Connector information.
     *
     * @param string $slug
     *
     * @return View
     */
    public function single( string $slug ): View
    {
        return view( 'providers.single', [ 'model' => Provider::with( [ 'projects' ] )->where( 'public', true )->where( 'slug', $slug )->firstOrFail() ] );
    }
}
