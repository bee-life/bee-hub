<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\Welcome;
use App\Models\MetaData\Project;
use App\Models\MetaData\Provider;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use DB;
use Hash;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Mail;
use Storage;
use Str;
use Throwable;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/welcome';

    protected $tokens;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware( 'guest' );
    }

    /**
     * Handle a registration request for the application.
     *
     * @param Request $request
     *
     * @return void
     */
    public function register( Request $request )
    {
        /**
         *  405 Method Not Allowed
         * The request method is known by the server but has been disabled and cannot be used. For
         * example, an API may forbid DELETE-ing a resource. The two mandatory methods, GET and HEAD,
         * must never be disabled and should not return this error code.
         */
        abort( 405 );
    }

    /**
     * View render for first registration step.
     *
     * @param Request $request
     *
     * @return View
     */
    public function getStep1( Request $request )
    {
        $expand = null;

        if ( ! empty( $request->session()->get( 'project-name' ) ) ) {
            $expand = 'project';
        }

        return view( 'auth.register-project', [ 'expand' => $expand ] );
    }

    /**
     * Processing for the first registration step.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function postStep1( Request $request )
    {
        $validated = $request->validate( [
            'project-name'        => 'required|string|max:200',
            'project-acronym'     => 'nullable|string|max:50',
            'project-description' => 'nullable|string|max:20000',
            'project-start-date'  => 'nullable|date_format:Y-m-d|before:project-end-date',
            'project-end-date'    => 'nullable|date_format:Y-m-d|after:project-start-date',
            'project-logo'        => 'nullable|image|max:2048',
            'project-website'     => 'nullable|string|url|max:100',
            'project-email'       => 'nullable|string|email|max:100',
            'project-phone'       => 'nullable|string|max:20',
            'action'              => 'required|string|in:next,previous',
        ] );

        if ( ! empty( $validated['project-logo'] ) ) {
            $validated['project-logo'] = Storage::putFile( 'public', $validated['project-logo'] );
        }

        $request->session()->put( $validated );


        if ( $validated['action'] == 'next' ) {
            return redirect()->route( 'register-step-2' );
        } else {
            return redirect()->route( 'register' );
        }
    }

    /**
     * View render for second registration step.
     *
     * @param Request $request
     *
     * @return View
     */
    public function getStep2( Request $request )
    {

        $data = [
            'smart-hives'     => [
                'hive-temperature',
                'hive-relative-humidity',
                'weight',
                'counts',
                'temperature',
                'relative-humidity',
                'co2',
                'sounds',
                'vibrations',
                'odors',
            ],
            'stress-factors'  => [
                'pesticides-residues',
                'pathogen-loads',
                'varroa',
                'nutritional',
                'weather',
                'landscape',
                'genetics',
                'invasive',
            ],
            'beekeeping-data' => [
                'beekeeper',
                'management',
            ],
            'mortality-data'  => [
                'winter',
                'seasonal',
            ],

        ];


        return view( 'auth.register-data', [ 'data' => $data ] );
    }

    /**
     * Processing for the second registration step.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function postStep2( Request $request )
    {
        $validated = $request->validate( [
            'smart-hives'           => 'nullable|array',
            'smart-hives.*'         => 'nullable|string|in:hive-temperature,hive-relative-humidity,weight,counts,temperature,relative-humidity,co2,sounds,vibrations,odors',
            'smart-hives-other'     => 'nullable|string|max:200',
            'stress-factors'        => 'nullable|array',
            'stress-factors.*'      => 'nullable|string|in:pesticides-residues,pathogen-loads,varroa,nutritional,weather,landscape,genetics,invasive',
            'stress-factors-other'  => 'nullable|string|max:200',
            'beekeeping-data'       => 'nullable|array',
            'beekeeping-data.*'     => 'nullable|string|in:beekeeper,management',
            'beekeeping-data-other' => 'nullable|string|max:200',
            'mortality-data'        => 'nullable|array',
            'mortality-data.*'      => 'nullable|string|in:winter,seasonal',
            'file-upload'           => 'nullable|file|mimes:csv,xml,xls,xlsx,sql,zip,tar,gz|max:10240',
            'file-upload-notes'     => 'nullable|string|max:20000',
            'remote-link'           => 'nullable|string|url|max:500',
            'remote-notes'          => 'nullable|string|max:20000',
            'api-upload'            => 'nullable|string|max:doc,docx,txt,zip,pdf',
            'api-notes'             => 'nullable|string|max:20000',
            'other-notes'           => 'nullable|string|max:20000',
            'contact-name'          => 'nullable|string|max:100',
            'contact-email'         => 'nullable|string|email|max:50',
            'data-use'              => 'required|string|in:open,restricted,closed',
            'action'                => 'required|string|in:next,previous',
        ] );
        if ( empty( $validated['smart-hives'] ) ) {
            $validated['smart-hives'] = [];
        }
        if ( empty( $validated['stress-factors'] ) ) {
            $validated['stress-factors'] = [];
        }
        if ( empty( $validated['beekeeping-data'] ) ) {
            $validated['beekeeping-data'] = [];
        }
        if ( empty( $validated['mortality-data'] ) ) {
            $validated['mortality-data'] = [];
        }

        if ( ! empty( $validated['file-upload'] ) ) {
            $validated['file-upload'] = Storage::putFile( 'public', $validated['file-upload'] );
        }
        if ( ! empty( $validated['api-upload'] ) ) {
            $validated['api-upload'] = Storage::putFile( 'public', $validated['api-upload'] );
        }


        $request->session()->put( $validated );


        if ( $validated['action'] == 'next' ) {
            return redirect()->route( 'register-step-3' );
        } else {
            return redirect()->route( 'register-step-1' );
        }
    }

    /**
     * View render for third registration step.
     *
     * @param Request $request
     *
     * @return View
     */
    public function getStep3( Request $request )
    {

        if ( ! empty( $request->old( 'owner' ) ) ) {
            $data = $request->old( 'owner' );
        } else {
            $data = $request->session()->get( 'owner' );
        }
        if ( isset( $data ) ) {
            foreach ( $data as $key => $item ) {
                if ( empty( $item['full-name'] ) ) {
                    unset( $data[ $key ] );
                }
            }
        }
        $count = ! empty( $data ) ? count( $data ) + 1 : 1;

        return view( 'auth.register-ownership', [ 'old' => $data, 'count' => $count ] );
    }

    /**
     * Processing for the third registration step.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function postStep3( Request $request )
    {
        $validated = $request->validate( [
            'owner'                   => 'required|array',
            'owner.*.full-name'       => 'nullable|string|max:200',
            'owner.*.short-name'      => 'nullable|string|max:50',
            'owner.*.address'         => 'nullable|string|max:20000',
            'owner.*.registry-number' => 'nullable|string|max:20',
            'owner.*.description'     => 'nullable|string|max:20000',
            'owner.*.logo'            => 'nullable|image|max:2048',
            'owner.*.website'         => [ 'nullable', 'string', 'max:100' ],
            'owner.*.email'           => 'nullable|string|email|max:100',
            'owner.*.phone'           => 'nullable|string|max:20',

            'action' => 'required|string|in:next,previous',
        ] );

        foreach ( $validated['owner'] as $key => $owner ) {
            if ( empty( $owner['full-name'] ) ) {
                unset( $validated['owner'][ $key ] );
            }
        }

        $old = $request->session()->get( 'owner' );

        foreach ( $validated['owner'] as $key => $owner ) {
            if ( ! empty( $owner['logo'] ) ) {
                $validated['owner'][ $key ]['logo'] = Storage::putFile( 'public', $owner['logo'] );
            }

            if ( isset( $old[ $key ] ) ) {
                $validated['owner'][ $key ] = array_merge( $old[ $key ], $owner );
            }
        }

        $request->session()->put( $validated );

        if ( $validated['action'] == 'next' ) {
            return redirect()->route( 'register-step-4' );
        } else {
            return redirect()->route( 'register-step-2' );
        }
    }

    /**
     * View render for fourth registration step.
     *
     * @param Request $request
     *
     * @return View|RedirectResponse
     */
    public function getStep4( Request $request )
    {
        if ( empty( session( 'owner' ) ) ) {
            return redirect()->route( 'register' );
        }

        return view( 'auth.register-review' );
    }

    /**
     * Processing for the fourth registration step.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws Throwable
     */
    public function postStep4( Request $request )
    {
        $data = $request->session()->all();
        DB::beginTransaction();

        if ( Auth::check() ) {
            $request->validate( [
                'ownership-consent' => 'accepted',
                'public-consent'    => 'accepted',
                'terms-consent'     => 'accepted',
            ] );

            $user = Auth::user();
        } else {
            $validated = $request->validate( [
                'user-name'         => 'required|string|max:200',
                'user-email'        => 'required|string|email|max:100|unique:users,email',
                'ownership-consent' => 'accepted',
                'public-consent'    => 'accepted',
                'terms-consent'     => 'accepted',
            ] );

            /**
             *  At this point we are validated and all required data is present, create the users and all inputted data, then notify the admins!
             */
            $user = User::create( [
                'name'     => $validated['user-name'],
                'email'    => $validated['user-email'],
                'password' => User::generatePassword(),
                'type'     => User::$user,
            ] );

            $token = Str::random( 60 );
            DB::table( 'password_resets' )->insert( [
                'email'      => $user->email,
                'token'      => Hash::make( $token ),
                'created_at' => Carbon::now()
            ] );
        }
        if ( isset( $data['file-upload'] ) || isset( $data['remote-link'] ) || isset( $data['other-notes'] ) ) {
            $type = 'static';
        } elseif ( isset( $data['api-notes'] ) || isset( $data['api-upload'] ) ) {
            $type = 'live';
        } else {
            $type = 'static';
        }

        $project = Project::create( [
            'name'           => $data['project-name'],
            'acronym'        => $data['project-acronym'],
            'description'    => $data['project-description'],
            'start-date'     => $data['project-start-date'],
            'end-date'       => $data['project-end-date'],
            'featured_image' => $data['project-logo'] ?? null,
            'website'        => $data['project-website'],
            'email'          => $data['project-email'],
            'phone'          => $data['project-phone'],
            'type'           => $type,
            'active'         => 0,
            'public'         => 0,
            'sharing_type'   => $data['data-use'],
        ] );

        $user->projects()->attach( $project, [
            'integration_contact' => $data['contact-name'],
            'integration_email'   => $data['contact-email'],
            'share_conditions'    => $data['data-use'],
            'metadata'            => [
                'smart-hives'           => $data['smart-hives'] ?? [],
                'smart-hives-other'     => $data['smart-hives-other'],
                'stress-factors'        => $data['stress-factors'] ?? [],
                'stress-factors-other'  => $data['stress-factors-other'],
                'beekeeping-data'       => $data['beekeeping-data'] ?? [],
                'beekeeping-data-other' => $data['beekeeping-data-other'],
                'mortality-data'        => $data['mortality-data'] ?? [],
                'file-upload'           => $data['file-upload'] ?? null,
                'file-upload-notes'     => $data['file-upload-notes'],
                'remote-link'           => $data['remote-link'],
                'remote-notes'          => $data['remote-notes'],
                'api-upload'            => $data['api-upload'] ?? null,
                'api-notes'             => $data['api-notes'],
                'other-notes'           => $data['other-notes'],
            ],
        ] );
        foreach ( $data['owner'] as $owner_data ) {
            $owner = Provider::create( [
                'full_name'       => $owner_data['full-name'],
                'short_name'      => $owner_data['short-name'],
                'address'         => $owner_data['address'],
                'registry_number' => $owner_data['registry-number'],
                'description'     => $owner_data['description'],
                'website'         => $owner_data['website'],
                'email'           => $owner_data['email'],
                'phone'           => $owner_data['phone'],
                'featured_image'  => $owner_data['logo'] ?? null,
                'public'          => 0,
            ] );

            $user->providers()->attach( $owner, [] );
            $project->providers()->attach( $owner );
        }

        DB::commit();

        //event( new Registered( $user ) );  Implement the event and use queue to send email

        if ( ! Auth::check() ) {
            Mail::send( new Welcome( $user, $token ) );
        }

        /**
         * TODO: Inform Admin
         */

        return redirect( $this->redirectPath() );
    }

    /**
     * View render for fourth registration step.
     *
     * @param Request $request
     *
     * @return View|RedirectResponse
     */
    public function getStep5( Request $request )
    {
        return view( 'auth.register-finish' );
    }
}
