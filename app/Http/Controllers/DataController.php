<?php

namespace App\Http\Controllers;

use App\Data\Content\HiveWeightContent;
use App\Http\Controllers\Data\BeeProducePricesController;
use App\Http\Controllers\Data\ColonyDevelopmentController;
use App\Http\Controllers\Data\ScalesDataController;
use App\Http\Controllers\Data\VarroaInfestationController;
use App\Http\Controllers\Data\WinterMortalityController;
use App\Models\Logs\Data;
use App\Models\MetaData\Descriptor;
use App\Models\MetaData\Project;
use Cache;
use Debugbar;
use Illuminate\Http\Request;
use Throwable;

class DataController extends Controller
{
    /**
     * Base entry point for Map queries.
     *
     * @param Request $request
     *
     * @return array
     * @throws Throwable
     *
     * @todo:
     *      - Preload the cache instead of on demand.
     *      - Finish refactoring existing datasets
     */
    public function get( Request $request ): array
    {
        Debugbar::startMeasure( 'Data', 'Data load' );
        $request->validate( [
            'descriptor' => 'string',
            'year'       => 'nullable|int',
        ] );

        $filters = $request->all();

        $return = [];

        $classes = collect( [
            'hive-weight'                           => ScalesDataController::class,
            'winter-mortality-aggregate'            => WinterMortalityController::class,
            'prices-honey'                          => BeeProducePricesController::class,
            'prices-beeswax'                        => BeeProducePricesController::class,
            'production-honey'                      => BeeProducePricesController::class,
            'production-beeswax'                    => BeeProducePricesController::class,
            'livestock-beehives'                    => BeeProducePricesController::class,
            'production-honey-tonnes-per-surface'   => BeeProducePricesController::class,
            'production-beeswax-tonnes-per-surface' => BeeProducePricesController::class,
            'varroa-infestation'                    => VarroaInfestationController::class,
            'pollen-pesticides'                     => null,
            'hive-population'                       => ColonyDevelopmentController::class,

        ] );

        /**
         * Set cache length per type of data.
         */
        $cache = collect( [
            'hive-weight'                           => 60 * 60 * 3,
            'winter-mortality-aggregate'            => 60 * 60 * 24,
            'prices-honey'                          => 60 * 60 * 24 * 7,
            'prices-beeswax'                        => 60 * 60 * 24 * 7,
            'production-honey'                      => 60 * 60 * 24 * 7,
            'production-beeswax'                    => 60 * 60 * 24 * 7,
            'livestock-beehives'                    => 60 * 60 * 24 * 7,
            'production-honey-tonnes-per-surface'   => 60 * 60 * 24 * 7,
            'production-beeswax-tonnes-per-surface' => 60 * 60 * 24 * 7,
            'varroa-infestation'                    => 60 * 60 * 24 * 7,
            'hive-population'                       => 60 * 60 * 24 * 7,
            'pollen-pesticides'                     => null,
        ] );

        if ( $classes->has( $filters['descriptor'] ) ) {
            ksort( $filters );
            $cacheKey = 'data';
            foreach ( $filters as $key => $filter ) {
                $cacheKey .= '-' . $key . '_' . $filter;
            }

            // TODO: Preload?
            if ( ! Cache::has( $cacheKey ) || is_local() ) {
                $descriptor = Descriptor::where( 'uid', $filters['descriptor'] )->with( [ 'projects' ] )->first();

                /** @var \App\Http\Controllers\Data\DataController $dataController */
                $dataController = new $classes[ $descriptor->uid ]( $descriptor, $filters );

                $return = [
                    'data'    => $dataController->get(),
                    'scale'   => $dataController->scale,
                    'filters' => $dataController->filters,
                    'legend'  => $dataController->getLegend(),
                ];

                /*
                 * @TODO: Implement Pollen Pesticides Controllers
            if ( isset( $descriptors['pollen-pesticides'] ) ) {
              $return = [
                  'data'         => $this->loadPollenContaminationData( $descriptors['pollen-pesticides'] ),
                  'years'        => 2011,
                  'current_year' => 2011,
                  'scale'        => null,
              ];
          }     */

                Cache::set( $cacheKey, $return, $cache[ $descriptor->uid ] );
            } else {
                $return = Cache::get( $cacheKey );
            }
        }
        Debugbar::stopMeasure( 'Data' );

        return $return;
    }

    /**
     * Load Hive Scales Data, to be displayed on the Map.
     *
     * @param Descriptor $descriptor
     *
     * @return array
     * @throws Throwable
     */
    protected function loadPollenContaminationData( Descriptor $descriptor ): array
    {
        $projects = Project::whereIn( 'id', [ Project::getIdFromUid( 'experimental-approach-to-unexplained-mortality-of-bee-colonies-in-wallonia' ) ] )
                           ->with( [ 'logs', 'logs.externalIds', 'logs.apiaryIds', 'logs.project.providers' ] )
                           ->get();
        $logs     = $projects->pluck( 'logs' )->flatten()->groupBy( 'apiary_id' );

        $data = [];
        foreach ( $logs as $log ) {
            /** @var Data $dataItem */
            $dataItem   = $log->data->where( 'descriptor_id', $descriptor->id )->first();
            $aggregates = $log->external_id->aggregates->keyBy( 'count' );

            $data[] = [
                'position'    => $log->getCoordinates(),
                'id'          => $log->project_id . '_' . $log->id,
                'tooltip'     => isset( $aggregates[3] ) ? $aggregates[3]->getTooltipText() : $log->getTooltipText( $descriptor ),
                'icon'        => implode( ' ', isset( $aggregates[3] ) ? $aggregates[3]->getIconClasses( $min, $max, 'range_diverging' ) : $dataItem->data->getIconClasses( 0, 0, 'invalid' ) ),
                'attribution' => $log->getTextSources(),
                //'color'       => isset( $aggregates[3] ) ? $aggregates[3]->getDiscreteValue( $min, $max ) : 0,
                'content'     => new HiveWeightContent( $descriptor, $log ),
                'connectors'  => $log->getProject(),
                'owners'      => $log->getProviders(),
            ];
        }

        return $data;
    }
}
