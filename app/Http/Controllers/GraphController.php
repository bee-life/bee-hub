<?php

namespace App\Http\Controllers;

use App\Console\Commands\ChartCacheCommand;
use App\Models\MetaData\Descriptor;
use Cache;
use Illuminate\Http\Request;
use Throwable;

class GraphController extends Controller
{
    /**
     * Base entry point for Map queries.
     *
     * @param Request $request
     *
     * @return array
     * @throws Throwable
     * @see ChartCacheCommand for data procesing and caching functions.
     *
     */
    public function get( Request $request )
    {
        $request->validate( [
            'descriptor_id' => 'required|string',
        ] );

        $data       = $request->all();
        $cache_key  = null;
        $descriptor = Descriptor::findOrFail( $data['descriptor_id'] );

        switch ( $descriptor->uid ) {
            case 'hive-weight':
            case 'pollen-pesticides':
            case 'temperature-daily':
            case 'relative-humidity':
            case 'rain':
            case 'solar-radiation':
            case 'wind-speed':
            case 'hive-temperature-daily':
            case 'hive-relative-humidity':
            case 'hive-production':
            case 'hive-population':
            case 'brood-cells':
            case 'beebread-cells':
            case 'hive-metabolic-resting-state':
            case 'maximum-available-flight-time':
                $request->validate( [ 'object_id' => 'required|int' ] );
                $cache_key = "graph-{$descriptor->uid}-{$data['object_id']}";
                break;
        }

        if ( isset( $cache_key ) && Cache::has( $cache_key ) ) {
            return Cache::get( $cache_key );
        }
        abort(503, 'Missed Cache for ' . (string) json_encode($data));
    }

    /**
     * Base entry point for Event queries.
     *
     * @param Request $request
     *
     * @return array
     * @throws Throwable
     *@see ChartCacheCommand for data procesing and caching functions.
     *
     */
    public function getEvents( Request $request )
    {
        $request->validate( [
            'object_id' => 'required|int',
        ] );

        $data       = $request->all();
        $cache_key  = "graph-events-{$data['object_id']}";

        if ( isset( $cache_key ) && Cache::has( $cache_key ) ) {
            return Cache::get( $cache_key );
        }
        abort(503, 'Missed Cache for ' . (string) json_encode($data));
    }
}
