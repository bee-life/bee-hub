<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AccountController extends Controller
{
    /**
     *
     * @var User
     */
    protected User $user;

    /**
     * PageController constructor.
     * Laravel Framework autoloads these repositories. Add them here, if you need any other.
     *
     */
    function __construct()
    {
    }

    /**
     * View a static page, defined in database
     *
     * @param string $slug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProfile()
    {
        return view( 'management.profile', [ 'user' => Auth::user() ] );
    }

    public function postAccountSettings( Request $request )
    {
        $validated = $request->validate( [
            'user-name'  => 'required|string|max:200',
            'user-email' => [
                'required',
                'string',
                'email',
                'max:100',
                Rule::unique( 'users', 'email' )->ignore( Auth::id() )
            ],
        ] );

        $user = Auth::user();

        $user->name = $validated['user-name'];
        if ( $user->email != $validated['user-email'] ) {
            $user->new_email = $validated['user-email'];
        }

        $user->save();

        return redirect()->route( 'user-account' )->with( 'action', 'account-updated' );
    }

    public function postPasswordSettings( Request $request )
    {
        $validated = $request->validate( [
            'password'           => 'required|string|confirmed',
        ] );

        return redirect()->route( 'user-account' )->with( 'action', 'password-updated' );
    }
}
