<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use App\Models\Traits\Base;
use App\Repositories\BaseRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

abstract class ResourceController extends Controller
{
    /**
     * @var BaseRepository
     */
    protected BaseRepository $repository;

    /**
     * @var string
     */
    protected string $baseView;


    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $data = [
            'items' => $this->repository->all(),
        ];

        return view( 'management.' . $this->baseView . '.index', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view( 'management.' . $this->baseView . '-edit' );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response|View
     */
    public function show( int $id ): View
    {
        $data = [
            'model' => $this->repository->find( $id )
        ];

        return view( 'management.' . $this->baseView . '.single', $data );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response|View
     */
    public function edit( int $id ): View
    {
        $data = [
            'id'   => $id,
            'model' => $this->repository->find( $id )
        ];

        return view( 'management.' . $this->baseView . '.edit', $data );
    }

    /**
     * Remove the specified resource from trash.
     *
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function destroy( int $id ): RedirectResponse
    {
        $item = $this->repository->find( $id );

        $url = $item::getListUrl();

        $this->repository->destroy( $id );

        return redirect( $url );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    abstract public function store( Request $request ): RedirectResponse;

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return RedirectResponse
     */
    abstract public function update( Request $request, int $id ): RedirectResponse;
}
