<?php

namespace App\Http\Controllers\Management;

use App\Models\MetaData\Project;
use App\Repositories\AuditLogsRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\ProviderRepository;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Storage;

class ProjectsController extends ResourceController
{

    protected string $baseView = 'projects';

    protected ProviderRepository $providers;
    protected AuditLogsRepository $auditLogs;

    /**
     * ProjectsController constructor.
     * Laravel Framework autoloads these repositories. Add them here, if you need any other.
     *
     * @param ProjectRepository $repository
     */
    public function __construct( ProjectRepository $repository, ProviderRepository $providers, AuditLogsRepository $auditLogsRepository )
    {
        $this->repository = $repository;
        $this->providers = $providers;
        $this->auditLogs  = $auditLogsRepository;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $data = [
            'providers' => '',
        ];

        return view( 'management.' . $this->baseView . '-edit', $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function store( Request $request ): RedirectResponse
    {
        abort( 501 );
        return redirect();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return RedirectResponse
     */
    public function update( Request $request, int $id ): RedirectResponse
    {
        /** @var Project $project */
        $project = Auth::user()->projects()->find( $id );

        if ( empty( $project ) ) {
            abort( '403' );
        }
        $validated = $request->validate( [
            'project-name'        => 'required|string|max:200',
            'project-acronym'     => 'nullable|string|max:50',
            'project-description' => 'nullable|string|max:20000',
            'project-start-date'  => 'nullable|date_format:Y-m-d|before:project-end-date',
            'project-end-date'    => 'nullable|date_format:Y-m-d|after:project-start-date',
            'project-logo'        => 'nullable|image|max:2048',
            'project-website'     => 'nullable|string|url|max:100',
            'project-email'       => 'nullable|string|email|max:100',
            'project-phone'       => 'nullable|string|max:20',
            'action'              => 'required|string|in:update',
        ] );

        $data = [
            'name'           => $request->input( 'project-name' ),
            'acronym'        => $request->input( 'project-acronym', null ),
            'description'    => $request->input( 'project-description', null ),
            'website'        => $request->input( 'project-website', null ),
            'email'          => $request->input( 'project-email', null ),
            'phone'          => $request->input( 'project-phone', null ),
        ];


        if ( ! empty( $validated['project-logo'] ) ) {
            Storage::delete( $project->featured_image );

            $data['featured_image'] = Storage::putFile( 'public', $validated['project-logo'] );
        }

        $this->repository->update( $project, $data );

        //Artisan::call( 'sitemap:generate' );

        return redirect()->route('projects.edit', $project);
    }
}
