<?php

namespace App\Http\Controllers\Management;

use App\Models\MetaData\Project;
use App\Repositories\AuditLogsRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\ProviderRepository;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Storage;

class ProvidersController extends ResourceController
{

    protected string $baseView = 'providers';

    protected AuditLogsRepository $auditLogs;
    protected ProjectRepository $projects;

    /**
     * ProvidersController constructor.
     * Laravel Framework autoloads these repositories. Add them here, if you need any other.
     *
     * @param ProviderRepository $repository
     */
    public function __construct( ProviderRepository $repository, ProjectRepository $projects, AuditLogsRepository $auditLogsRepository )
    {
        $this->repository = $repository;
        $this->projects   = $projects;
        $this->auditLogs  = $auditLogsRepository;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $data = [
            'projects' => $this->projects->all(),
        ];

        return view( 'management.' . $this->baseView . '-edit', $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function store( Request $request ): RedirectResponse
    {
        abort( 501 );

        return redirect();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return RedirectResponse
     */
    public function update( Request $request, int $id ): RedirectResponse
    {
        /** @var Project $model */
        $model = Auth::user()->providers()->find( $id );

        if ( empty( $model ) ) {
            abort( '403' );
        }

        $validated = $request->validate( [
            'full-name'       => 'nullable|string|max:200',
            'name'      => 'nullable|string|max:50',
            'address'         => 'nullable|string|max:20000',
            'registry-number' => 'nullable|string|max:20',
            'description'     => 'nullable|string|max:20000',
            'logo'            => 'nullable|image|max:2048',
            'website'         => [ 'nullable', 'string', 'max:100' ],
            'email'           => 'nullable|string|email|max:100',
            'phone'           => 'nullable|string|max:20',

            'action' => 'required|string|in:update',
        ] );
        $data      = [
            'full-name'       => $request->input( 'full-name' ),
            'name'            => $request->input( 'name', null ),
            'address'         => $request->input( 'address', null ),
            'registry-number' => $request->input( 'registry-number', null ),
            'description'     => $request->input( 'description', null ),
            'website'         => $request->input( 'website', null ),
            'email'           => $request->input( 'email', null ),
            'phone'           => $request->input( 'phone', null ),
        ];


        if ( ! empty( $validated['logo'] ) ) {
            Storage::delete( $model->featured_image );

            $data['featured_image'] = Storage::putFile( 'public', $validated['logo'] );
        }

        $this->repository->update( $model, $data );

        //Artisan::call( 'sitemap:generate' );

        return redirect()->route( 'providers.edit', $model );
    }
}
