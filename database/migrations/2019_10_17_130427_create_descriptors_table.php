<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Descriptor meta data.
        Schema::create( 'descriptors', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'name' );
            $table->string( 'slug' )->unique();
            $table->text( 'description' )->nullable()->change();
            $table->unsignedBigInteger( 'category_id' )->nullable();
            $table->string( 'table_name' );
            $table->string( 'description' );
            $table->string( 'featured_image', 512 )->nullable();
            $table->string( 'icon', 512 )->nullable();
            $table->string( 'since', 10 );
            $table->boolean( 'deprecated' )->default( false );
            $table->boolean( 'public' )->default( true );
            $table->string( 'uid', 100 )->unique();
            $table->timestamps();

            $table->foreign( 'category_id' )->references( 'id' )->on( 'descriptor_categories' );
        } );

        Schema::create( 'dataset_to_descriptor', function ( Blueprint $table ) {
            $table->unsignedBigInteger( 'descriptor_id' );
            $table->unsignedBigInteger( 'dataset_id' );

            $table->foreign( 'descriptor_id' )->references( 'id' )->on( 'descriptors' )->onDelete( 'cascade' );
            $table->foreign( 'dataset_id' )->references( 'id' )->on( 'datasets' )->onDelete( 'cascade' );
            $table->unique( [ 'descriptor_id', 'dataset_id' ] );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists( 'descriptors' );
        Schema::dropIfExists( 'project_to_descriptor' );
    }
};
