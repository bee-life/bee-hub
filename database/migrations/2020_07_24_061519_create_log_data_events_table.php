<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'log_data_events', function ( Blueprint $table ) {
            $table->id();
            $table->decimal( 'value', 40, 20 )->nullable();
            $table->string( 'unit', 10 )->nullable();
            $table->text( 'description' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'log_data_events' );
    }
};
