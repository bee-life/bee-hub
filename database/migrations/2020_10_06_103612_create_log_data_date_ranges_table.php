<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'log_data_date_ranges', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->date( 'value_from' );
            $table->date( 'value_to' );

            $table->index( [ 'value_from', 'value_to' ] );
        } );

        Schema::create( 'log_data_datetime_ranges', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->dateTime( 'value_from' );
            $table->dateTime( 'value_to' );

            $table->index( [ 'value_from', 'value_to' ] );
        } );

        Schema::create( 'log_data_time_ranges', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->time( 'value_from' );
            $table->time( 'value_to' );

            $table->index( [ 'value_from', 'value_to' ] );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'log_data_date_ranges' );
        Schema::dropIfExists( 'log_data_datetime_ranges' );
        Schema::dropIfExists( 'log_data_time_ranges' );
    }
};
