<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'regions', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'name', 120 );
            $table->unsignedBigInteger( 'country_id' );
            $table->string( 'nuts_id', 5 );
            $table->smallInteger( 'nuts_level' )->default( 0 );
            $table->unsignedBigInteger( 'parent_id' )->nullable();
            $table->bigInteger( 'population' )->nullable();
            $table->bigInteger( 'surface' )->nullable();
            $table->point( 'center' )->nullable();
            $table->geometry( 'area' )->nullable();


            $table->index( 'nuts_level' );
            $table->unique( 'nuts_id' );
            $table->foreign( 'parent_id' )->references( 'id' )->on( 'regions' );
            $table->foreign( 'country_id' )->references( 'id' )->on( 'countries' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'regions' );
    }
};
