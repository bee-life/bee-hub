<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'log_to_data', function ( Blueprint $table ) {
            $table->unsignedBigInteger( 'log_id' );
            $table->unsignedBigInteger( 'descriptor_id' );

            $table->unsignedBigInteger( 'data_id' );
            $table->string( 'data_type', 2 );

            $table->unsignedBigInteger( 'origin_id' )->nullable();
            $table->string( 'origin_type', 1 )->nullable();

            $table->foreign( 'log_id' )->references( 'id' )->on( 'logs' )->onDelete( 'cascade' );
            $table->foreign( 'descriptor_id' )->references( 'id' )->on( 'descriptors' )->onDelete( 'cascade' );
            $table->index( [ 'data_id', 'data_type' ] );
            $table->index( [ 'origin_id', 'origin_type' ] );
            $table->index( [ 'descriptor_id', 'data_id', 'data_type' ] );
            $table->unique( [ 'log_id', 'descriptor_id', 'data_id', 'data_type' ] );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'log_to_data' );
    }
};
