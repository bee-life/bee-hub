<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'log_data_dates', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->date( 'value' );

            $table->index( [ 'value' ] );
        } );

        Schema::create( 'log_data_datetimes', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->dateTime( 'value' );

            $table->index( [ 'value' ] );
        } );

        Schema::create( 'log_data_times', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->time( 'value' );

            $table->index( [ 'value' ] );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'log_data_dates' );
        Schema::dropIfExists( 'log_data_datetimes' );
        Schema::dropIfExists( 'log_data_times' );
    }
};
