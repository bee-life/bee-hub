<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'posts', function ( Blueprint $table ) {
            $table->foreign( 'lau_id' )->references( 'lau_id' )->on( 'laus' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'posts', function ( Blueprint $table ) {
            $table->dropForeign( [ 'lau_id' ] );
        } );
    }
};
