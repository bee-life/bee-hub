<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'datasets', function ( Blueprint $table ) {
            $table->id();
            $table->string( 'name' );
            $table->string( 'uid', 100 )->unique();
            $table->string( 'slug' )->unique();
            $table->text( 'description' )->nullable();
            $table->string( 'type', 16 );
            $table->string( 'status', 16 );
            $table->boolean( 'active' );
            $table->dateTime( 'last_synced_at' )->nullable();
            $table->string( 'sync_period' )->nullable();
            $table->boolean( 'historic_sync' )->nullable();
            $table->unsignedBigInteger('licence_id');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign( 'licence_id' )->references( 'id' )->on( 'licences' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'datasets' );
    }
};
