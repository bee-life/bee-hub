<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'origin_vendors', function ( Blueprint $table ) {
            $table->id();
            $table->string( 'name' );
            $table->string( 'slug', 191 )->unique();
            $table->string( 'uid', 100 )->unique();
            $table->text( 'description' )->nullable();
            $table->string( 'featured_image', 512 )->nullable();
            $table->string( 'icon', 512 )->nullable();
            $table->string( 'website' )->nullable();
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'origin_vendors' );
    }
};
