<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'laus', function ( Blueprint $table ) {
            $table->id();
            $table->string( 'name', 150 );
            $table->string( 'lau_id', 13 );
            $table->bigInteger( 'population' )->nullable();
            $table->bigInteger( 'surface' )->nullable();
            $table->string( 'nuts_id', 5 );
            $table->unsignedBigInteger( 'country_id' );
            $table->point( 'center' )->nullable();
            $table->geometry( 'area' )->nullable();

            $table->foreign( 'country_id' )->references( 'id' )->on( 'countries' );
            $table->foreign( 'nuts_id' )->references( 'nuts_id' )->on( 'regions' );
            $table->unique( [ 'lau_id', 'nuts_id' ] );
            $table->unique( [ 'lau_id', 'country_id' ] );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'laus' );
    }
};
