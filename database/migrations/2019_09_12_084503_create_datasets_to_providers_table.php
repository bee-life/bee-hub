<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'dataset_to_provider', function ( Blueprint $table ) {
            $table->unsignedBigInteger( 'provider_id' );
            $table->string( 'provider_type', 2 );
            $table->unsignedBigInteger( 'dataset_id' );
            $table->string( 'type', 50 );

            $table->foreign( 'dataset_id' )->references( 'id' )->on( 'datasets' )->onDelete( 'cascade' )->onDelete( 'cascade' );
            $table->unique( [ 'dataset_id', 'provider_id', 'provider_type' ] );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'dataset_to_provider' );
    }
};
