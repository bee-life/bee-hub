<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'users_to_datasets', function ( Blueprint $table ) {
            $table->unsignedBigInteger( 'user_id' );
            $table->unsignedBigInteger( 'dataset_id' );
            $table->string('status')->default('pending-verification');
            $table->string('integration_contact', 100);
            $table->string('integration_email', 100);
            $table->string('share_conditions', 20);
            $table->text('metadata')->nullable();
            $table->timestamps();

            $table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
            $table->foreign( 'dataset_id' )->references( 'id' )->on( 'datasets' );

            $table->unique( [ 'user_id', 'dataset_id' ] );
        } );
        Schema::create( 'users_to_providers', function ( Blueprint $table ) {
            $table->unsignedBigInteger( 'user_id' );
            $table->unsignedBigInteger( 'provider_id' );
            $table->string('status')->default('pending-verification');
            $table->timestamps();

            $table->unique( [ 'user_id', 'provider_id' ] );

            $table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
            $table->foreign( 'provider_id' )->references( 'id' )->on( 'providers' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'users_to_datasets' );
        Schema::dropIfExists( 'users_to_providers' );

        Schema::table( 'projects', function ( Blueprint $table ) {
            $table->dropColumn( 'sharing_type' );
        } );
    }
};
