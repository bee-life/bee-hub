<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'attachments', function ( Blueprint $table ) {
            $table->id();
            $table->string( 'name' );
            $table->text( 'description' )->nullable();
            $table->string( 'slug' )->unique();
            $table->string( 'mimetype', 50 );
            $table->unsignedBigInteger( 'attachment_id' );
            $table->string( 'attachment_type' );
            $table->timestamps();

            $table->index( [ 'attachment_id', 'attachment_type' ] );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'attachments' );
    }
};
