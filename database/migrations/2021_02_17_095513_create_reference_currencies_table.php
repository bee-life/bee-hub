<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'reference_currencies', function ( Blueprint $table ) {
            $table->id();
            $table->string( 'name' );
            $table->string( 'code' )->unique();
            $table->integer( 'number' )->unique();
            $table->integer( 'decimals' )->nullable();
            $table->string( 'symbol', 5 )->nullable();
            $table->boolean( 'symbol_prefix' )->default( true );
            $table->timestamps();
        } );

        Schema::create( 'log_reference_currencies', function ( Blueprint $table ) {
            $table->id();
            $table->decimal( 'value', 25, 8 );
            $table->unsignedBigInteger( 'reference_id' );

            $table->foreign( 'reference_id' )->references( 'id' )->on( 'reference_currencies' );
            $table->index( [ 'value', 'reference_id' ] );
        } );

        Schema::table( 'countries', function ( Blueprint $table ) {
            $table->unsignedBigInteger( 'currency_id' )->nullable();

            $table->foreign( 'currency_id' )->references( 'id' )->on( 'reference_currencies' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'log_reference_currencies' );
        Schema::table( 'countries', function ( Blueprint $table ) {
            $table->dropForeign( [ 'currency_id' ] );
            $table->dropColumn( 'currency_id' );
        } );

        Schema::dropIfExists( 'reference_currencies' );


    }
};
