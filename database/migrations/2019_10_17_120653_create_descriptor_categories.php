<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'descriptor_categories', function ( Blueprint $table ) {
            $table->id();
            $table->string( 'name', 255 );
            $table->string( 'slug', 191 )->unique();
            $table->string( 'uid', 100 )->unique();
            $table->text( 'description' )->nullable();
            $table->unsignedBigInteger( 'parent_id' )->nullable();
            $table->string( 'featured_image', 512 )->nullable();
            $table->string( 'icon', 512 )->nullable();
            $table->timestamps();

            $table->foreign( 'parent_id' )->references( 'id' )->on( 'descriptor_categories' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'descriptor_categories' );
    }
};
