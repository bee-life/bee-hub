<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'countries', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'name', 50 );
            $table->string( 'country_code', 2 );
            $table->string( 'iso3', 3 );
            $table->string( 'native_name', 100 )->nullable();
            $table->string( 'capital', 30 )->nullable();
            $table->string( 'continent', 2 )->nullable();
            $table->string( 'currency_code', 3 )->nullable();
            $table->string( 'currency_name', 20 )->nullable();
            $table->string( 'phone_prefix', 20 )->nullable();

            $table->unique( 'country_code' );
            $table->unique( 'iso3' );
            $table->index( 'continent' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'countries' );
    }
};
