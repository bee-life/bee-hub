<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'posts', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'number', 15 );
            $table->string( 'name', 80 )->nullable();
            $table->unsignedBigInteger( 'country_id' );
            $table->string( 'nuts_id', 5 );
            $table->string( 'lau_id', 13 )->nullable();
            $table->bigInteger( 'population' )->nullable();
            $table->bigInteger( 'surface' )->nullable();
            $table->point( 'center' )->nullable();
            $table->geometry( 'area' )->nullable();


            $table->foreign( 'nuts_id' )->references( 'nuts_id' )->on( 'regions' );
            $table->unique( [ 'number', 'country_id' ] );
            $table->foreign( 'country_id' )->references( 'id' )->on( 'countries' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'posts' );
    }
};
