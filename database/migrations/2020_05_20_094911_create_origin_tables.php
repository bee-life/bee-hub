<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'origin_methodologies', function ( Blueprint $table ) {
            $table->id();
            $table->string( 'name' );
            $table->string( 'slug', 191 )->unique();
            $table->string( 'uid', 100 )->unique();
            $table->text( 'description' )->nullable();
            $table->string( 'featured_image', 512 )->nullable();
            $table->string( 'icon', 512 )->nullable();
            $table->unsignedBigInteger( 'category_id' );
            $table->timestamps();

            $table->foreign( 'category_id' )->references( 'id' )->on( 'origin_methodology_categories' );
        } );
        Schema::create( 'origin_devices', function ( Blueprint $table ) {
            $table->id();
            $table->string( 'name' );
            $table->string( 'slug', 191 )->unique();
            $table->string( 'uid', 100 )->unique();
            $table->text( 'description' )->nullable();
            $table->string( 'featured_image', 512 )->nullable();
            $table->string( 'icon', 512 )->nullable();
            $table->unsignedBigInteger( 'category_id' );
            $table->unsignedBigInteger( 'vendor_id' );
            $table->timestamp('introduced_at')->nullable();
            $table->timestamps();

            $table->foreign( 'category_id' )->references( 'id' )->on( 'origin_device_categories' );
            $table->foreign( 'vendor_id' )->references( 'id' )->on( 'origin_vendors' );
        } );

        Schema::create( 'origin_publications', function ( Blueprint $table ) {
            $table->id();
            $table->string( 'title', 512 );
            $table->string( 'slug' )->unique();
            $table->string( 'uid', 100 )->unique();
            $table->string( 'author', 512 );
            $table->string( 'author_short', 100 );
            $table->year( 'year' );
            $table->string( 'medium', 512 )->nullable();
            $table->string( 'medium_short', 100 )->nullable();
            $table->string( 'editor', 512 )->nullable();
            $table->string( 'editor_short', 100 )->nullable();

            $table->string( 'issue', 100 )->nullable();
            $table->string( 'volume', 100 )->nullable();
            $table->string( 'edition', 100 )->nullable();
            $table->string( 'pages', 100 )->nullable();
            $table->string( 'website', 512 )->nullable();
            $table->date( 'website_date' )->nullable();
            $table->string( 'publication_type', 20 );

            $table->timestamps();
        } );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'origin_methodologies' );
        Schema::dropIfExists( 'origin_devices' );
        Schema::dropIfExists( 'origin_publications' );
    }
};
