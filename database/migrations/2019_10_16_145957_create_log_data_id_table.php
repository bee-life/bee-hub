<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'log_data_ids', function ( Blueprint $table ) {
            $table->id();
            $table->unsignedBigInteger( 'dataset_id' );
            $table->string( 'value' );
            $table->unsignedBigInteger( 'parent_id' )->nullable();

            $table->foreign( 'dataset_id' )->references( 'id' )->on( 'datasets' )->onDelete( 'cascade' );
            $table->foreign( 'parent_id' )->references( 'id' )->on( 'log_data_ids' )->onDelete( 'set null' );
            $table->unique( [ 'dataset_id', 'value' ] );
            $table->index( [ 'dataset_id', 'value', 'parent_id' ] );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'log_data_ids' );
    }
};
