<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reference_crop_pollination_dependency', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('crop_species_id');
            $table->string('impact');
            $table->decimal('dependency_from')->nullable();
            $table->decimal('dependency_to')->nullable();
            $table->timestamps();

            $table->foreign( 'crop_species_id' )->references( 'id' )->on( 'reference_crop_species' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reference_crop_pollination_dependency');
    }
};
