<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'logs', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->year( 'year' );
            $table->date( 'date' )->nullable();
            $table->time( 'time' )->nullable();
            $table->unsignedBigInteger( 'dataset_id' );
            $table->unsignedBigInteger( 'apiary_id' )->nullable();
            $table->unsignedBigInteger( 'hive_id' )->nullable();
            $table->timestamp( 'created_at' )->useCurrent();

            $table->foreign( 'dataset_id' )->references( 'id' )->on( 'datasets' )->onDelete( 'cascade' );
            $table->foreign( 'apiary_id' )->references( 'id' )->on( 'log_data_ids' )->onDelete( 'cascade' );
            $table->foreign( 'hive_id' )->references( 'id' )->on( 'log_data_ids' )->onDelete( 'cascade' );
            $table->index( 'year' );
            $table->index( 'date' );
            $table->index( [ 'date', 'time' ] );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'logs' );
    }
};
