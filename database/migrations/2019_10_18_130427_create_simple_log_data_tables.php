<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Simple Datatypes:
        Schema::create( 'log_data_booleans', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->boolean( 'value' );
        } );

        Schema::create( 'log_data_integers', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->bigInteger( 'value' );

            $table->index( [ 'value' ] );
        } );

        Schema::create( 'log_data_decimals', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->decimal( 'value', 40, 20 );

            $table->index( [ 'value' ] );
        } );

        Schema::create( 'log_data_strings', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'value', 200 );

            $table->index( [ 'value' ] );
        } );

        // More complex Datatypes:
        Schema::create( 'log_data_integer_ranges', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->bigInteger( 'value_from' )->nullable();
            $table->bigInteger( 'value_to' )->nullable();

            $table->index( [ 'value_from', 'value_to' ] );
        } );

        Schema::create( 'log_data_decimal_ranges', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->decimal( 'value_from', 40, 20 )->nullable();
            $table->decimal( 'value_to', 40, 20 )->nullable();

            $table->index( [ 'value_from', 'value_to' ] );
        } );

        Schema::create( 'log_data_percentages', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->decimal( 'value', 12, 5 );

            $table->index( [ 'value' ] );
        } );

        Schema::create( 'log_data_locations', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->point( 'value' )->nullable();
            $table->unsignedBigInteger( 'location_id' )->nullable();
            $table->string( 'location_type' )->nullable();

            //$table->index( [ 'value', 'location_id', 'location_type' ] );
            $table->index( [ 'location_id', 'location_type' ] );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'log_data_booleans' );
        Schema::dropIfExists( 'log_data_integers' );
        Schema::dropIfExists( 'log_data_decimals' );
        Schema::dropIfExists( 'log_data_strings' );
        Schema::dropIfExists( 'log_data_integer_ranges' );
        Schema::dropIfExists( 'log_data_decimal_ranges' );
        Schema::dropIfExists( 'log_data_percentages' );
        Schema::dropIfExists( 'log_data_locations' );
    }
};
