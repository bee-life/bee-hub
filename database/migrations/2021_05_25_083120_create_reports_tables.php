<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'reports', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'title' );
            $table->string( 'slug' )->unique();
            $table->string( 'author' )->nullable();
            $table->longText( 'abstract' );
            $table->string( 'meta_title' )->nullable();
            $table->string( 'meta_description' )->nullable();
            $table->longText( 'content' );
            $table->dateTime( 'published_at' );
            $table->string( 'featured_image', 512 )->nullable();

            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'reports' );
    }
};
