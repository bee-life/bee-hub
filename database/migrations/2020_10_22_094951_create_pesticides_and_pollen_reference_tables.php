<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'reference_pesticide_types', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'name', 100 );
            $table->string( 'abbreviation', 2 )->unique();
            $table->text( 'description' )->nullable();
            $table->string( 'featured_image', 512 )->nullable();
            $table->string( 'icon', 512 )->nullable();
            $table->boolean( 'deprecated' )->default( false );
            $table->timestamps();
        } );
        Schema::create( 'reference_pesticides', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'substance', 100 );
            $table->string( 'name', 100 );
            $table->text( 'description' )->nullable();
            $table->string( 'featured_image', 512 )->nullable();
            $table->string( 'icon', 512 )->nullable();
            $table->unsignedBigInteger( 'type_id' );
            $table->decimal( 'loq_wax' )->nullable();
            $table->decimal( 'loq_beebread' )->nullable();
            $table->decimal( 'loq_honey' )->nullable();
            $table->decimal( 'loq_pollen' )->nullable();
            $table->boolean( 'deprecated' )->default( false );
            $table->timestamps();

            $table->foreign( 'type_id' )->references( 'id' )->on( 'reference_pesticide_types' );
        } );

        Schema::create( 'reference_pollen', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'name', 100 );
            $table->string( 'type', 100 );
            $table->string( 'family', 100 );
            $table->string( 'code', 10 )->index();
            $table->text( 'description' )->nullable();
            $table->string( 'featured_image', 512 )->nullable();
            $table->string( 'icon', 512 )->nullable();
            $table->decimal( 'p' )->nullable();
            $table->decimal( 'q' )->nullable();
            $table->decimal( 'vol' )->nullable();
            $table->decimal( 'r' )->nullable();
            $table->decimal( 'vol_r' )->nullable();
            $table->boolean( 'deprecated' )->default( false );
            $table->timestamps();
        } );
        Schema::create( 'reference_land_usages', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'name', 200 );
            $table->string( 'group', 100 );
            $table->integer( 'sigec_code' )->nullable();
            $table->text( 'description' )->nullable();
            $table->string( 'featured_image', 512 )->nullable();
            $table->string( 'icon', 512 )->nullable();
            $table->decimal( 'dim_min_res' )->nullable();
            $table->decimal( 'dim_max_res' )->nullable();
            $table->decimal( 'pyr_min_res' )->nullable();
            $table->decimal( 'pyr_max_res' )->nullable();
            $table->decimal( 'bos_min_res' )->nullable();
            $table->decimal( 'bos_max_res' )->nullable();
            $table->string( 'source', 100 );
            $table->boolean( 'deprecated' )->default( false );
            $table->timestamps();
        } );

        Schema::create( 'log_reference_pesticides', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->decimal( 'value' )->nullable();
            $table->unsignedBigInteger( 'reference_id' );

            $table->foreign( 'reference_id' )->references( 'id' )->on( 'reference_pesticides' )->onDelete( 'cascade' );
            $table->index( [ 'value', 'reference_id' ] );
        } );
        Schema::create( 'log_reference_pollen', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->integer( 'amount' )->nullable();
            $table->decimal( 'fraction' )->nullable();
            $table->unsignedBigInteger( 'reference_id' );

            $table->foreign( 'reference_id' )->references( 'id' )->on( 'reference_pollen' )->onDelete( 'cascade' );
            $table->index( [ 'amount', 'fraction', 'reference_id' ] );
        } );
        Schema::create( 'log_reference_land_usages', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->integer( 'surface' )->nullable();
            $table->integer( 'radius' )->nullable();
            $table->unsignedBigInteger( 'reference_id' );

            $table->foreign( 'reference_id' )->references( 'id' )->on( 'reference_land_usages' )->onDelete( 'cascade' );
            $table->index( [ 'surface', 'radius', 'reference_id' ] );
        } );

        Schema::create( 'reference_crop_species', function ( Blueprint $table ) {
            $table->id();
            $table->string( 'name', 100 );
            $table->text( 'description' )->nullable();
            $table->string( 'featured_image', 512 )->nullable();
            $table->string( 'icon', 512 )->nullable();
            $table->string( 'species_name', 100 )->nullable();
            $table->string( 'gbif_id', 10 )->nullable();

            $table->unsignedBigInteger( 'parent_id' )->nullable();
            $table->foreign( 'parent_id' )->references( 'id' )->on( 'reference_crop_species' );

            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'log_data_pesticides' );
        Schema::dropIfExists( 'log_data_pollen' );
        Schema::dropIfExists( 'log_data_land_usages' );

        Schema::dropIfExists( 'reference_pesticides' );
        Schema::dropIfExists( 'reference_pollen' );
        Schema::dropIfExists( 'reference_land_usages' );

        Schema::dropIfExists( 'reference_pesticide_types' );
        Schema::dropIfExists( 'reference_crop_species' );
    }
};
