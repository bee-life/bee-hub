<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'contacts', function ( Blueprint $table ) {
            $table->id();
            $table->unsignedBigInteger( 'related_id' );
            $table->string( 'related_type' );
            $table->string( 'type' );
            $table->string( 'value' );
            $table->timestamps();

            $table->index( [ 'related_id', 'related_type' ] );
            $table->index( [ 'related_id', 'related_type', 'type' ] );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'provider_contacts' );
    }
};
