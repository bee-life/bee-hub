<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'providers', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'full_name' );
            $table->string( 'slug' )->unique();
            $table->string( 'name' )->nullable();
            $table->string( 'registry_number' )->nullable();
            $table->text( 'description' )->nullable();
            $table->string( 'featured_image', 512 )->nullable();
            $table->boolean( 'public' );
            $table->string( 'uid', 100 )->unique();
            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'providers' );
    }
};
