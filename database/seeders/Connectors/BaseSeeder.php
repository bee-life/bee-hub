<?php namespace Database\Seeders\Connectors;

use App\Exceptions\InvalidValueException;
use App\Models\Logs\DescriptorCategory;
use App\Models\Logs\Log;
use App\Models\Origins\Device;
use App\Models\Origins\DeviceCategory;
use App\Models\Origins\Methodology;
use App\Models\Origins\MethodologyCategory;
use App\Models\Origins\Vendor;
use App\Models\Project;
use App\Models\Provider;
use App\Models\Logs\Descriptor;
use Cache;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Str;
use Throwable;

abstract class BaseSeeder extends Seeder
{
    /**
     * @var Collection|Provider[]
     */
    protected $providers;
    /**
     * @var Project
     */
    protected $project;
    /**
     * @var Collection|Descriptor[]
     */
    protected $descriptors;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        stop_logging();
        ini_set( 'memory_limit', '2GB' );

        // Clear any caches, related to data
        Cache::clear();
        $this->descriptors = collect();

        try {
            DB::transaction( function () {
                $this->runSeeder();
            } );
        } catch ( Throwable $e ) {
            $this->command->info( 'There was an error while seeding the database using ' . static::class . '.' );
            $this->command->info( $e->getMessage() );
            $this->command->info( $e->getFile() . ' on line ' . $e->getLine() );
            $this->command->info( $e->getTraceAsString() );
        }
    }

    /**
     * Seed the application's database.
     *
     * @return void
     * @throws InvalidValueException
     */
    abstract protected function runSeeder(): void;


    /**
     * Create a new Connector, or remove existing data if it already exists.
     *
     * @param array $data
     */
    protected function createProject( array $data ): void
    {
        if ( empty( $data['slug'] ) && ! empty( $data['name'] ) ) {
            $data['slug'] = Str::slug( $data['name'] );
        }

        if ( empty( $data['uid'] ) ) {
            $data['uid'] = $data['slug'];
        }

        try {
            $this->project = Project::where( 'uid', $data['uid'] )->firstOrFail();
            $this->project->logs()->delete();
        } catch ( ModelNotFoundException $e ) {
            $this->project = Project::create( $data );
        }
    }

    /**
     * Check if connector already exists
     *
     * @param string $uid
     *
     * @return bool
     */
    protected function projectExists( string $uid ): bool
    {
        return Project::where( 'uid', $uid )->exists();
    }

    /**
     * Create new Datatypes based on inputted array.
     * Datatypes which slug already exists is just added to the connector.
     *
     * @param array       $data
     * @param string|null $version
     */
    protected function createDescriptors( array $data, string $version = null ): void
    {
        $existingDescriptors = Descriptor::all()->keyBy( 'uid' );
        $categories          = DescriptorCategory::all()->keyBy( 'uid' );

        foreach ( $data as $item ) {
            if ( empty( $item['uid'] ) ) {
                $item['uid'] = $item['slug'];
            }
            if ( ! empty( $item['category'] ) && ! empty( $categories[ $item['category'] ] ) ) {
                $item['category_id'] = $categories[ $item['category'] ]->id;
                unset( $item['category'] );
            }


            if ( empty( $existingDescriptors[ $item['uid'] ] ) ) {
                if ( isset( $version ) ) {
                    $item['since'] = $version;
                }
                $this->createdDescriptor( $item );
            } else {
                $this->attachDescriptor( $existingDescriptors[ $item['uid'] ] );
            }
        }
    }

    /**
     * Create new Connector Owners based on inputted array.
     * Owners which name already exists is just added to the connector.
     *
     * @param array $data
     */
    protected function createProviders( array $data ): void
    {
        foreach ( $data as $item ) {
            if ( empty( $item['slug'] ) ) {
                $item['slug'] = Str::slug( $item['full_name'] );
            }

            if ( empty( $item['uid'] ) ) {
                $item['uid'] = $item['slug'];
            }

            try {
                $this->attachProvider( Provider::where( 'uid', $item['uid'] )->firstOrFail() );
            } catch ( ModelNotFoundException $e ) {
                if ( isset( $version ) ) {
                    $item['since'] = $version;
                }
                $this->createProvider( $item );
            }
        }
    }

    /**
     * Create a new Descriptor.
     *
     * @param array $data
     */
    protected function createdDescriptor( array $data ): void
    {
        $this->descriptors[ $data['uid'] ] = Descriptor::create( $data );
        if ( ! empty( $this->project ) ) {
            $this->project->descriptors()->attach( $this->descriptors[ $data['uid'] ]->id );
        }
    }

    /**
     * Attach an existing Descriptor.
     *
     * @param Descriptor $datatype
     */
    protected function attachDescriptor( Descriptor $datatype ): void
    {
        $this->descriptors[ $datatype->uid ] = $datatype;

        if ( ! empty( $this->project ) && ! $this->project->descriptors->contains( $datatype->id ) ) {
            $this->project->descriptors()->attach( $datatype->id );
        }
    }

    /**
     * Create a new Owner.
     *
     * @param array $data
     */
    protected function createProvider( array $data ): void
    {
        $this->providers[ $data['uid'] ] = Provider::create( $data );
        $this->project->providers()->attach( $this->providers[ $data['uid'] ] );
    }

    /**
     * Attach an existing Owner.
     *
     * @param Model|Provider $owner
     */
    protected function attachProvider( Provider $owner ): void
    {
        $this->providers[ $owner->uid ] = $owner;
        if ( ! $this->project->providers->contains( $owner->id ) ) {
            $this->project->providers()->attach( $owner );
        }
    }

    /**
     * Get Vendor representation.
     *
     * @param array $data
     *
     * @return Vendor
     */
    protected function createVendor( array $data )
    {
        if ( empty( $data['slug'] ) ) {
            $data['slug'] = Str::slug( $data['name'] );
        }

        if ( empty( $data['uid'] ) ) {
            $data['uid'] = $data['slug'];
        }

        return Vendor::where( 'uid', $data['uid'] )->firstOrCreate( $data );
    }

    /**
     * Get Device representation.
     *
     * @param array $data
     *
     * @return Device
     */
    protected function createDevice( array $data )
    {
        if ( empty( $data['slug'] ) ) {
            $data['slug'] = Str::slug( $data['name'] );
        }

        if ( empty( $data['uid'] ) ) {
            $data['uid'] = $data['slug'];
        }

        return Device::where( 'uid', $data['uid'] )->firstOrCreate( $data );
    }


    /**
     * Get Device Category representation.
     *
     * @param array $data
     *
     * @return DeviceCategory
     */
    protected function createDeviceCategory( array $data )
    {
        if ( empty( $data['slug'] ) ) {
            $data['slug'] = Str::slug( $data['name'] );
        }

        if ( empty( $data['uid'] ) ) {
            $data['uid'] = $data['slug'];
        }

        return DeviceCategory::where( 'uid', $data['uid'] )->firstOrCreate( $data );
    }

    /**
     * Get a set of Methodology representations.
     *
     * @param array $data
     *
     * @return Methodology[]|Collection
     */
    protected function createMethodologies( array $data ): Collection
    {
        $return = collect();
        foreach ( $data as $item ) {
            $return[] = $this->createMethodology( $item );
        }

        return $return;
    }

    /**
     * Get Methodology representation.
     *
     * @param array $data
     *
     * @return Methodology
     */
    protected function createMethodology( array $data )
    {
        if ( empty( $data['slug'] ) ) {
            $data['slug'] = Str::slug( $data['name'] );
        }

        if ( empty( $data['uid'] ) ) {
            $data['uid'] = $data['slug'];
        }

        return Methodology::where( 'uid', $data['uid'] )->firstOrCreate( $data );
    }


    /**
     * Get Methodology Category representation.
     *
     * @param array $data
     *
     * @return MethodologyCategory
     */
    protected function createMethodologyCategory( array $data )
    {
        if ( empty( $data['slug'] ) ) {
            $data['slug'] = Str::slug( $data['name'] );
        }

        if ( empty( $data['uid'] ) ) {
            $data['uid'] = $data['slug'];
        }

        return MethodologyCategory::where( 'uid', $data['uid'] )->firstOrCreate( $data );
    }

    /**
     * Created a Log based on date string.
     *
     * @param string   $date_string The string representing the date and/or time of logged Log.
     * @param string   $format      Datetime format of the $date_string.
     * @param bool     $time        Optional, set false of exclude time information.
     * @param int|null $apiary_id
     * @param int|null $hive_id
     *
     * @return Log
     */
    protected function createLog( string $date_string, string $format, bool $time = true, int $apiary_id = null, int $hive_id = null ): Log
    {
        if ( $format == 'Y' ) {
            $data = [
                'year'       => $date_string,
                'project_id' => $this->project->id,
            ];
        } else {
            $datetime = Carbon::createFromFormat( $format, $date_string );

            $data = [
                'year'       => $datetime->year,
                'date'       => $datetime->format( 'Y-m-d' ),
                'time'       => $time ? $datetime->format( 'H:i:s' ) : null,
                'project_id' => $this->project->id,
            ];
        }

        $data['apiary_id'] = $apiary_id;
        $data['hive_id']   = $hive_id;

        return Log::create( $data );
    }
}
