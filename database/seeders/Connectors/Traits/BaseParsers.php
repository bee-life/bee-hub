<?php namespace Database\Seeders\Connectors\Traits;

use App\Exceptions\InvalidValueException;


/**
 * Trait BaseParsers
 *
 * Used to parse base Types.
 */
trait BaseParsers
{
    /**
     * Parse Boolean value.
     *
     * @param string  $string
     * @param boolean $nullable
     *
     * @return bool|null
     * @throws InvalidValueException
     */
    protected function parseBoolean( $string, bool $nullable = false ): ?bool
    {
        switch ( strtolower( $string ) ) {
            case 'yes':
            case 'true':
            case '1':
                return true;
            case 'no':
            case 'false':
            case '0':
                return false;
            default:
                if ( $nullable ) {
                    return null;
                } else {
                    throw new InvalidValueException( $string );
                }
        }
    }

    /**
     * Parse Numeric/Integer value.
     *
     * @param string $string
     *
     * @return int|null
     */
    protected function parseInteger( $string ): ?int
    {
        return is_numeric( $string ) ? intval( $string ) : null;
    }

    /**
     * Parse Numeric/Integer value.
     * Function takes care of the correct decimal point.
     *
     * @param string $string
     *
     * @return float|null
     */
    protected function parseFloat( $string ): ?float
    {
        $transformed = str_replace( ',', '.', $string );

        return is_numeric( $transformed ) ? floatval( $transformed ) : null;
    }

    /**
     * Parse percentage.
     * For values outside of range (< 0 and > 100) returns null.
     *
     * @param string $string
     *
     * @return float
     */
    protected function parsePercentage( $string ): ?float
    {
        $value = $this->parseFloat( $string );
        if ( $value >= 0 && $value <= 100 ) {
            return $value;
        } else {
            return null;
        }
    }
}
