<?php namespace Database\Seeders\Connectors\Traits;

use App\Exceptions\InvalidValueException;

/**
 * Trait AdvancedParsers
 *
 * Used to parse advanced Bee data.
 */
trait AdvancedParsers
{
    /**
     * @param string $string
     *
     * @return string
     * @throws InvalidValueException
     */
    protected function parseBreed( $string )
    {
        switch ( strtolower( $string ) ) {
            case 'local':
            case 'local bees':
                return 'local';
            case 'hybrid':
                return 'hybrid';
            case 'buckfast':
                return 'buckfast';
            case 'a. m. adami':
                return 'apis_melliifera_adami';
            case 'a. m. artemisia':
                return 'apis_melliifera_artemisia';
            case 'a. m. carnica':
                return 'apis_melliifera_carnica';
            case 'a. m. caucasia':
            case 'a. m. caucasica':
                return 'apis_melliifera_caucasia';
            case 'a. m. cecropia':
                return 'apis_melliifera_cecropia';
            case 'a. m. ccm':
                return 'apis_melliifera_ccm';
            case 'a. m. iberiensis':
                return 'apis_melliifera_iberiensis';
            case 'a. m. ligustica':
                return 'apis_melliifera_ligustica';
            case 'a. m. macedonica':
                return 'apis_melliifera_macedonica';
            case 'a. m. mellifera':
                return 'apis_melliifera_mellifera';
            case 'a. m. ruttneri':
                return 'apis_melliifera_ruttneri';
            case 'a. m. remipes':
                return 'apis_melliifera_remipes';
            case 'a. m. scutellata':
                return 'apis_melliifera_scutellata';
            case 'a. m. siciliana':
                return 'apis_melliifera_siciliana';
            case 'a. m. sossimai':
                return 'apis_melliifera_sossimai';
            case 'a. m. taurica':
                return 'apis_melliifera_taurica';
            default:
                throw new InvalidValueException( $string );
        }
    }
}
