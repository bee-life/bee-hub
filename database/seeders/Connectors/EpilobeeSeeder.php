<?php namespace Database\Seeders\Connectors;

use App\Data\Connectors\Traits\LocationParser;
use App\Exceptions\InvalidValueException;
use App\Imports\SemicolonCSVImport;
use App\Models\Logs\Log;
use App\Models\Locations\Region;
use Carbon\Carbon;
use Database\Seeders\Connectors\Traits\AdvancedParsers;
use Database\Seeders\Connectors\Traits\BaseParsers;
use Excel;

class EpilobeeSeeder extends BaseSeeder
{
    use LocationParser, BaseParsers, AdvancedParsers;

    /**
     * Seed the application's database.
     *
     * @return void
     * @throws InvalidValueException
     */
    protected function runSeeder(): void
    {
        $this->createProject( [
            'type'           => 'static',
            'name'           => 'Honey bee Winter mortality 2012-2014 - Epilobee analysis',
            'slug'           => 'honey-bee-winter-mortality-2012-2014-epilobee',
            'uid'            => 'epilobee',
            'acronym'        => 'Epilobee',
            'description'    => '<div>EPILOBEE was the first active epidemiological surveillance program implemented in 17 EU Member States, over 2 consecutive years (from autumn 2012 to summer 2014), following a harmonised protocol based on the EU reference laboratory guidelines. EFSA requested a statistical analysis on the EPILOBEE dataset to establish associations between colony mortalities and some factors including disease prevalence, the context of beekeeping and the apiary geographical distribution. The data set published is the result of the data cleaning and categorization performed on the EPILOBEE original dataset regarding winter mortality. The dataset comprises 4758 observations from apiaries across Europe.</div>',
            'website'        => 'https://zenodo.org/record/400232#.XljbzmnPyUl',
            'featured_image' => '93v7pz7TdREWWszPxZKUOA3Q95JijcwMXDIJsKLo.jpeg',
            'class_name'     => null,
            'active'         => true,
            'public'         => true,
            'last_synced_at' => Carbon::now(),
        ] );

        $this->createProviders(
            [
                [
                    'full_name'      => 'ANSES - European Reference Laboratory for Bees',
                    'name'           => 'ANSES',
                    'slug'           => 'anses-european-reference-laboratory-for-bees',
                    'uid'            => 'anses-european-reference-laboratory-for-bees',
                    'description'    => '<div>EPILOBEE List of Authors:<br><br>JACQUES, Antoine, LAURENT, Marion, RIBIERE-CHABERT, Magali, SAUSSAC, Mathilde, BOUGEARD, Stéphanie, HENDRIKX, Pascal, &amp; CHAUZAT, Marie-Pierre. (2016). Honey bee Winter mortality 2012-2014 - Epilobee analysis [Data set]. Zenodo.</div>',
                    'website'        => 'http://doi.org/10.5281/zenodo.400232',
                    'public'         => true,
                    'featured_image' => 'ahJvsDndVM38l5vQy8RFtzFdGolmvseSy5KApyJJ.jpeg',
                ],
            ]
        );

        $this->createDescriptors( [
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'External ID',
                'slug'        => 'external-id',
                'description' => 'External ID',
                'category'    => 'hive',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'Apiary ID',
                'slug'        => 'apiary-id',
                'description' => 'Apiary ID',
                'category'    => 'apiary',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_locations',
                'name'        => 'Colonies location',
                'slug'        => 'colonies-location',
                'description' => 'Where is the colony situated?',
                'category'    => 'environment',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_percentages',
                'name'        => 'Winter mortality',
                'slug'        => 'winter-mortality-percentage',
                'description' => 'Percent of Bee mortality during Winter.',
                'category'    => 'hive-health',
            ],
            [
                'table_name'  => 'log_data_integer_ranges',
                'name'        => 'Beekeepers age range',
                'slug'        => 'beekeepers-age-range',
                'description' => 'What is the age of Beekeepers.',
            ],
            [
                'table_name'  => 'log_data_strings',
                'name'        => 'Beekeepers activity',
                'slug'        => 'beekeepers-activity',
                'description' => 'Are beekeepers working profesionally, part time or as hobby?',
            ],
            [
                'table_name'  => 'log_data_integer_ranges',
                'name'        => 'Beekeeper for Range',
                'slug'        => 'beekeeper-for-range',
                'description' => 'How long is the beekeeper working with bees.',
            ],
            [
                'table_name'  => 'log_data_booleans',
                'name'        => 'Beekeeper Qualifications',
                'slug'        => 'has-beekeeper-qualifications',
                'description' => 'Is beekeeper qualified?',
            ],
            [
                'table_name'  => 'log_data_booleans',
                'name'        => 'Beekeeper Trainings',
                'slug'        => 'has-beekeeper-trainings',
                'description' => 'Had Beekeeper any professional training?',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Total number of colonies',
                'slug'        => 'beehive-population',
                'description' => 'How many colonies does the beekeeper have?',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Apiary Population',
                'slug'        => 'colonies-count',
                'description' => 'How many colonies does individual apiary have?',
            ],
            [
                'table_name'  => 'log_data_strings',
                'name'        => 'Colonies production type',
                'slug'        => 'colonies-production-type',
                'description' => 'What is the primary purpose of the beekeeping?',
            ],
            [
                'table_name'  => 'log_data_booleans',
                'name'        => 'Apiarist book',
                'slug'        => 'has-apiarist-book',
                'description' => 'Does the beekeeper track anything?',
            ],
            [
                'table_name'  => 'log_data_booleans',
                'name'        => 'Organization member',
                'slug'        => 'is-organization-member',
                'description' => 'Does the beekeeper track anything?',
            ],
            [
                'table_name'  => 'log_data_strings',
                'name'        => 'Primary breed name',
                'slug'        => 'primary-breed-name',
                'description' => 'What is the primary breed name being used by beekeeper?',
            ],
            [
                'table_name'  => 'log_data_percentages',
                'name'        => 'Winter mortality',
                'slug'        => 'winter-mortality-aggregate',
                'description' => 'Percent of Bee mortality during Winter.',
                'category'    => 'hive-aggregates',
            ],
            [
                'table_name'  => 'log_data_booleans',
                'name'        => 'Colony chronic depopulation',
                'slug'        => 'has-chronic-depopulation',
                'description' => 'Is the beekeeper experiencing chronic depopulation?',
            ],
            [
                'table_name'  => 'log_data_booleans',
                'name'        => 'Varroa Mites infestation',
                'slug'        => 'has-varroa-infestation',
                'description' => 'Does beekeeper issues with Varroa Mites infestation?',
            ],
            [
                'table_name'  => 'log_data_strings',
                'name'        => 'Colony environment',
                'slug'        => 'colony-environment-types',
                'description' => 'In what kind of environment are the colonies present?',
            ],
            [
                'table_name'  => 'log_data_booleans',
                'name'        => 'American Foulbrood',
                'slug'        => 'has-american-foulbrood',
                'description' => 'Does the colony suffer by American foulbrood?',
            ],
            [
                'table_name'  => 'log_data_booleans',
                'name'        => 'European Foulbrood',
                'slug'        => 'has-european-foulbrood',
                'description' => 'Does the colony suffer by European foulbrood?',
            ],
            [
                'table_name'  => 'log_data_booleans',
                'name'        => 'Chronic Paralysis',
                'slug'        => 'has-chronic-paralysis',
                'description' => 'Does the colony suffer by Chronic paralysis?',
            ],
            [
                'table_name'  => 'log_data_booleans',
                'name'        => 'Nosemosis',
                'slug'        => 'has-nosemosis',
                'description' => 'Does the colony suffer by Nosemosis?',
            ]
        ], '1.0' );

        $data = Excel::toArray( new SemicolonCSVImport, base_path( "/database/seeders/Connectors/raw/Epilobee-883eax2-sup-0002.xlsx" ) );

        foreach ( $data[0] as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }
            $event = Log::create( [
                'year'       => $this->parseYear( $row[38] ),
                'project_id' => $this->project->id,
            ] );

            $event->attachData( $this->descriptors['external-id'], $row[0] );
            $event->attachData( $this->descriptors['winter-mortality-percentage'], $row[1] );
            $event->attachData( $this->descriptors['beekeepers-age-range'], [ 'value' => $this->parseAge( $row[2] ) ] );
            $event->attachData( $this->descriptors['beekeepers-activity'], $this->parseActivity( $row[3] ) );
            $event->attachData( $this->descriptors['beekeeper-for-range'], [ 'value' => $this->parseBeekeeperFor( $row[4] ) ] );
            $event->attachData( $this->descriptors['has-beekeeper-qualifications'], $this->parseBoolean( $row[5] ) );
            $event->attachData( $this->descriptors['has-beekeeper-trainings'], $this->parseBoolean( $row[6] ) );
            // Unknown column 7: Coop_treat - yes/no
            $event->attachData( $this->descriptors['beehive-population'], $this->parsePopulationSize( $row[8] ) );
            $event->attachData( $this->descriptors['colonies-location'], [ 'location_id' => $this->parseRegionFromCountry( $row[9], true ), 'location_type' => Region::class ] );
            $event->attachData( $this->descriptors['colonies-count'], $this->parseApiaryPopulationSize( $row[10] ) );
            $event->attachData( $this->descriptors['colonies-production-type'], $this->parseProductionType( $row[11] ) );
            $event->attachData( $this->descriptors['has-apiarist-book'], $this->parseBoolean( $row[12] ) );
            $event->attachData( $this->descriptors['is-organization-member'], $this->parseBoolean( $row[13] ) );
            // Unknown column 14: Continue - yes/no
            $event->attachData( $this->descriptors['primary-breed-name'], $this->parseBreed( $row[15] ) );
            $event->attachData( $this->descriptors['has-chronic-depopulation'], $this->parseBoolean( $row[16] ) );
            // Unknown column 17: ClinSign_Brood - yes/no
            // Unknown column 18: ClinSign_Honeybees - yes/no
            // Unknown column 19: H_Rate_ColMortality - yes/no
            // Unknown column 20: H_Rate_HoneyMortality - yes/no
            // Unknown column 21: OtherEvent - yes/no
            // Unknown column 22: VarroaMites - yes/no
            // Unknown column 23: QueenProblems - yes/no
            // Unknown column 24: Management - Livestock/No Managment/Production + Livestock/Production + Livestock  + HealthConditions/etc.
            // Unknown column 25: Swarm_bought - 1__None/2__1 swarm bought/3__2 - 5 swarms bought/4__More than 5 swarms bought/5__No Management
            // Unknown column 26: Swarm_produced - 1__None/2__1 - 5 swarm(s) produced/3__6 - 10 swarms produced/4__More than 10 swarms produced/5__No Management
            // Unknown column 27: Queen_bought - 1__None/2__1 - 5 queen(s) bought/3__6 - 10 queens bought/4__More than 10 queens bought/5__No Management
            // Unknown column 28: Queen_produced - 1__None/2__1 - 5 queen(s) produced/3__6 - 10 queens produced/4__More than 10 queens produced/5__No Management
            // Unknown column 29: MidSeason_Target - Crops/Diverse/No Migration/Other/WildFlowers
            $event->attachData( $this->descriptors['colony-environment-types'], $this->parseEnvironment( $row[30] ) );
            $event->attachData( $this->descriptors['has-varroa-infestation'], $this->parseBooleanDisease( $row[31] ) );
            $event->attachData( $this->descriptors['has-chronic-paralysis'], $this->parseBooleanDisease( $row[32] ) );
            $event->attachData( $this->descriptors['has-american-foulbrood'], $this->parseBooleanDisease( $row[33] ) );
            $event->attachData( $this->descriptors['has-nosemosis'], $this->parseBooleanDisease( $row[34] ) );
            $event->attachData( $this->descriptors['has-european-foulbrood'], $this->parseBooleanDisease( $row[35] ) );
            // Unknown column 36: Migration - yes/no
            // Unknown column 37: Merger - yes/no
        }
    }

    /**
     * Get year from String.
     *
     * @param string $string
     *
     * @return int
     * @throws InvalidValueException
     */
    protected function parseYear( string $string ): int
    {
        switch ( $string ) {
            case 'First Year':
                return 2012;
            case 'Second Year':
                return 2013;
            default:
                throw new InvalidValueException( $string );
        }
    }

    /**
     * @param string $string
     *
     * @return array|int[]
     * @throws InvalidValueException
     */
    protected function parseAge( string $string ): array
    {
        switch ( $string ) {
            case '1___Less than 30':
                return [ null, 29 ];
            case '2___30-45':
                return [ 30, 44 ];
            case '3___45-65':
                return [ 45, 64 ];
            case '4___Over 65':
                return [ 65, null ];
            default:
                throw new InvalidValueException( $string );
        }
    }

    /**
     * @param string $string
     *
     * @return array|int[]
     * @throws InvalidValueException
     */
    protected function parseBeekeeperFor( string $string ): array
    {
        switch ( $string ) {
            case '1___Less than 2 years':
                return [ null, 1 ];
            case '2___2-5 years':
                return [ 2, 4 ];
            case '3___More than 5 years':
                return [ 5, null ];
            default:
                throw new InvalidValueException( $string );
        }
    }

    /**
     * Get Beekeeper type from String.
     *
     * @param string $string
     *
     * @return string
     * @throws InvalidValueException
     */
    protected function parseActivity( string $string ): string
    {
        switch ( $string ) {
            case 'Hobby':
            case 'Part_time':
            case 'Professional':
                return $string;
            default:
                throw new InvalidValueException( $string );
        }
    }


    /**
     * Get total beekeeper population size.
     * Note: Because the ranges are useless to us we take an approximation here.
     *
     * @param string $string
     *
     * @return int
     * @throws InvalidValueException
     */
    protected function parsePopulationSize( string $string ): int
    {
        switch ( $string ) {
            case '1___Less than 51 colonies':
                return 25;
            case '2___51 - 100':
                return 75;
            case '3___101 - 150':
                return 125;
            case '4___151 - 200':
                return 175;
            case '5___201 - 300':
                return 250;
            case '6___More than 300 colonies':
                return 300;
            default:
                throw new InvalidValueException( $string );
        }
    }

    /**
     * Get apiary population size.
     * Note: Because the ranges are useless to us we take an approximation here.
     *
     * @param string $string
     *
     * @return int
     * @throws InvalidValueException
     */
    protected function parseApiaryPopulationSize( string $string ): int
    {
        switch ( $string ) {
            case '1__Less than 5 colonies':
                return 3;
            case '2__6 - 10':
                return 8;
            case '3__11 - 20':
                return 15;
            case '4__21 - 50':
                return 35;
            case '5__More than 50 colonies':
                return 60;
            default:
                throw new InvalidValueException( $string );
        }
    }

    /**
     * @param string $string
     *
     * @return string
     * @throws InvalidValueException
     */
    protected function parseProductionType( string $string ): string
    {
        switch ( $string ) {
            case '01__Only Honey':
                return 'only_honey';
            case '02__Includes pollen production':
                return 'pollen';
            case '03__Includes pollination services':
                return 'pollination';
            case '04__Includes queens production':
                return 'queens';
            case '05__Includes royal jelly production':
                return 'royal_jelly';
            case '06__Includes swarm production':
                return 'swarm';
            case '07__Other':
                return 'other';
            default:
                throw new InvalidValueException( $string );
        }
    }

    /**
     * @param string $string
     *
     * @return string
     * @throws InvalidValueException
     */
    protected function parseEnvironment( string $string ): string
    {
        switch ( $string ) {
            case 'Diverse':
                return 'diverse';
            case 'Farmland':
                return 'farmland';
            case 'Flora':
                return 'flora';
            case 'None':
                return 'none';
            case 'Orchards':
                return 'orchards';
            case 'Town':
                return 'town';
            case 'Wood':
                return 'wood';
            default:
                throw new InvalidValueException( $string );
        }
    }


    /**
     * Parse Boolean value.
     *
     * @param string $string
     *
     * @return bool
     * @throws InvalidValueException
     */
    protected function parseBooleanDisease( string $string ): bool
    {
        switch ( strtolower( $string ) ) {
            case 'suffering':
                return true;
            case 'not_suffering':
                return false;
            default:
                throw new InvalidValueException( $string );
        }
    }

}
