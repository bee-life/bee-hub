<?php namespace Database\Seeders\Connectors;

use App\Imports\SemicolonCSVImport;
use App\Models\Locations\Region;
use App\Models\Logs\Log;
use Carbon\Carbon;
use Excel;

class ColossATSeeder2020 extends BaseSeeder
{
    /**
     * Seed the application's database.
     * @TODO: Work In Progress!!

     *
     * @return void
     */
    protected function runSeeder(): void
    {
        $this->createProject( [
            'type'           => 'static',
            'name'           => 'Austrian COLOSS Survey of Honey Bee Colony Winter Losses 2019/20 and Analysis of Hive Management Practices',
            'acronym'        => 'COLOSS AT 2019/2020',
            'description'    => '',
            'website'        => 'https://www.mdpi.com/1424-2818/12/3/99/htm',
            'phone'          => '',
            'email'          => '',
            'class_name'     => null,
            'active'         => true,
            'public'         => true,
            'sync_period'    => null, // Can be one of: null, 'hourly', 'daily', 'weekly', 'monthly', 'quarterly', 'yearly'
            'historic_sync'  => null, // Can be one of: null (no historic syncing),  false (should sync in future)
            'last_synced_at' => Carbon::now(),
        ] );

        $this->createProviders( [
            'name' => 'Coloss AT import',
        ] );

        $this->createDescriptors( [
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'External ID',
                'slug'        => 'external-id',
                'description' => 'External ID',
            ],
            [
                'table_name'  => 'log_data_locations',
                'name'        => 'Colonies location',
                'slug'        => 'colonies-location',
                'description' => 'Where is the colony situated?',
            ],
        ], '1.0' );


        $data = Excel::toArray( new SemicolonCSVImport, base_path( "/database/seeders/Connectors/raw/COLOSS/" ) )[0];

        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }


            $log = Log::create( [
                'year'       => 2020,
                'project_id' => $this->project->id,
            ] );

            $log->attachData( $this->descriptors['external-id'], $row[ $id ] );
            $log->attachData( $this->descriptors['colonies-location'], [ 'location_id' => $this->parseRegionFromPostNumber( $row[ $post_id ], 'BE' ), 'location_type' => Region::class ] );

            // Just in case we have missing or incomplete data, make sure we get at least some data.
            if ( ! empty( $row[ $before_winter_id ] ) && is_numeric( $row[ $before_winter_id ] ) ) {
                $log->attachData( $this->descriptors['colony-count-before-winter'], $row[ $before_winter_id ] );
            }
            if ( ! empty( $row[ $after_winter_id ] ) && is_numeric( $row[ $after_winter_id ] ) ) {
                $log->attachData( $this->descriptors['colony-count-after-winter'], $row[ $after_winter_id ] );
            }
            if ( ! empty( $row[ $before_winter_id ] ) && ! empty( $row[ $after_winter_id ] )
                 && is_numeric( $row[ $before_winter_id ] ) && is_numeric( $row[ $after_winter_id ] ) ) {
                $log->attachData( $this->descriptors['winter-mortality-percentage'], ( ( $row[ $before_winter_id ] - $row[ $after_winter_id ] ) / $row[ $before_winter_id ] ) * 100 );
            }
        }
    }
}
