<?php namespace Database\Seeders\Connectors;

use App\Data\Connectors\Traits\LocationParser;
use App\Exceptions\InvalidValueException;
use App\Imports\SemicolonCSVImport;
use App\Models\Locations\Country;
use App\Models\Locations\Lau;
use App\Models\Locations\Region;
use App\Models\Logs\Log;
use App\Models\Logs\Reference\ReferenceLandUse;
use App\Models\Logs\Reference\ReferencePesticide;
use App\Models\Logs\Reference\ReferencePollen;
use App\Models\References\LandUse;
use App\Models\References\Pesticide;
use App\Models\References\PesticideType;
use App\Models\References\Pollen;
use App\Models\References\Reference;
use Carbon\Carbon;
use Excel;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Schema;

class DEPABSeeder extends BaseSeeder
{
    use LocationParser;

    /**
     * Seed the application's database.
     *
     * @return void
     * @throws InvalidValueException
     */
    protected function runSeeder(): void
    {
        $this->createProject( [
            'type'           => 'static',
            'name'           => 'Experimental approach to unexplained mortality of bee colonies in Wallonia',
            'uid'            => 'depab',
            'acronym'        => 'DEPAB',
            'description'    => 'PROJECT CONTEXT
Within the framework of the project "Experimental approach to unexplained mortality of bee colonies in Wallonia (Belgium)"(Nº D32-0075), several beekeeping matrices (honey, wax, bee bread) were taken from the hives and analysed. The results of these analyses clearly indicate that the colonies are contaminated with both insecticides, acaricides but also by many fungicides. However, the exact sources
of contamination could not be identified. Only a significant link has been established between dieback and the average number of fungicide residues detected but also with the area of cultivation around the apiary. Thus, the greater the fungicide residue load, the larger the area under cultivation, the greater the likelihood of dieback for the colony.

In addition to these bee matrices, trap pollen samples were taken from most of the bee colonies of the apiaries participating in the study. A total of 158 samples were collected in 39 apiaries during the from July to October. They were then grouped according to the month of collection. Following this operation, there are 61 samples left. For each of them, a palynological analysis and an analysis of residues were carried out respectively by CARI (Belgium) and the Floramo laboratory (Italy). The analysis identified more than 30 different botanical families as a source of pollen in
the environment of the sampled apiaries. As for the analyses of pesticide residues, they put in evidence 6 fungicide residues (boscalid, cyprodinil, kresoxim-methyl, pyrimethanil, trifloxystrobin, tebuconazole) and 3 insecticides (dimethoate, imidacloprid, thiamethoxam). Given the dates of sampling, July to October, the presence in pollen of some of these contaminants such as boscalid or
dimethoate asks us about the agricultural or non-agricultural origin of these residues. For certain molecules, this late detection could be explained by amateur use or by the persistence of the product and the mobilization of it by plants planted after cultivation (ICNAF) or by weeds.

In order to clarify this situation, an in-depth analysis of both palynological data and occupation of the area was carried out. of the soil around the apiaries would make it possible to identify the sources of pollen contamination.

OBJECTIVE OF THE PROJECT
The objective of the project was to cross-reference the results obtained in project Experimental approach to unexplained mortality of bee colonies in Wallonia (Belgium) (Nº D32-0075): (1) palynological origin of trap pollen, (2) pesticide residues in trap pollen and (3) land use around the apiaries in order to identify the sources of contamination of the bee colonies in the different pesticide residues through pollen.',
            'website'        => '',
            'phone'          => '',
            'email'          => '',
            'featured_image' => null,
            'active'         => true,
            'public'         => true,
            'sync_period'    => null, // Can be one of: null, 'hourly', 'daily', 'weekly', 'monthly', 'quarterly', 'yearly'
            'historic_sync'  => null, // Can be one of: null (no historic syncing),  false (should sync in future)
            'last_synced_at' => Carbon::now(),
        ] );

        $this->createProviders(
            [
                [
                    'slug'            => 'cariasbl-beekeeping-center-of-research-and-information',
                    'uid'             => 'cari-beekeeping-center-of-research-and-information',
                ],
                [
                    'slug'           => 'cra-w',
                    'uid'            => 'cra-w',
                ],
            ]
        );

        $this->createDescriptors( [
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'External ID',
                'slug'        => 'external-id',
                'description' => 'External ID',
                'category'    => 'hive',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'Apiary ID',
                'slug'        => 'apiary-id',
                'description' => 'Apiary ID',
                'category'    => 'apiary',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_locations',
                'name'        => 'Colonies location',
                'slug'        => 'colonies-location',
                'description' => 'Where is the colony situated?',
                'category'    => 'environment',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_reference_pollen',
                'name'        => 'Pollen Composition',
                'slug'        => 'pollen-composition',
                'description' => 'Pallynological analyses of pollen',
                'category'    => 'hive-analyses',
            ],
            [
                'table_name'  => 'log_reference_pesticides',
                'name'        => 'Pollen Pesticides',
                'slug'        => 'pollen-pesticides',
                'description' => 'Pesticides analyses from the pollen samples.',
                'category'    => 'hive-analyses',
            ],
            [
                'table_name'  => 'log_reference_land_usages',
                'name'        => 'Surrounding Land Usage',
                'slug'        => 'apiary-land-usage',
                'description' => 'Surfaces of different type of agricultural land use around the apiaries within buffers of different radius.',
                'category'    => 'environment',
            ],
        ], '1.1' );

        $basepath = base_path( '/database/seeders/Connectors/raw/DEPAB/' );


        // Import Pesticides types reference library.
        PesticideType::insert( [
            [
                'name'         => 'Herbicide',
                'abbreviation' => 'H',
            ],
            [
                'name'         => 'Insecticide',
                'abbreviation' => 'I',
            ],
            [
                'name'         => 'Insecticide-acaricide',
                'abbreviation' => 'IA',
            ],
            [
                'name'         => 'Fungicides',
                'abbreviation' => 'F',
            ],
            [
                'name'         => 'Acaricide',
                'abbreviation' => 'A',
            ],
            [
                'name'         => 'Synergist',
                'abbreviation' => 'S',
            ],
        ] );

        /**
         * Import Pesticides reference library.
         * Note: we always skip first row as it contains headers.
         */
        $pesticideTypes = PesticideType::all()->keyBy( 'abbreviation' );

        $pesticides = [];
        $data       = Excel::toArray( new SemicolonCSVImport, $basepath . '/lib_pesticides.csv' )[0];

        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }

            $pesticides[ $row[0] ] = Pesticide::create( [
                'substance'    => $row[0],
                'name'         => $row[1],
                'type_id'      => $pesticideTypes[ $row[2] ]->id,
                'loq_wax'      => $row[3] == 'NA' ? null : str_replace( ',', '.', $row[3] ),
                'loq_beebread' => $row[4] == 'NA' ? null : str_replace( ',', '.', $row[4] ),
                'loq_honey'    => $row[5] == 'NA' ? null : str_replace( ',', '.', $row[5] ),
                'loq_pollen'   => $row[6] == 'NA' ? null : str_replace( ',', '.', $row[6] ),
            ] );
        }

        // Import Pollen details reference library.
        $pollen = [];
        $data   = Excel::toArray( new SemicolonCSVImport, $basepath . '/lib_pollen.csv' )[0];

        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }

            // TODO: Add References
            $pollen[ $row[0] ] = Pollen::create( [
                'name'   => $row[0],
                'type'   => $row[1],
                'family' => $row[2],
                'code'   => $row[3],
                //'ref'   => $row[4],
                //'ref'   => $row[5],
                'p'      => str_replace( ',', '.', $row[6] ),
                'q'      => str_replace( ',', '.', $row[7] ),
                'vol'    => str_replace( ',', '.', $row[8] ),
                'r'      => str_replace( ',', '.', $row[9] ),
                'vol_r'  => str_replace( ',', '.', $row[10] ),
            ] );
        }

        // Import Land usage reference library.
        $landUsage = [];
        $data      = Excel::toArray( new SemicolonCSVImport, $basepath . '/lib_SIGEC.csv' )[0];

        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }


            $landUsage[ $row[0] ] = LandUse::create( [
                'sigec_code'  => $row[0],
                'name'        => $row[1],
                'group'       => $row[2],
                'dim_min_res' => str_replace( ',', '.', $row[3] ),
                'dim_max_res' => str_replace( ',', '.', $row[4] ),
                'pyr_min_res' => str_replace( ',', '.', $row[5] ),
                'pyr_max_res' => str_replace( ',', '.', $row[6] ),
                'bos_min_res' => str_replace( ',', '.', $row[7] ),
                'bos_max_res' => str_replace( ',', '.', $row[8] ),
            ] );
        }

        // Import Apiary data.
        $apiaries = [];
        $data     = Excel::toArray( new SemicolonCSVImport, $basepath . '/Approximate_coordinates.csv' )[0];

        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }

            $apiaries[ $row[0] ] = [
                'id'                   => $row[0],
                'long'                 => str_replace( ',', '.', $row[1] ),
                'lat'                  => str_replace( ',', '.', $row[2] ),
                'dates'                => [],
                'pollen_composition'   => [],
                'pollen_contamination' => [],
                'land_use'             => [],
            ];
        }

        // Import sample dates
        $data = Excel::toArray( new SemicolonCSVImport, $basepath . '/pollen_dates.csv' )[0];
        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }

            if ( isset( $apiaries[ $row[1] ] ) ) {
                $data = [
                    'date'   => $row[3],
                    'colony' => $row[2] != 'NA' ? $row[2] : null,
                ];

                if ( isset( $apiaries[ $row[1] ]['dates'][ $row[0] ] ) ) {
                    $apiaries[ $row[1] ]['dates'][ $row[0] ][] = $data;
                } else {
                    $apiaries[ $row[1] ]['dates'][ $row[0] ] = [ $data ];
                }
            }
        }


        // Import Land usage around apiaries.
        $data = Excel::toArray( new SemicolonCSVImport, $basepath . '/pollen_agri.csv' )[0];
        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }

            if ( isset( $apiaries[ $row[0] ] ) ) {
                $apiaries[ $row[0] ]['land_use'][] = [
                    'code'   => $row[1],
                    'area'   => str_replace( ',', '.', $row[2] ),
                    'buffer' => str_replace( ',', '.', $row[3] ),
                ];
            }
        }

        // Import Pollen compositon.
        $data = Excel::toArray( new SemicolonCSVImport, $basepath . '/pollen_composition.csv' )[0];
        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }

            if ( isset( $apiaries[ $row[1] ] ) ) {
                $data = [
                    'id'         => $row[0],
                    'id2'        => $row[4],
                    'month'      => $row[3],
                    'pollen'     => $row[5],
                    'amount'     => $row[6],
                    'percentage' => str_replace( ',', '.', $row[7] ),
                    'category'   => $row[8],
                    'colony'     => $row[2] != 'NA' ? $row[2] : null,
                ];

                if ( isset( $apiaries[ $row[1] ]['pollen_composition'][ $row[0] ] ) ) {
                    $apiaries[ $row[1] ]['pollen_composition'][ $row[0] ][] = $data;
                } else {
                    $apiaries[ $row[1] ]['pollen_composition'][ $row[0] ] = [ $data ];
                }
            }
        }

        // Import Pesticides analysis.
        $data = Excel::toArray( new SemicolonCSVImport, $basepath . '/pollen_pesticides.csv' )[0];
        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }

            if ( isset( $apiaries[ $row[3] ] ) ) {

                $apiaries[ $row[3] ]['pollen_contamination'][ $row[2] ] = [
                    'id'    => $row[2],
                    'id2'   => $row[1],
                    'month' => $row[4],
                    'year'  => $row[5],

                    'contaminants' => [
                        'amitraz'                                  => isset( $row[6] ) ? str_replace( ',', '.', $row[6] ) : null,
                        'betacyfluthrine'                          => isset( $row[7] ) ? str_replace( ',', '.', $row[7] ) : null,
                        'bifenthrine'                              => isset( $row[8] ) ? str_replace( ',', '.', $row[8] ) : null,
                        'boscalid'                                 => isset( $row[9] ) ? str_replace( ',', '.', $row[9] ) : null,
                        'carbaryl'                                 => isset( $row[10] ) ? str_replace( ',', '.', $row[10] ) : null,
                        'chlorothalonil'                           => isset( $row[11] ) ? str_replace( ',', '.', $row[11] ) : null,
                        'chlorpyriphos'                            => isset( $row[12] ) ? str_replace( ',', '.', $row[12] ) : null,
                        'cyprodinil'                               => isset( $row[13] ) ? str_replace( ',', '.', $row[13] ) : null,
                        'deltamethrine'                            => isset( $row[14] ) ? str_replace( ',', '.', $row[14] ) : null,
                        'coumaphos'                                => isset( $row[15] ) ? str_replace( ',', '.', $row[15] ) : null,
                        'cyfluthrine'                              => isset( $row[16] ) ? str_replace( ',', '.', $row[16] ) : null,
                        'dimethoate'                               => isset( $row[17] ) ? str_replace( ',', '.', $row[17] ) : null,
                        'esfenvalerate'                            => isset( $row[18] ) ? str_replace( ',', '.', $row[18] ) : null,
                        'fenpropimorphe'                           => isset( $row[19] ) ? str_replace( ',', '.', $row[19] ) : null,
                        'heptenophos'                              => isset( $row[20] ) ? str_replace( ',', '.', $row[20] ) : null,
                        'indoxacarbe'                              => isset( $row[21] ) ? str_replace( ',', '.', $row[21] ) : null,
                        'iprodione'                                => isset( $row[22] ) ? str_replace( ',', '.', $row[22] ) : null,
                        'kresoxim.methyl'                          => isset( $row[23] ) ? str_replace( ',', '.', $row[23] ) : null,
                        'lambda.cyalothrine'                       => isset( $row[24] ) ? str_replace( ',', '.', $row[24] ) : null,
                        'piperonyl.butoxide'                       => isset( $row[25] ) ? str_replace( ',', '.', $row[25] ) : null,
                        'pirimicarbe'                              => isset( $row[26] ) ? str_replace( ',', '.', $row[26] ) : null,
                        'propamocarbe'                             => isset( $row[27] ) ? str_replace( ',', '.', $row[27] ) : null,
                        'pyraclostrobine'                          => isset( $row[28] ) ? str_replace( ',', '.', $row[28] ) : null,
                        'pyrimethanil'                             => isset( $row[29] ) ? str_replace( ',', '.', $row[29] ) : null,
                        'trifloxystrobine'                         => isset( $row[30] ) ? str_replace( ',', '.', $row[30] ) : null,
                        'tebuconazole'                             => isset( $row[31] ) ? str_replace( ',', '.', $row[31] ) : null,
                        'tau.fluvalinate'                          => isset( $row[32] ) ? str_replace( ',', '.', $row[32] ) : null,
                        'tebufenozide'                             => isset( $row[33] ) ? str_replace( ',', '.', $row[33] ) : null,
                        'terbuthylazine'                           => isset( $row[34] ) ? str_replace( ',', '.', $row[34] ) : null,
                        'zoxamide'                                 => isset( $row[35] ) ? str_replace( ',', '.', $row[35] ) : null,
                        'captane'                                  => isset( $row[36] ) ? str_replace( ',', '.', $row[36] ) : null,
                        'thiophanate.methyl'                       => isset( $row[37] ) ? str_replace( ',', '.', $row[37] ) : null,
                        'clothianidine'                            => isset( $row[38] ) ? str_replace( ',', '.', $row[38] ) : null,
                        'fipronil'                                 => isset( $row[39] ) ? str_replace( ',', '.', $row[39] ) : null,
                        'Fipronil.carboxamide'                     => isset( $row[40] ) ? str_replace( ',', '.', $row[40] ) : null,
                        'Fipronil.desulfinil'                      => isset( $row[41] ) ? str_replace( ',', '.', $row[41] ) : null,
                        'Fipronil.sulfone'                         => isset( $row[42] ) ? str_replace( ',', '.', $row[42] ) : null,
                        'imidaclopride'                            => isset( $row[43] ) ? str_replace( ',', '.', $row[43] ) : null,
                        'imidaclopride_metab_5_OH_Imidacloprid'    => isset( $row[44] ) ? str_replace( ',', '.', $row[44] ) : null,
                        'imidaclopride_metab_Desnitroimidacloprid' => isset( $row[45] ) ? str_replace( ',', '.', $row[45] ) : null,
                        'imidaclopride_metab_6_Cloronicotinc_acid' => isset( $row[46] ) ? str_replace( ',', '.', $row[46] ) : null,
                        'imidaclopride_metab_Urea_Derivate'        => isset( $row[47] ) ? str_replace( ',', '.', $row[47] ) : null,
                        'thiaclopride'                             => isset( $row[48] ) ? str_replace( ',', '.', $row[48] ) : null,
                        'thiamethoxam'                             => isset( $row[49] ) ? str_replace( ',', '.', $row[49] ) : null,
                    ],
                ];
            }
        }

        foreach ( $apiaries as $id => $apiary ) {
            $apiary['point'] = new Point( $apiary['lat'], $apiary['long'] );
            if ( ( $lau = Lau::contains( 'area', $apiary['point'] )->where( 'country_id', Country::getIdFromCountryCode( 'BE' ) )->first() ) != null ) {
                $apiary['location'] = $lau;
            } elseif ( ( $region = Region::contains( 'area', $apiary['point'] )->where( 'nuts_level', 3 )->where( 'country_id', Country::getIdFromCountryCode( 'BE' ) )->first() ) != null ) {
                $apiary['location'] = $region;
            }

            $apiary['location_id']   = isset( $apiary['location'] ) ? $apiary['location']->id : null;
            $apiary['location_type'] = isset( $apiary['location'] ) ? get_class( $apiary['location'] ) : null;

            $log = $this->createLog( 2011, 'Y', false );
            $log->attachData( $this->descriptors['apiary-id'], $id );
            $log->attachData( $this->descriptors['colonies-location'], [
                'location_id'   => $apiary['location_id'],
                'location_type' => $apiary['location_type'],
                'value'         => $apiary['point']
            ] );

            foreach ( $apiary['land_use'] as $land_use ) {
                $log->attachData( $this->descriptors['apiary-land-usage'], [
                    'surface'      => $land_use['area'] * 10000,
                    'radius'       => $land_use['buffer'],
                    'reference_id' => $landUsage[ $land_use['code'] ]->id
                ] );
            }

            foreach ( $apiary['dates'] as $sample_id => $date ) {
                $log = $this->createLog( $date[0]['date'], 'Y-m-d', false );

                $log->attachData( $this->descriptors['apiary-id'], $id );
                $log->attachData( $this->descriptors['colonies-location'], [
                    'location_id'   => $apiary['location_id'],
                    'location_type' => $apiary['location_type'],
                    'value'         => $apiary['point']
                ] );

                if ( isset( $apiary['pollen_contamination'][ $sample_id ] ) ) {
                    foreach ( $apiary['pollen_contamination'][ $sample_id ]['contaminants'] as $contaminator_name => $contaminator ) {
                        if ( isset( $contaminator ) ) {
                            $log->attachData( $this->descriptors['pollen-pesticides'], [
                                'value'        => $contaminator,
                                'reference_id' => $pesticides[ $contaminator_name ]->id
                            ] );
                        }
                    }
                }
                if ( isset( $apiary['pollen_composition'][ $sample_id ] ) ) {
                    foreach ( $apiary['pollen_composition'][ $sample_id ] as $composition ) {
                        $log->attachData( $this->descriptors['pollen-composition'], [
                            'amount'       => $composition['amount'],
                            'fraction'     => $composition['percentage'],
                            'reference_id' => $pollen[ $composition['pollen'] ]->id
                        ] );
                    }
                }
            }
        }
    }
}
