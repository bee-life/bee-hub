<?php namespace Database\Seeders\Connectors;

use App\Data\Connectors\Traits\LocationParser;
use App\Exceptions\InvalidValueException;
use Carbon\Carbon;

class TemplateSeeder extends BaseSeeder
{
    use LocationParser;

    /**
     * Seed the application's database.
     *
     * @return void
     * @throws InvalidValueException
     */
    protected function runSeeder(): void
    {
        $this->createProject( [
            'type'           => 'static',
            'name'           => '',
            'acronym'        => '',
            'uid'            => '',
            'slug'           => '',
            'description'    => '',
            'website'        => '',
            'phone'          => '',
            'email'          => '',
            'featured_image' => '',
            'class_name'     => 'App\Connectors\ExampleConnector',
            'active'         => true,
            'public'         => true,
            'sync_period'    => null, // Can be one of: null, 'hourly', 'daily', 'weekly', 'monthly', 'quarterly', 'yearly'
            'historic_sync'  => null, // Can be one of: null (no historic syncing),  false (should sync in future)
            'last_synced_at' => Carbon::now(),
        ] );

        $this->createProviders(
            [
                [
                    'full_name'       => '',
                    'name'            => '',
                    'uid'             => '',
                    'slug'            => '',
                    'address'         => '',
                    'registry_number' => '',
                    'description'     => '',
                    'featured_image'  => '',
                    'website'         => '',
                    'phone'           => '',
                    'email'           => '',
                    'post_id'         => '',
                    'public'          => true,
                ],
                [
                    'full_name'       => '',
                    'name'            => '',
                    'uid'             => '',
                    'slug'            => '',
                    'address'         => '',
                    'registry_number' => '',
                    'description'     => '',
                    'featured_image'  => '',
                    'website'         => '',
                    'phone'           => '',
                    'email'           => '',
                    'post_id'         => '',
                    'public'          => true,
                ],
            ]
        );

        $this->createDescriptors( [
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'External ID',
                'slug'        => 'external-id',
                'description' => 'External ID',
                'category'    => 'hive',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'Apiary ID',
                'slug'        => 'apiary-id',
                'description' => 'Apiary ID',
                'category'    => 'apiary',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_locations',
                'name'        => 'Colonies location',
                'slug'        => 'colonies-location',
                'description' => 'Where is the colony situated?',
                'category'    => 'environment',
                'public'      => false,
            ],
        ], '1.0' );


        $this->loadPosts( '' );
        // TODO: Load data
    }

}
