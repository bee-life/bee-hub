<?php namespace Database\Seeders\Connectors;

use App\Data\Connectors\Traits\LocationParser;
use App\Exceptions\InvalidValueException;
use App\Imports\SemicolonCSVImport;
use App\Models\Locations\Post;
use App\Models\Logs\Log;
use App\Models\Locations\Region;
use Carbon\Carbon;
use Excel;

class ColossBESeeder extends BaseSeeder
{
    use LocationParser;


    /**
     * Seed the application's database.
     *
     * @return void
     * @throws InvalidValueException
     */
    protected function runSeeder(): void
    {
        ini_set( 'memory_limit', '1024M' );

        $this->createProject( [
            'type'           => 'static',
            'name'           => 'BeeWallonie',
            'slug'           => 'prevention-of-honey-bee-colony-losses',
            'uid'            => 'coloss-belgium',
            'acronym'        => 'BeeWallonie',
            'description'    => '<div><a href=\"https://www.beewallonie.be\">Bee Wallonie</a> is a project subsidised by the Walloon Region (Belgium) which associates two partners: <a href=\"http://www.cari.be\">CARI asbl</a> and <a href=\"https://www.cra.wallonie.be/fr\">CRA-W</a>. The project is planned for the period from 01/01/2017 to 31/12/2022. It finances 1.5 FTEs. The Walloon Region has entrusted CARI asbl and CRA-W with the mission of supervising, promoting and developing beekeeping in Wallonia. <br><br>The CARI asbl is in charge of part 1 of the project \"Support and development of the beekeeping sector\". It is also in charge of part of part 2 \"Bee health, agriculture and environment\", the other part coming under the attributions of the CRA-W.<br><br>Under the second part of the project, CARI asbl and CRA-W, in collaboration with the <a href=\"https://www.honeybeevalley.eu\">HoneybeeValley</a>, have historically participated to the collection of winter mortality data following the methodology proposed by the COLOSS Association, including the <a href=\"https://fab-bbf.be\">FAB-BBF</a> (Federation Apicole Belge - Belgische Bijenhoudersfederatie). More information of the global COLOSS project <a href=\"https://coloss.org/core-projects/colony-losses-monitoring/\">here</a>.</div>',
            'website'        => 'https://www.beewallonie.be',
            // 'phone'          => '',
            'email'          => 'simon@cari.be',
            'featured_image' => 'SvhyfRW7wD4p7iiDlgqmHLBPUzUEEBrcT6zIjsp7.png',
            'class_name'     => null,
            'active'         => true,
            'public'         => true,
            'last_synced_at' => Carbon::now(),
        ] );

        $this->createProviders(
            [
                [
                    'full_name'       => 'CARIasbl - Beekeeping Center of Research and Information',
                    'name'            => 'CARIasbl',
                    'slug'            => 'cariasbl-beekeeping-center-of-research-and-information',
                    'uid'             => 'cari-beekeeping-center-of-research-and-information',
                    'address'         => 'Place Croix du Sud, 1 bte L7.04.01\r\n1348 LOUVAIN-LA-NEUVE\r\nBELGIQUE',
                    'description'     => '<div>CARI is a Belgian non-profit association created in June 1983 by a team of researchers from the Laboratory of Ecology of the Catholic University of Louvain (UCL). CARI is a Belgian association with a global outlook. The CARI: - provides services to beekeepers to enhance the value of their products (e.g. analyses of honey by an accredited laboratory). - disseminates beekeeping information by publishing the magazine Abeilles&amp;Cie but also by sharing content on the Internet (follow our blog: butine.info and find us on social networks). - offers several specialized weekend training per year (\"Les week-ends du CARI\"). - engages in research projects and collaborates with university teams. - participates in regional development (is co-financed by the European Honey Programme for Wallonia and Brussels; provides technical follow-up within this framework; runs the Bee Wallonie project). - exports a positive image of Walloon beekeeping and welcomes and supports BeeLife, an association formed by professionals in the beekeeping sector from different countries of the European Union working for the protection of pollinators from environmental threats.</div><div><br></div>',
                    'registry_number' => 'BE0424644620',
                    'website'         => 'http://www.cari.be',
                    'phone'           => '+32 10 47 34 16',
                    'email'           => 'info@cari.be',
                    'featured_image'  => 'ftVAmGz4VfDctOXQeszZ7pMm6uODksPXbwDXFo50.jpeg',
                    'post_id'         => 2484,
                    'public'          => true,
                ],
                [
                    'full_name'      => 'Honeybee Valley',
                    'name'           => 'Honeybee Valley',
                    'slug'           => 'honeybee-valley',
                    'uid'            => 'honeybee-valley',
                    'address'        => 'UGent',
                    'description'    => 'At Honeybee Valley we do this from a piece of history, the complexity of bee mortality and the multidisciplinary approach it requires, the social awareness and the needs it has brought with it. What steps has Ghent University taken to meet these needs? Where does Honeybee Valley position itself and where do we want to land in the long run? In the navigation bar you will find the subheadings \'History\', \'Foundation\' and \'Focus and vision\' in which all these aspects are briefly outlined. More specific information about our research activities can be found elsewhere in the \'Project portfolio\'. And if you want to get to know our \'partners\' or the \'staff members\' who are affiliated with Honeybee Valley, please go to the corresponding section.',
                    'website'        => 'https://www.honeybeevalley.eu',
                    // 'phone'          => '',
                    'email'          => 'ellen.danneels@ugent.be',
                    'featured_image' => 'tYpBpp73PQ6EGdNOjCSASOYqV0qaf0a8dt9PCywL.jpeg',
                    // 'post_id'        => '',
                    'public'         => true,
                ],
                [
                    'full_name'      => 'Centre de Recherches Agronomiques - Wallonie',
                    'name'           => 'CRA-W',
                    'slug'           => 'cra-w',
                    'uid'            => 'cra-w',
                    // 'address'         => '',
                    // 'registry_number' => '',
                    'description'    => 'Bee Wallonie is the screen of Walloon beekeeping and the know-how developed by beekeepers.',
                    'website'        => 'https://www.beewallonie.be',
                    // 'phone'           => '',
                    'email'          => 'gilles.sanmartin@gmail.com',
                    'featured_image' => 'LGWqgvn16QEKNbzK0wVifbAMIUcuavvJ2RCU5mwV.jpeg',
                    // 'post_id'         => '',
                    'public'         => true,
                ],
            ]
        );
        $this->createDescriptors( [
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'External ID',
                'slug'        => 'external-id',
                'description' => 'External ID',
                'category'    => 'hive',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_locations',
                'name'        => 'Colonies location',
                'slug'        => 'colonies-location',
                'description' => 'Where is the colony situated?',
                'category'    => 'environment',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Autumn Count before winter',
                'slug'        => 'colony-count-autumn',
                'description' => 'How many colonies where alive before winter',
                'category'    => 'apiary-observation',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Spring Count after winter',
                'slug'        => 'colony-count-spring',
                'description' => 'How many colonies where alive after winter',
                'category'    => 'apiary-observation',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Lost colonies because they had a queen problem',
                'slug'        => 'colony-lost-queen-problems',
                'description' => 'How many colonies where alive after winter',
                'category'    => 'hive-health',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Lost colonies because of natural disaster',
                'slug'        => 'colony-lost-natural-disaster',
                'description' => 'How many colonies where alive after winter',
                'category'    => 'hive-health',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Lost colonies because they were dead',
                'slug'        => 'colony-lost-dead-empty',
                'description' => 'Lost colonies because they were dead',
                'category'    => 'hive-health',
            ],
            [
                'table_name'  => 'log_data_percentages',
                'name'        => 'Winter mortality',
                'slug'        => 'winter-mortality-percentage',
                'description' => 'Percent of Bee mortality during Winter.',
                'category'    => 'hive-health',
            ],
            [
                'table_name'  => 'log_data_percentages',
                'name'        => 'Winter mortality',
                'slug'        => 'winter-mortality-aggregate',
                'description' => 'Percent of Bee mortality during Winter.',
                'category'    => 'hive-aggregates',
            ],
        ], '1.0' );

        // We have currently only data for Belgium.
        $this->loadCountries();
        $this->loadPosts( $this->countries['BE']->country_id, 'BE' );

        // Load each year separately
        $this->readFile( 'COLOSS_FAB - winter 2014_2015 Edited.xls', 3, 2015, 2,
            [
                'external-id'                => 1,
                'post-id'                    => 7,
                'colony-count-autumn'        => 12,
                'colony-count-spring'        => 23,
                'colony-lost-queen-problems' => 13,
                'colony-lost-dead-empty'     => 14,
            ] );

        $this->readFile( 'Code Book COLOSS Monitor 2016_Template_final_corrected-NS-v2.xlsx', 7, 2016, 3,
            [
                'external-id'                => 0,
                'post-id'                    => 6,
                'colony-count-autumn'        => 11,
                'colony-count-spring'        => 22,
                'colony-lost-queen-problems' => 13,
                'colony-lost-dead-empty'     => 14,
            ] );
        $this->readFile( 'COLOSS_Belgium_2017-MERGE only data.csv', 1, 2017, 0,
            [
                'external-id'                  => 1,
                'post-id'                      => 10,
                'colony-count-autumn'          => 19,
                'colony-count-spring'          => 30,
                'colony-lost-queen-problems'   => 20,
                'colony-lost-natural-disaster' => 21,
                'colony-lost-dead-empty'       => 22,
            ] );
        $this->readFile( 'Coloss_Belgium_2017_2018_cleaned.csv', 1, 2018, 0,
            [
                'external-id'                  => 0,
                'post-id'                      => 4,
                'colony-count-autumn'          => 11,
                'colony-count-spring'          => 22,
                'colony-lost-queen-problems'   => 12,
                'colony-lost-natural-disaster' => 13,
                'colony-lost-dead-empty'       => 14,
            ] );
        $this->readFile( 'COLOSS_2018_2019_Belgium.xlsx', 7, 2019, 3,
            [
                'external-id'                  => 0,
                'post-id'                      => 6,
                'colony-count-autumn'          => 11,
                'colony-count-spring'          => 22,
                'colony-lost-queen-problems'   => 12,
                'colony-lost-natural-disaster' => 13,
                'colony-lost-dead-empty'       => 14,
            ] );
        $this->readFile( 'COLOSS_2019_2020_Belgium.xlsx', 7, 2020, 3,
            [
                'external-id'                  => 0,
                'post-id'                      => 6,
                'colony-count-autumn'          => 11,
                'colony-count-spring'          => 22,
                'colony-lost-queen-problems'   => 12,
                'colony-lost-natural-disaster' => 13,
                'colony-lost-dead-empty'       => 14,
            ] );
    }

    /**
     * Load Coloss data from files and parse it.
     *
     * @param string $file     Source file name.
     * @param int    $skip     How many rows should be skipped in the beginning.
     * @param int    $year     Which year does the data correspond to.
     * @param int    $sheet_id Which Sheet contains our data.
     * @param array  $columns  An array of key-value pairs representing descriptor uid and corresponding row, that the descriptor is found in the document.
     *
     * @throws InvalidValueException
     */
    private function readFile( string $file, int $skip, int $year, int $sheet_id, array $columns )
    {
        $data = Excel::toArray( new SemicolonCSVImport, base_path( "/database/seeders/Connectors/raw/COLOSS/" . $file ) );

        foreach ( $data[ $sheet_id ] as $key => $row ) {
            if ( $key < $skip ) {
                continue;
            }

            // Skip those without or invalid location, they are useless to us.
            if ( empty( $row[0] )
                 || empty( $row[ $columns['post-id'] ] )
                 || ! is_numeric( $row[ $columns['post-id'] ] )
                 || empty( $this->posts['BE'][ $row[ $columns['post-id'] ] ] ) ) {
                continue;
            }

            $log = Log::create( [
                'year'       => $year,
                'project_id' => $this->project->id,
            ] );

            $log->attachData( $this->descriptors['external-id'], $row[ $columns['external-id'] ] );
            $log->attachData( $this->descriptors['colonies-location'], [
                'location_id'   => $this->parsePostNumber( $row[ $columns['post-id'] ], 'BE' )->id,
                'location_type' => Post::class
            ] );

            if ( ! empty( $row[ $columns['colony-count-spring'] ] ) && is_numeric( $row[ $columns['colony-count-spring'] ] ) ) {
                $log->attachData( $this->descriptors['colony-count-spring'], $row[ $columns['colony-count-spring'] ] );
            }

            if ( ! empty( $row[ $columns['colony-count-autumn'] ] ) && is_numeric( $row[ $columns['colony-count-autumn'] ] ) ) {
                $log->attachData( $this->descriptors['colony-count-autumn'], $row[ $columns['colony-count-autumn'] ] );
            }

            if ( isset( $columns['colony-lost-queen-problems'] ) && ! empty( $row[ $columns['colony-lost-queen-problems'] ] ) && is_numeric( $row[ $columns['colony-lost-queen-problems'] ] ) ) {
                $log->attachData( $this->descriptors['colony-lost-queen-problems'], $row[ $columns['colony-lost-queen-problems'] ] );
            }

            if ( isset( $columns['colony-lost-natural-disaster'] ) && ! empty( $row[ $columns['colony-lost-natural-disaster'] ] ) && is_numeric( $row[ $columns['colony-lost-natural-disaster'] ] ) ) {
                $log->attachData( $this->descriptors['colony-lost-natural-disaster'], $row[ $columns['colony-lost-natural-disaster'] ] );
            }

            if ( isset( $columns['colony-lost-dead-empty'] ) && ! empty( $row[ $columns['colony-lost-dead-empty'] ] ) && is_numeric( $row[ $columns['colony-lost-dead-empty'] ] ) ) {
                $log->attachData( $this->descriptors['colony-lost-dead-empty'], $row[ $columns['colony-lost-dead-empty'] ] );
            }
        }
    }
}
