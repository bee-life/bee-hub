<?php namespace Database\Seeders\Connectors;

use App\Data\Connectors\Traits\LocationParser;
use App\Exceptions\InvalidValueException;
use App\Imports\SemicolonCSVImport;
use App\Models\Logs\Data;
use App\Models\Logs\Data\DataId;
use App\Models\Logs\Data\DataLocation;
use App\Models\Logs\Log;
use App\Models\Locations\Lau;
use App\Models\Locations\Post;
use App\Models\Origins\DeviceCategory;
use App\Models\Origins\Vendor;
use App\Models\Project;
use App\Models\Provider;
use Carbon\Carbon;
use Database\Seeders\Connectors\Traits\BaseParsers;
use DB;
use Excel;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\File;

class CortevaSeeder extends BaseSeeder
{
    use LocationParser, BaseParsers;

    /**
     * Seed the application's database.
     *
     * @return void
     * @throws InvalidValueException
     */
    protected function runSeeder(): void
    {
        $this->createProject( [
            'type'           => 'static',
            'name'           => 'Corteva Feeding study with Colony Development',
            'uid'            => 'corteva-colony-development',
            'acronym'        => 'Corteva Colony Development',
            'description'    => '',
            'website'        => '',
            'phone'          => '',
            'email'          => '',
            'active'         => true,
            'public'         => true,
            'sync_period'    => null, // Can be one of: null, 'hourly', 'daily', 'weekly', 'monthly', 'quarterly', 'yearly'
            'historic_sync'  => null, // Can be one of: null (no historic syncing),  false (should sync in future)
            'last_synced_at' => Carbon::now(),
        ] );

        $this->createProviders(
            [
                [
                    'full_name' => 'Corteva',
                    'slug'      => 'corteva',
                    'uid'       => 'corteva-agrisciences',
                ],
            ]
        );

        $this->createDescriptors( [
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'External ID',
                'slug'        => 'external-id',
                'description' => 'External ID',
                'category'    => 'hive',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'Apiary ID',
                'slug'        => 'apiary-id',
                'description' => 'Apiary ID',
                'category'    => 'apiary',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_locations',
                'name'        => 'Colonies location',
                'slug'        => 'colonies-location',
                'description' => 'Where is the colony situated?',
                'category'    => 'environment',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Hive scales',
                'slug'        => 'hive-weight',
                'description' => 'Beehive weight in Kilograms',
                'category'    => 'hive-sensors',
            ],
            [
                'table_name'  => 'log_data_events',
                'name'        => 'Sampling',
                'slug'        => 'event-sampling',
                'description' => 'Logged events on various sampling.',
                'category'    => 'hive-interaction',
            ],
            [
                'table_name'  => 'log_data_events',
                'name'        => 'Hive Management',
                'slug'        => 'event-hive-management',
                'description' => 'Logged events on various Hive Management tasks.',
                'category'    => 'hive-interaction',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Population',
                'slug'        => 'hive-population',
                'description' => 'Number of adult bees in the whole colony. Data provider should clarify if it includes all bees or just inside (nurse) bees (excluding foragers). Usually, data gathered from several visists during the season, does not include foragers.',
                'category'    => 'hive-observation',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Total Brood Cells',
                'slug'        => 'brood-cells',
                'description' => 'Number of brood cells in the whole colony. It includes capped, larva and egg cells.',
                'category'    => 'hive-observation',
            ],

            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Total Pollen Cells',
                'slug'        => 'beebread-cells',
                'description' => 'Number of beebread cells in the whole colony.',
                'category'    => 'hive-observation',
            ],
        ], '1.0' );

        $basepath = base_path( '/database/seeders/Connectors/raw/CORTEVA/' );


        $samples = [
            [
                'file_name'       => 'S16-01455_Control_data_Weight__Colony_Assessment__Varroa.xlsx',
                'year'            => 2016,
                'weight_tab'      => 0,
                'assessments_tab' => 1,
                'varroa_tab'      => 2,
            ],
            [
                'file_name'       => 'S18-00591_Control_data_Weight__Colony_Assessment__Varroa.xlsx',
                'year'            => 2018,
                'weight_tab'      => 0,
                'assessments_tab' => 1,
                'varroa_tab'      => 2,
                'virus_tab'       => 3,
            ],
        ];


        $apiaryId = DataId::firstOrCreate( [
            'project_id' => $this->project->id,
            'value'      => 'Pforzheim-Eutingen',
        ] );

        $hives = collect( [
            DataId::firstOrCreate( [
                'value'      => 'Ca',
                'project_id' => $this->project->id,
                'parent_id'  => $apiaryId->id,
            ] ),
            DataId::firstOrCreate( [
                'value'      => 'Cb',
                'project_id' => $this->project->id,
                'parent_id'  => $apiaryId->id,
            ] ),
            DataId::firstOrCreate( [
                'value'      => 'Cc',
                'project_id' => $this->project->id,
                'parent_id'  => $apiaryId->id,
            ] ),
            DataId::firstOrCreate( [
                'value'      => 'Cd',
                'project_id' => $this->project->id,
                'parent_id'  => $apiaryId->id,
            ] ),
            DataId::firstOrCreate( [
                'value'      => 'Ce',
                'project_id' => $this->project->id,
                'parent_id'  => $apiaryId->id,
            ] ),
        ] );

        $apiaryLocation = DataLocation::firstOrCreate( [
            'value'         => new Point( 48.9, 8.7 ),
            'location_id'   => 21415,
            'location_type' => Post::class,
        ] );

        foreach ( $samples as $sample ) {
            /**
             * Load Excel file
             */
            $data = Excel::toArray( new SemicolonCSVImport, $basepath . $sample['file_name'] );

            $aggregates = collect();

            /**
             * Start with weight import.
             */
            foreach ( $data[ $sample['weight_tab'] ] as $index => $row ) {
                if ( $index <= 1 || empty( $row[1] ) ) {
                    continue;
                }

                for ( $i = 0; $i < 5; $i ++ ) {
                    /**
                     * As weight is measured only once, we can already pre-aggregate the values by creating a seperate Log without time.
                     */

                    if ( $row[ 2 + $i ] != 'Hive dead' ) {
                        $log = $this->createLog( $row[10] . ' 23:30', 'j.m.Y H:i', true, $apiaryId->id, $hives[ $i ]->id );

                        Data::insert( [
                            [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'colonies-location' ), 'data_id' => $apiaryLocation->id, 'data_type' => 'l' ],
                            [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'apiary-id' ), 'data_id' => $apiaryId->id, 'data_type' => 'id' ],
                            [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'external-id' ), 'data_id' => $hives[ $i ]->id, 'data_type' => 'id' ]
                        ] );

                        $log->attachData( $this->descriptors['hive-weight'], $this->parseFloat( $row[ 2 + $i ] ) );
                    }

                    $log = $this->createLog( $row[10], 'j.m.Y', false, $apiaryId->id, $hives[ $i ]->id );

                    Data::insert( [
                        [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'colonies-location' ), 'data_id' => $apiaryLocation->id, 'data_type' => 'l' ],
                        [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'apiary-id' ), 'data_id' => $apiaryId->id, 'data_type' => 'id' ],
                        [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'external-id' ), 'data_id' => $hives[ $i ]->id, 'data_type' => 'id' ]
                    ] );
                    if ( $row[ 2 + $i ] != 'Hive dead' ) {
                        $log->attachData( $this->descriptors['hive-weight'], $this->parseFloat( $row[ 2 + $i ] ) );
                    } else {
                        $log->attachData( $this->descriptors['event-hive-management'], [ 'description' => $row[ 2 + $i ] ] );
                    }

                    if ( ! empty( $row[9] ) ) {
                        $log->attachData( $this->descriptors['event-hive-management'], [ 'description' => $row[5] ] );
                    }
                    if ( $aggregates->has( $row[10] ) ) {
                        $aggregates[ $row[10] ]->put( $i, $log );
                    } else {
                        $aggregates[ $row[10] ] =collect( [ $i => $log ] );
                    }
                }
            }

            /**
             * Parse Colony development.
             */
            $count = 0;
            foreach ( $data[ $sample['assessments_tab'] ] as $index => $row ) {
                if ( $index == 0 ) {
                    continue;
                }

                if ( $count == 0 ) {
                    $descriptor = $this->descriptors['hive-population'];
                } elseif ( $count == 1 ) {
                    $descriptor = $this->descriptors['brood-cells'];
                } elseif ( $count == 2 ) {
                    $descriptor = $this->descriptors['beebread-cells'];
                }

                for ( $i = 0; $i < 5; $i ++ ) {
                    if ( $row[ 3 + $i ] == 'Hive dead' ) {
                        continue;
                    }

                    if ( $aggregates->has( $row[10] ) && isset( $aggregates[ $row[10] ][ $i ] ) ) {
                        $aggregates[ $row[10] ][ $i ]->attachData( $descriptor, $this->parseInteger( $row[ 3 + $i ] ) );
                    } else {
                        $log = $this->createLog( $row[10], 'j.m.Y', false, $apiaryId->id, $hives[ $i ]->id );

                        Data::insert( [
                            [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'colonies-location' ), 'data_id' => $apiaryLocation->id, 'data_type' => 'l' ],
                            [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'apiary-id' ), 'data_id' => $apiaryId->id, 'data_type' => 'id' ],
                            [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'external-id' ), 'data_id' => $hives[ $i ]->id, 'data_type' => 'id' ]
                        ] );

                        $log->attachData( $descriptor, $this->parseInteger( $row[ 3 + $i ] ) );

                        if ( $aggregates->has( $row[10] ) ) {
                            $aggregates[ $row[10] ]->put( $i, $log );
                        } else {
                            $aggregates[ $row[10] ] = collect( [ $i => $log ] );
                        }
                    }
                }
                $count = ( $count + 1 ) % 3;
            }
        }
    }
}
