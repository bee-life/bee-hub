<?php namespace Database\Seeders\Connectors;

use App\Data\Connectors\Traits\LocationParser;
use App\Data\Connectors\VarroaAlertConnector;
use App\Exceptions\InvalidValueException;

class VarroaAlertSeeder extends BaseSeeder
{
    use LocationParser;

    /**
     * Seed the application's database.
     *
     * @return void
     * @throws InvalidValueException
     * @see VarroaAlertConnector: responsible for data loading.
     */
    protected function runSeeder(): void
    {
        $this->createProject( [
            'type'           => 'live',
            'name'           => 'VarroaAlert (Varroa-Warndienst)',
            'slug'           => 'varroaalert-varroa-warndienst',
            'uid'            => 'varroawarndienst',
            'acronym'        => 'VarroaAlert',
            'description'    => '<div>VarroaAlert (Varroa-Warndienst) is a freely accessible web-based platform launched in in 2017 by the Austrian Chamber of Agriculture (LKO) in collaboration with the Austrian Beekeeping Federation (BÖ) and the Austrian Agency for Health and Food Safety (AGES). It collects and analyses data on the infestation of honeybee colonies with the parasitic mite Varroa destructor. Infestation data is provided by beekeepers enrolled in a sentinel apiary program (currently 73 apiaries with 664 beehives distributed across Austria). A predictive algorithm performs risk assessments and triggers alerts if mite loads exceed predefined thresholds. Moreover, the system issues region specific, weather-based recommendations for an efficient application of veterinary drugs. The website, which is expected to reduce colony losses caused by Varroosis, has become an important source of information for &gt;29.000 beekeepers, counting &gt;90.000 visits per year. In 2018, an innovative technology for automated Varroa diagnosis has been integrated in the program.</div>',
            'website'        => 'https://bienengesundheit.at',
            'phone'          => '+43 676 9312369',
            'email'          => 'm.rubinigg@biene-oesterreich.at',
            'class_name'     => VarroaAlertConnector::class,
            'featured_image' => 'uG9LuhDaLk4T6I0SxTE7x2mz9PFWSjiHfjxpJjC4.jpeg',
            'sync_period'    => 'daily',
            'historic_sync'  => false,
            'active'         => true,
            'public'         => true,
            'last_synced_at' => null,
        ] );

        $this->createProviders(
            [
                [
                    'full_name'       => 'dr. Mag. rer. nat. Michael Rubinigg',
                    'name'            => 'dr. Mag. rer. nat. Michael Rubinigg',
                    'slug'            => 'dr-mag-rer-nat-michael-rubinigg',
                    'address'         => 'Georg Coch Platz 3/11a  1010 Wien Austria',
                    'registry_number' => 'ZVR 119792951',
                    'description'     => '',
                    'featured_image'  => '1MpmUSGmqZpyp8oSzdMb6yanQiWyPl3vGTMKdRIf.jpeg',
                    'website'         => 'https://www.biene-oesterreich.at',
                    'phone'           => '+43 676 9312369',
                    'email'           => 'm.rubinigg@biene-oeesterreich.at',
                    'post_id'         => 822,
                    'public'          => true,
                ],
                [
                    'full_name'       => 'Ländliche Fortbildungsinstitut Österreich',
                    'name'            => 'LFI',
                    'slug'            => 'laendliches-fortbildungsinstitut-oesterreich',
                    'address'         => 'Schauflergasse 6 1015 Wien Austria',
                    'registry_number' => 'ZVR 957299895',
                    'description'     => '',
                    'featured_image'  => '',
                    'website'         => 'https://warndienst.at',
                    'phone'           => null,
                    'email'           => null,
                    'post_id'         => 822,
                    'public'          => true,
                ],
            ]
        );

        $this->createDescriptors( [
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'External ID',
                'slug'        => 'external-id',
                'description' => 'External ID',
                'category'    => 'hive',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_locations',
                'name'        => 'Colonies location',
                'slug'        => 'colonies-location',
                'description' => 'Where is the colony situated?',
                'category'    => 'environment',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Varroa Infestation Alerts',
                'slug'        => 'varroa-infestation-alerts',
                'description' => 'What is the alertness level of Varroa Infestation',
                'category'    => 'environment-health',
            ],
            [
                'table_name'  => 'log_data_text',
                'name'        => 'Content',
                'slug'        => 'content',
                'description' => 'Any content related to the data point.',
                'category'    => 'meta',
            ],
            [
                'table_name'  => 'log_data_dates',
                'name'        => 'Valid From',
                'slug'        => 'valid-from',
                'description' => 'The provided information is valid from a certain date.',
                'category'    => 'meta',
            ],
            [
                'table_name'  => 'log_data_dates',
                'name'        => 'Valid To',
                'slug'        => 'valid-to',
                'description' => 'The provided information is valid until a certain date.',
                'category'    => 'meta',
            ],
            [
                'table_name'  => 'log_data_datetimes',
                'name'        => 'Updated at',
                'slug'        => 'updated-at',
                'description' => 'The last time the value was updated by source.',
                'category'    => 'meta',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_decimal',
                'name'        => 'Varroa infestation',
                'slug'        => 'varroa-infestation',
                'description' => 'Average Varroa infestation value',
                'category' => 'environment-health',
            ],
            [
                'table_name'  => 'log_data_decimal',
                'name'        => 'Varroa Infestation Threshold',
                'slug'        => 'varroa-investation-threshold',
                'description' => '',
                'category' => 'environment-health',
            ],
            [
                'table_name'  => 'log_data_decimal',
                'name'        => 'Varroa Infestation Threshold Deviation',
                'slug'        => 'varroa-investation-threshold-deviation',
                'description' => '',
                'category' => 'environment-health',
            ],
            [
                'table_name'  => 'log_data_datetimes',
                'name'        => 'Varroa infestation measurement time',
                'slug'        => 'varroa-infestation-measurement-time',
                'description' => 'The date and time of last Varroa Infestation measurement',
                'category' => 'environment-health',
            ],
            [
                'table_name'  => 'log_data_integer',
                'name'        => 'Apiary Total',
                'slug'        => 'apiary-counts',
                'description' => 'Total number of apiaries included in calculation',
                'category' => 'meta',
            ],
            [
                'table_name'  => 'log_data_integer',
                'name'        => 'Colony Total',
                'slug'        => 'colony-counts',
                'description' => 'Total number of Bee colonies included in calculation',
                'category' => 'meta',
            ],
            [
                'table_name'  => 'log_data_integer',
                'name'        => 'Data Points Total',
                'slug'        => 'data-counts',
                'description' => 'Total number of raw data point included in calculation',
                'category' => 'meta',
            ],
        ], '1.0' );
    }
}
