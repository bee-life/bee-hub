<?php namespace Database\Seeders\Connectors;

use App\Data\Connectors\CARIScalesConnector;
use App\Data\Connectors\Traits\LocationParser;
use App\Models\Origins\DeviceCategory;

class CARIScalesSeeder extends BaseSeeder
{
    use LocationParser;

    /**
     * Seed the application's database.
     *
     * @return void
     * @see CARIScalesConnector: responisble for data load.
     */
    protected function runSeeder(): void
    {
        $this->createProject( [
            'type'           => 'live',
            'name'           => 'National Beekeeping Programme - Wallonie (BE)',
            'slug'           => 'national-beekeeping-programme',
            'uid'            => 'cari-scales',
            'acronym'        => 'National Beekeeping Programme',
            'description'    => '<div>National Beekeeping Programmes are subsidies provided to the beekeeping sector for its development. They come from the Common Agricultural Policy budget. In the Wallonia region of Belgium, the main beekeeping leaders (UFAWB, URRW and CARI) defined twenty years ago the bases of operation of what was called \"the Honey programme\". In order to be able to keep the relevance of the actions undertaken, they defined the mode of operation of the Steering Committee, now renamed the \"Honey Committee\", which is responsible for defining the lines of work and adapting them according to the new requirements. Provincial representation (ideally two representatives per province and for Brussels) and one representative from each of the associations were needed to improve the transmission of information between the different structures. The election is democratic and direct, which is why each beekeeper present at the \"Namur day\" (a day where the annual activities are presented to the regional beekeeping sector) has the opportunity to elect the candidates of his/her choice. Candidatures are open to everyone who is willing to invest in the follow-up of the programme. This programme is executed by the <a href=\"http://www.cari.be/\">CARI</a> where its permanent staff team makes sure to implement the activities defined by the Honey Committee.<br><br>More on how the European Beekeeping Programmes are applied in Wallonia <a href=\"http://www.cari.be/medias/actuapi/actuapi69.pdf\">here</a>.<br><br>More on the National Beekeeping Programmes of different EU countries <a href=\"https://ec.europa.eu/info/food-farming-fisheries/animals-and-animal-products/animal-products/honey/national-apiculture-programmes_en\">here</a>.<br><br></div>',
            'website'        => 'https://cari.be',
            'phone'          => '+32 10 47 34 16',
            'email'          => 'info@cari.be',
            'featured_image' => 'yMZ6L60AXwMkRn5H6unOPR6lz0X7hc9GBSZAJpyd.jpeg',
            'class_name'     => CARIScalesConnector::class,
            'active'         => true,
            'public'         => true,
            'sync_period'    => 'daily',
            'historic_sync'  => false,
            'last_synced_at' => null,
        ] );

        $this->createProviders(
            [
                [
                    'full_name'       => 'CARIasbl - Beekeeping Center of Research and Information',
                    'name'            => 'CARIasbl',
                    'slug'            => 'cariasbl-beekeeping-center-of-research-and-information',
                    'uid'             => 'cari-beekeeping-center-of-research-and-information',
                    'address'         => 'Place Croix du Sud, 1 bte L7.04.01\r\n1348 LOUVAIN-LA-NEUVE\r\nBELGIQUE',
                    'description'     => '<div>CARI is a Belgian non-profit association created in June 1983 by a team of researchers from the Laboratory of Ecology of the Catholic University of Louvain (UCL). CARI is a Belgian association with a global outlook. The CARI: - provides services to beekeepers to enhance the value of their products (e.g. analyses of honey by an accredited laboratory). - disseminates beekeeping information by publishing the magazine Abeilles&amp;Cie but also by sharing content on the Internet (follow our blog: butine.info and find us on social networks). - offers several specialized weekend training per year (\"Les week-ends du CARI\"). - engages in research projects and collaborates with university teams. - participates in regional development (is co-financed by the European Honey Programme for Wallonia and Brussels; provides technical follow-up within this framework; runs the Bee Wallonie project). - exports a positive image of Walloon beekeeping and welcomes and supports BeeLife, an association formed by professionals in the beekeeping sector from different countries of the European Union working for the protection of pollinators from environmental threats.</div><div><br></div>',
                    'registry_number' => 'BE0424644620',
                    'website'         => 'http://www.cari.be',
                    'phone'           => '+32 10 47 34 16',
                    'email'           => 'info@cari.be',
                    'featured_image'  => 'ftVAmGz4VfDctOXQeszZ7pMm6uODksPXbwDXFo50.jpeg',
                    'post_id'         => 2484,
                    'public'          => true,
                ],
            ]
        );

        $this->createDescriptors( [
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'External ID',
                'slug'        => 'external-id',
                'description' => 'External ID',
                'category'    => 'hive',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'Apiary ID',
                'slug'        => 'apiary-id',
                'description' => 'Apiary ID',
                'category'    => 'apiary',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_locations',
                'name'        => 'Colonies location',
                'slug'        => 'colonies-location',
                'description' => 'Where is the colony situated?',
                'category'    => 'environment',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_datetimes',
                'name'        => 'Updated at',
                'slug'        => 'updated-at',
                'description' => 'The last time the value was updated by source.',
                'category'    => 'meta',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Hive scales',
                'slug'        => 'hive-weight',
                'description' => 'Beehive weight in Kilograms',
                'category'    => 'hive-sensors',
            ],
            [
                'table_name'  => 'log_data_percentages',
                'name'        => 'Outside relative humidity',
                'slug'        => 'relative-humidity',
                'description' => 'Outside relative humidity in percentage',
                'category'    => 'weather-sensors',
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Outside temperature',
                'slug'        => 'temperature',
                'description' => 'Outside temperature in Celsius degrees',
                'category'    => 'weather-sensors',
            ],
            [
                'table_name'  => 'log_data_decimal_ranges',
                'name'        => 'Daily Outside temperature',
                'slug'        => 'temperature-daily',
                'description' => 'Outside lowest and highest temperature in Celsius degrees',
                'category'    => 'weather-aggregates',
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Inside temperature',
                'slug'        => 'hive-temperature',
                'description' => 'Inside temperature in Celsius degrees',
                'category'    => 'hive-sensors',
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Rain',
                'slug'        => 'rain',
                'description' => 'Rain in liters per square meter',
                'category'    => 'weather-sensors',
            ],

        ], '1.0' );

        $capaz                = $this->createVendor( [
            'name'    => 'CAPAZ',
            'uid'     => 'capaz',
            'website' => 'https://capaz.de',
        ] );
        $weightSensorCategory = $this->createDeviceCategory( [
                'name' => 'Hive Weight sensors',
                'uid'  => 'hive-weight-sensors',
            ]
        );
        $scaleSensor          = $this->createDevice( [
            'name'        => 'Stockwaage GSM 200',
            'uid'         => 'capaz-stockwaage-gsm-200',
            'vendor_id'   => $capaz->id,
            'category_id' => $weightSensorCategory->id,
        ] );
    }
}
