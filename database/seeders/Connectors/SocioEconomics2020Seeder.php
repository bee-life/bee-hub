<?php namespace Database\Seeders\Connectors;

use App\Data\Aggregators\SocioEconomicAlgorithms;
use App\Data\Aggregators\SocioeconomicPollinators;
use App\Data\Connectors\Traits\LocationParser;
use App\Exceptions\InvalidValueException;
use App\Imports\CommaCSVImport;
use App\Imports\SemicolonCSVImport;
use App\Jobs\AggregateDataItemJob;
use App\Models\Locations\Region;
use App\Models\Origins\Publication;
use App\Models\References\CropPollinationDependency;
use App\Models\References\CropSpecies;
use App\Models\References\Currency;
use Carbon\Carbon;
use Excel;
use Str;

class SocioEconomics2020Seeder extends BaseSeeder
{
    use LocationParser;

    /**
     * Seed the application's database.
     *
     * @return void
     * @throws InvalidValueException
     * @todo:
     *      - Handle correctly BELU region
     *      - Include the missing data in the database (annotations)
     *
     */
    protected function runSeeder(): void
    {
        $this->createProject( [
            'type'           => 'static',
            'name'           => 'European Social-Economic Statistics on Beekeeping',
            'acronym'        => 'Social-Economic Statistics',
            'uid'            => 'social-economic-statistics',
            'slug'           => 'european-social-economic-statistics-on-beekeeping',
            'active'         => true,
            'public'         => true,
            'sync_period'    => null, // Can be one of: null, 'hourly', 'daily', 'weekly', 'monthly', 'quarterly', 'yearly'
            'historic_sync'  => null, // Can be one of: null (no historic syncing),  false (should sync in future)
            'last_synced_at' => Carbon::now(),
        ] );

        $this->createProviders(
            [
                [
                    'full_name'       => 'Biene Österreich - Imkereidachverband',
                    'name'            => 'Biene Österreich',
                    'uid'             => 'biene-oesterrecih-imkereidachverband',
                    'slug'            => 'biene-oesterrecih-imkereidachverband',
                    'address'         => 'Georg-Coch Platz3/11a, 1010 Wien',
                    'featured_image'  => '1MpmUSGmqZpyp8oSzdMb6yanQiWyPl3vGTMKdRIf.jpeg',
                    'registry_number' => 'ZVR 119792951',
                    'website'         => 'https://www.biene-oesterreich.at/',
                    'phone'           => '+43 6767703157',
                    'email'           => 'office@biene-oesterreich.at',
                    'post_id'         => 822,
                    'public'          => true,
                ],
                [
                    'full_name'       => 'Food and Agriculture Organization of the United Nations',
                    'name'            => 'FAO',
                    'uid'             => 'fao',
                    'slug'            => 'fao',
                    'address'         => '',
                    'registry_number' => '',
                    'description'     => '',
                    'featured_image'  => '',
                    'website'         => 'http://www.fao.org/faostat',
                    'public'          => true,
                ],
            ]
        );

        $this->createDescriptors( [
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'External ID',
                'slug'        => 'external-id',
                'description' => 'External ID',
                'category'    => 'hive',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'Apiary ID',
                'slug'        => 'apiary-id',
                'description' => 'Apiary ID',
                'category'    => 'apiary',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_locations',
                'name'        => 'Colonies location',
                'slug'        => 'colonies-location',
                'description' => 'Where is the colony situated?',
                'category'    => 'environment',
                'public'      => false,
            ],
        ], '1.0' );


        $regions            = Region::select( [ 'id', 'nuts_id', 'name', 'country_id' ] )->with( [ 'country' ] )->get()->keyBy( 'nuts_id' );
        $currencyReferences = Currency::all()->keyBy( 'code' );

        $basePath = base_path( '/database/seeders/Connectors/raw/SOCIO-ECONOMICS/2020/' );

        $cropSpecies = collect();
        $data        = Excel::toArray( new SemicolonCSVImport, $basePath . '/crop_species.csv' )[0];
        $parents     = collect();
        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }
            $cropSpecies[ $row[0] ] = CropSpecies::firstOrCreate( [
                'name'         => $row[1],
                'species_name' => $row[2] == 'NULL' ? null : $row[2],
                'gbif_id'      => $row[3] == 'NULL' ? null : $row[3],
            ] );
            $parents[ $row[0] ]     = $row[4] == 'NULL' ? null : $row[4];
        }
        foreach ( $cropSpecies as $id => $crop ) {
            $crop->parent_id = $parents[ $id ];
            $crop->save();
        }

        $references = collect();
        $data       = Excel::toArray( new SemicolonCSVImport, $basePath . '/reference.csv' )[0];

        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }

            $references[ $row[0] ] = Publication::create( [
                'author_short'     => $row[1],
                'author'           => $row[2] ?? $row[1],
                'year'             => $row[3],
                'title'            => $row[4],
                'medium_short'     => $row[5] ?? null,
                'medium'           => $row[6] ?? null,
                'editor_short'     => $row[7] ?? null,
                'editor'           => $row[8] ?? null,
                'issue'            => $row[9] ?? null,
                'volume'           => $row[10] ?? null,
                'edition'          => $row[11] ?? null,
                'pages'            => $row[12] ?? null,
                'website'          => $row[13] ?? null,
                'website_date'     => $row[14] ?? null,
                'publication_type' => $row[15],
            ] );
        }


        $organizations = collect();
        $data          = Excel::toArray( new SemicolonCSVImport, $basePath . '/organisation.csv' )[0];

        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }

            $organizations[ $row[0] ] = [
                'acronym' => $row[1],
                'name'    => $row[2],
                'website' => $row[3],
            ];
        }
        $operators = collect();
        $data      = Excel::toArray( new SemicolonCSVImport, $basePath . '/operator.csv' )[0];

        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }

            $operators[ $row[0] ] = [
                'name'            => $row[1],
                'surname'         => $row[2],
                'organisation_id' => $row[3], // References $organizations key.
            ];
        }
        $annotations = collect();
        $data        = Excel::toArray( new SemicolonCSVImport, $basePath . '/annotation.csv' )[0];

        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }

            $annotations[ $row[0] ] = [
                'name'        => $row[1],
                'operator_id' => $row[2], // References $operators key.
            ];
        }

        $items = collect();
        $data  = Excel::toArray( new SemicolonCSVImport, $basePath . '/item.csv' )[0];

        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }

            $items[ $row[0] ] = [
                'name'      => $row[1],
                'parent_id' => $row[2] != 'NULL' ? $row[2] : null,  // References $items (itself) key.
            ];
        }
        $production = collect();
        $data       = Excel::toArray( new CommaCSVImport(), $basePath . '/production.csv' )[0];

        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }
            if ( $regions->has( $row[1] ) ) {
                if ( $row[5] == 2 ) {
                    $unit = 'tonnes';
                } else {
                    $unit = 'hectare';
                }

                $descriptor = 'production-' . Str::slug( $items[ $row[4] ]['name'] ) . '-' . $unit;

                if ( ! $this->descriptors->has( $descriptor ) ) {
                    $this->createDescriptors( [
                        [
                            'table_name'  => 'log_data_decimals',
                            'name'        => $items[ $row[4] ]['name'] . ' ' . $items[ $row[3] ]['name'] . ' in ' . $unit,
                            'slug'        => $descriptor,
                            'description' => '',
                            'category'    => 'socioeconomic-production' . ( $unit == 'hectare' ? '-area' : null ),
                            'public'      => false,
                            'since'       => '1.2',
                        ],
                    ] );
                }

                $production_key = $row[1] . '_' . $row[2];

                if ( $production->has( $production_key ) ) {
                    $log = $production[ $production_key ];
                } else {
                    $log = $this->createLog( $row[2], 'Y', false );
                    $log->attachData( $this->descriptors['colonies-location'], [
                        'location_id'   => $regions[ $row[1] ]->id,
                        'location_type' => get_class( $regions[ $row[1] ] ),
                    ] );
                }

                $log->attachData( $this->descriptors[ $descriptor ], [ 'value' => intval( $row[8] ) ], $references[ $row[5] ] );

                $production[ $production_key ] = $log;
            }
        }

        $prices = collect();
        $data   = Excel::toArray( new CommaCSVImport(), $basePath . '/prices.csv' )[0];

        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }
            if ( $regions->has( $row[1] ) ) {
                $descriptor = 'prices-' . Str::slug( $items[ $row[4] ]['name'] );

                if ( ! $this->descriptors->has( $descriptor ) ) {
                    $this->createDescriptors( [
                        [
                            'table_name'  => 'log_reference_currencies',
                            'name'        => $items[ $row[4] ]['name'] . ' ' . $items[ $row[3] ]['name'],
                            'slug'        => $descriptor,
                            'uid'         => $descriptor,
                            'description' => '',
                            'category'    => 'socioeconomic-prices',
                            'public'      => false,
                            'since'       => '1.2',
                        ],
                    ] );
                }
                $prices_key = $row[1] . '_' . $row[2];

                if ( $prices->has( $prices_key ) ) {
                    $log = $prices[ $prices_key ];
                } else {
                    $log = $this->createLog( $row[2], 'Y', false );
                    $log->attachData( $this->descriptors['colonies-location'], [
                        'location_id'   => $regions[ $row[1] ]->id,
                        'location_type' => get_class( $regions[ $row[1] ] ),
                    ] );
                }

                if ( $row[5] == 6 ) {
                    $currency_id = $currencyReferences['USD']->id;
                } else {
                    $currency_id = $regions[ $row[1] ]->country->currency->id;
                }

                $log->attachData( $this->descriptors[ $descriptor ], [ 'value' => intval( $row[8] ), 'reference_id' => $currency_id ], $references[ $row[5] ] );

                $prices[ $prices_key ] = $log;
            }
        }

        $beehives = collect();
        $data     = Excel::toArray( new CommaCSVImport(), $basePath . '/beehives.csv' )[0];

        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }
            if ( $regions->has( $row[1] ) ) {
                $descriptor = substr( 'livestock-' . Str::slug( $items[ $row[3] ]['name'] ), 0, 63 );

                if ( ! $this->descriptors->has( $descriptor ) ) {
                    $this->createDescriptors( [
                        [
                            'table_name'  => 'log_data_decimals',
                            'name'        => $items[ $row[3] ]['name'],
                            'slug'        => $descriptor,
                            'description' => '',
                            'category'    => 'socioeconomic-livestock',
                            'public'      => false,
                            'since'       => '1.2',
                        ],
                    ] );
                }


                $log = $this->createLog( $row[2], 'Y', false );
                $log->attachData( $this->descriptors['colonies-location'], [
                    'location_id'   => $regions[ $row[1] ]->id,
                    'location_type' => get_class( $regions[ $row[1] ] ),
                ] );

                $log->attachData( $this->descriptors[ $descriptor ], [ 'value' => intval( $row[7] ) ], $references[ $row[4] ] );


                $beehives[ $row[0] ] = $log;
            }
        }

        $pollination_dependency = collect();

        $data = Excel::toArray( new CommaCSVImport(), $basePath . '/pollination_dependency.csv' )[0];

        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }
            $pollination_dependency[ $row[0] ] = CropPollinationDependency::firstOrCreate( [
                'crop_species_id' => $cropSpecies[ $row[2] ]->id,
                'impact'          => $row[6] == 'NULL' ? null : $row[6],
                'dependency_from' => $row[7] == 'NULL' ? null : $row[7],
                'dependency_to'   => $row[8] == 'NULL' ? null : $row[8],
            ] );
        }

        $objects = [
            new SocioEconomicAlgorithms(),
            new SocioeconomicPollinators(),
        ];

        foreach ( $objects as $object ) {
            ( new AggregateDataItemJob( $object ) )->handle();
        }
    }
}
