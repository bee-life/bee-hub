<?php namespace Database\Seeders\Connectors;

use App\Data\Connectors\Traits\LocationParser;
use App\Exceptions\InvalidValueException;
use App\Models\Origins\Methodology;
use App\Models\Origins\MethodologyCategory;
use Carbon\Carbon;

class EFPSAAlgorithmsForExistingDataSeeder extends BaseSeeder
{
    use LocationParser;

    /**
     * Seed the application's database.
     *
     * @return void
     * @throws InvalidValueException
     */
    protected function runSeeder(): void
    {
        // We need to create Descriptors that are required for existing data algorithms,
        // produced for EFSA EU Bee Partnership Prototype Platform.
        $this->createProject( [
            'type'           => 'processed',
            'name'           => 'EU Bee Partnership Prototype Plaftorm',
            'slug'           => 'eu-bee-partnership-prototype-platform',
            'uid'            => 'eubeeppp',
            'acronym'        => 'EUBEEPPP',
            'description'    => '',
            // 'website'        => '',
            // 'phone'          => '',
            // 'email'          => '',
            'class_name'     => null,
            'active'         => true,
            'public'         => true,
            'last_synced_at' => Carbon::now(),
        ] );
        $this->createProviders(
            [
                [
                    'full_name'       => 'European Food Safety Authority',
                    'name'            => 'EFSA',
                    'slug'            => 'european-food-safety-authority',
                    'address'         => 'Via Carlo Magno 1A
43126 Parma - ITALY',
                    'description'     => '',
                    'registry_number' => '',
                    'website'         => 'http://www.efsa.europa.eu/',
                    'phone'           => '+39 0521 036 111',
                    'email'           => '',
                    'post_id'         => 69938,
                    'public'          => true,
                ],
            ]
        );

        $this->createDescriptors( [
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'External ID',
                'slug'        => 'external-id',
                'description' => 'External ID',
                'category'    => 'hive',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_locations',
                'name'        => 'Colonies location',
                'slug'        => 'colonies-location',
                'description' => 'Where is the colony situated?',
                'category'    => 'environment',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_datetimes',
                'name'        => 'Updated at',
                'slug'        => 'updated-at',
                'description' => 'The last time the value was updated by source.',
                'category'    => 'meta',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_integer',
                'name'        => 'Data Points Total',
                'slug'        => 'data-counts',
                'description' => 'Total number of raw data point included in calculation',
                'category' => 'meta',
            ],
            // Data Aggregation
            [
                'table_name'  => 'log_data_decimal_ranges',
                'name'        => 'Daily Outside temperature',
                'slug'        => 'temperature-daily',
                'description' => 'Outside lowest and highest temperature in Celsius degrees',
                'category'    => 'weather-aggregates',
            ],
            [
                'table_name'  => 'log_data_decimal_ranges',
                'name'        => 'Daily Hive temperature',
                'slug'        => 'hive-temperature-daily',
                'description' => 'In-hive lowest and highest temperature in Celsius degrees',
                'category'    => 'hive-aggregates',
            ],
            [
                'table_name'  => 'log_data_percentages',
                'name'        => 'Winter mortality',
                'slug'        => 'winter-mortality-aggregate',
                'description' => 'Percent of Bee mortality during Winter.',
                'category'    => 'hive-aggregates',
            ],
            // 2.1. Colony management detection
            [
                'table_name'  => 'log_data_events',
                'name'        => 'Hive Management Detection',
                'slug'        => 'event-hive-management-prediction',
                'description' => 'Predicted events on various Hive Management tasks.',
                'category'    => 'hive-interaction',
            ],
            // 2.2. Daily colony production
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Colony daily production',
                'slug'        => 'hive-production',
                'description' => 'Amount of produce gathered in a day.',
                'category'    => 'hive-calculations',
            ],
            // 2.3. Colony Production period detection
            [
                'table_name'  => 'log_data_date_range',
                'name'        => 'Colony Production Period',
                'slug'        => 'hive-production-period',
                'description' => 'Higher colony production period.',
                'category'    => 'hive-calculations',
            ],        [
                'table_name'  => 'log_data_date_range',
                'name'        => 'Colony Production Period',
                'slug'        => 'hive-super-production-period',
                'description' => 'Highest colony production period.',
                'category'    => 'hive-calculations',
            ],
            [
                'table_name'  => 'log_data_date_range',
                'name'        => 'Colony Wintering Duration',
                'slug'        => 'hive-wintering-duration',
                'description' => 'Time of last honey extraction and first spring production.',
                'category'    => 'hive-calculations',
            ],
            // 2.4. Colony consumption during the winter
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Colony Wintering Consumption',
                'slug'        => 'hive-wintering-consumption',
                'description' => 'Amount of produce consumed during a wintering duration (since last honey extraction and until first spring production).',
                'category'    => 'hive-calculations',
            ],
            // 2.5. Metabolic resting state
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Colony Metabolic Resting State',
                'slug'        => 'hive-metabolic-resting-state',
                'description' => 'Amount of produce consumed during metabolic resting state (night).',
                'category'    => 'hive-calculations',
            ],
            // 2.6. Available flight time
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Maximum available flight time',
                'slug'        => 'maximum-available-flight-time',
                'description' => 'The production of a beehive depends highly on the available flight time of bees and the available resources in the environment. If it is possible to calculate the amount of available flight time based on available sensor data, better insights could be given into what influences honey production. This information provides the potential for foraging coming from the colony. For example, if two apiaries are positioned in two highly different elevations and environments, with the help of the temperature, humidity, rain and solar radiation sensors one could determine if the cause of lower production is directly caused by reduced flight time in this less suitable environment. At a later stage, it is possible to complement the calculation with climatic and landscape data if this data is made available, so that the availability of resources can be taken into account. The data is provided in hours per day.',
                'category'    => 'hive-calculations',
            ],
        ], '1.1' );

        // Todo: Include EUBEEPPP Logo.
        $methodologyCategory = $this->createMethodologyCategory( [
            'name'        => 'EUBEEPPP Existing Data Algorithms',
            'uid'         => 'eubeeppp-existing',
            'description' => 'A group of Algorithms developed for the existing datasets within the European Bee Partnerships Prototype Platform on Bee Health. The existing data is based on the Proof of Concept Bee-Hub platform.',
        ] );

        $this->createMethodologies( [
            [
                'name'        => 'Colony management detection Algorithm',
                'uid'         => 'eubeeppp-colony-management-detection',
                'description' => 'Weight (scales) and temperature sensors gathered so far in the PoC do not include colony management data, i.e. what the beekeeper did on what day or at what time. Without this information, the context of what is happening within the hives can only be deducted from the data of hive scales and inside temperature. Additionally, we can use the weather station data to exclude natural events like snowing.<br>
Based on years of beekeeping management practices we are able to identify certain management events happening within the colony based on the scale data.<br>
These kinds of assumptions are not without a risk, as we could be misinterpreting the data.',
                'category_id' => $methodologyCategory->id,
            ],
            [
                'name'        => 'Daily colony production',
                'uid'         => 'eubeeppp-daily-colony-production',
                'description' => 'For a beekeeper the most important information is to know the production of his/her colonies. According to the data provider CARI asbl, the scale information is the part of their website that is most visited by its members. Beekeepers seek to have a benchmarket for their production and they survey the evolution of the weight of referenced monitored hives in their region. If we can calculate average productivity within regions, we can provide an indication about how much honey a colony may produce now and in the future to the beekeepers in the region.<br>
In order to calculate this, we first need to calculate the daily production of a colony. Because the amount of collected pollen, nectar and honeydew, and consequently produced honey, and wax, varies greatly between species, geographical location and time, we can only calculate net production of all colony resources per day. Furthermore, the liquid/semi liquid component of the hive contributes the most to the change of weight of the hive.<br>
Farmers may be willing to have access to the daily weight gain (or trends in the period, see 2.3) of the colonies located in the surroundings of their fields as from the hive weight gain, they may get an indication about the efficacy of the cropping practices they endeavor (e.g. planing edges, flowering strips, select specific varieties of crops, manage their spraying activity, etc.)<br>
Citizens may be curious to explore the daily weight gain of the honey bee colonies (or trends in the period, see 2.3) in their surroundings for educational or recreational purposes, because they are curious to see how the bees of their beekeeper neighbours are developing or because they know that if bees thrive, the environment in which they are living is healthy.',
                'category_id' => $methodologyCategory->id,
            ],
            [
                'name'        => 'Colony Production period detection',
                'uid'         => 'eubeeppp-colony-production-period-detection',
                'description' => 'For a beekeeper it is important to know when their bees are most productive and how they need to adapt their management practices to work in synergy with the development of their colonies (e.g. add honey supers or prevent swarming). Also it is important for them to understand when it is best to harvest the honey supers, in order either to maximise their productions or to get to an optimum balance of harvesting and feeding.<br>
Additionally, decision makers may be interested in using the honey bee colonies as bio-indicators of the landscape and apply this concept to different policies affecting the landscape, such as the agricultural policy, the habitats directive, or the sustainable use of pesticide directive. The former two, for example, affect the landscape profile in increasing or reducing the flowering resources available to pollinators. A poor evolution of the colony weight in some periods of the year and in certain locations, may indicate a problem in the implementation of the latter, despite of the fact that there are more adapted approaches to verify it.   By watching trends of colony production, we can discover  increases or decreases in production trends within a period of time or even along the season. ',
                'category_id' => $methodologyCategory->id,
            ],
            [
                'name'        => 'Colony consumption during the winter',
                'uid'         => 'eubeeppp-colony-winter-consumption',
                'description' => 'The productive beekeeping season finalises with the last harvesting of honey. Afterwards it is common practice to estimate the level of honey reserves of the colonies to check if they have enough to survive the winter. Should this not be the case, then the beekeepers normally feed their colonies with a sugar-content syrup.  Nonetheless, these feeding events might be done too late  or may not provide the necessary amount and compromise colony winter survival.',
                'category_id' => $methodologyCategory->id,
            ],
            [
                'name'        => 'Metabolic resting state of the colony',
                'uid'         => 'eubeeppp-colony-metabolic-resting-state',
                'description' => 'Inside the colonies, honeybees rely on nectar and pollen consumption for their own survival or to feed their brood and the queen. During the day during the active foraging period, their high foraging activity creates large weight variations and the consumption information gets mixed with the foraging. As a result, it is impossible to calculate the amount of reserves that the colony is consuming. Nonetheless, during the night, there is no foraging activity, all the bees are inside the colony and the work of the bees is devoted to evaporating the water of the nectar collected during the day and basal consumption. Outside of the active foraging season, when there is no foraging and therefore no work on transformation of nectar into honey, the weight of the colony can give us an indication about the metabolic resting state. The amount of activity of bees is however reduced because there is less caring work to take deal with, as the queen reduces her laying activity. The colony metabolic resting state by the whole colony could be estimated at a level between the weight during the night’s active period and the inactive period.',
                'category_id' => $methodologyCategory->id,
            ],
            [
                'name'        => 'How many hours of flight time are available to the bees',
                'uid'         => 'eubeeppp-available-flight-time',
                'description' => 'The production of a beehive depends highly on the available flight time of bees and the available resources in the environment. If it is possible to calculate the amount of available flight time based on available sensor data, better insights could be given into what influences honey production. This information provides the potential for foraging coming from the colony. For example, if two apiaries are positioned in two highly different elevations and environments, with the help of the temperature, humidity, rain and solar radiation sensors one could determine if the cause of lower production is directly caused by reduced flight time in this less suitable environment. At a later stage, it is possible to complement the calculation with climatic and landscape data if this data is made available, so that the availability of resources can be taken into account. ',
                'category_id' => $methodologyCategory->id,
            ],
        ] );

        $methodologyCategory = $this->createMethodologyCategory( [
            'name'        => 'EUBEEPPP New Data Algorithms',
            'uid'         => 'eubeeppp-new',
            'description' => 'A group of Algorithms developed for the new datasets within the European Bee Partnerships Prototype Platform on Bee Health. The existing data is based on the Proof of Concept Bee-Hub platform.',
        ] );


        $this->createMethodologies( [
            [
                'name'        => 'Production yield per region area.',
                'uid'         => 'eubeeppp-production-per-area',
                'description' => 'To make production data from FAOSTAT comparable between countries, we introduced a simple calculation: division of production in kilograms with sizes of the region in square kilometers.',
                'category_id' => $methodologyCategory->id,
            ],
        ] );
    }

}
