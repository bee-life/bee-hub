<?php namespace Database\Seeders\Connectors;

use App\Data\Connectors\Traits\LocationParser;
use App\Exceptions\InvalidValueException;
use App\Imports\SemicolonCSVImport;
use App\Models\Locations\Region;
use App\Models\Logs\Log;
use App\Models\Origins\Publication;
use App\Models\Origins\Source;
use Carbon\Carbon;
use Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class ColossSeeder2015 extends BaseSeeder
{
    use LocationParser;

    /**
     * Seed the application's database.
     *
     * @return void
     * @throws InvalidValueException
     */
    protected function runSeeder(): void
    {
        $this->createProject( [
            'type'           => 'static',
            'name'           => 'Prevention of honey bee COlony LOSSes',
            'slug'           => 'prevention-of-honey-bee-colony-losses-2015',
            'uid'            => 'coloss-2015',
            'acronym'        => 'COLOSS',
            'description'    => 'The colony loss monitoring group has been active since the start of the COLOSS COST action, now the COLOSS Association, to study reasons for colony losses. Participating countries each carry out an annual survey of beekeepers by questionnaire, with the aim of collecting information from a nationally representative sample of beekeepers. This makes it possible to compare colony loss rates between countries and to use the international data collected to understand better the distribution of colony losses. To enable proper comparisons, a standardised beekeeper questionnaire was developed and is updated each year by the group for use by each country.  About 30 countries currently participate in monitoring. At the individual level, many thousands of beekeepers take part each year. This is an example of citizen science/crowdsourcing, with the voluntary participation of many beekeepers enabling much wider scale data collection than would otherwise be possible. Participating countries return their data for analysis annually before an agreed deadline, usually 1st of July, and the results are used for press releases and scientific publications. We produce jointly authored journal papers for dissemination of results to the research community.
In Belgium a number of institutions have historically participated to the collection of winter mortality data, including the FAB-BBF (Federation Apicole Belge - Belgische Bijenhoudersfederatie), CARI, HoneybeeValley, CRA-Wallonie, and the Université de Liege.

More information of the global project <a href="https://coloss.org/core-projects/colony-losses-monitoring/">here</a>.',
            'website'        => 'https://coloss.org/',
            // 'phone'          => '',
            // 'email'          => '',
            'class_name'     => null,
            'active'         => true,
            'public'         => true,
            'last_synced_at' => Carbon::now(),
        ] );

        $this->createProviders(
            [
                [
                    'full_name'       => 'Brodschneider et al 2017',
                    'name'            => '',
                    'slug'            => '',
                    'uid'             => '',
                    'address'         => '',
                    'registry_number' => '',
                    'description'     => '',
                    'featured_image'  => '',
                    'website'         => '',
                    'phone'           => '',
                    'email'           => '',
                    'post_id'         => null,
                    'public'          => true,
                ],

            ]
        );

        $this->createDescriptors( [
            [
                'table_name'  => 'log_data_locations',
                'name'        => 'Colonies location',
                'slug'        => 'colonies-location',
                'description' => 'Where is the colony situated?',
                'category'    => 'environment',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_percentages',
                'name'        => 'Winter mortality',
                'slug'        => 'winter-mortality-percentage',
                'description' => 'Percent of Bee mortality during Winter.',
                'category'    => 'hive-health',
            ],
            [
                'table_name'  => 'log_data_integer',
                'name'        => 'Beekeeper counts',
                'slug'        => 'beekeeper-count',
                'description' => 'Number of Beekeepers',
                'category'    => 'meta',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Autumn Count before winter',
                'slug'        => 'colony-count-autumn',
                'description' => 'How many colonies where alive before winter',
                'category'    => 'apiary-observation',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Spring Count after winter',
                'slug'        => 'colony-count-spring',
                'description' => 'How many colonies where alive after winter',
                'category'    => 'apiary-observation',
            ],
            [
                'table_name'  => 'log_data_decimal_ranges',
                'name'        => 'Winter mortality Confidence Interval',
                'slug'        => 'winter-mortality-confidence-interval',
                'description' => 'The confidence interval while calculating the winter mortality.',
                'category'    => 'hive-health',
            ],
            [
                'table_name'  => 'log_data_percentages',
                'name'        => 'Winter mortality',
                'slug'        => 'winter-mortality-aggregate',
                'description' => 'Percent of Bee mortality during Winter.',
                'category'    => 'hive-aggregates',
            ],
        ], '1.0' );

        $data = Excel::toArray( new SemicolonCSVImport, base_path( "/database/seeders/Connectors/raw/COLOSS/Dataset - Brodschneider et al 2017.xlsx" ) )[0];

        $origin = Publication::firstOrCreate( [
            'title'            => 'Preliminary analysis of loss rates of honey bee colonies during winter 2015/16 from the COLOSS survey',
            'uid'              => 'preliminary-analysis-of-loss-rates-of-honey-bee-colonies-during-',
            'author'           => 'Robert Brodschneider, Alison Gray, Romée van der Zee, Noureddine Adjlane, Valters Brusbardis, Jean-Daniel Charrière, Robert Chlebo, Mary F Coffey, Karl Crailsheim, Bjørn Dahle, Jiří Danihlík, Ellen Danneels, Dirk C de Graaf, Marica Maja Dražić, Mariia Fedoriak, Ivan Forsythe, Miroljub Golubovski, Ales Gregorc, Urszula Grzęda, Ian Hubbuck, Rahşan İvgin Tunca, Lassi Kauko, Ole Kilpinen, Justinas Kretavicius, Preben Kristiansen, Maritta Martikkala, Raquel Martín-Hernández, Franco Mutinelli, Magnus Peterson, Christoph Otten, Aslı Ozkirim, Aivar Raudmets, Noa Simon-Delso, Victoria Soroker, Grazyna Topolska, Julien Vallon, Flemming Vejsnæs & Saskia Woehl',
            'year'             => 2016,
            'website'          => 'https://doi.org/10.1080/00218839.2016.1260240',
            'publication_type' => 'Report',
        ] );


        foreach ( $data as $key => $row ) {
            if ( $key == 0 || empty( $row[0] ) ) {
                continue;
            }

            switch ( trim( $row[0] ) ) {
                case 'Northern Ireland':
                    $country = 458;
                    break;
                case 'Wales':
                    $country = 13;
                    break;
                case 'Scotland':
                    $country = 467;
                    break;
                case 'Italy (Veneto region':
                    $country = 824;
                    break;
                case 'England/Wales':
                    // Not compatible with our Nuts declaration, so skip it
                    continue 2;
                default:
                    $country = $this->parseRegionFromCountry( trim( $row[0] ), true );
            }

            $spring_count       = intval( $row[2] );
            $winter_loss        = floatval( str_replace( ',', '.', $row[3] ) );
            $autumn_count       = intval( $spring_count - $spring_count * $winter_loss * 0.01 );
            $winter_loss_ci_min = floatval( str_replace( ',', '.', $row[4] ) );
            $winter_loss_ci_max = floatval( str_replace( ',', '.', $row[5] ) );

            $log = Log::create( [
                'year'       => 2015,
                'project_id' => $this->project->id,
            ] );

            $log->attachData( $this->descriptors['colonies-location'], [ 'location_id' => $country, 'location_type' => Region::class ] );

            $log->attachData( $this->descriptors['winter-mortality-percentage'], $winter_loss, $origin );
            $log->attachData( $this->descriptors['winter-mortality-confidence-interval'], [ 'value' => [ $winter_loss_ci_min, $winter_loss_ci_max ] ], $origin );
            $log->attachData( $this->descriptors['beekeeper-count'], intval( $row[1] ), $origin );

            $log->attachData( $this->descriptors['colony-count-autumn'], $spring_count, $origin );
            $log->attachData( $this->descriptors['colony-count-spring'], $autumn_count, $origin );
        }
    }

}
