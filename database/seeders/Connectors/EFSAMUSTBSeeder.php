<?php namespace Database\Seeders\Connectors;

use App\Data\Connectors\Traits\LocationParser;
use App\Exceptions\InvalidValueException;
use App\Imports\SemicolonCSVImport;
use App\Models\Logs\Data;
use App\Models\Logs\Data\DataId;
use App\Models\Logs\Data\DataLocation;
use App\Models\Locations\Post;
use Carbon\Carbon;
use Database\Seeders\Connectors\Traits\BaseParsers;
use Excel;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class EFSAMUSTBSeeder extends BaseSeeder
{
    use LocationParser, BaseParsers;

    /**
     * Seed the application's database.
     *
     * @return void
     * @throws InvalidValueException
     */
    protected function runSeeder(): void
    {
        $this->createProject( [
            'type'           => 'static',
            'name'           => 'MUST-B. Field data collection for honey bee colony model evaluation',
            'slug'           => 'must-b-field-data-collection-for-honey-bee-colony-model-evaluation',
            'uid'            => 'efsa-mustb-2020',
            'acronym'        => 'MUST-B field data',
            'description'    => '<div>The aim of this project is to provide real field data from different environments and locations of Europe for validation of the ApisRAM model (OC/EFSA/SCER/2016/03). This model is developed to assist in the evaluation of the risks of pesticides to honeybee colonies in a multistressor environment. It allows to extrapolate the impact that may be shown at the individual bee to colony level.&nbsp;</div>',
            'website'        => '',
            'phone'          => '+351915071515',
            'email'          => 'nunocapela.bio@gmail.com',
            'featured_image' => 'yVqPJGpP8eMz0iKmDaYZfH0GPeZHvnYVD9ysiOfU.jpeg',
            // 'class_name'     => 'App\Connectors\ExampleConnector',
            'active'         => true,
            'public'         => true,
            'sync_period'    => null, // Can be one of: null, 'hourly', 'daily', 'weekly', 'monthly', 'quarterly', 'yearly'
            'historic_sync'  => null, // Can be one of: null (no historic syncing),  false (should sync in future)
            'last_synced_at' => Carbon::now(),
        ] );

        $this->createProviders(
            [
                [
                    'full_name'       => 'Nuno Xavier Jesus Capela',
                    'slug'            => 'nuno-xavier-jesus-capela',
                    'name'            => 'Capela',
                    'address'         => 'DCV, Calçada Martim de Freitas 3000-456 Coimbra',
                    'registry_number' => '',
                    'description'     => '<div>PhD student gathering data on honeybee colony development under to project MUST-B.</div>',
                    'phone'           => '+351915071515',
                    'email'           => 'nunocapela.bio@gmail.com',
                    'post_id'         => 127022,
                    'public'          => true,
                ],
            ]
        );

        $this->createDescriptors( [
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'External ID',
                'slug'        => 'external-id',
                'description' => 'External ID',
                'category'    => 'hive',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_ids',
                'name'        => 'Apiary ID',
                'slug'        => 'apiary-id',
                'description' => 'Apiary ID',
                'category'    => 'apiary',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_locations',
                'name'        => 'Colonies location',
                'slug'        => 'colonies-location',
                'description' => 'Where is the colony situated?',
                'category'    => 'environment',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Hive scales',
                'slug'        => 'hive-weight',
                'description' => 'Beehive weight in Kilograms',
                'category'    => 'hive-sensors',
            ],
            [
                'table_name'  => 'log_data_percentages',
                'name'        => 'Inside relative humidity',
                'slug'        => 'hive-relative-humidity',
                'description' => 'Inside relative humidity in percentage',
                'category'    => 'hive-sensors',
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Inside temperature',
                'slug'        => 'hive-temperature',
                'description' => 'Inside temperature in Celsius degrees',
                'category'    => 'hive-sensors',
            ],

            [
                'table_name'  => 'log_data_percentages',
                'name'        => 'Outside relative humidity',
                'slug'        => 'relative-humidity',
                'description' => 'Outside relative humidity in percentage',
                'category'    => 'weather-sensors',
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Outside temperature',
                'slug'        => 'temperature',
                'description' => 'Outside temperature in Celsius degrees',
                'category'    => 'weather-sensors',
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Rain',
                'slug'        => 'rain',
                'description' => 'Rain in liters per square meter',
                'category'    => 'weather-sensors',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Solar Radiation',
                'slug'        => 'solar-radiation',
                'description' => 'Solar Radiation in wat/m²',
                'category'    => 'weather-sensors',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Wind Direction',
                'slug'        => 'wind-direction',
                'description' => 'Wind Direction in Degreases',
                'category'    => 'weather-sensors',
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Wind Gust',
                'slug'        => 'wind-gust',
                'description' => 'Wind Gust in km/h',
                'category'    => 'weather-sensors',
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Wind Speed',
                'slug'        => 'wind-speed',
                'description' => 'Wind Speed in km/h',
                'category'    => 'weather-sensors',
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Dew Point',
                'slug'        => 'dew-point',
                'description' => 'Dew Point in °C',
                'category'    => 'weather-sensors',
            ],
            [
                'table_name'  => 'log_data_events',
                'name'        => 'Monitoring',
                'slug'        => 'event-monitoring',
                'description' => 'Apiary and colonies monitoring.',
                'category'    => 'hive-observation',
            ],
            [
                'table_name'  => 'log_data_events',
                'name'        => 'Scale Weight Change',
                'slug'        => 'event-scale-weight',
                'description' => 'Logged events on scale weight changes.',
                'category'    => 'hive-interaction',
            ],
            [
                'table_name'  => 'log_data_events',
                'name'        => 'Sampling',
                'slug'        => 'event-sampling',
                'description' => 'Logged events on various sampling.',
                'category'    => 'hive-interaction',
            ],
            [
                'table_name'  => 'log_data_events',
                'name'        => 'Hive Management',
                'slug'        => 'event-hive-management',
                'description' => 'Logged events on various Hive Management tasks.',
                'category'    => 'hive-interaction',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Total Capped Cells',
                'slug'        => 'capped-cells',
                'description' => 'Number of capped cells in the whole colony.',
                'category'    => 'hive-observation',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Total Uncapped Cells',
                'slug'        => 'uncapped-cells',
                'description' => 'Number of uncapped cells in the whole colony. It includes eggs and larva.',
                'category'    => 'hive-observation',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Total Larva Cells',
                'slug'        => 'larva-cells',
                'description' => 'Number of larva cells in the whole colony.',
                'category'    => 'hive-observation',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Total Eggs Cells',
                'slug'        => 'eggs-cells',
                'description' => 'Number of egg cells in the whole colony.',
                'category'    => 'hive-observation',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Total Brood Cells',
                'slug'        => 'brood-cells',
                'description' => 'Number of brood cells in the whole colony. It includes capped, larva and egg cells.',
                'category'    => 'hive-observation',
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Egg rate',
                'slug'        => 'egg-rate',
                'description' => 'Queen egg rate in the previous 21 days. This rate can be calculated from the amount of total brood dividing by the 21 "incubation" days.',
                'category'    => 'hive-observation',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Total Pollen Cells',
                'slug'        => 'beebread-cells',
                'description' => 'Number of beebread cells in the whole colony.',
                'category'    => 'hive-observation',
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Kg Honey/nectar',
                'slug'        => 'produce-amount',
                'description' => 'Kg of honey/nectar in the whole colony. ',
                'category'    => 'hive-observation',
            ],
            [
                'table_name'  => 'log_data_decimals',
                'name'        => 'Harvested honey (yield)',
                'slug'        => 'honey-yield',
                'description' => 'Kg of honey that were harvested by the beekeeper. It can help to calculate the beekeepers\' profit and the amount of honey left in the colony after harvest (for wintering).',
                'category'    => 'hive-yields',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Population',
                'slug'        => 'hive-population',
                'description' => 'Number of adult bees in the whole colony. Data provider should clarify if it includes all bees or just inside (nurse) bees (excluding foragers). Usually, data gathered from several visists during the season, does not include foragers.',
                'category'    => 'hive-observation',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Hive materials',
                'slug'        => 'hive-materials-weight',
                'description' => 'Weight of all hive materials in kg in the colony including frames weight (wood and wax), boxes, base, roof, etc. This information can help tp calculate the amount of honey within a colony when scale data is available. If scale data is available but materials weight its not, it will hep to know the hive model to extrapolate the materials weight.',
                'category'    => 'hive-elements',
            ],
            [
                'table_name'  => 'log_data_booleans',
                'name'        => 'Queen presence',
                'slug'        => 'has-queen',
                'description' => 'Whether the queen is present and healthy and if queen is not present or if it is a new queen that did not start laying eggs yet. ',
                'category'    => 'hive-observation',
            ],
        ], '1.0' );
        $basepath = base_path( '/database/seeders/Connectors/raw/EFSA/MUSTB/' );

        $weatherSensorCategory = $this->createDeviceCategory( [
                'name' => 'Weather Stations',
                'uid'  => 'weather-station',
            ]
        );
        $weightSensorCategory  = $this->createDeviceCategory( [
                'name' => 'Hive Weight sensors',
                'uid'  => 'hive-weight-sensors',
            ]
        );
        $apis                  = $this->createVendor( [
            'name'        => 'Apis Technology',
            'uid'         => 'apis-technology',
            'description' => 'Apis Technology is a startup based in Aveiro (Portugal) that researches, develops and sells products for the beekeeping sector. We help beekeepers to produce data about their bees in order to improve their businesses and operations.',
            'website'     => 'https://apistech.eu',
        ] );
        $scaleSensor           = $this->createDevice( [
            'name'        => 'B-Hive Stand Alone',
            'uid'         => 'apis-technology-b-hive-stand-alone',
            'vendor_id'   => $apis->id,
            'category_id' => $weightSensorCategory->id,
        ] );
        $spectrum              = $this->createVendor( [
            'name'    => 'Spectrum Technologies, Inc.',
            'uid'     => 'spectrum-technologies-inc',
            'website' => 'https://www.specmeters.com',
        ] );
        $weatherSensor         = $this->createDevice( [
            'name'        => 'WatchDog 2900ET Weather Station',
            'uid'         => 'spectrum-technologies-inc-watchdog-2900et-weather-station',
            'vendor_id'   => $spectrum->id,
            'category_id' => $weatherSensorCategory->id,
        ] );

        $sources = [
            'data'           => 'Lanscape influence Scale data.xlsx',
            'weather-CB'     => 'Weather CB 2019-2020.xlsx',
            'weather-Lousa'  => 'Weather Lousa 2019-2020.xlsx',
            'weather-Burgos' => null,
        ];

        $apiaries = [
            [
                'data_source'    => $sources['data'],
                'weather_source' => $sources['weather-CB'],
                'apiary_name'    => 'EFSA CB',
                'apiary_prefix'  => 'CB',
                'location'       => [
                    'value'         => new Point( 39.85, - 7.16 ),
                    'location_id'   => 271029,
                    'location_type' => Post::class,
                ],
                'weather_device' => $weatherSensor,
                'hives'          => [
                    [
                        'sample_name'  => 'CB1',
                        'events_tab'   => 5,
                        'scale_tab'    => 3,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'CB2',
                        'events_tab'   => 7,
                        'scale_tab'    => 4,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'CB3',
                        'events_tab'   => 9,
                        'scale_tab'    => 5,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'CB4',
                        'events_tab'   => 11,
                        'scale_tab'    => 6,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'CB5',
                        'events_tab'   => 13,
                        'scale_tab'    => 7,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'CB14',
                        'events_tab'   => 9,
                        'scale_tab'    => 8,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'CB11',
                        'events_tab'   => 11,
                        'scale_tab'    => 9,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'CB12',
                        'events_tab'   => 13,
                        'scale_tab'    => 10,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'CB13',
                        'events_tab'   => 13,
                        'scale_tab'    => 11,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'CB15',
                        'events_tab'   => 13,
                        'scale_tab'    => 12,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'CB16',
                        'events_tab'   => 13,
                        'scale_tab'    => 13,
                        'scale_device' => $scaleSensor,
                    ],
                ],
            ],
            [
                'data_source'            => $sources['data'],
                'weather_source'         => $sources['weather-Lousa'],
                'apiary_name'            => 'EFSA Lousa',
                'apiary_prefix'          => 'L',
                'location'               => [
                    'value'         => new Point( 40.04, - 8.24 ),
                    'location_id'   => 217303,
                    'location_type' => Post::class,
                ],
                'weather_device'         => $weatherSensor,
                'weather_tab'            => 12,
                'colony_development_tab' => 0,
                'hives'                  => [
                    [
                        'sample_name'  => 'L1',
                        'events_tab'   => 5,
                        'scale_tab'    => 14,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'L3',
                        'events_tab'   => 7,
                        'scale_tab'    => 15,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'L4',
                        'events_tab'   => 9,
                        'scale_tab'    => 16,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'L5',
                        'events_tab'   => 11,
                        'scale_tab'    => 17,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'L11',
                        'events_tab'   => 11,
                        'scale_tab'    => 18,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'L2',
                        'events_tab'   => 11,
                        'scale_tab'    => 19,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'L3',
                        'events_tab'   => 11,
                        'scale_tab'    => 20,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'L5',
                        'events_tab'   => 11,
                        'scale_tab'    => 21,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'L8',
                        'events_tab'   => 11,
                        'scale_tab'    => 22,
                        'scale_device' => $scaleSensor,
                    ],
                ],
            ],
            [
                'data_source'            => $sources['data'],
                'weather_source'         => $sources['weather-Burgos'],
                'apiary_name'            => 'EFSA Lousa',
                'apiary_prefix'          => 'L',
                'location'               => [
                    'value'         => new Point( 40.04, - 8.24 ),
                    'location_id'   => 217303,
                    'location_type' => Post::class,
                ],
                'weather_device'         => $weatherSensor,
                'weather_tab'            => 12,
                'colony_development_tab' => 0,
                'hives'                  => [
                    [
                        'sample_name'  => 'Burgos2',
                        'events_tab'   => 5,
                        'scale_tab'    => 0,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'Burgos3',
                        'events_tab'   => 7,
                        'scale_tab'    => 1,
                        'scale_device' => $scaleSensor,
                    ],
                    [
                        'sample_name'  => 'Burgos5',
                        'events_tab'   => 9,
                        'scale_tab'    => 2,
                        'scale_device' => $scaleSensor,
                    ],
                ],
            ],
        ];
        $data     = Excel::toArray( new SemicolonCSVImport, $basepath . $sources['data'] );

        foreach ( $apiaries as $apiary ) {
            echo "Working: " . $apiary['apiary_name'] . PHP_EOL;

            $apiaryId = DataId::firstOrCreate( [
                'project_id' => $this->project->id,
                'value'      => $apiary['apiary_name'],
            ] );

            $apiaryLocation = DataLocation::firstOrCreate( $apiary['location'] );


            if ( ! empty( $apiary['weather_source'] ) ) {
                /**
                 * First load Apiary scale data, like weather and events.
                 */
                $weather = Excel::toArray( new SemicolonCSVImport, $basepath . $apiary['weather_source'] );

                foreach ( $weather[0] as $index => $row ) {
                    if ( $index < 4 || empty( $row[0] ) ) {
                        continue;
                    }
                    if ( str_contains( $row[0], '-' ) ) {
                        if ($row[0][4] == '-'){
                            $date = $row[0];
                        }else{
                            $date = implode( '-', array_reverse( explode( '-', $row[0] ) ) );
                        }
                        $time = $row[1];
                    } else {
                        $date = $this->parseDate( $row[0] );
                        $time = $this->parseTime( $row[1] );
                    }

                    $log = $this->createLog( $date . ' ' . $time, 'Y-m-d H:i', true, $apiaryId->id );

                    Data::insert( [
                        [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'colonies-location' ), 'data_id' => $apiaryLocation->id, 'data_type' => 'l' ],
                        [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'apiary-id' ), 'data_id' => $apiaryId->id, 'data_type' => 'id' ]
                    ] );

                    $fields = [
                        [ 2, 'solar-radiation', 'parseInteger' ],
                        [ 3, 'relative-humidity', 'parseFloat' ],
                        [ 4, 'temperature', 'parseFloat' ],
                        [ 5, 'rain', 'parseFloat' ],
                        [ 6, 'wind-direction', 'parseInteger' ],
                        [ 7, 'wind-gust', 'parseInteger' ],
                        [ 8, 'wind-speed', 'parseInteger' ],
                        [ 9, 'dew-point', 'parseFloat' ],
                    ];

                    foreach ( $fields as $field ) {
                        if ( ! empty( $row[ $field[0] ] ) || intval( $row[ $field[0] ] ) == 0 ) {
                            $log->attachData( $this->descriptors[ $field[1] ], $this->{$field[2]}( $row[ $field[0] ] ), $weatherSensor );
                        }
                    }
                }
                unset( $weather );
            }

            /**
             * Parse individual Hive data
             */
            foreach ( $apiary['hives'] as $hive ) {
                echo "\tWorking: " . $hive['sample_name'] . PHP_EOL;
                $hiveId = DataId::firstOrCreate( [
                    'value'      => $hive['sample_name'],
                    'project_id' => $this->project->id,
                    'parent_id'  => $apiaryId->id,
                ] );

                /**
                 * Parse Events.
                 *
                 * foreach ( $data[ $hive['events_tab'] ] as $index => $row ) {
                 * if ( $index == 0 || empty( $row[6] ) ) {
                 * continue;
                 * }
                 *
                 * // Is time information present?
                 * if ( ! empty( $row[7] ) ) {
                 * $string = $row[6] . ' ' . $row[7];
                 * $time   = true;
                 * } else {
                 * $string = $row[6];
                 * $time   = false;
                 * }
                 *
                 * // Is date in correct format?
                 * if ( strpos( $row[6], '/' ) ) {
                 * $format = 'd/m/y';
                 * } else {
                 * $format = 'Y-m-d';
                 * }
                 *
                 * $log = $this->createLog( $string, $time ? $format . ' H:i' : $format, false, $apiaryId->id, $hiveId->id );
                 *
                 * Data::insert( [
                 * [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'colonies-location' ), 'data_id' => $apiaryLocation->id, 'data_type' => 'l' ],
                 * [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'apiary-id' ), 'data_id' => $apiaryId->id, 'data_type' => 'id' ],
                 * [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'external-id' ), 'data_id' => $hiveId->id, 'data_type' => 'id' ]
                 * ] );
                 *
                 *
                 * if ( strtolower( $row[2] ) == 'monitoring' ) {
                 * $log->attachData( $this->descriptors['event-monitoring'], [ 'description' => $row[5], 'value' => null ] );
                 * } elseif ( strtolower( $row[2] ) == 'output' && strtolower( $row[4] ) == 'samples' ) {
                 * $log->attachData( $this->descriptors['event-sampling'], [ 'value' => $this->parseFloat( $row[3] ), 'description' => $row[5] ] );
                 * } elseif ( ( strtolower( $row[2] ) == 'output' || strtolower( $row[2] ) == 'input' ) && strtolower( $row[4] ) == 'kg' ) {
                 * $value = abs( $this->parseFloat( $row[3] ) );
                 * if ( strtolower( $row[2] ) == 'output' ) {
                 * $value *= - 1;
                 * }
                 * $log->attachData( $this->descriptors['event-scale-weight'], [ 'value' => $value, 'unit' => strtolower( $row[4] ), 'description' => $row[5] ] );
                 * } else {
                 * $log->attachData( $this->descriptors['event-hive-management'], [ 'description' => $row[5] ] );
                 * }
                 * }
                 */
                /**
                 * Parse Hive sensors data.
                 */
                $log = null;

                foreach ( $data[ $hive['scale_tab'] ] as $index => $row ) {
                    if ( $index == 0 || empty( $row[3] ) ) {
                        continue;
                    }

                    $date = $this->parseDateTime( $row[3] );
                    $log  = $this->createLog( $date, 'Y-m-d H:i', true, $apiaryId->id, $hiveId->id );

                    Data::insert( [
                        [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'colonies-location' ), 'data_id' => $apiaryLocation->id, 'data_type' => 'l' ],
                        [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'apiary-id' ), 'data_id' => $apiaryId->id, 'data_type' => 'id' ],
                        [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'external-id' ), 'data_id' => $hiveId->id, 'data_type' => 'id' ]
                    ] );

                    $log->attachData( $this->descriptors['hive-temperature'], $this->parseFloat( $row[0] ), $scaleSensor );
                    $log->attachData( $this->descriptors['hive-weight'], $this->parseFloat( $row[1] ), $scaleSensor );
                    $log->attachData( $this->descriptors['hive-relative-humidity'], $this->parsePercentage( $row[2] ), $scaleSensor );
                }
            }
            /**
             * Parse Colony development.
             *
             * foreach ( $data[ $apiary['colony_development_tab'] ] as $index => $row ) {
             * if ( $index == 0 || empty( $row[0] ) || empty( $row[1] ) ) {
             * continue;
             * }
             * $hiveId = DataId::firstOrCreate( [
             * 'value'      => $apiary['apiary_prefix'] . $row[1],
             * 'project_id' => $this->project->id,
             * 'parent_id'  => $apiaryId->id,
             * ] );
             *
             * $log = $this->createLog( $row[0], 'Y-m-d', false, $apiaryId->id, $hiveId->id );
             *
             * Data::insert( [
             * [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'colonies-location' ), 'data_id' => $apiaryLocation->id, 'data_type' => 'l' ],
             * [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'apiary-id' ), 'data_id' => $apiaryId->id, 'data_type' => 'id' ],
             * [ 'log_id' => $log->id, 'descriptor_id' => getDescriptorIdByUid( 'external-id' ), 'data_id' => $hiveId->id, 'data_type' => 'id' ]
             * ] );
             *
             * $log->attachData( $this->descriptors['capped-cells'], $this->parseInteger( $row[2] ) );
             * $log->attachData( $this->descriptors['uncapped-cells'], $this->parseInteger( $row[3] ) );
             * $log->attachData( $this->descriptors['larva-cells'], $this->parseInteger( $row[4] ) );
             * $log->attachData( $this->descriptors['eggs-cells'], $this->parseInteger( $row[5] ) );
             * $log->attachData( $this->descriptors['brood-cells'], $this->parseInteger( $row[6] ) );
             * $log->attachData( $this->descriptors['egg-rate'], $this->parseFloat( $row[7] ) );
             * $log->attachData( $this->descriptors['beebread-cells'], $this->parseInteger( $row[8] ) );
             * $log->attachData( $this->descriptors['produce-amount'], $this->parseFloat( $row[9] ) );
             * $log->attachData( $this->descriptors['honey-yield'], $this->parseFloat( $row[10] ) );
             * $log->attachData( $this->descriptors['hive-population'], $this->parseInteger( $row[11] ) );
             * $log->attachData( $this->descriptors['hive-materials-weight'], $this->parseFloat( $row[12] ) );
             * $log->attachData( $this->descriptors['has-queen'], $this->parseBoolean( $row[13], true ) );
             * } */
        }
    }

    protected function parseDate( $date )
    {
        return \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject( $this->parseInteger( $date ) )->format( 'Y-m-d' );
    }

    protected function parseTime( $date )
    {
        return \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject( $this->parseFloat( $date ) )->format( 'H:i' );
    }

    protected function parseDateTime( $date )
    {
        return \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject( $this->parseFloat( $date ) )->format( 'Y-m-d H:i' );
    }
}
