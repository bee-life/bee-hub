<?php namespace Database\Seeders\Connectors;

use App\Data\Connectors\Traits\LocationParser;
use App\Exceptions\InvalidValueException;
use App\Imports\CommaCSVImport;
use App\Models\Locations\Region;
use App\Models\Logs\Log;
use App\Models\Origins\Publication;
use Carbon\Carbon;
use Excel;
use Str;

class ColossSeeder2017 extends BaseSeeder
{
    use LocationParser;

    /**
     * Seed the application's database.
     *
     * @return void
     * @throws InvalidValueException
     */
    protected function runSeeder(): void
    {
        $this->createProject( [
            'type'           => 'static',
            'name'           => 'Prevention of honey bee COlony LOSSes',
            'slug'           => 'prevention-of-honey-bee-colony-losses-2017',
            'uid'            => 'coloss-2017',
            'acronym'        => 'COLOSS',
            'description'    => 'The colony loss monitoring group has been active since the start of the COLOSS COST action, now the COLOSS Association, to study reasons for colony losses. Participating countries each carry out an annual survey of beekeepers by questionnaire, with the aim of collecting information from a nationally representative sample of beekeepers. This makes it possible to compare colony loss rates between countries and to use the international data collected to understand better the distribution of colony losses. To enable proper comparisons, a standardised beekeeper questionnaire was developed and is updated each year by the group for use by each country.  About 30 countries currently participate in monitoring. At the individual level, many thousands of beekeepers take part each year. This is an example of citizen science/crowdsourcing, with the voluntary participation of many beekeepers enabling much wider scale data collection than would otherwise be possible. Participating countries return their data for analysis annually before an agreed deadline, usually 1st of July, and the results are used for press releases and scientific publications. We produce jointly authored journal papers for dissemination of results to the research community.
In Belgium a number of institutions have historically participated to the collection of winter mortality data, including the FAB-BBF (Federation Apicole Belge - Belgische Bijenhoudersfederatie), CARI, HoneybeeValley, CRA-Wallonie, and the Université de Liege.

More information of the global project <a href="https://coloss.org/core-projects/colony-losses-monitoring/">here</a>.',
            'website'        => 'https://coloss.org/',
            // 'phone'          => '',
            // 'email'          => '',
            'class_name'     => null,
            'active'         => true,
            'public'         => true,
            'last_synced_at' => Carbon::now(),
        ] );

        $this->createProviders(
            [
                [
                    'full_name'       => 'Gray et al 2019',
                    'name'            => '',
                    'slug'            => '',
                    'uid'             => '',
                    'address'         => '',
                    'registry_number' => '',
                    'description'     => '',
                    'featured_image'  => '',
                    'website'         => '',
                    'phone'           => '',
                    'email'           => '',
                    'post_id'         => null,
                    'public'          => true,
                ],

            ]
        );

        $this->createDescriptors( [
            [
                'table_name'  => 'log_data_locations',
                'name'        => 'Colonies location',
                'slug'        => 'colonies-location',
                'description' => 'Where is the colony situated?',
                'category'    => 'environment',
                'public'      => false,
            ],
            [
                'table_name'  => 'log_data_percentages',
                'name'        => 'Winter mortality',
                'slug'        => 'winter-mortality-percentage',
                'description' => 'Percent of Bee mortality during Winter.',
                'category'    => 'hive-health',
            ],
            [
                'table_name'  => 'log_data_integer',
                'name'        => 'Beekeeper counts',
                'slug'        => 'beekeeper-count',
                'description' => 'Number of Beekeepers',
                'category'    => 'meta',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Autumn Count before winter',
                'slug'        => 'colony-count-autumn',
                'description' => 'How many colonies where alive before winter',
                'category'    => 'apiary-observation',
            ],
            [
                'table_name'  => 'log_data_integers',
                'name'        => 'Spring Count after winter',
                'slug'        => 'colony-count-spring',
                'description' => 'How many colonies where alive after winter',
                'category'    => 'apiary-observation',
            ],
            [
                'table_name'  => 'log_data_decimal_ranges',
                'name'        => 'Winter mortality Confidence Interval',
                'slug'        => 'winter-mortality-confidence-interval',
                'description' => 'The confidence interval while calculating the winter mortality.',
                'category'    => 'hive-health',
            ],
            [
                'table_name'  => 'log_data_percentages',
                'name'        => 'Winter mortality',
                'slug'        => 'winter-mortality-aggregate',
                'description' => 'Percent of Bee mortality during Winter.',
                'category'    => 'hive-aggregates',
            ],
        ], '1.0' );

        $data = Excel::toArray( new CommaCSVImport, base_path( "/database/seeders/Connectors/raw/COLOSS/Gray et al 2019 - Winter 2017_18.csv" ) )[0];

        $origin = Publication::firstOrCreate( [
            'uid' => Str::slug( 'Loss rates of honey bee colonies during winter 2017/18 in 36 coun' ),
        ], [
            'title'            => 'Loss rates of honey bee colonies during winter 2017/18 in 36 countries participating in the COLOSS survey, including effects of forage sources',
            'slug'             => Str::slug( 'Loss rates of honey bee colonies during winter 2017/18 in 36 coun' ),
            'author'           => 'Alison Gray, Robert Brodschneider, Noureddine Adjlane, Alexis Ballis, Valters Brusbardis, Jean-Daniel Charrière, Robert Chlebo, Mary F. Coffey, Bram Cornelissen, Cristina Amaro da Costa, Tamás Csáki, Bjørn Dahle, Jiří Danihlík, Marica Maja Dražić, Garth Evans, Mariia Fedoriak, Ivan Forsythe, Dirk de Graaf, Aleš Gregorc, Jes Johannesen, Lassi Kauko, Preben Kristiansen, Maritta Martikkala, Raquel Martín-Hernández, Carlos Aurelio Medina-Flores, Franco Mutinelli, Solenn Patalano, Plamen Petrov, Aivar Raudmets, Vladimir A. Ryzhikov, Noa Simon-Delso, Jevrosima Stevanovic, Grazyna Topolska, Aleksandar Uzunov, Flemming Vejsnaes, Anthony Williams, Marion ZammitMangion & Victoria Soroker',
            'year'             => 2019,
            'website'          => 'https://doi.org/10.1080/00218839.2019.1615661',
            'publication_type' => 'Report',
        ] );


        foreach ( $data as $key => $row ) {
            if ( $key == 0 ) {
                continue;
            }

            if ( ! empty( trim( $row[0] ) ) ) {
                switch ( trim( $row[0] ) ) {
                    case 'Northern Ireland':
                        $country = 458;
                        break;
                    case 'Wales':
                        $country = 13;
                        break;
                    case 'Scotland':
                        $country = 467;
                        break;
                    case 'England':
                        // Not compatible with our Nuts declaration, so skip it
                        continue 2;
                    case 'Algeriaa':
                    case 'Bulgariaa,b':
                    case 'Francea':
                    case 'Italya':
                    case 'Portugala':
                    case 'Spaina':
                        $country = $this->parseRegionFromCountry( Str::beforeLast( $row[0], 'a' ), true );
                        break;
                    case 'Overallc':
                        continue 2;
                    default:
                        $country = $this->parseRegionFromCountry( trim( $row[0] ), true );
                }

                $winter_loss     = floatval( str_replace( ',', '.', $row[3] ) );
                $beekeeper_count = intval( str_replace( '.', '', $row[1] ) );
            } else {
                $log = Log::create( [
                    'year'       => 2017,
                    'project_id' => $this->project->id,
                ] );

                $log->attachData( $this->descriptors['colonies-location'], [ 'location_id' => $country, 'location_type' => Region::class ] );

                $log->attachData( $this->descriptors['winter-mortality-percentage'], $winter_loss, $origin );
                $log->attachData( $this->descriptors['beekeeper-count'], $beekeeper_count, $origin );
            }
        }
    }

}
