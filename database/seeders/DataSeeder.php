<?php namespace Database\Seeders;

use App\Data\Aggregators\SocioEconomicAlgorithms;
use App\Data\Aggregators\WinterMortalityCounts;
use Cache;
use Database\Seeders\Connectors\CARIScalesSeeder;
use Database\Seeders\Connectors\ColossBESeeder;
use Database\Seeders\Connectors\ColossSeeder2009;
use Database\Seeders\Connectors\ColossSeeder2012;
use Database\Seeders\Connectors\ColossSeeder2015;
use Database\Seeders\Connectors\ColossSeeder2016;
use Database\Seeders\Connectors\DEPABSeeder;
use Database\Seeders\Connectors\EFPSAAlgorithmsForExistingDataSeeder;
use Database\Seeders\Connectors\EFSAMUSTBSeeder;
use Database\Seeders\Connectors\EpilobeeSeeder;
use Database\Seeders\Connectors\SocioEconomics2020Seeder;
use Database\Seeders\Connectors\VarroaAlertSeeder;
use Database\Seeders\Meta\MethodologiesSeeder;
use DB;
use Illuminate\Database\Seeder;
use Schema;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        stop_logging();
        if ( is_production() ) {
            return;
        }

        // Clear all data to allow re-seeding
        Schema::disableForeignKeyConstraints();
        DB::table( 'project_to_provider' )->truncate();
        DB::table( 'log_data_aggregate_to_project' )->truncate();
        DB::table( 'project_to_descriptor' )->truncate();
        DB::table( 'log_to_data' )->truncate();
        DB::table( 'log_data_booleans' )->truncate();
        DB::table( 'log_data_dates' )->truncate();
        DB::table( 'log_data_date_ranges' )->truncate();
        DB::table( 'log_data_datetimes' )->truncate();
        DB::table( 'log_data_datetime_ranges' )->truncate();
        DB::table( 'log_data_decimals' )->truncate();
        DB::table( 'log_data_decimal_ranges' )->truncate();
        DB::table( 'log_data_ids' )->truncate();
        DB::table( 'log_data_integers' )->truncate();
        DB::table( 'log_data_events' )->truncate();
        DB::table( 'log_data_integer_ranges' )->truncate();
        DB::table( 'log_data_locations' )->truncate();
        DB::table( 'log_data_percentages' )->truncate();
        DB::table( 'log_data_strings' )->truncate();
        DB::table( 'log_data_text' )->truncate();
        DB::table( 'log_data_times' )->truncate();
        DB::table( 'log_data_time_ranges' )->truncate();
        DB::table( 'log_data_aggregates' )->truncate();
        DB::table( 'log_reference_land_usages' )->truncate();
        DB::table( 'log_reference_pesticides' )->truncate();
        DB::table( 'log_reference_pollen' )->truncate();
        DB::table( 'origin_device_categories' )->truncate();
        DB::table( 'origin_methodology_categories' )->truncate();
        DB::table( 'origin_vendors' )->truncate();
        DB::table( 'origin_devices' )->truncate();
        DB::table( 'origin_methodologies' )->truncate();
        DB::table( 'origin_publications' )->truncate();
        DB::table( 'origin_sources' )->truncate();
        DB::table( 'reference_land_usages' )->truncate();
        DB::table( 'reference_pesticides' )->truncate();
        DB::table( 'reference_pesticide_types' )->truncate();
        DB::table( 'reference_pollen' )->truncate();
        DB::table( 'reference_crop_species' )->truncate();
        DB::table( 'descriptors' )->truncate();
        DB::table( 'logs' )->truncate();
        DB::table( 'users_to_providers' )->truncate();
        DB::table( 'users_to_projects' )->truncate();
        DB::table( 'providers' )->truncate();
        DB::table( 'projects' )->truncate();
        Schema::enableForeignKeyConstraints();

        // Clear any caches, related to data
        Cache::clear();

        $this->call( MethodologiesSeeder::class );
        $this->call( EFPSAAlgorithmsForExistingDataSeeder::class );

        $this->call( EFSAMUSTBSeeder::class );
        return;
        $this->call( CARIScalesSeeder::class );

        $this->call( EpilobeeSeeder::class );
        $this->call( ColossBESeeder::class );
        $this->call( ColossSeeder2009::class );
        $this->call( ColossSeeder2012::class );
        $this->call( ColossSeeder2015::class );
        $this->call( ColossSeeder2016::class );

        $this->call( DEPABSeeder::class );
        $this->call( SocioEconomics2020Seeder::class );

        $this->call( VarroaAlertSeeder::class );

        Cache::clear();
        /**
         * One time aggregation for imported data.
         */
        ( new WinterMortalityCounts() )->run();
        ( new SocioEconomicAlgorithms() )->run();
    }
}
