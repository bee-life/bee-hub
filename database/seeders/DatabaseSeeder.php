<?php namespace Database\Seeders;

use App\Models\News;
use App\Models\Page;
use App\Models\User;
use Cache;
use Database\Seeders\Meta\CountriesSeeder;
use Database\Seeders\Meta\CurrenciesSeeder;
use Database\Seeders\Meta\DataCategorizationSeeder;
use Database\Seeders\Meta\LausGeoSeeder;
use Database\Seeders\Meta\LausSeeder;
use Database\Seeders\Meta\CountriesGeoSeeder;
use Database\Seeders\Meta\NutsGeoSeeder;
use Database\Seeders\Meta\PostsAdditionalSeeder;
use Database\Seeders\Meta\PostsSeeder;
use Database\Seeders\Meta\PostToLauSeeder;
use Database\Seeders\Meta\RegionsSeeder;
use Database\Seeders\Meta\RegionStatisticsSeeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Laravel\Telescope\Telescope;
use OptimistDigital\MenuBuilder\Models\Menu;
use OptimistDigital\MenuBuilder\Models\MenuItem;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        stop_logging();
        Cache::clear();

        User::create( [
            'name'     => 'Administrator',
            'email'    => 'admin@bee-hub.org',
            'password' => bcrypt( 'notrealpassword' ),
            'type'     => 'administrator',
        ] );
        User::create( [
            'name'     => 'Editor',
            'email'    => 'editor@bee-hub.org',
            'password' => bcrypt( 'notrealpassword' ),
            'type'     => 'editor',
        ] );

        Page::create( [
            'title'            => 'About',
            'slug'             => 'about',
            'content'          => '<div><strong>The Bee Hub is an integrative tool to centralise, analyse and visualise bee and pollinator-related data based on principles of collaboration and conservation.</strong></div><div><br></div><div>Bees and other insect pollinators are becoming increasingly relevant in the public debate. European authorities now recognise the environmental risks pollinators face and the need for institutional action. Given their importance for ecosystems and their role in our food security, the commitment to protect pollinators has been growing, and data is essential to fulfilling this commitment.&nbsp;</div><div><br></div><div>Different agents and stakeholders are continually collecting data related to the status of pollinators, such as researchers, environmental, health or agricultural authorities, national beekeeping or farming associations. The Bee Hub has been conceived to valorise their efforts and improve collaborations based on data-sharing. At the same time, the Bee Hub is constantly developing to provide access to valuable data from different consenting sources. In a collaborative spirit, the Bee Hub centralises and presents this data, also working as a communicative tool for the benefit of bees and pollinators in general.</div><div><br></div><div>The Bee Hub is coordinated by <a href="http://www.bee-life.eu/">BeeLife European Beekeeping Coordination</a>, an NGO focused on the protection of pollinators and biodiversity in Europe. BeeLife has initialised the first stages of this integrative platform within the <a href="http://io-bee.eu/">Internet of Bees</a> project.&nbsp;</div><div><br></div><div>The Bee Hub is also an attempt to materialise the conclusions of the <a href="http://www.efsa.europa.eu/en/supporting/pub/%E2%80%A8en-1423">EU Bee Parntership</a> regarding the need for further bee-data integration. The partnership is a stakeholder platform dynamised by the <a href="https://www.efsa.europa.eu/">European Food Safety Authority</a> that includes representatives from the beekeeping and farming sectors, NGOs, veterinarians, academia, industry, producers, and scientists.&nbsp;</div><div><br></div><div>This new tool also includes developments from the <a href="http://beexml.org/">Apimondia working group on the standardisation of data on bees - Bee XML</a>. Bee XML is the ongoing measure to reach a new model for sharing bee data, and the Bee Hub aims at implementing these standards.</div><div><br></div><div>For more information regarding the Bee Hub, possibilities of participation to the initiative and data collaboration, please contact <a href="mailto:info@bee-life.eu">info@bee-life.eu&nbsp;</a><br><br><img src="/images/LOGObee-life 3606.png" style="margin-left: auto; margin-right: auto;max-width: 200px;"><br><br></div>',
            'meta_title'       => 'About - The Bee Hub',
            'meta_description' => 'The Bee Hub is an integrative tool to centralise, analyse and visualise bee and pollinator-related data based on principles of collaboration and conservation.'
        ] );


        $menu = Menu::create( [
            'name'   => 'Main Menu',
            'slug'   => 'main-menu',
            'locale' => 'en_US',
        ] );

        MenuItem::create( [
            'menu_id' => $menu->id,
            'name'    => 'News',
            'class'   => 'OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL',
            'value'   => '/news',
            'target'  => '_self',
            'order'   => 1,
        ] );
        MenuItem::create( [
            'menu_id' => $menu->id,
            'name'    => 'Data providers',
            'class'   => 'OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL',
            'value'   => '/providers',
            'target'  => '_self',
            'order'   => 2,
        ] );
        MenuItem::create( [
            'menu_id' => $menu->id,
            'name'    => 'Participating projects',
            'class'   => 'OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL',
            'value'   => '/projects',
            'target'  => '_self',
            'order'   => 3,
        ] );
        MenuItem::create( [
            'menu_id' => $menu->id,
            'name'    => 'About',
            'class'   => 'App\Menus\Page',
            'value'   => '1',
            'target'  => '_self',
            'order'   => 4,
        ] );

        News::create( [
            'title'        => 'The Bee Hub - An Integrative Platform for Pollinator Health  Data',
            'slug'         => 'an-integrative-platform-for-pollinator-health-data',
            'published_at' => '2020-02-04 11:00:00',
            'excerpt'      => '"The Bee Hub, a Big Data approach to improve our understanding of pollinator diversity and threats in Europe makes its first public launch. We are aiming to achieve a platform that includes any relevant data linked to pollinators, especially bees (but not necessarily restricted to bees).',
            'content'      => '<div>The Bee Hub, a Big Data approach to improve our understanding of pollinator diversity and threats in Europe makes its first public launch. We are aiming to achieve a platform that includes any relevant data linked to pollinators, especially bees (but not necessarily restricted to bees). Today, we publish the first step on its development, made available online at <a href="http://www.bee-hub.org">www.bee-hub.org</a> [1]. The initial goal that we reach today is the creation and first publication of a Proof of Concept for the Bee Hub. This work has been completed within the frame of <a href="http://io-bee.eu/">The Internet of Bees Project (IoBee)</a> [2]. This Proof of Concept will continue to be developed and will be finalised during the first semester of 2020. Afterwards, the Bee Hub will be further developed, moving on to a prototype-building phase, expanding its data sources. <strong>Interested parties in collaborating and providing data are welcome in current and upcoming stages, thus enriching the collaborative nature of this new platform for a better future for pollinators</strong>.&nbsp;</div><div><br></div><div><a href="https://www.bee-life.eu/">BeeLife European Beekeeping Coordination</a> is currently developing this new tool to improve the efforts of monitoring and protection of pollinators. The Bee Hub is a practical application of its vision for a safer environment for pollinators. More sustainable and innovative agricultural practices, as well as improving collaborations between farmers and beekeepers, are part of BeeLife’s vision for the future. Achieving its core objective of protecting bees and pollinators rely on cooperation among institutional agents, researchers and stakeholders. The goal is to achieve a multiparty collaboration between beekeepers, researchers, farmers, environmentalists and European institutions. Ultimately, the Bee Hub will be able to integrate as many sources of data as possible. <figure data-trix-attachment="{&quot;contentType&quot;:&quot;image&quot;,&quot;height&quot;:1078,&quot;url&quot;:&quot;https://lh5.googleusercontent.com/iYq83naTD2wQ5Q4T9nPvXRrcLnp2HLn6-kF5vtnKk5wYUQLkMxwJTqA2NAt62LkmePMmlG-NQiGaua0e3nGl8ECX977vsd-aR126UhVzynIooH7IYqdLS4UMyn6yocptzhiuaSs&quot;,&quot;width&quot;:1600}" data-trix-content-type="image" data-trix-attributes="{&quot;caption&quot;:&quot;Environmental stressors on pollinators continue to rise, calling for new tools to help tackle the current pollinator and biodiversity crisis.&quot;}" class="attachment attachment--preview"><img src="https://lh5.googleusercontent.com/iYq83naTD2wQ5Q4T9nPvXRrcLnp2HLn6-kF5vtnKk5wYUQLkMxwJTqA2NAt62LkmePMmlG-NQiGaua0e3nGl8ECX977vsd-aR126UhVzynIooH7IYqdLS4UMyn6yocptzhiuaSs"><figcaption class="attachment__caption attachment__caption--edited">Environmental stressors on pollinators continue to rise, calling for new tools to help tackle the current pollinator and biodiversity crisis.</figcaption></figure></div><div><br></div><div>In the current first phase, we are checking the feasibility of the platform and the first required measures for proper data integration. We are building practical applications for the hub once the Proof of Concept proves to be successful.</div><div><br></div><div>In the long run, we would aim to achieve a European (even worldwide) platform that includes any relevant data linked to pollinators, especially bees (but not necessarily restricted to bees). With such a platform, we are aiming at creating a communicative tool on the status of pollinators and pollination, since both are essential for food security and biodiversity. This platform will provide a user-friendly window on the status of pollinators in real-time, identifying problematic areas and harmless areas. One of the main goals is to achieve an application of current standardisation efforts for bee and pollinator related data, thus implementing protocols such as those established by the Bee XML [3].</div><div><br></div><div>There are two main aspects which the Bee Hub follows in line with the mission and values of BeeLife. First, it aims to be a useful and effective tool in data gathering and sharing. Its focus relies on a non-profit scheme. Secondly, it aims to be a collaborative tool in which beekeepers, monitoring device producers and research centres reach a new level of cooperation. It is also a materialisation of the <a href="https://www.efsa.europa.eu/en/press/news/180629-0">EU Bee Health Partnership</a>, of which BeeLife is an active stakeholder [4].&nbsp;</div><div><br></div><div>The current Proof of Concept ensures the reception, storage and management of data in full security. It involves visualisation features and accessibility of the data performed through automated data processing integrated into the interface.</div><div><br><br><br></div><div><figure data-trix-attachment="{&quot;contentType&quot;:&quot;image&quot;,&quot;height&quot;:903,&quot;url&quot;:&quot;https://lh3.googleusercontent.com/aYHxY98PAT4audzWAQLuCawJpctt-tLZimuMrUFpdTAoc0AI8-VIC76AsNSjKUd-FSv8Ras2chv6xjrZQTWiUxXtW2hvDMBgae201ZI1RKt8U55tNbFIkmr9KEodALvys0zeGjQ&quot;,&quot;width&quot;:1600}" data-trix-content-type="image" data-trix-attributes="{&quot;caption&quot;:&quot;Current development of the Bee Hub as a public interactive platform on bee and pollinator data&quot;}" class="attachment attachment--preview"><img src="https://lh3.googleusercontent.com/aYHxY98PAT4audzWAQLuCawJpctt-tLZimuMrUFpdTAoc0AI8-VIC76AsNSjKUd-FSv8Ras2chv6xjrZQTWiUxXtW2hvDMBgae201ZI1RKt8U55tNbFIkmr9KEodALvys0zeGjQ"><figcaption class="attachment__caption attachment__caption--edited">Current development of the Bee Hub as a public interactive platform on bee and pollinator data</figcaption></figure></div><div><br></div><div>To continue developing this tool that will be beneficial for field practitioners such as beekeepers, veterinarians or naturalists, researchers, policymakers and the general public, BeeLife requires the necessary collaborations and support. We foresee a tool that will help politicians better understand the impact of European and national legislation on pollinators, such as a useful tool to centralise and make relevant information available to the public. By integrating data from both publicly-funded research and from citizen science, it is possible to provide a never before seen tool to understand the drivers of pollinator decline. Besides, it could also help in agricultural and beekeeping economic activities, for example, by providing insights on the best times and locations for managed insect pollination. The interdisciplinary nature of the Bee Hub lies on its goals and its functioning, which integrates environmental and technological perspectives to help tackle a significant challenge for Europe and the world, preserving pollinators and their essential role in our ecosystems.&nbsp;</div><div><br><br></div><div><strong>-ENDS-</strong></div><div>&nbsp;</div><div>&nbsp;</div><div><strong>Contact: Andrés SALAZAR, BeeLife European Beekeeping Coordination: </strong><a href="mailto:info@bee-life.eu"><strong>info@bee-life.eu</strong></a></div><div>&nbsp;</div><div>&nbsp;</div><div>[1] <a href="http://www.bee-hub.org">www.bee-hub.org</a></div><div>[2] BeeLife is a partner of the Internet of Bees project, a European Union financed project to develop innovative solutions for in-hive monitoring, field monitoring and Spatial Decision Support Systems (SDSS). The project began in 2017 and will be finalised during the first semester of 2020. For more information, visit <a href="http://www.io-bee.eu">www.io-bee.eu</a></div><div>[3] Based on the Extensible Markup Language (XML), a programming language for storing and transporting data, Bee XML is the ongoing measure to reach a new model for sharing bee data. <a href="http://beexml.org">http://beexml.org</a></div><div>[4] EFSA. 2018. <em>Terms of Reference for the EU Bee Partnership</em>. Retrieved from <a href="http://www.efsa.europa.eu/en/supporting/pub/en-1423">http://www.efsa.europa.eu/en/supporting/pub/en-1423</a>&nbsp;</div><div><br><br></div><div><strong>NOTE TO EDITORS:</strong></div><div>&nbsp;</div><div><strong>BeeLife European Beekeeping Coordination</strong> is an association formed by professionals of the beekeeping sector from different countries of the European Union. Its main activity is the study of the impact on bees of environmental threats such as pesticides or genetically modified organisms (GMOs).BeeLife works for the protection of bees based on the principle that “bees serve as the canary in the gold mine” sounding the alarm that “something is wrong in the environment”. Not least, bees create 30% of all our food by pollinating fruits, vegetables and arable crops such as sunflower and oilseed rape, having an inherent value that the Coordination is working to protect.</div><div><br></div><div><strong>The Bee Hub </strong>is an integrative platform for the integration and communication of pollinator-related data. Still in its early stages of development as a Proof of Concept, it is directed by BeeLife European Beekeeping Coordination. The Bee Hub is an initial application of conclusions derived from overlapping initiatives relating to bee and pollinator health such as the <a href="https://www.efsa.europa.eu/en/press/news/180629-0">EU Bee Partnership</a>, <a href="http://io-bee.eu/">The Internet of Bees</a> project and the <a href="http://beexml.org/">Apimondia working group on the standardisation of data on bees - Bee XML</a>.&nbsp;</div><div><br></div>',
        ] );

        $this->call( CurrenciesSeeder::class );
        $this->call( CountriesSeeder::class );
        $this->call( RegionsSeeder::class );
        $this->call( LausSeeder::class );
        $this->call( PostsSeeder::class );
        $this->call( NutsGeoSeeder::class );
        $this->call( CountriesGeoSeeder::class );
        $this->call( LausGeoSeeder::class );
        $this->call( PostsAdditionalSeeder::class );
        $this->call( PostToLauSeeder::class );
        $this->call( RegionStatisticsSeeder::class );
        $this->call( DataCategorizationSeeder::class );
        $this->call( CurrenciesSeeder::class );
    }
}
