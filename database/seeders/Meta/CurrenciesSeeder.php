<?php namespace Database\Seeders\Meta;


use App\Models\Locations\Country;
use App\Models\Logs\Reference\ReferenceCurrency;
use App\Models\References\Currency;
use Illuminate\Database\Seeder;

class CurrenciesSeeder extends Seeder
{
    /**
     * Run DB seed
     *
     * Source of the information is ISO 4217
     * @link https://www.currency-iso.org/en/home.html
     */
    public function run()
    {
        stop_logging();

        ReferenceCurrency::truncate();

        $url = 'https://www.currency-iso.org/dam/downloads/lists/list_one.xml';

        $xml = simplexml_load_string( file_get_contents( $url ) );

        $json = json_encode( $xml );

        $array = json_decode( $json, true );

        foreach ( $array['CcyTbl']['CcyNtry'] as $currency ) {
            if ( $currency['CcyNm'] != 'No universal currency' ) {
                Currency::updateOrCreate( [
                    'name'     => $currency['CcyNm'],
                    'code'     => $currency['Ccy'],
                    'number'   => intval( $currency['CcyNbr'] ),
                    'decimals' => $currency['CcyMnrUnts'] != 'N.A.' ? intval( $currency['CcyMnrUnts'] ) : null,
                ] );
            }
        }

        $currencies = Currency::all()->keyBy( 'code' );

        /**
         * Currency symbols source:
         * @link https://www.xe.com/symbols.php
         */
        $symbols = [
            'ALL' => 'Lek',
            'AFN' => '؋',
            'ARS' => '$',
            'AWG' => 'ƒ',
            'AUD' => '$',
            'AZN' => '₼',
            'BSD' => '$',
            'BBD' => '$',
            'BYN' => 'Br',
            'BZD' => 'BZ$',
            'BMD' => '$',
            'BOB' => '$b',
            'BAM' => 'KM',
            'BWP' => 'P',
            'BGN' => 'лв',
            'BRL' => 'R$',
            'BND' => '$',
            'KHR' => '៛',
            'CAD' => '$',
            'KYD' => '$',
            'CLP' => '$',
            'CNY' => '¥',
            'COP' => '$',
            'CRC' => '₡',
            'HRK' => 'kn',
            'CUP' => '₱',
            'CZK' => 'Kč',
            'DKK' => 'kr',
            'DOP' => 'RD$',
            'XCD' => '$',
            'EGP' => '£',
            'SVC' => '$',
            'EUR' => '€',
            'FKP' => '£',
            'FJD' => '$',
            'GHS' => '¢',
            'GIP' => '£',
            'GTQ' => 'Q',
            'GGP' => '£',
            'GYD' => '$',
            'HNL' => 'L',
            'HKD' => '$',
            'HUF' => 'Ft',
            'ISK' => 'kr',
            'INR' => '₹',
            'IDR' => 'Rp',
            'IRR' => '﷼',
            'IMP' => '£',
            'ILS' => '₪',
            'JMD' => 'J$',
            'JPY' => '¥',
            'JEP' => '£',
            'KZT' => 'лв',
            'LAK' => '₭',
            'LBP' => '£',
            'LRD' => '$',
            'MKD' => 'ден',
            'MYR' => 'RM',
            'MUR' => '₨',
            'MXN' => '$',
            'MNT' => '₮',
            'MZN' => 'MT',
            'NAD' => '$',
            'NPR' => '₨',
            'ANG' => 'ƒ',
            'NZD' => '$',
            'NIO' => 'C$',
            'NGN' => '₦',
            'NOK' => 'kr',
            'OMR' => '﷼',
            'PKR' => '₨',
            'PAB' => 'B/.',
            'PYG' => 'Gs',
            'PEN' => 'S/.',
            'PHP' => '₱',
            'PLN' => 'zł',
            'QAR' => '﷼',
            'RON' => 'lei',
            'RUB' => '₽',
            'SHP' => '£',
            'SAR' => '﷼',
            'RSD' => 'Дин.',
            'SCR' => '₨',
            'SGD' => '$',
            'SBD' => '$',
            'SOS' => 'S',
            'ZAR' => 'R',
            'LKR' => '₨',
            'SEK' => 'kr',
            'CHF' => 'CHF',
            'SRD' => '$',
            'SYP' => '£',
            'TWD' => 'NT$',
            'THB' => '฿',
            'TTD' => 'TT$',
            'TRY' => '₺',
            'TVD' => '$',
            'UAH' => '₴',
            'GBP' => '£',
            'USD' => '$',
            'UYU' => '$U',
            'UZS' => 'лв',
            'VEF' => 'Bs',
            'VND' => '₫',
            'YER' => '﷼',
            'ZWD' => 'Z$',
        ];

        $symbol_suffix = [
            'CZK',
            'HUF',
            'MAD',
            'PLN',
            'RUB',
            'SAR',
            'SEK',
            'THB',
            'TRY',
            'VND',
        ];


        foreach ( $currencies as $currency ) {
            if ( isset( $symbols[ $currency->code ] ) ) {
                $currency->symbol = $symbols[ $currency->code ];
            }

            if ( in_array( $currency->code, $symbol_suffix ) ) {
                $currency->symbol_prefix = false;
            }

            $currency->save();
        }

        $countries = Country::all();

        if ( $countries->isNotEmpty() ) {
            foreach ( $countries as $country ) {
                if ( isset( $currencies[ $country->currency_code ] ) ) {
                    $country->currency_id = $currencies[ $country->currency_code ]->id;
                    $country->save();
                }
            }
        }
    }
}
