<?php namespace Database\Seeders\Meta;

use App\Models\Locations\Country;
use App\Models\Locations\Region;
use Crockett\CsvSeeder\CsvSeeder;
use DB;
use Illuminate\Support\Collection;

class RegionsSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->filename        = base_path( '/database/seeders/Meta/raw/ref-nuts-2016-01m.geojson/NUTS_AT_2016.csv' );
        $this->table           = 'regions';
        $this->delimiter       = ',';
        $this->skip_header_row = true;
        $this->mapping         = [
            0 => 'country_code',
            1 => 'nuts_id',
            2 => 'name'
        ];
        $countries             = Country::all()->keyBy( 'country_code' );
        // Two exceptions that use different country codes
        $countries['UK'] = $countries->where( 'country_code', 'GB' )->first();
        $countries['EL'] = $countries->where( 'country_code', 'GR' )->first();

        $this->insert_callback = function ( Collection $chunk ) use ( $countries ) {
            $values = [];
            foreach ( $chunk as $row ) {
                $values[] = $countries[ $row['country_code'] ]->id . ',"' .
                            $row['name'] . '","' .
                            $row['nuts_id'] . '"';
            }
            DB::insert( 'INSERT INTO `regions` (`country_id`, `name`, `nuts_id`) VALUES ( ' . implode( '),(', $values ) . ')' );
        };
    }


    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        stop_logging();

        parent::run();

        $regions         = Region::with( [ 'country' ] )->get();
        $regions_by_nuts = $regions->keyBy( 'nuts_id' );
        $counter         = 0;
        foreach ( $regions as $region ) {
            if ( strlen( $region->nuts_id ) > 2 ) {
                if ( isset( $regions_by_nuts[ substr( $region->nuts_id, 0, - 1 ) ] ) ) {
                    $parent_nuts = substr( $region->nuts_id, 0, - 1 );
                    $nuts_level  = strlen( $region->nuts_id ) - 2;
                } elseif ( isset( $regions_by_nuts[ substr( $region->nuts_id, 0, - 2 ) ] ) ) {
                    $parent_nuts = substr( $region->nuts_id, 0, - 2 );
                    $nuts_level  = strlen( $region->nuts_id ) - 3;
                }
                $region->parent_id  = $regions_by_nuts[ $parent_nuts ]->id;
                $region->nuts_level = $nuts_level;

                $region->save();
                $counter ++;
            }

            // Add Native country name from NUTS data.
            if ( $region->nuts_level == 0 ) {
                $region->name = mb_convert_case( mb_strtolower( $region->name ), MB_CASE_TITLE, "UTF-8" );
                $region->save();
                $region->country->native_name = $region->name;
                $region->country->save();
            }
        }
        $this->console( "Modified $counter entries." );
    }
}
