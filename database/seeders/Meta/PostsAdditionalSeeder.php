<?php namespace Database\Seeders\Meta;

use App\Models\Locations\Country;
use Crockett\CsvSeeder\CsvSeeder;
use DB;
use Exception;
use Illuminate\Support\Collection;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class PostsAdditionalSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->filename          = base_path( '/database/seeders/Meta/raw/geonames.org/allCountries.txt' );
        $this->table             = 'posts';
        $this->delimiter         = "\t";
        $this->skip_header_row   = false;
        $this->model_guard       = false;
        $this->disable_query_log = true;
        $this->insert_chunk_size = 200;
        $this->mapping           = [
            0  => 'country',
            1  => 'code',
            2  => 'name',
            3  => 'admin name1',
            4  => 'admin code1',
            5  => 'admin name2',
            6  => 'admin code2',
            7  => 'admin name3',
            8  => 'admin code4',
            9  => 'latitude',
            10 => 'longitude',
            11 => 'accuracy',
        ];

        $countries = Country::all()->keyBy( 'country_code' );
        // Two exceptions that use different country codes
        $countries['UK'] = $countries->where( 'country_code', 'GB' )->first();
        $countries['EL'] = $countries->where( 'country_code', 'GR' )->first();

        $this->insert_callback = function ( Collection $chunk ) use ( $countries ) {
            try {
                DB::beginTransaction();
                foreach ( $chunk as $row ) {
                    if ( $countries->has( $row['country'] ) ) {
                        $point = ( new Point( floatval( $row['latitude'] ), floatval( $row['longitude'] ) ) )->toWKT();
                        DB::update( 'UPDATE `posts`'
                                    . ' SET `center` = ST_PointFromText(\'' . $point . '\'), `name` = ?'
                                    . ' WHERE `number` = ?'
                                    . ' AND `country_id` = ' . $countries[ $row['country'] ]->id,
                            [ $row['name'], $row['code'] ] );
                    }
                }
                DB::commit();
            } catch ( Exception $e ) {
                var_dump( $e->getMessage() );
                echo PHP_EOL . PHP_EOL;
            }
        };
    }

    /**
     * Run DB seed
     */
    public function run()
    {
        stop_logging();

        $this->runSeeder();
    }

}
