<?php namespace Database\Seeders\Meta;

use App\Models\Locations\Lau;
use App\Models\Locations\Region;
use Grimzy\LaravelMysqlSpatial\Types\Geometry;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use JsonMachine\JsonMachine;
use Laravel\Telescope\Telescope;

class LausGeoSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run( \Spinen\Geometry\Geometry $geometry )
    {
        ini_set( 'memory_limit', '2048M' );

        stop_logging();
        $json_a = JsonMachine::fromFile( base_path( "database/seeders/Meta/raw/LAU.geojson" ), '/features' );

        $laus = Lau::all()->keyBy( 'lau_id' );

        $counter   = 0;
        $processed = 0;

        foreach ( $json_a as $lau ) {
            $counter ++;
            if ( isset( $laus[ $lau['properties']['LAU_CODE'] ] ) ) {
                $area = json_encode( $lau['geometry'] );
                /** @var \Spinen\Geometry\Geometries\GeometryCollection $areaPolygon */
                $areaPolygon = $geometry->parseGeoJson( $area );

                $current_lau         = $laus[ $lau['properties']['LAU_CODE'] ];
                $current_lau->area   = Geometry::fromJson( $area );
                $current_lau->center = Geometry::fromJson( $areaPolygon->getCentroid()->out( 'json' ) );

                if ( empty( $current_lau->surface ) ) {
                    // TODO: Does not work, investigate
                    // $current_lau->surface = $areaPolygon->square_meters;
                }
                $current_lau->save();
                $processed ++;
            }
        }
        unset( $json_a );

        console_log( "Processed: $processed out of $counter" );
    }

}
