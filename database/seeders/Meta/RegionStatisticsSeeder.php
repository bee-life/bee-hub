<?php namespace Database\Seeders\Meta;

use App\Models\Locations\Location;
use App\Models\Locations\Region;
use Illuminate\Database\Seeder;

class RegionStatisticsSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        stop_logging();

        console_log( 'Calculate Region statistics based on LAU initial values.' );
        $this->process( 3, 'laus' );
        $this->process( 2, 'children' );
        $this->process( 1, 'children' );
        $this->process( 0, 'children' );
    }

    /**
     * Process a Location Group.
     *
     * @param int    $locations
     * @param string $children
     */
    protected function process( int $level, string $children ): void
    {
        console_log( "Processing NUTS Level $level." );
        $locations      = Region::whereNutsLevel( $level )->with( [ $children ] )->get();
        $processedCount = 0;

        foreach ( $locations as $location ) {
            $surface      = $population = 0;
            $surfaceCount = $populationCount = 0;

            /** @var Location $child */
            foreach ( $location->$children as $child ) {
                if ( ! empty( $child->surface ) ) {
                    $surface += $child->surface;
                    $surfaceCount ++;
                }
                if ( ! empty( $child->population ) ) {
                    $population += $child->population;
                    $populationCount ++;
                }
            }

            // Make sure each child has a value, otherwise we must ignore it.
            if ( $surfaceCount == $location->$children->count() && $location->surface != $surface ) {
                $location->surface = $surface;
            }
            if ( $populationCount == $location->$children->count() && $location->population != $population ) {
                $location->population = $population;
            }

            // Only count valid
            if ( $location->isDirty() ) {
                $location->save();
                $processedCount ++;
            }
        }

        console_log( "Processed " . $processedCount . ' items out of ' . $locations->count() );
    }
}
