<?php namespace Database\Seeders\Meta;

use App\Models\Origins\Methodology;
use App\Models\Origins\MethodologyCategory;
use Illuminate\Database\Seeder;
use Str;

class MethodologiesSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        stop_logging();

        $simpleCategory = MethodologyCategory::create( [
            'name'        => 'Statistical Methodology',
            'uid'         => 'statistical',
            'description' => 'A set of statistical methodologies used to aggregate various data.',
        ] );

        $methodologies = [
            [
                'name'        => 'Maximum value',
                'uid'         => 'maximum',
                'description' => 'The value was selected as the highest value from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Minimum value',
                'uid'         => 'minimum',
                'description' => 'The value was selected as the lower value from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Minimum and Maximum Range',
                'uid'         => 'min-max',
                'description' => 'The range was selected as the lowest and highest values from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Data Range',
                'uid'         => 'range',
                'description' => 'The range was calculated as the difference between minimum and maximum values from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'First value',
                'uid'         => 'first',
                'description' => 'The value was selected as the first value from a set of values in a given day or year, order by date and time.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Last value',
                'uid'         => 'last',
                'description' => 'The value was selected as the last value from a set of values in a given day or year, order by date and time.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Average',
                'uid'         => 'average',
                'description' => 'The value was calculated as the average value (arithmetical mean) from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Median',
                'uid'         => 'median',
                'description' => 'The value was calculated as the median value (middle value) from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Mode',
                'uid'         => 'mode',
                'description' => 'The value was selected as the mode value (most frequent value) from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Sum',
                'uid'         => 'sum',
                'description' => 'The value was calculated as the sum of all values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Product',
                'uid'         => 'product',
                'description' => 'The value was calculated as the multiplication of all values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Standard Deviation',
                'uid'         => 'standard-deviation',
                'description' => 'The value was calculated as the standard deviation value from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Confidence Interval 80%',
                'uid'         => 'confidence-interval-80',
                'description' => 'The values where calculated as the confidence interval of 80% from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Confidence Interval 85%',
                'uid'         => 'confidence-interval-85',
                'description' => 'The values where calculated as the confidence interval of 85% from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Confidence Interval 90%',
                'uid'         => 'confidence-interval-90',
                'description' => 'The values where calculated as the confidence interval of 90% from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Confidence Interval 95%',
                'uid'         => 'confidence-interval-95',
                'description' => 'The values where calculated as the confidence interval of 95% from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Confidence Interval 99%',
                'uid'         => 'confidence-interval-99',
                'description' => 'The values where calculated as the confidence interval of 99% from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Confidence Interval 99.5%',
                'uid'         => 'confidence-interval-995',
                'description' => 'The values where calculated as the confidence interval of 99.5% from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
            [
                'name'        => 'Confidence Interval 99.9%',
                'uid'         => 'confidence-interval-999',
                'description' => 'The values where calculated as the confidence interval of 99.9% from a set of values in a given day or year.',
                'category_id' => $simpleCategory->id,
            ],
        ];

        foreach ( $methodologies as $methodology ) {
            $methodology['slug'] = Str::slug( $methodology['name'] );
            Methodology::create( $methodology );
        }
    }
}
