<?php namespace Database\Seeders\Meta;

use App\Models\Locations\Country;
use App\Models\Locations\Region;
use Grimzy\LaravelMysqlSpatial\Types\Geometry;
use Illuminate\Database\Seeder;
use JsonMachine\JsonMachine;

class CountriesGeoSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        stop_logging();

        // First copy all countries to NUTS regions, make them level 0.
        $countries = Country::with( [ 'countryRegion' ] )->get()->keyBy( 'name' );

        /** @var Country $country */
        foreach ( $countries as $country ) {
            if ( ! empty( $country->countryRegion ) ) {
                continue;
            }
            $country->countryRegion()->create( [ 'name' => $country->name, 'nuts_level' => 0, 'nuts_id' => $country->country_code ] );
        }

        $json_a = JsonMachine::fromFile( base_path( "database/seeders/Meta/raw/Countries10M.geojson" ), '/features' );
        $regions = Region::where( 'nuts_level', 0 )->get()->keyBy( 'nuts_id' );


        foreach ( $json_a as $region ) {
            //dd($region['properties']);
            switch($region['properties']['ISO_A2']){
                case '-99':
                case 'GB':
                case 'GR':
                    continue 2;
            }

            $current_region       = $regions[ $region['properties']['ISO_A2'] ];
            if(!$current_region->hasArea()){
                $current_region->area = Geometry::fromJson( json_encode( $region ) );

            }

            if(!$current_region->hasPopulation()) {
                $current_region->population = $region['properties']['POP_EST'];
            }

            $current_region->save();
        }
    }
}
