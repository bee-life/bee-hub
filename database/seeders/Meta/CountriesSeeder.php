<?php namespace Database\Seeders\Meta;

use App\Models\Locations\Country;
use App\Models\References\Currency;
use Crockett\CsvSeeder\CsvSeeder;
use Illuminate\Support\Collection;

class CountriesSeeder extends CsvSeeder
{
    protected $currencies;

    public function __construct()
    {
        $this->filename          = base_path( '/database/seeders/Meta/raw/geonames.org/countryInfo.txt' );
        $this->table             = 'countries';
        $this->delimiter         = "\t";
        $this->skip_header_row   = false;
        $this->model_guard       = false;
        $this->disable_query_log = true;
        $this->offset_rows       = 51;
        $this->mapping           = [
            0  => 'country_code',
            1  => 'iso3',
            2  => 'iso_numeric',
            3  => 'fips',
            4  => 'name',
            5  => 'capital',
            6  => 'area',
            7  => 'population',
            8  => 'continent',
            9  => 'tld',
            10 => 'currency_code',
            11 => 'currency_name',
            12 => 'phone_prefix',
            13 => 'postal_code_format',
            14 => 'postal_code_regex',
            15 => 'languages',
            16 => 'geoname_id',
            17 => 'neighbours',
            18 => 'equivalent_fips_code',
        ];

        $this->currencies = Currency::all()->keyBy( 'code' );

        $this->insert_callback = function ( Collection $chunk ) {
            foreach ( $chunk as $row ) {

                Country::create( [
                    'country_code'  => $row['country_code'],
                    'iso3'          => $row['iso3'],
                    'name'          => $row['name'],
                    'capital'       => $row['capital'],
                    'continent'     => $row['continent'],
                    'currency_code' => $row['currency_code'],
                    'currency_name' => $row['currency_name'],
                    'phone_prefix'  => $row['phone_prefix'],
                    'currency_id'   => $this->currencies->has( $row['currency_code'] ) ? $this->currencies[ $row['currency_code'] ]->id : null,
                ] );
            }
        };
    }

    /**
     * Run DB seed
     */
    public function run()
    {
        stop_logging();

        $this->runSeeder();
    }


}
