<?php namespace Database\Seeders\Meta;

use App\Models\Locations\Lau;
use App\Models\Locations\Post;
use App\Models\Locations\Region;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Laravel\Telescope\Telescope;

class PostToLauSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::disableQueryLog();
        Telescope::stopRecording();
        Model::unguard();

        $regions   = Region::select( [ 'nuts_id', 'name', 'country_id' ] )->whereNutsLevel( 3 )->get();
        $processed = 0;

        foreach ( $regions as $region ) {
            $start   = microtime( true );
            $counter = 0;
            $posts   = Post::select( [ 'id', 'center', 'lau_id' ] )->whereNutsId( $region->nuts_id )->get();

            if ( $posts->isEmpty() ) {
                continue;
            }
            DB::beginTransaction();

            foreach ( $posts as $post ) {
                if ( isset( $post->center ) ) {
                    $lau = Lau::select( [ 'lau_id', 'nuts_id' ] )->whereCountryId( $region->country_id )->contains( 'area', $post->center )->first();

                    if ( isset ( $lau ) ) {
                        $post->lau_id = $lau->lau_id;
                        $post->save();
                        $counter ++;
                    }
                }
            }
            console_log( "Processed {$region->name}: $counter out of {$posts->count()}. (" . ( microtime( true ) - $start ) . ")" );

            DB::commit();
            $processed ++;
        }
        console_log( "Processed {$processed} out of {$region->count()} regions. (" . ( microtime( true ) - LARAVEL_START ) . ")" );
    }
}
