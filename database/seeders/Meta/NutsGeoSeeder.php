<?php namespace Database\Seeders\Meta;

use App\Models\Locations\Region;
use Grimzy\LaravelMysqlSpatial\Types\Geometry;
use Illuminate\Database\Seeder;
use JsonMachine\JsonMachine;

class NutsGeoSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        stop_logging();

        $json_a = JsonMachine::fromFile(base_path( "database/seeders/Meta/raw/ref-nuts-2016-01m.geojson/NUTS_LB_2016_4326.geojson" ), '/features');

        $regions = Region::all()->keyBy( 'nuts_id' );

        foreach ( $json_a as $region ) {
            $current_region         = $regions[ $region['id'] ];
            $current_region->center = Geometry::fromJson( json_encode( $region ) );
            //$current_region->save(); // Should only save once, as we have the same data for center and area.
        }
        unset( $json_a );

        $files = [
            'NUTS_RG_01M_2016_4326_LEVL_0.geojson',
            'NUTS_RG_01M_2016_4326_LEVL_1.geojson',
            'NUTS_RG_01M_2016_4326_LEVL_2.geojson',
            'NUTS_RG_01M_2016_4326_LEVL_3.geojson',
        ];

        foreach ( $files as $file ) {
            $json_a = JsonMachine::fromFile(base_path( "database/seeders/Meta/raw/ref-nuts-2016-01m.geojson/" . $file ), '/features');

            foreach ( $json_a as $region ) {
                $current_region       = $regions[ $region['id'] ];
                $current_region->area = Geometry::fromJson( json_encode( $region ) );
                $current_region->save();
            }

            unset( $json_a );
        }
    }
}
