<?php namespace Database\Seeders\Meta;

use App\Models\Locations\Country;
use Crockett\CsvSeeder\CsvSeeder;
use DB;
use Illuminate\Support\Collection;

class PostsSeeder extends CsvSeeder
{

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        stop_logging();

        $files = [
            'pc2018_at_NUTS-2016_v1.0.csv',
            'pc2018_be_NUTS-2016_v1.0.csv',
            'pc2018_bg_NUTS-2016_v1.0.csv',
            'pc2018_ch_NUTS-2016_v1.0.csv',
            'pc2018_cy_NUTS-2016_v1.0.csv',
            'pc2018_cz_NUTS-2016_v1.0.csv',
            'pc2018_de_NUTS-2016_v1.0.csv',
            'pc2018_dk_NUTS-2016_v1.0.csv',
            'pc2018_ee_NUTS-2016_v1.0.csv',
            'pc2018_el_NUTS-2016_v1.0.csv',
            'pc2018_es_NUTS-2016_v1.0.csv',
            'pc2018_fi_NUTS-2016_v1.0.csv',
            'pc2018_fr_NUTS-2016_v1.0.csv',
            'pc2018_hr_NUTS-2016_v1.0.csv',
            'pc2018_hu_NUTS-2016_v1.0.csv',
            'pc2018_ie_NUTS-2016_v1.0.csv',
            'pc2018_is_NUTS-2016_v1.0.csv',
            'pc2018_it_NUTS-2016_v1.0.csv',
            'pc2018_lt_NUTS-2016_v1.0.csv',
            'pc2018_lu_NUTS-2016_v1.0.csv',
            'pc2018_lv_NUTS-2016_v1.0.csv',
            'pc2018_mt_NUTS-2016_v1.0.csv',
            'pc2018_nl_NUTS-2016_v1.0.csv',
            'pc2018_no_NUTS-2016_v1.0.csv',
            'pc2018_pl_NUTS-2016_v1.0.csv',
            'pc2018_pt_NUTS-2016_v1.0.csv',
            'pc2018_ro_NUTS-2016_v1.0.csv',
            'pc2018_se_NUTS-2016_v1.0.csv',
            'pc2018_si_NUTS-2016_v1.0.csv',
            'pc2018_sk_NUTS-2016_v1.0.csv',
            'pc2018_uk_NUTS-2016_v3.0.csv',
        ];

        $countries = Country::all()->keyBy( 'country_code' );
        // Two exceptions that use different country codes
        $countries['UK'] = $countries->where( 'country_code', 'GB' )->first();
        $countries['EL'] = $countries->where( 'country_code', 'GR' )->first();

        foreach ( $files as $file ) {
            $this->offset_rows = 1;

            $this->seedFromCSV( base_path( "/database/seeders/Meta/raw/pc2018_NUTS-2016_v3.0/$file" ), 'posts', null, ';', null, [
                0 => 'nuts_id',
                1 => 'number'
            ], function ( Collection $chunk ) use ( $countries ) {
                $values = [];
                foreach ( $chunk as $row ) {
                    $values[] = $countries[ substr( $row['nuts_id'], 0, 2 ) ]->id . ',"' .
                                $row['number'] . '","' .
                                $row['nuts_id'] . '"';
                }

                DB::insert( 'INSERT INTO `posts` (`country_id`, `number`, `nuts_id`) VALUES ( ' . implode( '),(', $values ) . ')' );
            } );
        }
    }


    /**
     * Parse a CSV row into a DB insertable array
     * Override in order to remove single quotes.
     *
     * @param array $row     List of CSV columns
     * @param array $mapping Array of csvCol => dbCol
     *
     * @return Collection
     */
    protected function parseRow( array $row, array $mapping )
    {
        $columns = new Collection();
        // apply mapping to a given row
        foreach ( $mapping as $csv_index => $column_name ) {
            $column_value = ( array_key_exists( $csv_index, $row ) && ! empty( $row[ $csv_index ] ) )
                ? str_replace( '\'', '', $row[ $csv_index ] )
                : null;

            $columns->put( $column_name, $column_value );
        }

        $columns = $this->aliasColumns( $columns );

        $columns = $this->hashColumns( $columns );

        return $columns;
    }
}
