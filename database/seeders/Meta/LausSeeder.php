<?php namespace Database\Seeders\Meta;

use App\Models\Locations\Country;
use App\Models\Locations\Region;
use Crockett\CsvSeeder\CsvSeeder;
use DB;
use Illuminate\Database\Eloquent\Model;
use Laravel\Telescope\Telescope;
use Illuminate\Support\Collection;

class LausSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->filename        = base_path( '/database/seeders/Meta/raw/EU-28-LAU-2019-NUTS-2016.csv' );
        $this->table           = 'laus';
        $this->delimiter       = ';';
        $this->skip_header_row = true;
        $this->mapping         = [
            0  => 'nuts_id',
            1  => 'lau_id',
            2  => 'name',
            5  => 'population',
            6  => 'surface',
            20 => 'country_code',
        ];
        $this->write_logs      = true;
        $countries             = Country::all()->keyBy( 'country_code' );
        $regions               = Region::all()->keyBy( 'nuts_id' );
        // Two exceptions that use different country codes
        $countries['UK'] = $countries->where( 'country_code', 'GB' )->first();
        $countries['EL'] = $countries->where( 'country_code', 'GR' )->first();

        $this->insert_callback = function ( Collection $chunk ) use ( $countries, $regions ) {
            $values = [];
            foreach ( $chunk as $row ) {
                if ( isset( $regions[ $row['nuts_id'] ] ) ) {
                    $values[] = $countries[ $row['country_code'] ]->id . ',"' .
                                str_replace( '\'', '\\\'', $row['name'] ) . '","' .
                                $row['nuts_id'] . '","' .
                                $row['lau_id'] . '",' .
                                ( empty( $row['population'] ) || $row['population'] == 'n.a.' ? 'NULL' : $row['population'] ) . ',' .
                                ( empty( $row['surface'] ) ? 'NULL' : $row['surface'] );
                }
            }
            if ( ! empty( $values ) ) {
                try {
                    DB::insert( 'INSERT INTO `laus` (`country_id`, `name`, `nuts_id`, `lau_id`, `population`, `surface`) VALUES ( ' . implode( '),(', $values ) . ')' );
                } catch ( \Exception $e ) {
                    dd( $e );
                }
            }
        };
    }


    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        stop_logging();

        parent::run();
    }
}
