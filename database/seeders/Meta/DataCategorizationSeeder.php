<?php namespace Database\Seeders\Meta;

use App\Models\Logs\DescriptorCategory;
use Illuminate\Database\Seeder;
use Str;

class DataCategorizationSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        stop_logging();

        $categories = [
            [
                'name'     => 'Weather Information',
                'uid'      => 'weather',
                'children' => [
                    [
                        'name' => 'Weather Sensors',
                        'uid'  => 'weather-sensors',
                    ],
                    [
                        'name' => 'Weather Forecast',
                        'uid'  => 'weather-forecast',
                    ],
                    [
                        'name' => 'Weather Aggregates',
                        'uid'  => 'weather-aggregates',
                    ],
                ],
            ],
            [
                'name'     => 'Apiary Information',
                'uid'      => 'apiary',
                'children' => [
                    [
                        'name' => 'Apiary environment',
                        'uid'  => 'apiary-environment',
                    ],
                    [
                        'name' => 'Apiary Level Observations',
                        'uid'  => 'apiary-observation',
                    ],
                    [
                        'name' => 'Apiary Level Interactions',
                        'uid'  => 'apiary-interaction',
                    ],
                ],
            ],
            [
                'name'     => 'Hive Information',
                'uid'      => 'hive',
                'children' => [
                    [
                        'name' => 'Hive Elements',
                        'uid'  => 'hive-elements',
                    ],
                    [
                        'name' => 'Hive Sensors',
                        'uid'  => 'hive-sensors',
                    ],
                    [
                        'name' => 'Hive Analyses',
                        'uid'  => 'hive-analyses',
                    ],
                    [
                        'name' => 'Hive Aggregates',
                        'uid'  => 'hive-aggregates',
                    ],
                    [
                        'name' => 'Hive Calculation',
                        'uid'  => 'hive-calculations',
                    ],
                    [
                        'name' => 'Hive Level Observations',
                        'uid'  => 'hive-observation',
                    ],
                    [
                        'name' => 'Hive Level Interactions',
                        'uid'  => 'hive-interaction',
                    ],
                    [
                        'name' => 'Hive Health Information',
                        'uid'  => 'hive-health',
                    ],
                    [
                        'name' => 'Hive Production Information',
                        'uid'  => 'hive-yields',
                    ],
                ],
            ],
            [
                'name'     => 'Environment Information',
                'uid'      => 'environment',
                'children' => [
                    [
                        'name' => 'General Health Information',
                        'uid'  => 'environment-health',
                    ],
                ],
            ],
            [
                'name'     => 'Meta Information',
                'uid'      => 'meta',
                'children' => [],
            ],
            [
                'name'     => 'Socioeconomic Information',
                'uid'      => 'socioeconomic',
                'children' => [
                    [
                        'name' => 'Goods Production',
                        'uid'  => 'socioeconomic-production',
                    ],
                    [
                        'name' => 'Goods Production Area',
                        'uid'  => 'socioeconomic-production-area',
                    ],
                    [
                        'name' => 'Goods Import',
                        'uid'  => 'socioeconomic-import',
                    ],
                    [
                        'name' => 'Goods Export',
                        'uid'  => 'socioeconomic-export',
                    ],
                    [
                        'name' => 'Livestock',
                        'uid'  => 'socioeconomic-livestock',
                    ],
                    [
                        'name' => 'Prices',
                        'uid'  => 'socioeconomic-prices',
                    ],
                ],
            ],
        ];

        foreach ( $categories as $category ) {
            $this->saveCategory( $category );
        }
    }

    /**
     * @param array $category List of CSV columns
     *
     * @return void
     */
    protected function saveCategory( array $category, $parent_id = null ): void
    {
        $categoryModel = DescriptorCategory::create( [
            'name'      => $category['name'],
            'slug'      => Str::slug( $category['name'] ),
            'uid'       => $category['uid'],
            'parent_id' => $parent_id,
        ] );

        if ( ! empty( $category['children'] ) ) {
            foreach ( $category['children'] as $child ) {
                $this->saveCategory( $child, $categoryModel->id );
            }
        }
    }
}
