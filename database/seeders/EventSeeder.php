<?php namespace Database\Seeders;

use Cache;
use Database\Seeders\Connectors\CARIScalesSeeder;
use Database\Seeders\Connectors\ColossBESeeder;
use Database\Seeders\Connectors\ColossSeeder2009;
use Database\Seeders\Connectors\ColossSeeder2012;
use Database\Seeders\Connectors\ColossSeeder2015;
use Database\Seeders\Connectors\ColossSeeder2016;
use Database\Seeders\Connectors\ColossSeeder2017;
use Database\Seeders\Connectors\ColossSeeder2018;
use Database\Seeders\Connectors\CortevaSeeder;
use Database\Seeders\Connectors\DEPABSeeder;
use Database\Seeders\Connectors\EFPSAAlgorithmsForExistingDataSeeder;
use Database\Seeders\Connectors\EFSAMUSTBSeeder;
use Database\Seeders\Connectors\EpilobeeSeeder;
use Database\Seeders\Connectors\SocioEconomics2020Seeder;
use Database\Seeders\Connectors\VarroaAlertSeeder;
use Database\Seeders\Meta\CurrenciesSeeder;
use Database\Seeders\Meta\MethodologiesSeeder;
use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        stop_logging();

        // Clear any caches, related to data
        Cache::forget( 'descriptors' );

        $this->call( EFSAMUSTBSeeder::class );
        return;
        $this->call( CARIScalesSeeder::class );

        $this->call( EpilobeeSeeder::class );
        $this->call( ColossBESeeder::class );
        $this->call( ColossSeeder2009::class );
        $this->call( ColossSeeder2012::class );
        $this->call( ColossSeeder2015::class );
        $this->call( ColossSeeder2016::class );
        $this->call( ColossSeeder2017::class );
        $this->call( ColossSeeder2018::class );

        $this->call( DEPABSeeder::class );
        $this->call( SocioEconomics2020Seeder::class );

        $this->call( VarroaAlertSeeder::class );
        $this->call( CortevaSeeder::class );
    }
}
