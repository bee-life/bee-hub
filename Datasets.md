# About Datasets
This document represents a complete list of currently used datasets within the Beehub, when they were added and which data does it contain with some technical data of implementation. The order in this document represents the reverse order datasets were added to the application, from newest to oldest.

## Version 1 - Proof of Concept (POC)

### National Beekeeping Programme by CARI - 20. 3. 2020
Implementation includes daily API query to cari.be and historic data since 24. 3. 2016.

[About the project](https://bee-ppp.eu/projects/national-beekeeping-programme)

About the provider:
- [CARI](https://bee-ppp.eu/providers/cari-beekeeping-center-of-research-and-information)
 
### VarroaAlert - 6. 3. 2020

Implementation includes daily API query to [bienengesundheit.at](https://bienengesundheit.at).

[About the project](https://bee-ppp.eu/projects/varroaalert-varroa-warndienst)

About the providers:
- [Biene Östereich](https://bee-ppp.eu/providers/biene-oesterreich-imkereidachverband) 
- [Ländliches Fortbildungsionstitut Östereich](https://bee-ppp.eu/providers/laendliches-fortbildungsinstitut-oesterreich)
 
### Prevention of honey bee COlony LOSSes - 3. 2. 2020

Implementation includes import of several Excel databases, one for each year (2015 - 2018)

[About the project](https://bee-ppp.eu/projects/prevention-of-honey-bee-colony-losses)
  
About the providers:
- [CARI](https://bee-ppp.eu/providers/cari-beekeeping-center-of-research-and-information)
- [Honeybee Valley](https://bee-ppp.eu/providers/honeybee-valley) 
- [Centre de Recherches Agronomiques - Wallonie](https://bee-ppp.eu/providers/cra-w) 

### Epilobee Analysis - 25. 10. 2019

Implementation includes import of a single Excel database.

[About the project](https://bee-ppp.eu/projects/honey-bee-winter-mortality-2012-2014-epilobee)

About the providers: 
- [ANSES](https://bee-ppp.eu/providers/anses-european-reference-laboratory-for-bees) 

